import { ChangeDetectionStrategy, Component, computed, Input } from '@angular/core';
import { ConfigGroup } from 'src/app/classes/config-group';
import { ProfileSelectorValue } from 'src/app/classes/config-value/profile-selector-value';
import { GroupType } from 'src/app/enums/group-types';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-profile-selector-input',
	templateUrl: './profile-selector-input.component.html',
	styleUrls: ['./profile-selector-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileSelectorInputComponent implements BaseInputComponent {
	@Input() groups: readonly ConfigGroup[] = [];
	@Input() input!: ProfileSelectorValue;
	@Input() name = '';

	readonly GT = GroupType;

	groupSelectOpts = computed(() => this.groups.filter(({ name }) => name === this.input.definition.profileType));

	selectedGroups = computed(() =>
		this.input.value().reduce((accu: ConfigGroup[], isUsed: boolean, index: number) => {
			const matchedGroup = this.groupSelectOpts().find(group => group.id() === index);
			if (isUsed && matchedGroup) {
				return [...accu, matchedGroup];
			}
			return accu;
		}, [])
	);

	onAdd(value: ConfigGroup) {
		this.input.value.update(groups => {
			groups[value.id()] = true;
			return groups;
		});
	}
}
