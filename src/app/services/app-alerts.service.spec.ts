import { TestBed } from '@angular/core/testing';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { AppAlert, AppAlertType } from '../models/app-alert.model';
import { AppAlertsService } from './app-alerts.service';

describe('AppAlertsService', () => {
	const successAlert: AppAlert = {
		message: 'SUCCESS',
		type: AppAlertType.SUCCESS
	};
	const warnAlert: AppAlert = {
		message: 'WARN',
		type: AppAlertType.WARN
	};
	const errorAlert: AppAlert = {
		message: 'ERROR',
		type: AppAlertType.ERROR,
		err: 'error message'
	};
	let service: AppAlertsService;
	let logSpy: jasmine.Spy;
	let errorSpy: jasmine.Spy;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		service = TestBed.inject(AppAlertsService);
		service.alerts.set([]);
		logSpy = spyOn(console, 'log');
		errorSpy = spyOn(console, 'error');
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should show', () => {
		service.show(successAlert);
		expect(service.alerts()[0]).toEqual(successAlert);
		expect(service.alerts().length).toBe(1);
		expect(logSpy).toHaveBeenCalledWith(successAlert.message);

		service.show(errorAlert);
		expect(service.alerts()[1]).toEqual(errorAlert);
		expect(service.alerts().length).toBe(2);
		expect(errorSpy).toHaveBeenCalledWith(errorAlert.message, errorAlert.err);
	});

	it('should show success alert', () => {
		service.showSuccessAlert('test', 20);
		expect(service.alerts()[0]).toEqual({
			message: 'test',
			type: AppAlertType.SUCCESS,
			hideDelay: 20
		});
	});

	it('should show warn alert', () => {
		service.showWarnAlert('test', 20);
		expect(service.alerts()[0]).toEqual({
			message: 'test',
			type: AppAlertType.WARN,
			hideDelay: 20
		});
	});

	it('should show error alert', () => {
		service.showErrorAlert('test', { test: 'message' }, 20);
		expect(service.alerts()[0]).toEqual({
			message: 'test',
			type: AppAlertType.ERROR,
			hideDelay: 20,
			err: {
				test: 'message'
			}
		});
	});

	it('should remove', () => {
		service.show(successAlert);
		service.show(errorAlert);
		service.show(warnAlert);
		expect(service.alerts()[0]).toEqual(successAlert);
		expect(service.alerts()[1]).toEqual(errorAlert);
		expect(service.alerts()[2]).toEqual(warnAlert);
		expect(service.alerts().length).toBe(3);

		service.remove(errorAlert);
		expect(service.alerts()[0]).toEqual(successAlert);
		expect(service.alerts()[1]).toEqual(warnAlert);
		expect(service.alerts().length).toBe(2);
	});
});
