/**
 * Saber lock modes
 */
export enum SaberLock {
	DISABLED = 0,
	IGNITION,
	RETRACTION,
	IGNITION_RETRACTION
}
