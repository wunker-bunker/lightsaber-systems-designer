import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe to create an array of numbers from 0 to length - 1.
 * Used to iterate over an array in a template.
 * @example <div *ngFor="let i of 5 | trackIndexByLength">{{i}}</div>
 */
@Pipe({
	name: 'trackIndexByLength',
	pure: true
})
export class TrackIndexByLengthPipe implements PipeTransform {
	/**
	 * Create an array of numbers from 0 to length - 1.
	 * @param length The length of the array.
	 * @returns The array of numbers.
	 */
	transform(length: number): readonly number[] {
		const indexArr: number[] = [];
		for (let i = 0; i < length; i++) {
			indexArr.push(i);
		}
		return indexArr;
	}
}
