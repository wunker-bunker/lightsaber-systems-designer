import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import { MockConfigVarRowComponent } from 'src/environments/test-data.karma';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { InvalidVariablesComponent } from './invalid-variables.component';

describe('InvalidVariablesComponent', () => {
	let component: InvalidVariablesComponent;
	let fixture: ComponentFixture<InvalidVariablesComponent>;
	const modalMock: jasmine.SpyObj<NgbActiveModal> = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

	configureTestSuite({
		imports: [...unitTestImports],
		declarations: [InvalidVariablesComponent, OrderByPipe, MockConfigVarRowComponent, ...unitTestDeclarations],
		providers: [{ provide: NgbActiveModal, useValue: modalMock }, ...unitTestProviders]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(InvalidVariablesComponent);
		component = fixture.componentInstance;
		component.variables = testData.configVars;
		component.variables.forEach(confVar => spyOn(confVar.configValue(), 'autoFixValue'));
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should auto fix all', () => {
		component.autoFixAll();
		component.variables.forEach(confVar => expect(confVar.configValue().autoFixValue).toHaveBeenCalledTimes(1));
	});
});
