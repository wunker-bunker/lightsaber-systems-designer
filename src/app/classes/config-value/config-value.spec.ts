import { computed } from '@angular/core';
import { ValueTypeUknown, VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { UnknownDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue } from './config-value';

class TestValue extends ConfigValue<string, ValueTypeUknown, UnknownDefinition> {
	toString = computed(() => this.value());

	valid = computed(() => this.value().length > 0);

	constructor(value: string, def: UnknownDefinition) {
		super(value, def, def.type);
	}

	autoFixValue(): void {
		return;
	}

	protected parseValue(valStr: string): string {
		return valStr;
	}
}

describe('ConfigValue', () => {
	const def: UnknownDefinition = {
		description: 'desc',
		type: VALUE_TYPE.UNKNOWN
	};

	it('should create an instance', () => {
		const val = new TestValue('test', def);
		expect(val).toBeTruthy();
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toEqual('test');
	});

	it('should print toString', () => {
		expect(new TestValue('test', def).toString()).toEqual('test');
	});

	describe('should validate', () => {
		const val = new TestValue('test', def);

		it('valid when truthy', () => {
			val.value.set('test');
			expect(val.valid()).toBeTrue();
		});

		it('invalid when falsy', () => {
			val.value.set('');
			expect(val.valid()).toBeFalse();
		});
	});
});
