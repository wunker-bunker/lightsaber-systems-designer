import { computed } from '@angular/core';
import { RGBW_VALUE } from 'src/app/constants/regex';
import { Color } from 'src/app/models/color.model';
import { BaseDefinition, ColorDefinition } from 'src/app/models/variable-dictionary.model';
import { ValueType, ValueTypeColor } from '../../enums/value-type.enum';
import { ConfigValue, ConfigValueTypes } from './config-value';

export const COLOR_VAL_MIN = 0;
export const COLOR_VAL_MAX = 1023;

export class ColorValue extends ConfigValue<Color, ValueTypeColor, ColorDefinition> {
	readonly toString;

	readonly valid;

	constructor(value: string, def: ColorDefinition) {
		super(value, def, def.type);
		this.toString = computed(
			() => `${this.value().red},${this.value().green},${this.value().blue},${this.value().white}`
		);
		this.valid = computed(
			() =>
				this.value().red >= COLOR_VAL_MIN &&
				this.value().red <= COLOR_VAL_MAX &&
				this.value().green >= COLOR_VAL_MIN &&
				this.value().green <= COLOR_VAL_MAX &&
				this.value().blue >= COLOR_VAL_MIN &&
				this.value().blue <= COLOR_VAL_MAX &&
				this.value().white >= COLOR_VAL_MIN &&
				this.value().white <= COLOR_VAL_MAX
		);
	}

	static isColorValue(value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>): value is ColorValue {
		return value instanceof ColorValue;
	}

	autoFixValue(): void {
		return;
	}

	protected parseValue(strVal: string): Color {
		const result = RGBW_VALUE.exec(strVal);
		return result
			? {
					red: this.valueLimit(+result[1]),
					green: this.valueLimit(+result[2]),
					blue: this.valueLimit(+result[3]),
					white: this.valueLimit(+result[4])
			  }
			: {
					red: 0,
					green: 0,
					blue: 0,
					white: 0
			  };
	}

	private valueLimit(val: number): number {
		return isNaN(val) || val === null || val === undefined
			? COLOR_VAL_MIN
			: val < COLOR_VAL_MIN
			? COLOR_VAL_MIN
			: val > COLOR_VAL_MAX
			? COLOR_VAL_MAX
			: val;
	}
}
