import { computed, signal, WritableSignal } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { BaseFile } from './base-file';

/**
 * Base class for all text files.
 */
export abstract class TextFile extends BaseFile {
	/**
	 * The text of the file.
	 */
	readonly text: WritableSignal<string>;

	/**
	 * Gets the string representation of the file.
	 * @returns The string representation of the file.
	 */
	readonly toString;

	/**
	 * Flag to indicate if the file is valid.
	 * @returns True if the file is valid, false otherwise.
	 */
	readonly valid;

	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 * @returns True if the file has been modified since it was last saved, false otherwise.
	 */
	protected readonly _dirtyText;

	/**
	 * Stores the original text of the file.
	 * Used to determine if the file is dirty.
	 * @returns The original text of the file.
	 */
	private readonly _originalText: WritableSignal<string>;

	/**
	 * Creates a new instance of the TextFile class.
	 * @param text The text of the file.
	 * @param type The type of the file.
	 * @param filePath The path to the file.
	 */
	constructor(text: string, type: ConfigFileType, filePath: string) {
		super(type, filePath);
		this._originalText = signal(text);
		this.text = signal(text);
		this.toString = computed(() => this.text());
		this.valid = computed(() => true);
		this._dirtyText = computed(() => this.text() !== this._originalText());
	}

	/**
	 * Marks the file as saved.
	 */
	markAsSaved(): void {
		this._originalText.set(this.toString());
	}

	/**
	 * Reverts the file to the last saved state.
	 * This will reset the dirty flag.
	 */
	revert(): void {
		this.text.set(this._originalText());
	}
}
