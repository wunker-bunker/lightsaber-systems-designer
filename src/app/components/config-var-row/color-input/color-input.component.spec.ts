import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ColorEvent } from 'ngx-color';
import { ColorValue, COLOR_VAL_MAX } from 'src/app/classes/config-value/color-value';
import { ConfigVar } from 'src/app/classes/config-var';
import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { ColorToRgbStylePipe } from 'src/app/pipes/color-to-rgb-style.pipe';
import { ColorToWhiteStylePipe } from 'src/app/pipes/color-to-white-style.pipe';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { ColorInputComponent } from './color-input.component';

describe('ColorInputComponent', () => {
	let component: ColorInputComponent;
	let fixture: ComponentFixture<ColorInputComponent>;
	let variable: ConfigVar;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [ColorInputComponent, ColorToRgbStylePipe, ColorToWhiteStylePipe, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		const v = testData.configVars.find(confVar => confVar.configValue().type === VALUE_TYPE.COLOR);
		const val = v?.configValue();

		if (!v || !val || !ColorValue.isColorValue(val)) {
			throw new Error('No color config var found in test data');
		}

		variable = v;
		fixture = TestBed.createComponent(ColorInputComponent);
		component = fixture.componentInstance;
		component.input = val;
		component.name = variable.name;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should change rgb', fakeAsync(() => {
		const newColorValue = Math.round((100 * COLOR_VAL_MAX) / 255);
		const origWhite = component.input.value().white;

		component.changeRgbComplete({
			color: {
				rgb: {
					r: 100,
					a: 0,
					b: 100,
					g: 100
				}
			}
		} as unknown as ColorEvent);
		tick(100);
		expect(component.input.value().red).toEqual(newColorValue);
		expect(component.input.value().green).toEqual(newColorValue);
		expect(component.input.value().blue).toEqual(newColorValue);
		expect(component.input.value().white).toEqual(origWhite);
		expect(component.input.valid()).toBeTrue();
	}));

	it('should change white', fakeAsync(() => {
		const newWhiteValue = Math.round(0.6 * COLOR_VAL_MAX);
		const origRed = component.input.value().red;
		const origGreen = component.input.value().green;
		const origBlue = component.input.value().blue;

		component.changeWhiteComplete({
			color: {
				rgb: {
					r: 0,
					a: 0.6,
					b: 0,
					g: 0
				}
			}
		} as unknown as ColorEvent);
		tick(100);
		expect(component.input.value().red).toEqual(origRed);
		expect(component.input.value().green).toEqual(origGreen);
		expect(component.input.value().blue).toEqual(origBlue);
		expect(component.input.value().white).toEqual(newWhiteValue);
		expect(component.input.valid()).toBeTrue();
	}));
});
