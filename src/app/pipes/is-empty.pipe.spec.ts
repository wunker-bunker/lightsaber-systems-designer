import { IsEmptyPipe } from './is-empty.pipe';

describe('IsEmptyPipe', () => {
	const pipe = new IsEmptyPipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('should transform and check if value is empty', () => {
		expect(pipe.transform(null)).toBeTrue();
		expect(pipe.transform('nothing')).toBeFalse();
		expect(pipe.transform(0)).toBeTrue();
		expect(pipe.transform({})).toBeTrue();
	});
});
