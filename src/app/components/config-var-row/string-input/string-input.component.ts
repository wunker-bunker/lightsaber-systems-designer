import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { StringValue } from 'src/app/classes/config-value/string-value';
import { UnknownValue } from 'src/app/classes/config-value/unknown-value';

@Component({
	selector: 'lsd-string-input',
	templateUrl: './string-input.component.html',
	styleUrls: ['./string-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class StringInputComponent {
	@Input() input!: StringValue | UnknownValue;
	@Input() name = '';
	@Input() simpleView = false;
}
