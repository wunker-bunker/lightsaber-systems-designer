/**
 * Bitfield model for variables in the config files.
 * It is an array of booleans
 */
export type Bitfield = boolean[];
