/**
 * Sets the mapping behavior of the blade profile settings.
 */
export enum MappingBehavior {
	STATIC = 0,
	AUDIO,
	MOTION,
	ANGULAR
}

/**
 * Sets the mapping color of the blade profile.
 */
export enum MappingColor {
	LEGACY = 0,
	HEATMAP
}
