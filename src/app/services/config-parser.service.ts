import { Injectable } from '@angular/core';
import { BaseFile } from '../classes/config-file/base-file';
import { ColorConfig } from '../classes/config-file/color-config';
import { ConfigFile } from '../classes/config-file/config-file';
import { FontConfig } from '../classes/config-file/font-config';
import { LedConfig } from '../classes/config-file/led-config';
import { MainConfig } from '../classes/config-file/main-config';
import { PrefsConfig } from '../classes/config-file/prefs-config';
import { SpecialConfig } from '../classes/config-file/special-config';
import { TextFile } from '../classes/config-file/text-file';
import { ConfigGroup } from '../classes/config-group';
import { StringValue } from '../classes/config-value/string-value';
import { ConfigVar } from '../classes/config-var';
import { GROUP_MATCH, MULTI_VAR, VAR_MATCH } from '../constants/regex';
import { COLOR_DICTIONARY, MAIN_DICT, PREFS_DICTIONARY } from '../dictionaries/variable.dict';
import { ConfigFileType } from '../enums/config-file-type.enum';
import { GroupType } from '../enums/group-types';
import { ProfileNames } from '../enums/profile-names';
import { StringDefinition, VariableDefinition, VariableDictionary } from '../models/variable-dictionary.model';

/**
 * Service to parse config files.
 */
@Injectable({
	providedIn: 'root'
})
export class ConfigParserService {
	/**
	 * Parse a config file.
	 * @param fileName The name of the file.
	 * @param filePath The path of the file.
	 * @param fileText The text of the file.
	 * @returns The parsed config file.
	 */
	parseFileText(fileName: string, filePath: string, fileText: string): BaseFile | null {
		if (fileName === ConfigFileType.SPECIAL || fileName === ConfigFileType.LEDS) {
			return this.determineTextFileConfigType(fileName, filePath, fileText);
		} else {
			const lines: string[] = fileText.split('\n');

			// Variables and groups used to make configs
			const mainVars: ConfigVar[] = [];
			const groups: ConfigGroup[] = [];

			// Pointer to the current set of variables being parsed
			let currentVars: ConfigVar[] = mainVars;

			// Iterate through the lines and determine what to do with them
			lines.forEach(line => {
				// Check if it's a variable line
				const varMatch = line.match(VAR_MATCH);

				// Check if it's a group block
				const groupMatch = line.match(GROUP_MATCH);

				// Check if it's a variable line
				// Else check if it's a group block
				if (varMatch) {
					// Push the variable to the current set of variables
					const newVar = this.determineVar(fileName, varMatch);
					if (newVar) {
						currentVars.push(newVar);
					}
				} else if (groupMatch) {
					// Create a new group and push it to the groups array
					const newGroup = new ConfigGroup(+groupMatch[2], groupMatch[1]);
					groups.push(newGroup);
					currentVars = newGroup.variables(); // Set new group of vars
				}
			});

			// Iterate through the groups and add the variables to the correct group
			groups.forEach(group => {
				// Check if the group has a name
				// Push the variable to the group if it does
				if (!group.groupName()) {
					let newVar: ConfigVar;
					let newVal: StringValue;

					// Determine the group type
					switch (group.name) {
						case GroupType.color:
							newVal = new StringValue('', COLOR_DICTIONARY.cname as StringDefinition);
							newVal.value.set(`Color ${group.id()}`);
							// Create a new variable for the color group
							newVar = new ConfigVar(ProfileNames.cname, newVal);
							break;
						case GroupType.profile:
							newVal = new StringValue('', MAIN_DICT.pname as StringDefinition);
							newVal.value.set(`Profile ${group.id()}`);
							// Create a new variable for the profile group
							newVar = new ConfigVar(ProfileNames.pname, newVal);
							break;
						default:
							return;
					}
					group.variables.mutate(value => value.push(newVar));
				}
			});

			// Return the config file after determining the type and building it
			return this.determineConfigType(fileName, filePath, mainVars, groups);
		}
	}

	/**
	 * Creates the ConfigVar object from the match.
	 * @param regexMatchArr The match array from the regex.
	 * @param regexMatchArr."0"
	 * @param regexMatchArr."1"
	 * @param dict The dictionary to use for the variable.
	 * @param regexMatchArr."2"
	 * @returns The ConfigVar object.
	 */
	private createVar([, name, value]: RegExpMatchArray, dict: VariableDictionary): ConfigVar {
		// Use the MULTI_VAR regex to check if it's a multi-var
		const multiVarMatch = name.match(MULTI_VAR);
		let def: VariableDefinition;

		// If it's a multi-var, use the dictionary with the # at the end
		// Otherwise, use the dictionary with the name
		if (multiVarMatch && !!dict[`${multiVarMatch[1]}#`]) {
			def = dict[`${multiVarMatch[1]}#`];
		} else {
			def = dict[name];
		}

		return new ConfigVar(name, ConfigVar.valueFrom(value, def));
	}

	/**
	 * Determine the type of variable to create.
	 * @param fileName The name of the file.
	 * @param filePath The path of the file.
	 * @param variables The variables to add to the config.
	 * @param groups The groups to add to the config.
	 * @returns The config file object.
	 */
	private determineConfigType(
		fileName: string,
		filePath: string,
		variables: ConfigVar[],
		groups: ConfigGroup[]
	): ConfigFile | null {
		// Decide what type of Config object to use
		switch (fileName) {
			case ConfigFileType.FONT:
				return new FontConfig(variables, filePath);
			case ConfigFileType.MAIN:
				return new MainConfig(variables, groups, filePath);
			case ConfigFileType.COLORS:
				return new ColorConfig(variables, groups, filePath);
			case ConfigFileType.PREF:
				return new PrefsConfig(variables, groups, filePath);
			default:
				return null;
		}
	}

	/**
	 * Determine the type of text file to create.
	 * @param fileName The name of the file.
	 * @param filePath The path to the file.
	 * @param text The text of the file.
	 * @returns The text file object.
	 */
	private determineTextFileConfigType(fileName: string, filePath: string, text: string): TextFile | null {
		// Decide what type of Config object to use
		switch (fileName) {
			case ConfigFileType.LEDS:
				return new LedConfig(text, filePath);
			case ConfigFileType.SPECIAL:
				return new SpecialConfig(text, filePath);
			default:
				return null;
		}
	}

	/**
	 * Determine the type of variable to create.
	 * @param fileName The name of the file the variable is in.
	 * @param varMatch The match array from the variable regex.
	 * @returns The variable object.
	 */
	private determineVar(fileName: string, varMatch: RegExpMatchArray): ConfigVar | null {
		// Decide what type of Config object to use
		switch (fileName) {
			case ConfigFileType.PREF:
				return this.createVar(varMatch, PREFS_DICTIONARY);
			case ConfigFileType.COLORS:
				return this.createVar(varMatch, COLOR_DICTIONARY);
			case ConfigFileType.FONT:
			case ConfigFileType.MAIN:
				// case ConfigFileType.SPECIAL:
				return this.createVar(varMatch, MAIN_DICT);
			default:
				return null;
		}
	}
}
