import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';

/**
 * Sort order for lodash orderBy.
 * @see https://lodash.com/docs/4.17.15#orderBy
 */
type SortOrder = 'desc' | 'asc';

/**
 * Pipe to order an array by a property.
 * @example <div *ngFor="let item of items | orderBy:'name'">...</div>
 */
@Pipe({
	name: 'orderBy',
	pure: true
})
export class OrderByPipe implements PipeTransform {
	/**
	 * Order an array by a property.
	 * @param arr The array to order.
	 * @param propertyName The property or properties to order by.
	 * @param direction The sort order or sort orders.
	 * @returns The ordered array.
	 * @see https://lodash.com/docs/4.17.15#orderBy
	 */
	transform<T>(
		arr: T[] | readonly T[] | null,
		propertyName: keyof T | (keyof T)[],
		direction: SortOrder | SortOrder[]
	): T[] {
		return orderBy(arr, propertyName, direction);
	}
}
