import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { DeleteConfirmComponent } from './delete-confirm.component';

describe('DeleteConfirmComponent', () => {
	let component: DeleteConfirmComponent;
	let fixture: ComponentFixture<DeleteConfirmComponent>;
	const modalMock: jasmine.SpyObj<NgbActiveModal> = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

	configureTestSuite({
		imports: [ReactiveFormsModule, ...unitTestImports],
		declarations: [DeleteConfirmComponent, ...unitTestDeclarations],
		providers: [{ provide: NgbActiveModal, useValue: modalMock }, ...unitTestProviders]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DeleteConfirmComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
