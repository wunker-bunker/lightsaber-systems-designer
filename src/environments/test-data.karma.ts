import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Routes } from '@angular/router';
import { compact, sortBy } from 'lodash';
import { join } from 'path';
import { BaseFile } from 'src/app/classes/config-file/base-file';
import { ColorConfig } from 'src/app/classes/config-file/color-config';
import { FontConfig } from 'src/app/classes/config-file/font-config';
import { LedConfig } from 'src/app/classes/config-file/led-config';
import { MainConfig } from 'src/app/classes/config-file/main-config';
import { PrefsConfig } from 'src/app/classes/config-file/prefs-config';
import { SpecialConfig } from 'src/app/classes/config-file/special-config';
import { ConfigGroup } from 'src/app/classes/config-group';
import { BitfieldValue } from 'src/app/classes/config-value/bitfield-value';
import { BooleanValue } from 'src/app/classes/config-value/boolean-value';
import { ColorValue } from 'src/app/classes/config-value/color-value';
import { EnumerationValue } from 'src/app/classes/config-value/enumeration-value';
import { IntegerArrayValue } from 'src/app/classes/config-value/integer-array-value';
import { IntegerValue } from 'src/app/classes/config-value/integer-value';
import { ProfileSelectorValue } from 'src/app/classes/config-value/profile-selector-value';
import { StringValue } from 'src/app/classes/config-value/string-value';
import { UnknownValue } from 'src/app/classes/config-value/unknown-value';
import { ConfigVar } from 'src/app/classes/config-var';
import { LightsaberSystem } from 'src/app/classes/lightsaber-system';
import { SoundFile } from 'src/app/classes/sound-file';
import { ConfigBodyOpts } from 'src/app/components/config-list/config-list.component';
import { LIBRARY_DIR_NAME } from 'src/app/constants/strings';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ConfigParserService } from 'src/app/services/config-parser.service';
import {
	testColorConfigText,
	testFontConfigText,
	testFontLedText,
	testMainConfigText,
	testPrefConfigText,
	testSpecialConfigText
} from './test-config-text';

@Component({ template: '' })
export class TestComponent {}

interface TestFile {
	fileName: string;
	filePath: string;
	fileText: string;
}

export const testHomeDir = join('/', 'home', 'test');

export const testSystemList = ['test-system-1', 'test-system-2'] as const;

export const testFontsNames = [
	'NIGHTFALL',
	'SHOTO',
	'CRYSTAL_FOCUS_REBOOT',
	'YOUNGLING',
	'NEBULA_REBOOT',
	'GREYMEAT'
] as const;

export const testSoundFileNames = [
	'blaster.wav',
	'clash1.wav',
	'lockup.wav',
	'poweron.wav',
	'poweroff.wav',
	'hum.wav'
] as const;

export const testTrackFileNames = ['track1.wav', 'track2.wav', 'track3.wav'] as const;

export const testFonts = testFontsNames.map((fontName, index) => `${index + 1}-${fontName}`);

export const testLibraryLocation = join(testHomeDir, LIBRARY_DIR_NAME);

export const testNewLibraryLocation = join(testHomeDir, 'new-library');

export const testExportPath = join(testHomeDir, 'export');

export const testNewFontName = 'new-font';

export const testNewFontNoLedName = 'new-font-no-led';

export const testNewSoundName = 'new-sound.wav';

export const testNewFontPath = join(testHomeDir, testNewFontName);

export const testNewFontNoLedPath = join(testHomeDir, testNewFontNoLedName);

export const testNewSoundPath = join(testHomeDir, testNewSoundName);

export const testNewSystemName = 'new-system';

export const testNewSystemPath = join(testHomeDir, testNewSystemName);

export const testNewSystemLibraryPath = join(testLibraryLocation, 'systems', testNewSystemName);

export const testFontDirEntries = [ConfigFileType.FONT, ConfigFileType.LEDS, 'tracks', ...testSoundFileNames];

export const testFontNoLedDirEntries = [ConfigFileType.FONT, 'tracks', ...testSoundFileNames];

export const testSystemDirEntries = [
	ConfigFileType.MAIN,
	ConfigFileType.COLORS,
	ConfigFileType.PREF,
	ConfigFileType.SPECIAL,
	...testFonts
];

export const testSystemLocations = testSystemList.reduce((acc: { [key: string]: string }, systemName) => {
	acc[systemName] = join(testLibraryLocation, 'systems', systemName);
	return acc;
}, {});

export const testSystemLocationPaths = Object.values(testSystemLocations);

export const testSystemLocation1 = testSystemLocations[testSystemList[0]];

export const testSystemLocation2 = testSystemLocations[testSystemList[1]];

type TestFileMap = {
	[key in (typeof testSystemList)[number]]: TestFile;
};

export const testMainConfigFiles = testSystemList.reduce((acc: TestFileMap, systemName) => {
	acc[systemName] = {
		fileName: ConfigFileType.MAIN,
		filePath: join(testSystemLocations[systemName], ConfigFileType.MAIN),
		fileText: testMainConfigText
	};
	return acc;
}, {} as never);

export const mainConfigFile = testMainConfigFiles[testSystemList[0]];

export const testColorConfigFiles = testSystemList.reduce((acc: TestFileMap, systemName) => {
	acc[systemName] = {
		fileName: ConfigFileType.COLORS,
		filePath: join(testSystemLocations[systemName], ConfigFileType.COLORS),
		fileText: testColorConfigText
	};
	return acc;
}, {} as TestFileMap);

export const colorConfigFile = testColorConfigFiles[testSystemList[0]];

export const testPrefConfigFiles = testSystemList.reduce((acc: TestFileMap, systemName) => {
	acc[systemName] = {
		fileName: ConfigFileType.PREF,
		filePath: join(testSystemLocations[systemName], ConfigFileType.PREF),
		fileText: testPrefConfigText
	};
	return acc;
}, {} as TestFileMap);

export const prefConfigFile = testPrefConfigFiles[testSystemList[0]];

export const testSpecialConfigFiles = testSystemList.reduce((acc: TestFileMap, systemName) => {
	acc[systemName] = {
		fileName: ConfigFileType.SPECIAL,
		filePath: join(testSystemLocations[systemName], ConfigFileType.SPECIAL),
		fileText: testSpecialConfigText
	};
	return acc;
}, {} as TestFileMap);

export const specialConfigFile = testSpecialConfigFiles[testSystemList[0]];

type TestFileNameMap = {
	[key in (typeof testSystemList)[number]]: string[];
};

export const testSoundFilePaths = testSystemList.reduce((acc: TestFileNameMap, systemName) => {
	acc[systemName] = testFonts.reduce((acc2: string[], font) => {
		acc2.push(
			...testSoundFileNames.map(soundFileName => join(testSystemLocations[systemName], font, soundFileName))
		);
		return acc2;
	}, []);
	return acc;
}, {} as TestFileNameMap);

export const soundFilePaths = testSoundFilePaths[testSystemList[0]];

export const testTrackFilePaths = testSystemList.reduce((acc: TestFileNameMap, systemName) => {
	acc[systemName] = testFonts.reduce((acc2: string[], font) => {
		acc2.push(
			...testTrackFileNames.map(trackFileName =>
				join(testSystemLocations[systemName], font, 'tracks', trackFileName)
			)
		);
		return acc2;
	}, []);
	return acc;
}, {} as TestFileNameMap);

export const trackFilePaths = testTrackFilePaths[testSystemList[0]];

export const testSoundAndTrackFilePaths = testSystemList.reduce((acc: TestFileNameMap, systemName) => {
	acc[systemName] = [...testSoundFilePaths[systemName], ...testTrackFilePaths[systemName]];
	return acc;
}, {} as TestFileNameMap);

export const soundAndTrackFilePaths = [...soundFilePaths, ...trackFilePaths];

type TestFileArrayMap = {
	[key in (typeof testSystemList)[number]]: TestFile[];
};

export const testFontConfigFiles = testSystemList.reduce((acc: TestFileArrayMap, systemName) => {
	acc[systemName] = testFonts.map(font => ({
		fileName: ConfigFileType.FONT,
		filePath: join(testSystemLocations[systemName], font, ConfigFileType.FONT),
		fileText: testFontConfigText
	}));
	return acc;
}, {} as TestFileArrayMap);

export const fontConfigFiles = testFontConfigFiles[testSystemList[0]];

export const testLedConfigFiles = testSystemList.reduce((acc: TestFileArrayMap, systemName) => {
	acc[systemName] = testFonts.map(font => ({
		fileName: ConfigFileType.LEDS,
		filePath: join(testSystemLocations[systemName], font, ConfigFileType.LEDS),
		fileText: testFontLedText
	}));
	return acc;
}, {} as TestFileArrayMap);

export const ledConfigFiles = [
	...testFonts.map(font => ({
		fileName: ConfigFileType.LEDS,
		filePath: join(testSystemLocation1, font, ConfigFileType.LEDS),
		fileText: testFontLedText
	}))
];

@Component({
	selector: 'lsd-config-var-row',
	template: ''
})
export class MockConfigVarRowComponent {
	@Input() confVar!: ConfigVar;
}

@Component({
	selector: 'lsd-font-list',
	template: ''
})
export class MockFontListComponent {
	@Input() fontConfigs: FontConfig[] = [];
	@Input() selectFont!: number;
	@Output() selectFontChange: EventEmitter<number> = new EventEmitter();
	@Output() valid: EventEmitter<void> = new EventEmitter();
}

@Component({
	selector: 'lsd-config-list',
	template: ''
})
export class MockConfigListComponent {
	@Input() bodyOpts!: ConfigBodyOpts;
	@Input() cardBody!: TemplateRef<any>;
	@Input() groups!: ConfigGroup[];
	@Input() isValid = false;
	@Input() name!: string;
	@Output() valid: EventEmitter<void> = new EventEmitter();
}

@Component({
	selector: 'lsd-group-config-list',
	template: ''
})
export class MockGroupConfigListComponent {
	@Input() groups!: ConfigGroup[];
	@Input() isValid = false;
	@Input() name!: string;
	@Output() valid: EventEmitter<void> = new EventEmitter();
}

@Component({
	selector: 'lsd-update-indicator',
	template: ''
})
export class MockUpdateIndicatorComponent {
	@Input() disableRestart = false;
}

@Component({
	selector: 'lsd-systems-list',
	template: ''
})
export class MockSystemListComponent {}

export class TestData {
	static routes: Routes = [
		{
			path: '',
			redirectTo: 'systems',
			pathMatch: 'full'
		},
		{
			path: 'systems',
			component: TestComponent
		},
		{
			path: 'systems/:systemName',
			component: TestComponent
		}
	];

	bitfieldValues: BitfieldValue[] = [];
	booleanValues: BooleanValue[] = [];
	colorConfig!: ColorConfig;
	colorValues: ColorValue[] = [];
	configGroups: ConfigGroup[] = [];
	configVars: ConfigVar[] = [];
	configs: BaseFile[] = [];
	enumValues: EnumerationValue[] = [];
	fontConfigs: FontConfig[] = [];
	intArrValues: IntegerArrayValue[] = [];
	intValues: IntegerValue[] = [];
	ledConfigs: LedConfig[] = [];
	mainConfig!: MainConfig;
	prefConfig!: PrefsConfig;
	profileSelectValues: ProfileSelectorValue[] = [];
	soundFiles: SoundFile[] = [];
	specialConfig!: SpecialConfig;
	strValues: StringValue[] = [];
	system!: LightsaberSystem;
	systems: LightsaberSystem[] = [];
	unknownValues: UnknownValue[] = [];

	private _configParser: ConfigParserService;

	constructor() {
		this._configParser = new ConfigParserService();
		this.generateTestData();
	}

	generateTestData(): void {
		this.systems = this.generateSystemList();
		this.system = this.systems[0];
		this.mainConfig = this.system.mainConfig();
		this.colorConfig = this.system.colorConfig();
		this.prefConfig = this.system.prefConfig();
		this.specialConfig = this.system.specialConfig();
		this.fontConfigs = this.system.fontConfigs();
		this.ledConfigs = this.system.ledConfigs();
		this.soundFiles = this.system.sounds();
		this.configs = [
			this.mainConfig,
			this.colorConfig,
			this.prefConfig,
			this.specialConfig,
			...this.fontConfigs,
			...this.ledConfigs
		];
		this.configVars = [
			...this.mainConfig.variables(),
			...this.mainConfig
				.groups()
				.reduce((varArr: ConfigVar[], group: ConfigGroup) => [...varArr, ...group.variables()], []),
			...this.colorConfig.variables(),
			...this.colorConfig
				.groups()
				.reduce((varArr: ConfigVar[], group: ConfigGroup) => [...varArr, ...group.variables()], []),
			...this.prefConfig.variables(),
			...this.fontConfigs.reduce(
				(fontVars: ConfigVar[], config: FontConfig): ConfigVar[] => [...fontVars, ...config.variables()],
				[]
			)
		];
		this.configGroups = sortBy(
			[...this.mainConfig.groups(), ...this.colorConfig.groups(), ...this.prefConfig.groups()],
			'id'
		);
		this.bitfieldValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is BitfieldValue => BitfieldValue.isBitfieldValue(value));
		this.booleanValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is BooleanValue => BooleanValue.isBooleanValue(value));
		this.colorValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is ColorValue => ColorValue.isColorValue(value));
		this.enumValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is EnumerationValue => EnumerationValue.isEnumerationValue(value));
		this.intArrValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is IntegerArrayValue => IntegerArrayValue.isIntegerArrayValue(value));
		this.intValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is IntegerValue => IntegerValue.isIntegerValue(value));
		this.strValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is StringValue => StringValue.isStringValue(value));
		this.profileSelectValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is ProfileSelectorValue => ProfileSelectorValue.isProfileSelectorValue(value));
		this.unknownValues = this.configVars
			.map(variable => variable.configValue())
			.filter((value): value is UnknownValue => UnknownValue.isUnknownValue(value));
	}

	private generateSystemList(): LightsaberSystem[] {
		const newSystems = testSystemList.map(
			systemName =>
				new LightsaberSystem(
					systemName,
					testSystemLocations[systemName],
					compact([
						this._configParser.parseFileText(
							testMainConfigFiles[systemName].fileName,
							testMainConfigFiles[systemName].filePath,
							testMainConfigFiles[systemName].fileText
						),
						this._configParser.parseFileText(
							testColorConfigFiles[systemName].fileName,
							testColorConfigFiles[systemName].filePath,
							testColorConfigFiles[systemName].fileText
						),
						this._configParser.parseFileText(
							testPrefConfigFiles[systemName].fileName,
							testPrefConfigFiles[systemName].filePath,
							testPrefConfigFiles[systemName].fileText
						),
						this._configParser.parseFileText(
							testSpecialConfigFiles[systemName].fileName,
							testSpecialConfigFiles[systemName].filePath,
							testSpecialConfigFiles[systemName].fileText
						),
						...testFontConfigFiles[systemName].map(fontConfigFile =>
							this._configParser.parseFileText(
								fontConfigFile.fileName,
								fontConfigFile.filePath,
								fontConfigFile.fileText
							)
						),
						...testLedConfigFiles[systemName].map(ledConfigFile =>
							this._configParser.parseFileText(
								ledConfigFile.fileName,
								ledConfigFile.filePath,
								ledConfigFile.fileText
							)
						)
					]),
					testSoundAndTrackFilePaths[systemName].map(
						(fileName: string) =>
							new SoundFile(fileName, `lsd://${encodeURIComponent(fileName)}` as SafeResourceUrl)
					)
				)
		);

		// Mark all systems as saved
		newSystems.forEach(system => system.markAsSaved());

		return newSystems;
	}
}
