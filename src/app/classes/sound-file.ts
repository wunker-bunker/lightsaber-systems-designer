import { signal, WritableSignal } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { toInteger } from 'lodash';
import { SOUND_NAME } from '../constants/regex';
import { SOUND_DICT, SOUND_UNKNOWN_DESCR } from '../dictionaries/sounds.dict';
import { BaseSoundDefinition, SoundDefinition } from '../models/sound-dictionary.model';

/**
 * A sound file.
 * Contains the path to the file and the path to the file as a URI.
 */
export class SoundFile implements BaseSoundDefinition {
	/**
	 * The defintion sound name.
	 */
	readonly defSoundName: string;

	/**
	 * The description of the sound.
	 * This is the description from the sound dictionary.
	 */
	readonly description: string;

	/**
	 * The id of the sound. Corresponds to the id of the font.
	 */
	readonly id: WritableSignal<number>;

	/**
	 * Flag indicating if the sound is a multi sound.
	 * A multi sound is a sound that has multiple versions.
	 */
	readonly isMulti: boolean;

	/**
	 * Flag indicating if the sound is a track.
	 */
	readonly isTrack: boolean;

	/**
	 * Flag indicating if the sound loops.
	 */
	readonly loops: boolean;

	/**
	 * The minimum number of sounds for a multi sound.
	 */
	readonly minCount?: number;

	/**
	 * The name of the font the sound belongs to.
	 */
	readonly name: WritableSignal<string>;

	/**
	 * The name of the sound.
	 * This is the name of the sound file without the id and extension.
	 */
	readonly soundName: string;

	/**
	 * The number of the sound.
	 * This is the number at the end of the sound name for multi sounds.
	 * For example, the sound name for the 3rd version of the sound "sound" would be "sound3".
	 */
	readonly soundNumber: number;

	/**
	 * The id of the sound before it was changed.
	 * Used to revert the id if revert is called.
	 */
	private readonly _previousId: WritableSignal<number>;

	/**
	 * Creates a new SoundFile.
	 * @param filePath The path to the sound file.
	 * @param uriPath The path to the sound file as a URI, used for the audio element.
	 */
	constructor(public readonly filePath: string, public readonly uriPath: SafeResourceUrl) {
		// Get the id, name, and sound name from the file path.
		const id = +filePath.replace(SOUND_NAME, '$1');
		const name = filePath.replace(SOUND_NAME, '$2');
		this.id = signal(id);
		this.name = signal(name);
		this._previousId = signal(id);

		this.soundName = filePath.replace(SOUND_NAME, '$3');

		// Determine if the sound is a track.
		this.isTrack = filePath.includes('tracks');

		// Get the sound number and definition sound name from the sound name.
		const parsedNumber = this.soundName.replace(/\D+/g, '');
		this.soundNumber = parsedNumber ? toInteger(parsedNumber) : 0;
		this.defSoundName = this.soundName.replace(/\d+$/, '#');

		// Get the sound definition from the sound dictionary and set the description, loops, and isMulti.
		const def: SoundDefinition = SOUND_DICT[this.defSoundName];
		this.description = def?.description || SOUND_UNKNOWN_DESCR;
		this.loops = def?.loops || false;
		this.isMulti = def?.isMulti;

		if (def?.isMulti) {
			this.minCount = def.minCount;
		}
	}

	/**
	 * Reverts the id to the previous id.
	 */
	revert(): void {
		this.id.set(this._previousId());
	}
}
