import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { Bitfield } from 'src/app/models/bitfield.model';
import { BitfieldDefinition } from 'src/app/models/variable-dictionary.model';
import { BitfieldValue } from './bitfield-value';

describe('BitfieldValue', () => {
	const def: BitfieldDefinition = {
		bitLength: 5,
		description: 'desc',
		type: VALUE_TYPE.BITFIELD
	};
	const validStr = '10110';
	const validObj: Bitfield = [true, false, true, true, false];
	const shortStr = '101';
	const shortObj: Bitfield = [true, false, true, false, false];

	it('should create an instance', () => {
		const val = new BitfieldValue(validStr, def);
		expect(val).toBeTruthy();
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toEqual(validObj);
		expect(val.toString()).toEqual(validStr);

		const shortVal = new BitfieldValue(shortStr, def);
		expect(shortVal).toBeTruthy();
		expect(shortVal.type).toEqual(def.type);
		expect(shortVal.valid()).toBeTrue();
		expect(shortVal.value()).toEqual(shortObj);
		expect(shortVal.toString()).toEqual(shortStr + '00');
	});

	it('should print toString', () => {
		expect(new BitfieldValue(validStr, def).toString()).toEqual(validStr);
	});

	describe('should validate', () => {
		const val = new BitfieldValue('00000', def);
		it('valid when field is correct length', () => {
			val.value.set([...validObj]);
			expect(val.valid()).toBeTrue();
		});

		it('invalid when field is incorrect length', () => {
			val.value.set([false, false, false]);
			expect(val.valid()).toBeFalse();
		});
	});

	it('should do nothing for auto fix value', () => {
		const val = new BitfieldValue(validStr, def);
		val.autoFixValue();
	});
});
