import { computed, signal } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { BaseFile } from './base-file';

class TestBaseFile extends BaseFile {
	dirty;
	toString;
	valid;
	value;
	private _oldValue;

	constructor(value: string) {
		super(ConfigFileType.SPECIAL, 'testpath');
		this._oldValue = signal(value);
		this.value = signal(value);
		this.dirty = computed(() => this.value() !== this._oldValue());
		this.toString = computed(() => this.value());
		this.valid = computed(() => !!this.value());
	}

	markAsSaved(): void {
		this._oldValue.set(this.value());
	}

	revert(): void {
		this.value.set(this._oldValue());
	}
}

describe('BaseFile', () => {
	let obj: TestBaseFile;

	beforeEach(() => {
		obj = new TestBaseFile('this is a test');
	});

	it('should create an instance', () => {
		expect(new TestBaseFile('test')).toBeTruthy();
	});

	it('toString should print value', () => {
		expect(obj.toString()).toEqual('this is a test');
	});

	it('markAsSaved should set dirty to false', () => {
		expect(obj.dirty()).toBeFalse();
		obj.value.set('new val');
		expect(obj.dirty()).toBeTrue();
		obj.markAsSaved();
		expect(obj.dirty()).toBeFalse();
	});

	it('revert should set value from oldValue', () => {
		expect(obj.dirty()).toBeFalse();
		obj.value.set('new val');
		expect(obj.dirty()).toBeTrue();
		obj.revert();
		expect(obj.dirty()).toBeFalse();
		expect(obj.value()).toEqual('this is a test');
	});

	it('validate should return true unless empty', () => {
		expect(obj.valid()).toBeTrue();
		obj.value.set('');
		expect(obj.valid()).toBeFalse();
	});
});
