/**
 * Color model for variables in the config files.
 * It is an object with the following properties:
 * - red: number
 * - green: number
 * - blue: number
 * - white: number
 */
export interface Color {
	/**
	 * Blue value
	 */
	blue: number;

	/**
	 * Green value
	 */
	green: number;

	/**
	 * Red value
	 */
	red: number;

	/**
	 * White value
	 */
	white: number;
}
