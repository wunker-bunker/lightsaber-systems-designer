/**
 * JSON representation of a light saber system.
 */
export interface LightSaberSystemJSON {
	/**
	 * The name of the light saber system.
	 */
	name: string;

	/**
	 * The directory path of the light saber system.
	 */
	path: string;
}

/**
 * JSON representation of a ConfigGroup.
 */
export interface ConfigGroupJSON {
	/**
	 * The id of the ConfigGroup.
	 */
	id: number;

	/**
	 * The name of the ConfigGroup.
	 */
	name: string;

	/**
	 * The variables of the ConfigGroup.
	 */
	variables: ConfigVarJSON[];
}

/**
 * JSON representation of a ConfigVar.
 */
export interface ConfigVarJSON {
	/**
	 * The value of the ConfigVar.
	 */
	configValue: string;

	/**
	 * The name of the ConfigVar.
	 */
	name: string;
}
