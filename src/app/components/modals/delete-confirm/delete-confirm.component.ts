import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-delete-confirm',
	templateUrl: './delete-confirm.component.html',
	styleUrls: ['./delete-confirm.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteConfirmComponent {
	name = '';

	constructor(public settings: SettingsService, public activeModal: NgbActiveModal) {}
}
