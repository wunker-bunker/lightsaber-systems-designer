import { moveItemInArray } from '@angular/cdk/drag-drop';
import { join } from 'path';
import { TestData, testLibraryLocation } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { COLOR_DICTIONARY, MAIN_DICT } from '../dictionaries/variable.dict';
import { GroupType } from '../enums/group-types';
import { ProfileNames } from '../enums/profile-names';
import { ConfigGroupJSON } from '../models/light-saber-system.json';
import { IntegerDefinition } from '../models/variable-dictionary.model';
import { BaseFile } from './config-file/base-file';
import { ConfigGroup } from './config-group';
import { IntegerValue } from './config-value/integer-value';
import { LightsaberSystem } from './lightsaber-system';
import { SoundFile } from './sound-file';

describe('LightsaberSystem', () => {
	const name = 'testSystem';
	const systemPath = join(testLibraryLocation, name);
	let configFiles: BaseFile[];
	let system: LightsaberSystem;

	beforeEach(() => {
		testData.generateTestData();
		configFiles = [
			testData.colorConfig,
			testData.mainConfig,
			testData.prefConfig,
			...testData.fontConfigs,
			testData.specialConfig,
			...testData.ledConfigs
		];

		system = new LightsaberSystem(name, systemPath, configFiles, testData.soundFiles);
	});

	it('should create an instance', () => {
		expect(system).toBeTruthy();
		expect(system.name).toEqual(name);
		expect(system.path).toEqual(systemPath);
		expect(system.mainConfig().toString()).toEqual(testData.mainConfig.toString());
		expect(system.colorConfig().toString()).toEqual(testData.colorConfig.toString());
		expect(system.prefConfig().toString()).toEqual(testData.prefConfig.toString());
		expect(system.specialConfig().toString()).toEqual(testData.specialConfig.toString());
		expect(system.fontConfigs().toString()).toEqual(testData.fontConfigs.toString());
		expect(system.ledConfigs().toString()).toEqual(testData.ledConfigs.toString());
		expect(system.sounds().length).toEqual(testData.soundFiles.length);
		expect(system.dirty()).toBeFalse();
		expect(system.valid()).toBeTrue();
	});

	it('should get font config by id', () => {
		const font1 = system.getFontConfigById(1);
		const font2 = system.getFontConfigById(2);
		const nullFont = system.getFontConfigById(-1);
		expect(font1).toBeDefined();
		expect(font2).toBeDefined();
		expect(nullFont).toBeUndefined();
	});

	it('should get led config by id', () => {
		const led1 = system.getLedConfigById(1);
		const led2 = system.getLedConfigById(2);
		const nullLed = system.getLedConfigById(-1);
		expect(led1).toBeDefined();
		expect(led2).toBeDefined();
		expect(nullLed).toBeUndefined();
	});

	it('should get sounds by id', () => {
		const fontId = 1;
		const sounds = system.getSoundsById(fontId);
		expect(sounds).toEqual(
			testData.soundFiles.filter((sound: SoundFile) => sound.id() === fontId && !sound.isTrack)
		);
	});

	it('should get tracks by id', () => {
		const fontId = 2;
		const sounds = system.getTracksById(fontId);
		expect(sounds).toEqual(
			testData.soundFiles.filter((sound: SoundFile) => sound.isTrack && sound.id() === fontId)
		);
	});

	it('should mark as saved', () => {
		const variable = system.mainConfig().getVariableByName('offd');
		const value = variable?.configValue();

		if (!value || !IntegerValue.isIntegerValue(value)) {
			throw new Error('variable was undefined');
		}

		value.value.set(100);

		expect(system.mainConfig().dirty()).toBeTrue();
		expect(value.dirty()).toBeTrue();
		expect(system.dirty()).toBeTrue();
		expect(system.valid()).toBeTrue();
		expect(value.value()).toEqual(100);
		expect(value.valid()).toBeTrue();

		system.markAsSaved();
		expect(system.mainConfig().dirty()).toBeFalse();
		expect(value.dirty()).toBeFalse();
		expect(system.dirty()).toBeFalse();
		expect(system.valid()).toBeTrue();
		expect(value.value()).toEqual(100);
		expect(value.valid()).toBeTrue();
	});

	it('should revert', () => {
		const variable = system.mainConfig().getVariableByName('offd');
		const value = variable?.configValue();

		if (!value || !IntegerValue.isIntegerValue(value)) {
			throw new Error('variable was undefined');
		}

		const origValue = value.value();

		value.value.set(100);

		expect(system.mainConfig().dirty()).toBeTrue();
		expect(value.dirty()).toBeTrue();
		expect(value.valid()).toBeTrue();
		expect(system.dirty()).toBeTrue();
		expect(system.valid()).toBeTrue();
		expect(value.value()).toEqual(100);

		system.revert();
		expect(system.mainConfig().dirty()).toBeFalse();
		expect(value.dirty()).toBeFalse();
		expect(value.valid()).toBeTrue();
		expect(system.dirty()).toBeFalse();
		expect(system.valid()).toBeTrue();
		expect(value.value()).toEqual(origValue);
	});

	it('should validate', () => {
		const invalidVal = (MAIN_DICT.offd as IntegerDefinition).maxValue + 1;
		const variable = system.mainConfig().getVariableByName('offd');
		const value = variable?.configValue();

		if (!value || !IntegerValue.isIntegerValue(value)) {
			throw new Error('variable was undefined');
		}

		value.value.set(100);

		expect(system.mainConfig().dirty()).toBeTrue();
		expect(value.dirty()).toBeTrue();
		expect(system.dirty()).toBeTrue();
		expect(system.valid()).toBeTrue();
		expect(value.valid()).toBeTrue();
		expect(value.value()).toEqual(100);

		value.value.set(invalidVal);
		expect(system.mainConfig().dirty()).toBeTrue();
		expect(value.dirty()).toBeTrue();
		expect(system.dirty()).toBeTrue();
		expect(system.valid()).toBeFalse();
		expect(value.valid()).toBeFalse();
		expect(value.value()).toEqual(invalidVal);
	});

	it('should copy configs', () => {
		const newTestData = new TestData();
		newTestData.generateTestData();
		let newConfigFiles = [
			newTestData.colorConfig,
			newTestData.mainConfig,
			newTestData.prefConfig,
			...newTestData.fontConfigs,
			newTestData.specialConfig,
			...newTestData.ledConfigs
		];
		let newSystem = newTestData.system;
		let newVariable = newSystem.mainConfig().getVariableByName('offd');
		let newVariableValue = newVariable?.configValue();
		const variable = system.mainConfig().getVariableByName('offd');
		const value = variable?.configValue();

		if (
			!newVariableValue ||
			!IntegerValue.isIntegerValue(newVariableValue) ||
			!value ||
			!IntegerValue.isIntegerValue(value)
		) {
			throw new Error('variable was undefined');
		}

		const updatedValue = 100;
		const origValue = value?.value();

		value.value.set(updatedValue);
		system.markAsSaved();
		expect(system.mainConfig().dirty()).toBeFalse();
		expect(value.dirty()).toBeFalse();
		expect(value.valid()).toBeTrue();
		expect(system.dirty()).toBeFalse();
		expect(system.valid()).toBeTrue();
		expect(value.value()).toEqual(updatedValue);

		expect(newSystem.mainConfig().dirty()).toBeFalse();
		expect(newVariableValue.dirty()).toBeFalse();
		expect(newVariableValue.valid()).toBeTrue();
		expect(newSystem.dirty()).toBeFalse();
		expect(newSystem.valid()).toBeTrue();
		expect(newVariableValue.value()).toEqual(origValue);

		newSystem.copyConfigsFrom(system);
		newVariable = newSystem.mainConfig().getVariableByName('offd');
		newVariableValue = newVariable?.configValue();

		if (!newVariableValue || !IntegerValue.isIntegerValue(newVariableValue)) {
			throw new Error('variable was undefined');
		}

		expect(newSystem.mainConfig().dirty()).toBeFalse();
		expect(newVariableValue.dirty()).toBeFalse();
		expect(newVariableValue.valid()).toBeTrue();
		expect(newSystem.dirty()).toBeFalse();
		expect(newSystem.valid()).toBeTrue();
		expect(newVariableValue.value()).toEqual(updatedValue);

		newConfigFiles = [
			newTestData.colorConfig,
			newTestData.mainConfig,
			newTestData.prefConfig,
			newTestData.fontConfigs[0],
			newTestData.specialConfig,
			newTestData.ledConfigs[0]
		];
		newSystem = new LightsaberSystem(name, systemPath, newConfigFiles, newTestData.soundFiles);

		try {
			newSystem.copyConfigsFrom(system);
			fail();
		} catch (e) {
			if (!(e instanceof Error)) {
				throw e;
			}
			expect(e.message).toEqual("Can't copy configs due to structure differnece");
		}
	});

	it('should order blade profiles', () => {
		// Move index (group id) 8 to 0
		const ogId = 8;
		const newId = 0;
		const fontId = 1;
		const newOrder = system
			.mainConfig()
			.groups()
			.map(group => group.id());
		moveItemInArray(newOrder, ogId, newId);

		const ogProfile8 = system.mainConfig().getBladeProfileById(ogId);

		const fontPref1 = system.prefConfig().getPrefByFontId(fontId);
		const fontProfilePref1 = fontPref1?.getVariableByName(GroupType.profile)?.configValue().value();

		if (!fontProfilePref1 || !ogProfile8 || !fontPref1) {
			throw new Error('variable was undefined');
		}

		expect(fontProfilePref1).toBe(ogId);

		system.orderBladeProfiles(newOrder);

		const newProfile8 = system.mainConfig().getBladeProfileById(newId);

		const newFontPref1 = system.prefConfig().getPrefByFontId(fontId);
		const newFontProfilePref1 = newFontPref1?.getVariableByName(GroupType.profile)?.configValue();

		if (!newFontProfilePref1 || !newProfile8 || !newFontPref1) {
			throw new Error('variable was undefined');
		}
		expect(newFontProfilePref1.value()).toBe(newId);
		expect(newProfile8.variables()).toEqual(ogProfile8.variables());
		expect(newProfile8.name).toEqual(ogProfile8.name);
		expect(system.mainConfig().valid()).toBeTrue();
		expect(system.valid()).toBeTrue();
	});

	it('should order color profiles', () => {
		const ogId = 20;
		const newId = 4;
		const fontId = 1;
		const newOrder = system
			.colorConfig()
			.groups()
			.map(group => group.id());
		moveItemInArray(newOrder, ogId, newId); // Move index (group id) 20 to 4

		const ogProfile20 = system.colorConfig().getColorProfileById(ogId);

		const fontPref1 = system.prefConfig().getPrefByFontId(fontId);
		const fontProfilePref1 = fontPref1?.getVariableByName(GroupType.color)?.configValue().value();

		if (!fontProfilePref1 || !ogProfile20 || !fontPref1) {
			throw new Error('variable was undefined');
		}

		expect(fontProfilePref1).toBe(ogId);

		system.orderColorProfiles(newOrder);

		const newProfile20 = system.colorConfig().getColorProfileById(newId);

		const newFontPref1 = system.prefConfig().getPrefByFontId(fontId);
		const newFontProfilePref1 = newFontPref1?.getVariableByName(GroupType.color)?.configValue().value();

		if (!newFontProfilePref1 || !newProfile20 || !newFontPref1) {
			throw new Error('variable was undefined');
		}
		expect(newFontProfilePref1).toBe(newId);
		expect(newProfile20.variables()).toEqual(ogProfile20.variables());
		expect(newProfile20.name).toEqual(ogProfile20.name);
		expect(system.colorConfig().valid()).toBeTrue();
		expect(system.valid()).toBeTrue();
	});

	it('should order fonts', () => {
		const ogId = 4;
		const newId = 1;
		const newOrder = system.fontConfigs().map(font => font.id());
		moveItemInArray(newOrder, ogId - 1, newId - 1); // Font IDs are 1 indexed

		const ogFont4 = system.getFontConfigById(ogId);
		const ogLed4 = system.getLedConfigById(ogId);
		const ogPref4 = system.prefConfig().getPrefByFontId(ogId);

		if (!ogFont4 || !ogLed4 || !ogPref4) {
			throw new Error('variable was undefined');
		}

		system.orderFonts(newOrder);

		const newFont1 = system.getFontConfigById(newId);
		const newLed1 = system.getLedConfigById(newId);
		const newPref1 = system.prefConfig().getPrefByFontId(newId);

		if (!newFont1 || !newLed1 || !newPref1) {
			throw new Error('variable was undefined');
		}

		expect(newFont1).toEqual(ogFont4);
		expect(newLed1).toEqual(ogLed4);
		expect(newPref1).toEqual(ogPref4);
		expect(newFont1.valid()).toBeTrue();
		expect(newLed1.valid()).toBeTrue();
		expect(newPref1.valid()).toBeTrue();
		expect(system.valid()).toBeTrue();
	});

	it('should add a blade profile', () => {
		// Get the size of the mainConfig groups array
		const newId = system.mainConfig().groups().length;

		// create a new ConfigGroup instance
		const profile: ConfigGroupJSON = {
			id: newId,
			name: GroupType.profile,
			variables: [
				{
					configValue: 'test profile',
					name: ProfileNames.pname
				}
			]
		};
		const newProfile = ConfigGroup.fromJSON(profile, MAIN_DICT);

		// call addBladeProfile with the new ConfigGroup instance
		system.addBladeProfile(profile);

		// assert that mainConfig.groups has been updated to include the new ConfigGroup
		expect(system.mainConfig().groups().length).toEqual(newId + 1);
		expect(system.mainConfig().getBladeProfileById(newId)?.toString()).toEqual(newProfile.toString());
	});

	it('should add a color profile', () => {
		// Get the size of the colorConfig groups array
		const newId = system.colorConfig().groups().length;

		// create a new ConfigGroup instance
		const profile: ConfigGroupJSON = {
			id: newId,
			name: GroupType.color,
			variables: [
				{
					configValue: 'test color',
					name: ProfileNames.cname
				}
			]
		};
		const newProfile = ConfigGroup.fromJSON(profile, COLOR_DICTIONARY);

		// call addColorProfile with the new ConfigGroup instance
		system.addColorProfile(profile);

		// assert that colorConfig.groups has been updated to include the new ConfigGroup
		expect(system.colorConfig().groups().length).toEqual(newId + 1);
		expect(system.colorConfig().getColorProfileById(newId)?.toString()).toEqual(newProfile.toString());
	});

	describe('Deleting profiles', () => {
		let colorProfile: ConfigGroupJSON;
		let bladeProfile: ConfigGroupJSON;
		let ogColorGroupLength: number;
		let ogBadeGroupLength: number;

		beforeEach(() => {
			ogColorGroupLength = system.colorConfig().groups().length;
			ogBadeGroupLength = system.mainConfig().groups().length;
			// Create a new profile
			colorProfile = {
				id: ogColorGroupLength,
				name: GroupType.color,
				variables: [
					{
						configValue: 'test color',
						name: ProfileNames.cname
					}
				]
			};
			// Create a new profile
			bladeProfile = {
				id: -1,
				name: GroupType.profile,
				variables: [
					{
						configValue: 'test profile',
						name: ProfileNames.pname
					}
				]
			};
		});

		it('should delete a blade profile', () => {
			// Add the profile to the system
			system.addBladeProfile(bladeProfile);

			const newBladeProfile = system.mainConfig().getBladeProfileById(system.mainConfig().groups().length - 1);

			if (!newBladeProfile) {
				throw new Error('newBladeProfile was undefined');
			}

			// Delete the profile
			system.deleteBladeProfile(newBladeProfile);

			// Assert system that the profile has been deleted
			expect(system.mainConfig().groups().length).toEqual(ogBadeGroupLength);
			expect(system.mainConfig().getBladeProfileById(ogBadeGroupLength)).toBeUndefined();
		});

		it('should delete a color profile', () => {
			// Add the profile to the system
			system.addColorProfile(colorProfile);

			const newColorProfile = system.colorConfig().getColorProfileById(system.colorConfig().groups().length - 1);

			if (!newColorProfile) {
				throw new Error('newColorProfile was undefined');
			}

			// Delete the profile
			system.deleteColorProfile(newColorProfile);

			// Assert that the profile has been deleted
			expect(system.colorConfig().groups().length).toEqual(ogColorGroupLength);
			expect(system.colorConfig().getColorProfileById(ogColorGroupLength)).toBeUndefined();
		});

		// should delete a blade profile and update a profile that references it
		it('should delete a blade profile and update a profile that references it', () => {
			// Add the profile to the system
			system.addBladeProfile(bladeProfile);

			// Update a variable from a group in prefConfig to reference the new profile
			const ogPref = system.prefConfig().getPrefByFontId(1);
			const ogPrefId = ogPref?.getVariableByName(GroupType.profile)?.configValue();

			const newBladeProfile = system.mainConfig().getBladeProfileById(system.mainConfig().groups().length - 1);

			if (!ogPrefId || !IntegerValue.isIntegerValue(ogPrefId) || !newBladeProfile) {
				throw new Error('variable was undefined');
			}

			ogPrefId.value.set(newBladeProfile.id());

			// Delete the profile
			system.deleteBladeProfile(newBladeProfile);

			// Assert that the profile has been deleted
			expect(system.mainConfig().groups().length).toEqual(ogBadeGroupLength);

			// Assert that ogPrefId has been updated to reference the default profile
			expect(ogPrefId.value()).toEqual(0);
		});

		it('should not delete a non-existent blade or color profile', () => {
			// Delete a non-existent profile
			system.deleteBladeProfile(ConfigGroup.fromJSON(bladeProfile, MAIN_DICT));
			system.deleteColorProfile(ConfigGroup.fromJSON(colorProfile, COLOR_DICTIONARY));

			// Assert that the groups array is the same length as ogBadeGroupLength
			expect(system.mainConfig().groups().length).toEqual(ogBadeGroupLength);
			expect(system.colorConfig().groups().length).toEqual(ogColorGroupLength);
		});
	});

	describe('should test getAvailableSoundNames', () => {
		const fontId = 1;
		let defSoundNames: string[];

		beforeEach(() => {
			defSoundNames = system
				.sounds()
				.filter(sound => !sound.isTrack && sound.id() === fontId)
				.map(sound => sound.defSoundName);
		});

		it('when excluding a sound name', () => {
			const excludeDefName = defSoundNames.find(defSoundName => defSoundName.includes('#'));
			const availableSoundNames = system.getAvailableSoundNames(fontId, excludeDefName);

			if (!excludeDefName) {
				throw new Error('excludeDefName was undefined');
			}

			expect(availableSoundNames).not.toContain(excludeDefName);
			expect(availableSoundNames).not.toContain(
				defSoundNames.find(defSoundName => !defSoundName.includes('#')) ?? ''
			);
		});

		it('when not excluding a sound name', () => {
			const availableSoundNames = system.getAvailableSoundNames(fontId);

			expect(availableSoundNames).not.toContain(
				defSoundNames.find(defSoundName => !defSoundName.includes('#')) ?? ''
			);
		});
	});

	// See soundFileNames in test-data..karma.ts for list of sound files
	describe('should test getAvailableTrackName', () => {
		it('when it has 1 track', () => {
			const fontId = 1;
			system.sounds.update(sounds => sounds.filter(sound => sound.id() === fontId && sound.soundNumber === 1));

			const availableTrackName = system.getAvailableTrackName(fontId);

			expect(availableTrackName).toEqual('track2');
		});

		it('when it has 2 tracks with one missing inbetween', () => {
			const fontId = 2;
			system.sounds.update(sounds => sounds.filter(sound => sound.id() === fontId && sound.soundNumber !== 2));

			const availableTrackName = system.getAvailableTrackName(fontId);

			expect(availableTrackName).toEqual('track2');
		});

		it('when it has 0 tracks', () => {
			const fontId = 3;
			system.sounds.update(sounds => sounds.filter(sound => sound.id() !== fontId));

			const availableTrackName = system.getAvailableTrackName(fontId);

			expect(availableTrackName).toEqual('track1');
		});
	});
});
