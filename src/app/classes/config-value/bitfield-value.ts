import { computed } from '@angular/core';
import { BITMASK_VALUE } from 'src/app/constants/regex';
import { ValueType, ValueTypeBitfield } from 'src/app/enums/value-type.enum';
import { Bitfield } from 'src/app/models/bitfield.model';
import { BaseDefinition, BitfieldDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class BitfieldValue extends ConfigValue<Bitfield, ValueTypeBitfield, BitfieldDefinition> {
	readonly toString = computed(() => {
		let str = '';
		this.value().forEach(bit => (str += bit ? '1' : '0'));
		return str;
	});

	readonly valid = computed(() => this.value().length === this.definition.bitLength);

	constructor(value: string, def: BitfieldDefinition) {
		super(value, def, def.type);
	}

	static isBitfieldValue(value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>): value is BitfieldValue {
		return value instanceof BitfieldValue;
	}

	autoFixValue() {
		return;
	}

	protected parseValue(strVal: string) {
		const value = BITMASK_VALUE.test(strVal)
			? strVal
					.slice(0, this.definition.bitLength)
					.split('')
					.map(bit => bit === '1')
			: new Array<boolean>(this.definition.bitLength).fill(false);
		while (value.length < this.definition.bitLength) {
			value.push(false);
		}
		return value;
	}
}
