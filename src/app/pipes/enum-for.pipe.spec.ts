import { EnumForPipe } from './enum-for.pipe';

enum TestEnum {
	ELECTRON,
	TESTING,
	GEORGE,
	LSD
}

describe('EnumForPipe', () => {
	let pipe: EnumForPipe;

	beforeEach(() => {
		pipe = new EnumForPipe();
	});

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('will reduce basic enums to simple key/value pair', () => {
		// Standard way enums work
		expect(TestEnum.ELECTRON).toBe(0);
		expect(TestEnum.LSD).toBe(3);
		expect(TestEnum[0]).toEqual('ELECTRON');
		expect(TestEnum[3]).toEqual('LSD');

		const result = pipe.transform(TestEnum);

		if (!result) {
			throw new Error('result was undefined');
		}

		expect(result.ELECTRON).toBe(0);
		expect(result.LSD).toBe(3);
		expect(result[0]).toBeUndefined();
		expect(result[3]).toBeUndefined();
	});
});
