/**
 * The Smooth Swing Mode
 */
export enum SmoothSwingMode {
	DISABLED = 0,
	STANDARD,
	HYBRID,
	PITCH
}
