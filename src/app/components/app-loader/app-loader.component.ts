import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AppLoaderService } from 'src/app/services/app-loader.service';

@Component({
	selector: 'lsd-app-loader',
	templateUrl: './app-loader.component.html',
	styleUrls: ['./app-loader.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppLoaderComponent {
	constructor(public loaderServ: AppLoaderService) {}
}
