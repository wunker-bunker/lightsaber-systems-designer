import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { AppComponent } from './app.component';
import { AppLoaderComponent } from './components/app-loader/app-loader.component';

describe('AppComponent', () => {
	let fixture: ComponentFixture<AppComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [AppComponent, AppLoaderComponent, AppLoaderComponent, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AppComponent);
	});

	it('should create the app', () => {
		const app = fixture.componentInstance;
		expect(app).toBeTruthy();
	});
});
