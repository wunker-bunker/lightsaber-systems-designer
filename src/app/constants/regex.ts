import { ConfigFileType } from '../enums/config-file-type.enum';

/**
 * Regular expressions used for parsing config files.
 * Used to get the id and name of a font or LED file.
 *
 * Example: ./system/123-FontName
 */
export const FONT_REGEX = new RegExp(
	`^.*[/\\\\](\\d+)-([a-zA-Z0-9_-]+)[/\\\\](?:${ConfigFileType.FONT.replace(
		'.',
		'\\.'
	)}|${ConfigFileType.LEDS.replace('.', '\\.')})$`
);

/**
 * Regular expressions used for parsing integer variables in config files.
 *
 * Example: 123
 */
export const INTGER_VALUE = /^\d+$/;

/**
 * Regular expressions used for parsing bitmask variables in config files.
 * Bitmasks are a series of 0s and 1s.
 * The minimum length is 2.
 *
 * Example: 010010
 */
export const BITMASK_VALUE = /^([01]){2,}$/;

/**
 * Regular expressions used for parsing string variables in config files.
 * Strings are a series of alphanumeric characters.
 * The minimum length is 1.
 */
export const STRING_VALUE = /^\w+$/;

/**
 * Regular expressions used for parsing RGB variables in config files.
 * RGB values are a series of 3 integers separated by commas.
 *
 * Example: 255,255,255
 */
export const RGBW_VALUE = /^(-?\d{1,4}),(-?\d{1,4}),(-?\d{1,4}),(-?\d{1,4})$/;

/**
 * Regular expressions used for parsing variable names that have multiple instances.
 * The dictionary key is the variable name with the number replaced with a #.
 *
 * Example: bolt1, bolt2, bolt3, etc.
 */
export const MULTI_VAR = /^(\w*[^0-9])\d{1,2}$/;

/**
 * Regular expressions used for parsing variables.
 * Exclude comments and groups.
 *
 * Example: hforce=1000
 */
export const VAR_MATCH = /^\s*(?!##)([\w%]+)=([\w #,-]+)(?!\])*\s*$/;

/**
 * Regular expressions used for parsing groups headers.
 *
 * Example: [color=1]
 */
export const GROUP_MATCH = /^\s*\[(\w+)=(\d+)\]\s*$/;

/**
 * Regular expressions used for validating system names.
 * System names cannot contain the following characters: \ / < > : ; , ? " * |.
 * The minimum length is 1.
 *
 * Examples:
 * - My System
 * - My_System
 * - My-System
 * - MySystem
 * - MySystem123
 * - 123MySystem
 */
export const SYSTEM_NAME = /^[^\\<>:;,?"*|/]+$/;

/**
 * Regular expressions used for validating sound file names.
 * Filepath must be in the following format:
 *
 * ./system/123-SoundName/tracks/TrackName.wav or ./system/123-SoundName/TrackName.wav
 *
 * Capture groups:
 * 1. Font ID
 * 2. Font Name
 * 3. Sound Name
 */
export const SOUND_NAME = /^.*[/\\](\d+)-([a-zA-Z0-9_-]+)[/\\](?:tracks[/\\])?([A-Za-z0-9]*)\.wav/;

/**
 * Regular expressions used for validating a wav filename.
 */
export const WAV_FILE = /^.+\.wav$/;

/**
 * Regular expressions used for validating a font name.
 * Font names cannot contain the following characters: \ / < > : ; , ? " * |.
 *
 * Examples:
 * - MyFont
 * - My_Font
 * - My-Font
 * - MyFont123
 */
export const FONT_NAME = /^[a-zA-Z0-9_-]+$/;
