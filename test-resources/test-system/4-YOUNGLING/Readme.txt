                            _ _                     _                                                
/\_/\___  _   _ _ __   __ _| (_)_ __   __ _        | |__  _   _                                      
\_ _/ _ \| | | | '_ \ / _` | | | '_ \ / _` |       | '_ \| | | |                                     
 / \ (_) | |_| | | | | (_| | | | | | | (_| |       | |_) | |_| |                                     
 \_/\___/ \__,_|_| |_|\__, |_|_|_| |_|\__, |       |_.__/ \__, |                                     
                      |___/           |___/               |___/                                      
   __               _     ___ _       _           __       _                   ___           _       
  / /  ___  _ __ __| |   / __\ | __ _| | _____   / _\ __ _| |__   ___ _ __    / __\__  _ __ | |_ ___ 
 / /  / _ \| '__/ _` |  /__\// |/ _` | |/ / _ \  \ \ / _` | '_ \ / _ \ '__|  / _\/ _ \| '_ \| __/ __|
/ /__| (_) | | | (_| | / \/  \ | (_| |   < (_) | _\ \ (_| | |_) |  __/ |    / / | (_) | | | | |_\__ \
\____/\___/|_|  \__,_| \_____/_|\__,_|_|\_\___/  \__/\__,_|_.__/ \___|_|    \/   \___/|_| |_|\__|___/
                                                                                                       

"Youngling" CF7SE
by LordBlako - Created January 2014
http://www.youtube.com/LordBlakoSaberFonts

** UPGRADED to smoothswing by David Maltais - JediArmsDealer - 01/2021
** Restoring stock/former "non-smoothswing" behavior of the font is simply achieved by removing the lswing/hswing
   pairs in the font directory. Some other contents like (accent) swings and spins have been altered or removed from
   the font and stored in the /extra folder of the font. Updated fonts also have their combo files renamed as force
   sounds. Feel free to mix-match-remap or duplicate those to also have force-clash sounds.

This is a Font for the youngest of the Jedi order, the Younglings. They normally use a short bladed saber
during their practices.
The font design captures this specific lightsaber attributes and comes with a high pitched sound.

The Crystal Focus 7 Special Edition of this Font fully supports the new Crystal Focus v7 DS functions 
and includes mentoring quotes from Master Obi-Wan Kenobi.

More of my fonts are available at www.saberfont.com