import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { IntegerDefinition } from 'src/app/models/variable-dictionary.model';
import { IntegerValue } from './integer-value';

describe('NumberValue', () => {
	const def: IntegerDefinition = {
		description: 'desc',
		maxValue: 100,
		minValue: 0,
		type: VALUE_TYPE.INTEGER
	};

	it('should create an instance', () => {
		const value = new IntegerValue('0', def);
		expect(value).toBeTruthy();
		expect(value.definition).toEqual(def);
		expect(value.type).toEqual(def.type);
		expect(value.valid()).toBeTrue();
		expect(value.value()).toBe(0);
	});

	it('should print as a string', () => {
		expect(new IntegerValue('45', def).toString()).toEqual('45');
	});

	describe('should validate', () => {
		const value = new IntegerValue('0', def);

		it('valid when value is >= minValue and <= maxValue', () => {
			value.value.set(0);
			expect(value.valid()).toBeTrue();
			value.value.set(100);
			expect(value.valid()).toBeTrue();
		});

		it('invalid when value < minValue or > maxValue', () => {
			value.value.set(-1);
			expect(value.valid()).toBeFalse();
			value.value.set(101);
			expect(value.valid()).toBeFalse();
		});
	});

	describe('should auto fix value', () => {
		it('when below min value', () => {
			const value = new IntegerValue(`${def.minValue - 1}`, def);
			expect(value.valid()).toBeFalse();
			expect(value.value()).toEqual(def.minValue - 1);
			value.autoFixValue();
			expect(value.valid()).toBeTrue();
			expect(value.value()).toEqual(def.minValue);
		});

		it('when above max value', () => {
			const value = new IntegerValue(`${def.maxValue + 1}`, def);
			expect(value.valid()).toBeFalse();
			expect(value.value()).toEqual(def.maxValue + 1);
			value.autoFixValue();
			expect(value.valid()).toBeTrue();
			expect(value.value()).toEqual(def.maxValue);
		});

		it('when in range', () => {
			const intVal = (def.maxValue - def.minValue) / 2;
			const value = new IntegerValue(`${intVal}`, def);
			expect(value.valid()).toBeTrue();
			expect(value.value()).toEqual(intVal);
			value.autoFixValue();
			expect(value.valid()).toBeTrue();
			expect(value.value()).toEqual(intVal);
		});
	});
});
