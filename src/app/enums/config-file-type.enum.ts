/**
 * Enum for the different config file types
 * that can be loaded.
 */
export enum ConfigFileType {
	MAIN = 'config.txt',
	COLORS = 'colors.txt',
	PREF = 'prefs.txt',
	SPECIAL = 'special.txt',
	FONT = 'font_config.txt',
	LEDS = 'leds.txt'
}
