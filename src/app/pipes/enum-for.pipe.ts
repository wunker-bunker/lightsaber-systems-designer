import { Pipe, PipeTransform } from '@angular/core';
import { Enumeration, EnumerationType } from '../models/enumeration.model';

type ReducedEnum = { [key: string]: number };

/**
 * Pipe to convert an enumeration to a reduced enumeration that can be used in *ngFor.
 * The reduced enumeration only contains the keys and values of the enumeration.
 * This is mostly used for enumerations that contain numbers as values.
 * Best used in pair with the keyvalue pipe.
 * @example <option [ngValue]="opt.value" *ngFor="let opt of enumeration | enumFor | keyvalue">{{ opt.key }}</option>
 */
@Pipe({
	name: 'enumFor',
	pure: true
})
export class EnumForPipe implements PipeTransform {
	transform(enu: Enumeration): ReducedEnum | null {
		return enu
			? Object.keys(enu)
					.map((key: string): EnumerationType => enu[key])
					.filter((item: EnumerationType) => typeof item === 'number')
					.reduce((fullObj: ReducedEnum, value: EnumerationType): ReducedEnum => {
						fullObj[`${enu[value]}`] = +value;
						return fullObj;
					}, {})
			: null;
	}
}
