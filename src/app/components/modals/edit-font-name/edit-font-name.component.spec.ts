import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { EditFontNameComponent } from './edit-font-name.component';

describe('EditFontNameComponent', () => {
	let component: EditFontNameComponent;
	let fixture: ComponentFixture<EditFontNameComponent>;
	const modalMock: jasmine.SpyObj<NgbActiveModal> = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

	configureTestSuite({
		imports: [ReactiveFormsModule, ...unitTestImports],
		declarations: [EditFontNameComponent, ...unitTestDeclarations],
		providers: [{ provide: NgbActiveModal, useValue: modalMock }, ...unitTestProviders]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditFontNameComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should get and set name', () => {
		const name = component.name;

		if (!name) {
			throw new Error('name is null');
		}

		component.setName('test system');
		expect(name.value).toEqual('test system');
	});

	it('should print errors to console', () => {
		const name = component.name;

		if (!name) {
			throw new Error('name is null');
		}

		const warnSpy = spyOn(console, 'warn').and.callThrough();
		name.setValue('test ');
		name.updateValueAndValidity();
		component.printForm();
		expect(console.warn).toHaveBeenCalledWith(name.errors);

		warnSpy.calls.reset();
		name.setValue('test_FONT_1');
		name.updateValueAndValidity();
		component.printForm();
		expect(console.warn).not.toHaveBeenCalled();
	});
});
