                      _                                             
  /\ /\__ _ _ __ ___ (_)_ __   ___                                  
 / //_/ _` | '_ ` _ \| | '_ \ / _ \                                 
/ __ \ (_| | | | | | | | | | | (_) |                                
\/  \/\__,_|_| |_| |_|_|_| |_|\___/                                                                                                     
 _                 __ _       _     _       _                       
| |__  _   _    /\ \ (_) __ _| |__ | |_ ___| |_ ___  _ __ _ __ ___  
| '_ \| | | |  /  \/ / |/ _` | '_ \| __/ __| __/ _ \| '__| '_ ` _ \ 
| |_) | |_| | / /\  /| | (_| | | | | |_\__ \ || (_) | |  | | | | | |
|_.__/ \__, | \_\ \/ |_|\__, |_| |_|\__|___/\__\___/|_|  |_| |_| |_|
       |___/            |___/                                       


The Force Unleashed II Kamino 
By Nightstorm aka Mace Windu on FX Forums
More of my fonts are available at www.saberfont.com

** UPGRADED to smoothswing by David Maltais - JediArmsDealer - 01/2021
** Restoring stock/former "non-smoothswing" behavior of the font is simply achieved by removing the lswing/hswing
   pairs in the font directory. Some other contents like (accent) swings and spins have been altered or removed from
   the font and stored in the /extra folder of the font. Updated fonts also have their combo files renamed as force
   sounds. Feel free to mix-match-remap or duplicate those to also have force-clash sounds.

This is a remix/remaster of The Force Unleashed II: Remastered font.  
It's based on the planet Kamino where the story of The Force Unleashed II begins and ends.
I've used elements extracted directly from the game audio and mixed to give a feeling of being in the game once again.
I hope you enjoy it!

This font is compatible with Crystal Focus v7 sound boards and below by Plecter Labs.
It consists of:

4 Boots
4 Blaster effects
4 Combo Effects
4 Spin effects
4 Stab effects
16 Clash effects
16 Swing effects
2 Force effects
2 Power off effects
5 Power on effects
1 lockup effect
1 hum loop
