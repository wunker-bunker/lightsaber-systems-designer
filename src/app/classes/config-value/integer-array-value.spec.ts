import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { IntegerArrayDefinition } from 'src/app/models/variable-dictionary.model';
import { IntegerArrayValue } from './integer-array-value';

/*  */ describe('IntegerArrayValue', () => {
	const def: IntegerArrayDefinition = {
		arrLength: 4,
		description: 'desc',
		maxValueArr: [100, 200, 50, 10],
		minValueArr: [-5, 0, 20, 5],
		type: VALUE_TYPE.INTEGER_ARR,
		expandable: false
	};
	const expandableDef: IntegerArrayDefinition = {
		description: 'desc',
		maxValueArr: [100],
		minValueArr: [0],
		type: VALUE_TYPE.INTEGER_ARR,
		expandable: true,
		arrLength: 5
	};
	const validString = '10,100,30,7';
	const validArr = [10, 100, 30, 7];
	const invalidString = '2,-2,10,5';
	const invalidArr = [2, -2, 10, 5];

	it('should create an instance', () => {
		const val = new IntegerArrayValue(validString, def);
		expect(val).toBeTruthy();
		expect(val.definition).toEqual(def);
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.validArr()).toEqual([true, true, true, true]);
		expect(val.value()).toEqual(validArr);

		const inval = new IntegerArrayValue(invalidString, def);
		expect(inval).toBeTruthy();
		expect(inval.definition).toEqual(def);
		expect(inval.type).toEqual(def.type);
		expect(inval.valid()).toBeFalse();
		expect(inval.validArr()).toEqual([true, false, false, true]);
		expect(inval.value()).toEqual(invalidArr);
	});

	it('should print toString', () => {
		expect(new IntegerArrayValue(validString, def).toString()).toEqual(validString);
	});

	describe('should validate', () => {
		let val: IntegerArrayValue;

		describe('non-expandable', () => {
			beforeEach(() => {
				val = new IntegerArrayValue('0,0,0,0', def);
			});

			it('valid when values are within bounds of corresponding index of min/max value', () => {
				val.value.set(validArr);
				expect(val.valid()).toBeTrue();
				val.value.set([...def.maxValueArr]);
				expect(val.valid()).toBeTrue();
				val.value.set([...def.minValueArr]);
				expect(val.valid()).toBeTrue();
			});

			it('invalid when any value is out of bounds of respective index', () => {
				val.value.set(invalidArr);
				expect(val.valid()).toBeFalse();
				val.value.set([...validArr]);
				val.value.mutate(value => (value[0] = def.minValueArr[0] - 1));
				expect(val.valid()).toBeFalse();
				val.value.set([...validArr]);
				val.value.mutate(value => (value[3] = def.maxValueArr[3] + 1));
				expect(val.valid()).toBeFalse();
				val.value.set(def.maxValueArr.map(max => max + 1));
				expect(val.valid()).toBeFalse();
				val.value.set(def.minValueArr.map(min => min - 1));
				expect(val.valid()).toBeFalse();
			});
		});

		describe('expandable', () => {
			beforeEach(() => {
				val = new IntegerArrayValue('0,0,0,0', expandableDef);
			});

			it('valid when values are within bounds of corresponding index of min/max value', () => {
				val.value.set(validArr);
				expect(val.valid()).toBeTrue();
				val.value.set([0, 20, 100]);
				expect(val.valid()).toBeTrue();
				val.value.set([0, 100]);
				expect(val.valid()).toBeTrue();
				val.value.set([0, 20, 10, 32, 100]);
				expect(val.valid()).toBeTrue();
			});

			it('invalid when any value is out of bounds of respective index', () => {
				val.value.set(invalidArr);
				expect(val.valid()).toBeFalse();
				val.value.set([...validArr]);
				val.value.mutate(value => (value[0] = expandableDef.minValueArr[0] - 1));
				expect(val.valid()).toBeFalse();
				val.value.set([...validArr]);
				val.value.mutate(value => (value[3] = expandableDef.maxValueArr[0] + 1));
				expect(val.valid()).toBeFalse();
				val.value.set(expandableDef.maxValueArr.map(max => max + 1));
				expect(val.valid()).toBeFalse();
				val.value.set(expandableDef.minValueArr.map(min => min - 1));
				expect(val.valid()).toBeFalse();
				val.value.set([0, 1, 2, 3, 4, 5, 6]);
				expect(val.valid()).toBeFalse();
			});
		});
	});

	describe('should auto fix values', () => {
		let val: IntegerArrayValue;

		describe('non-expandable', () => {
			beforeEach(() => {
				val = new IntegerArrayValue('0,0,0,0', def);
			});

			it('greater than max values', () => {
				const bigValues = def.maxValueArr.map(max => max + 1);
				bigValues.push(0);
				const fixedValues = [...def.maxValueArr];
				val.value.set(bigValues);
				val.valid();
				expect(val.valid()).toBeFalse();
				expect(val.value()).toEqual(bigValues);
				expect(val.value().length).toEqual(bigValues.length);
				val.autoFixValue();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(fixedValues);
				expect(val.value().length).toEqual(val.definition.arrLength);
			});

			it('less than min values', () => {
				const smallValues = def.minValueArr.map(min => min - 1);
				smallValues.pop();
				const fixedValues = [...def.minValueArr];
				val.value.set(smallValues);
				val.valid();
				expect(val.valid()).toBeFalse();
				expect(val.value()).toEqual(smallValues);
				expect(val.value().length).toEqual(smallValues.length);
				val.autoFixValue();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(fixedValues);
				expect(val.value().length).toEqual(val.definition.arrLength);
			});

			it('valid values', () => {
				val.value.set(validArr);
				val.valid();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(validArr);
				expect(val.value().length).toEqual(validArr.length);
				val.autoFixValue();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(validArr);
				expect(val.value().length).toEqual(val.definition.arrLength);
			});
		});

		describe('expandable', () => {
			beforeEach(() => {
				val = new IntegerArrayValue('0,0,0,0', expandableDef);
			});

			it('greater than max values', () => {
				const bigValues = [
					expandableDef.maxValueArr[0] + 1,
					expandableDef.maxValueArr[0] + 1,
					expandableDef.maxValueArr[0] + 1,
					expandableDef.maxValueArr[0] + 1,
					expandableDef.maxValueArr[0] + 1,
					expandableDef.maxValueArr[0] + 1
				];
				const fixedValues = [
					expandableDef.maxValueArr[0],
					expandableDef.maxValueArr[0],
					expandableDef.maxValueArr[0],
					expandableDef.maxValueArr[0],
					expandableDef.maxValueArr[0]
				];
				val.value.set(bigValues);
				val.valid();
				expect(val.valid()).toBeFalse();
				expect(val.value()).toEqual(bigValues);
				expect(val.value().length).toEqual(bigValues.length);
				val.autoFixValue();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(fixedValues);
				expect(val.value().length).toEqual(val.definition.arrLength);
			});

			it('less than min values', () => {
				const smallValues = [
					expandableDef.minValueArr[0] - 1,
					expandableDef.minValueArr[0] - 1,
					expandableDef.minValueArr[0] - 1
				];
				const fixedValues = [
					expandableDef.minValueArr[0],
					expandableDef.minValueArr[0],
					expandableDef.minValueArr[0]
				];
				val.value.set(smallValues);
				expect(val.valid()).toBeFalse();
				expect(val.value()).toEqual(smallValues);
				expect(val.value().length).toEqual(smallValues.length);
				val.autoFixValue();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(fixedValues);
				expect(val.value().length).toEqual(smallValues.length);
			});

			it('valid values', () => {
				val.value.set(validArr);
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(validArr);
				expect(val.value().length).toEqual(validArr.length);
				val.autoFixValue();
				expect(val.valid()).toBeTrue();
				expect(val.value()).toEqual(validArr);
				expect(val.value().length).toEqual(validArr.length);
			});
		});
	});
});
