import { UNKNOWN_DEFINITION } from 'src/app/dictionaries/variable.dict';
import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { UnknownValue } from './unknown-value';

describe('UnknownValue', () => {
	it('should create an instance', () => {
		const value = new UnknownValue('test');
		expect(value).toBeTruthy();
		expect(value.type).toEqual(VALUE_TYPE.UNKNOWN);
		expect(value.definition).toEqual(UNKNOWN_DEFINITION);
		expect(value.value()).toEqual('test');
		expect(value.valid()).toBeTrue();
	});

	describe('should validate', () => {
		const value = new UnknownValue();
		it('blank string is invalid', () => {
			value.value.set('');
			expect(value.valid()).toBeFalse();
		});

		it('non-blank string is valid', () => {
			value.value.set('test');
			expect(value.valid()).toBeTrue();
		});
	});

	it('should print toString', () => {
		expect(new UnknownValue('test').toString()).toEqual('test');
	});

	it('should auto fix value', () => {
		const value = new UnknownValue();
		expect(value.value()).toEqual('');
		expect(value.valid()).toBeFalse();
		value.autoFixValue();
		expect(value.value()).toEqual('Unknown Value');
		expect(value.valid()).toBeTrue();
	});
});
