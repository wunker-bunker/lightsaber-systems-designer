import { computed, Signal, signal } from '@angular/core';
import { ValueType } from 'src/app/enums/value-type.enum';
import { Bitfield } from 'src/app/models/bitfield.model';
import { Color } from 'src/app/models/color.model';
import { EnumerationType } from 'src/app/models/enumeration.model';
import { BaseDefinition } from 'src/app/models/variable-dictionary.model';
import { BitfieldValue } from './bitfield-value';
import { BooleanValue } from './boolean-value';
import { ColorValue } from './color-value';
import { EnumerationValue } from './enumeration-value';
import { IntegerArrayValue } from './integer-array-value';
import { IntegerValue } from './integer-value';
import { ProfileSelectorValue } from './profile-selector-value';
import { StringValue } from './string-value';
import { UnknownValue } from './unknown-value';

export type ConfigValueTypes = string | number | number[] | EnumerationType | Color | boolean | Bitfield;

export type ConfigValueClasses =
	| BitfieldValue
	| BooleanValue
	| ColorValue
	| EnumerationValue
	| IntegerArrayValue
	| IntegerValue
	| StringValue
	| ProfileSelectorValue
	| UnknownValue;

export abstract class ConfigValue<T extends ConfigValueTypes, V extends ValueType, D extends BaseDefinition> {
	readonly dirty;
	readonly value;

	private readonly _originalVal;

	abstract readonly toString: Signal<string>;
	abstract readonly valid: Signal<boolean>;

	constructor(value: string, public readonly definition: D, public readonly type: V) {
		this._originalVal = signal(value);
		this.value = signal(this.parseValue(value));
		this.dirty = computed(() => this.toString() !== this._originalVal());
	}

	markAsSaved(): void {
		this._originalVal.set(this.toString());
	}

	revert(): void {
		this.value.set(this.parseValue(this._originalVal()));
	}

	abstract autoFixValue(): void;

	protected abstract parseValue(strVal: string): T;
}
