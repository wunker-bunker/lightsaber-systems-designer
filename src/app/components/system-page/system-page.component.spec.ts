import { moveItemInArray } from '@angular/cdk/drag-drop';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { join } from 'path';
import { BehaviorSubject } from 'rxjs';
import { FontConfig } from 'src/app/classes/config-file/font-config';
import { IntegerValue } from 'src/app/classes/config-value/integer-value';
import { LightsaberSystem } from 'src/app/classes/lightsaber-system';
import { SoundFile } from 'src/app/classes/sound-file';
import { GroupType } from 'src/app/enums/group-types';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import { AppAlertsService } from 'src/app/services/app-alerts.service';
import { AppLoaderService } from 'src/app/services/app-loader.service';
import { FileManagementService } from 'src/app/services/file-management.service';
import { ModalService } from 'src/app/services/modal.service';
import {
	MockConfigListComponent,
	MockFontListComponent,
	MockGroupConfigListComponent,
	MockSystemListComponent,
	MockUpdateIndicatorComponent,
	testFonts,
	testLibraryLocation,
	testSystemLocation1
} from 'src/environments/test-data.karma';
import {
	configureTestSuite,
	openDirecotrySpy,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { ConfigGroupListBody } from '../config-list/config-list.component';
import { SystemPageComponent } from './system-page.component';

describe('SystemPageComponent', () => {
	const route = {
		data: new BehaviorSubject<{ system: LightsaberSystem }>({ system: testData.system })
	};
	const loaderServ: jasmine.SpyObj<AppLoaderService> = jasmine.createSpyObj('AppLoaderService', ['setState']);
	const fileServ: jasmine.SpyObj<FileManagementService> = jasmine.createSpyObj('FileManagementService', [
		'saveSystem',
		'exportSystem',
		'exportConfigs',
		'importConfigs',
		'importFontFromDir',
		'importSound',
		'renameSound',
		'deleteFont',
		'deleteSound'
	]);
	const alertServ: jasmine.SpyObj<AppAlertsService> = jasmine.createSpyObj('AppAlertsService', [
		'showSuccessAlert',
		'showErrorAlert'
	]);
	const modalServ: jasmine.SpyObj<ModalService> = jasmine.createSpyObj('ModalService', [
		'openDirectory',
		'editFontName',
		'preferences',
		'openSoundFile',
		'chooseSoundName'
	]);
	let component: SystemPageComponent;
	let fixture: ComponentFixture<SystemPageComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [
			SystemPageComponent,
			OrderByPipe,
			MockFontListComponent,
			MockConfigListComponent,
			MockGroupConfigListComponent,
			MockUpdateIndicatorComponent,
			MockSystemListComponent,
			...unitTestDeclarations
		],
		providers: [
			{ provide: ActivatedRoute, useValue: route },
			{ provide: AppLoaderService, useValue: loaderServ },
			{ provide: FileManagementService, useValue: fileServ },
			{ provide: AppAlertsService, useValue: alertServ },
			{ provide: ModalService, useValue: modalServ },
			...unitTestProviders
		]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SystemPageComponent);
		component = fixture.componentInstance;
		route.data.next({ system: testData.system });
		fixture.detectChanges();
	});

	afterEach(() => {
		loaderServ.setState.calls.reset();
		fileServ.saveSystem.calls.reset();
		fileServ.exportSystem.calls.reset();
		fileServ.exportConfigs.calls.reset();
		fileServ.importConfigs.calls.reset();
		fileServ.importFontFromDir.calls.reset();
		fileServ.importSound.calls.reset();
		fileServ.renameSound.calls.reset();
		fileServ.deleteFont.calls.reset();
		fileServ.deleteSound.calls.reset();
		alertServ.showSuccessAlert.calls.reset();
		alertServ.showErrorAlert.calls.reset();
		modalServ.editFontName.calls.reset();
		modalServ.openDirectory.calls.reset();
		modalServ.preferences.calls.reset();
		modalServ.openSoundFile.calls.reset();
		modalServ.chooseSoundName.calls.reset();
	});

	it('should create', () => {
		const fontId = 1;
		const font = testData.system.getFontConfigById(fontId);
		const led = testData.system.getLedConfigById(fontId);

		if (!font || !led) {
			throw new Error('Test data not set up correctly');
		}

		expect(component).toBeTruthy();
		expect(component.system()).toEqual(testData.system);
		expect(component.selectedFontId()).toEqual(fontId);
		expect(component.ledConfig()).toEqual(led);
		expect(component.fontConfig()).toEqual(font);
		const profile = testData.system
			.prefConfig()
			.getPrefByFontId(fontId)
			?.getVariableByName('profile')
			?.configValue();
		const color = testData.system.prefConfig().getPrefByFontId(fontId)?.getVariableByName('color')?.configValue();

		if (!profile || !color || !IntegerValue.isIntegerValue(profile) || !IntegerValue.isIntegerValue(color)) {
			throw new Error('Test data not set up correctly');
		}

		expect(component.selectedBladeConfigVar()?.value()).toEqual(profile.value());
		expect(component.selectedColorConfigVar()?.value()).toEqual(color.value());
	});

	describe('should save', () => {
		it('success', async () => {
			fileServ.saveSystem.and.resolveTo();
			await component.save();
			expect(fileServ.saveSystem).toHaveBeenCalledWith(testData.system);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		});

		it('fail', async () => {
			fileServ.saveSystem.and.rejectWith(null);
			await component.save();
			expect(fileServ.saveSystem).toHaveBeenCalledWith(testData.system);
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		});
	});

	describe('should add font', () => {
		const ogFontName = '1-NIGHTFALL';
		const fontPath = join('test-resources', 'test-system', ogFontName);
		const newName = 'NIGHTFALLing';
		let newId: number;

		beforeEach(() => {
			const fontConfigs = component.system()?.fontConfigs();

			if (!fontConfigs) {
				throw new Error('Test data not set up correctly');
			}

			newId = fontConfigs.length + 2;
			fileServ.importFontFromDir.and.resolveTo(newId);
			modalServ.openDirectory.and.resolveTo(fontPath);
			modalServ.editFontName.and.resolveTo(newName);
		});

		it('when found directory and added font', fakeAsync(async () => {
			await component.addFont();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(modalServ.editFontName).toHaveBeenCalledWith(ogFontName);
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Importing font from ${fontPath}`);
			tick();
			expect(fileServ.importFontFromDir).toHaveBeenCalledWith(fontPath, newName, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		}));

		it('when found directory and failed add font', fakeAsync(async () => {
			fileServ.importFontFromDir.and.rejectWith('error');
			await component.addFont();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(modalServ.editFontName).toHaveBeenCalledWith(ogFontName);
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Importing font from ${fontPath}`);
			tick();
			expect(fileServ.importFontFromDir).toHaveBeenCalledWith(fontPath, newName, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		}));

		it('when directory canceled', async () => {
			modalServ.openDirectory.and.rejectWith('canceled');
			await component.addFont();
			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(modalServ.editFontName).not.toHaveBeenCalled();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.importFontFromDir).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});

		it('when font name add canceled', async () => {
			modalServ.editFontName.and.rejectWith('canceled');
			await component.addFont();
			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(modalServ.editFontName).toHaveBeenCalledWith(ogFontName);
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.importFontFromDir).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});
	});

	describe('should export', () => {
		const exportPath = join(testLibraryLocation, 'export');

		beforeEach(() => {
			fileServ.exportSystem.and.resolveTo();
			modalServ.openDirectory.and.resolveTo(exportPath);
		});

		it('when found directory and exported system', fakeAsync(async () => {
			await component.export();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(loaderServ.setState).toHaveBeenCalledWith(
				true,
				`Exporting system to ${exportPath}, this will take a while...`
			);
			tick();
			expect(fileServ.exportSystem).toHaveBeenCalledWith(exportPath, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		}));

		it('when found directory and failed exported system', fakeAsync(async () => {
			fileServ.exportSystem.and.rejectWith(null);
			await component.export();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(loaderServ.setState).toHaveBeenCalledWith(
				true,
				`Exporting system to ${exportPath}, this will take a while...`
			);
			tick();
			expect(fileServ.exportSystem).toHaveBeenCalledWith(exportPath, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		}));

		it('when directory canceled', async () => {
			modalServ.openDirectory.and.rejectWith('canceled');
			await component.export();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.exportSystem).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});
	});

	describe('should export configs', () => {
		const exportPath = join(testLibraryLocation, 'export');

		beforeEach(() => {
			fileServ.exportConfigs.and.resolveTo();
			modalServ.openDirectory.and.resolveTo(exportPath);
		});

		it('when directory found and export configs', fakeAsync(async () => {
			await component.exportConfigs();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Exporting configs to ${exportPath}`);
			tick();
			expect(fileServ.exportConfigs).toHaveBeenCalledWith(exportPath, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		}));

		it('when directory found and failed export configs', fakeAsync(async () => {
			fileServ.exportConfigs.and.rejectWith('fail');
			await component.exportConfigs();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Exporting configs to ${exportPath}`);
			tick();
			expect(fileServ.exportConfigs).toHaveBeenCalledWith(exportPath, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		}));

		it('when directory canceled', async () => {
			modalServ.openDirectory.and.rejectWith('canceled');
			await component.exportConfigs();
			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.exportConfigs).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});
	});

	describe('should import configs', () => {
		const importPath = join(testLibraryLocation, 'import');

		beforeEach(() => {
			fileServ.importConfigs.and.resolveTo();
			modalServ.openDirectory.and.resolveTo(importPath);
		});

		it('when directory found and imported configs', fakeAsync(async () => {
			await component.importConfigs();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Importing configs from ${importPath}`);
			tick();
			expect(fileServ.importConfigs).toHaveBeenCalledWith(importPath, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		}));

		it('when directory found and failed imported configs', fakeAsync(async () => {
			fileServ.importConfigs.and.rejectWith('fail');
			await component.importConfigs();

			const system = component.system();

			if (!system) {
				throw new Error('Test data not set up correctly');
			}

			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Importing configs from ${importPath}`);
			tick();
			expect(fileServ.importConfigs).toHaveBeenCalledWith(importPath, system);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		}));

		it('when directory cancled', async () => {
			modalServ.openDirectory.and.rejectWith('canceled');
			await component.importConfigs();
			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.importConfigs).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});
	});

	it('should open preferences', () => {
		modalServ.preferences.and.rejectWith(null);
		component.openPreferences();
		expect(modalServ.preferences).toHaveBeenCalled();
	});

	it('should open system directory', () => {
		component.openSystemDir();

		const system = component.system();

		if (!system) {
			throw new Error('Test data not set up correctly');
		}

		expect(openDirecotrySpy).toHaveBeenCalledOnceWith(system.path);
	});

	describe('should order group', () => {
		let newOrder: readonly number[];
		let system: LightsaberSystem;

		beforeEach(() => {
			const s = component.system();

			if (!s) {
				throw new Error('Test data not set up correctly');
			}

			system = s;

			spyOn(system, 'orderBladeProfiles');
			spyOn(system, 'orderColorProfiles');
			newOrder = system
				.mainConfig()
				.groups()
				.map(group => group.id());
		});

		it('when type is color', () => {
			component.orderGroup([newOrder, component.groupBodyOpts()[1]]);
			expect(system.orderBladeProfiles).not.toHaveBeenCalled();
			expect(system.orderColorProfiles).toHaveBeenCalledWith(newOrder);
		});

		it('when type is blade', () => {
			component.orderGroup([newOrder, component.groupBodyOpts()[0]]);
			expect(system.orderBladeProfiles).toHaveBeenCalledWith(newOrder);
			expect(system.orderColorProfiles).not.toHaveBeenCalled();
		});
	});

	it('should order fonts', () => {
		const ogId = 4;
		const newId = 1;
		const system = component.system();

		if (!system) {
			throw new Error('Test data not set up correctly');
		}

		const newOrder = system.fontConfigs().map(font => font.id());
		moveItemInArray(newOrder, ogId - 1, newId - 1); // Font IDs are 1 indexed
		spyOn(system, 'orderFonts');

		component.orderFonts(newOrder);
		expect(system.orderFonts).toHaveBeenCalledWith(newOrder);
	});

	describe('should add profile', () => {
		let addBladeProfileSpy: jasmine.Spy;
		let addColorProfileSpy: jasmine.Spy;
		let system: LightsaberSystem;

		beforeEach(() => {
			const s = component.system();

			if (!s) {
				throw new Error('Test data not set up correctly');
			}

			system = s;
			addBladeProfileSpy = spyOn(system, 'addBladeProfile').and.callThrough();
			addColorProfileSpy = spyOn(system, 'addColorProfile').and.callThrough();
		});

		it('when type is color', async () => {
			const currentProfileCount = system.colorConfig().groups().length;

			await component.addProfile(component.groupBodyOpts()[1]);

			expect(addColorProfileSpy).toHaveBeenCalled();
			expect(addBladeProfileSpy).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(system.colorConfig().groups().length).toEqual(currentProfileCount + 1);
		});

		it('when type is blade', async () => {
			const currentProfileCount = system.mainConfig().groups().length;

			await component.addProfile(component.groupBodyOpts()[0]);

			expect(addBladeProfileSpy).toHaveBeenCalled();
			expect(addColorProfileSpy).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(system.mainConfig().groups().length).toEqual(currentProfileCount + 1);
		});

		it('when save fails', async () => {
			spyOn(component, 'save').and.returnValue(Promise.reject('Test Error'));

			await component.addProfile(component.groupBodyOpts()[0]);

			expect(addBladeProfileSpy).toHaveBeenCalled();
			expect(addColorProfileSpy).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});
	});

	describe('should add sound', () => {
		const fontId = 1;
		const testSoundFilePath = join(testSystemLocation1, testFonts[0], 'clash2.wav');
		const testTrackFilePath = join(testSystemLocation1, testFonts[0], 'tracks', 'track4.wav');
		let newSoundFile: SoundFile;
		let newTrackFile: SoundFile;
		let system: LightsaberSystem;

		beforeEach(() => {
			const s = component.system();

			if (!s) {
				throw new Error('Test data not set up correctly');
			}

			system = s;
			component.selectedFontId.set(fontId);
			newSoundFile = new SoundFile(
				testSoundFilePath,
				`lsd://${encodeURIComponent(testSoundFilePath)}` as SafeResourceUrl
			);
			newTrackFile = new SoundFile(
				testTrackFilePath,
				`lsd://${encodeURIComponent(testTrackFilePath)}` as SafeResourceUrl
			);
		});

		it('when sound is a track', async () => {
			const isTrack = true;
			modalServ.openSoundFile.and.resolveTo(testTrackFilePath);
			fileServ.importSound.and.resolveTo(newTrackFile);

			await component.addSound(isTrack);

			expect(modalServ.openSoundFile).toHaveBeenCalledWith(isTrack);
			expect(fileServ.importSound).toHaveBeenCalledWith(testTrackFilePath, 'track4', system, fontId, isTrack);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		});

		it('when sound is not a track', async () => {
			const isTrack = false;
			modalServ.openSoundFile.and.resolveTo(testSoundFilePath);
			modalServ.chooseSoundName.and.resolveTo('poweron');
			fileServ.importSound.and.resolveTo(newSoundFile);

			await component.addSound(isTrack);

			expect(modalServ.openSoundFile).toHaveBeenCalledWith(isTrack);
			expect(fileServ.importSound).toHaveBeenCalledWith(testSoundFilePath, 'poweron', system, fontId, isTrack);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		});

		it('when user cancels open file dialog', async () => {
			const isTrack = false;
			modalServ.openSoundFile.and.rejectWith(null);

			await component.addSound(isTrack);

			expect(modalServ.openSoundFile).toHaveBeenCalledWith(isTrack);
			expect(fileServ.importSound).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
		});

		it('when import sound fails', async () => {
			const isTrack = false;
			modalServ.openSoundFile.and.resolveTo(testSoundFilePath);
			modalServ.chooseSoundName.and.resolveTo('poweron');
			fileServ.importSound.and.rejectWith('Test Error');

			await component.addSound(isTrack);

			expect(modalServ.openSoundFile).toHaveBeenCalledWith(isTrack);
			expect(fileServ.importSound).toHaveBeenCalledWith(testSoundFilePath, 'poweron', system, fontId, isTrack);
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		});

		it('when user cancels choose sound name dialog', async () => {
			const isTrack = false;
			modalServ.openSoundFile.and.resolveTo(testSoundFilePath);
			modalServ.chooseSoundName.and.rejectWith(null);

			await component.addSound(isTrack);

			expect(modalServ.openSoundFile).toHaveBeenCalledWith(isTrack);
			expect(fileServ.importSound).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
		});
	});

	describe('should change sound name', () => {
		const fontId = 1;
		const soundName = 'poweron';
		let sound: SoundFile;
		let system: LightsaberSystem;

		beforeEach(() => {
			const s = component.system();

			if (!s) {
				throw new Error('Test data not set up correctly');
			}

			system = s;
			component.selectedFontId.set(fontId);
			sound = system.getSoundsById(fontId)[0];
		});

		it('when user enters a new name', async () => {
			modalServ.chooseSoundName.and.resolveTo(soundName);
			fileServ.renameSound.and.resolveTo(sound);

			await component.changeSoundName(sound);

			expect(modalServ.chooseSoundName).toHaveBeenCalledWith(
				system.getAvailableSoundNames(component.selectedFontId(), sound.defSoundName)
			);
			expect(fileServ.renameSound).toHaveBeenCalledWith(soundName, sound, system);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
		});

		it('when user cancels choose sound name dialog', async () => {
			modalServ.chooseSoundName.and.rejectWith(null);

			await component.changeSoundName(sound);

			expect(modalServ.chooseSoundName).toHaveBeenCalledWith(
				system.getAvailableSoundNames(component.selectedFontId(), sound.defSoundName)
			);
			expect(fileServ.renameSound).not.toHaveBeenCalled();
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
		});

		it('when rename sound fails', async () => {
			modalServ.chooseSoundName.and.resolveTo(soundName);
			fileServ.renameSound.and.rejectWith('Test Error');

			await component.changeSoundName(sound);

			expect(modalServ.chooseSoundName).toHaveBeenCalledWith(
				system.getAvailableSoundNames(component.selectedFontId(), sound.defSoundName)
			);
			expect(fileServ.renameSound).toHaveBeenCalledWith(soundName, sound, system);
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		});
	});

	describe('delete font', () => {
		const fontId = 1;
		let fontConfig: FontConfig;
		let system: LightsaberSystem;

		beforeEach(() => {
			const s = component.system();
			const f = s?.getFontConfigById(fontId);

			if (!s || !f) {
				throw new Error('Test data not set up correctly');
			}

			system = s;
			fontConfig = f;
		});

		it('when user confirms', async () => {
			fileServ.deleteFont.and.resolveTo();

			await component.deleteFont(fontConfig);

			expect(fileServ.deleteFont).toHaveBeenCalledWith(fontConfig, system);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledTimes(2);
		});

		it('when delete font fails', async () => {
			fileServ.deleteFont.and.rejectWith('Test Error');

			await component.deleteFont(fontConfig);

			expect(fileServ.deleteFont).toHaveBeenCalledWith(fontConfig, system);
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledTimes(2);
		});
	});

	describe('should delete group', () => {
		let profileGroupListBody: ConfigGroupListBody;
		let colorGroupListBody: ConfigGroupListBody;
		let system: LightsaberSystem;

		beforeEach(() => {
			const pglb = component.groupBodyOpts().find(body => body.groupType === GroupType.profile);
			const cglb = component.groupBodyOpts().find(body => body.groupType === GroupType.color);
			const s = component.system();

			if (!pglb || !cglb || !s || pglb.groups().length === 0 || cglb.groups().length === 0) {
				throw new Error('Test data not set up correctly');
			}

			profileGroupListBody = pglb;
			colorGroupListBody = cglb;
			system = s;
		});

		it('when group is a blade profile', async () => {
			const bodyGroupLength = profileGroupListBody.groups().length;
			const systemGroupLength = system.mainConfig().groups().length;
			const group = profileGroupListBody.groups()[0];

			await component.deleteGroup([group, profileGroupListBody]);

			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(profileGroupListBody.groups().length).toEqual(bodyGroupLength - 1);
			expect(system.mainConfig().groups().length).toEqual(systemGroupLength - 1);
		});

		it('when group is a color', async () => {
			const bodyGroupLength = colorGroupListBody.groups().length;
			const systemGroupLength = system.colorConfig().groups().length;
			const group = colorGroupListBody.groups()[0];

			await component.deleteGroup([group, colorGroupListBody]);

			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(colorGroupListBody.groups().length).toEqual(bodyGroupLength - 1);
			expect(system.colorConfig().groups().length).toEqual(systemGroupLength - 1);
		});

		it('when save fails', async () => {
			const group = profileGroupListBody.groups()[0];
			spyOn(component, 'save').and.rejectWith('Test Error');

			await component.deleteGroup([group, profileGroupListBody]);

			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
		});
	});

	describe('should delete sound', () => {
		let sound: SoundFile;
		let system: LightsaberSystem;

		beforeEach(() => {
			const s = component.system();

			if (!s) {
				throw new Error('Test data not set up correctly');
			}

			system = s;

			sound = system.getSoundsById(component.selectedFontId())[0];
		});

		it('when user confirms', async () => {
			fileServ.deleteSound.and.resolveTo();

			await component.deleteSound(sound);

			expect(fileServ.deleteSound).toHaveBeenCalledWith(sound, system);
			expect(alertServ.showSuccessAlert).toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledTimes(2);
		});

		it('when delete sound fails', async () => {
			fileServ.deleteSound.and.rejectWith('Test Error');

			await component.deleteSound(sound);

			expect(fileServ.deleteSound).toHaveBeenCalledWith(sound, system);
			expect(alertServ.showSuccessAlert).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
			expect(loaderServ.setState).toHaveBeenCalledTimes(2);
		});
	});
});
