import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { BooleanDefinition } from 'src/app/models/variable-dictionary.model';
import { BooleanValue } from './boolean-value';

describe('BooleanValue', () => {
	const def: BooleanDefinition = {
		description: 'desc',
		type: VALUE_TYPE.BOOLEAN
	};

	it('should create an instance', () => {
		const val = new BooleanValue('1', def);
		expect(val).toBeTruthy();
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toBeTrue();
	});

	it('should print toString', () => {
		expect(new BooleanValue('0', def).toString()).toEqual('0');
		expect(new BooleanValue('1', def).toString()).toEqual('1');
	});

	it('should be valid all the time', () => {
		const val = new BooleanValue('blah', def);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toBeFalse();
	});

	it('should do nothing for auto fix value', () => {
		const val = new BooleanValue('1', def);
		val.autoFixValue();
	});
});
