// import { Injectable, OnDestroy } from '@angular/core';
// import { Observable, Subject } from 'rxjs';
// import { PortInfo } from 'serialport';
// import { environment } from 'src/environments/environment';

// const SERIAL_PORT = environment.environment === 'PROD' ? require('serialport') : require('debug')('serialport');

// // const ReadLine = SerialPort.parsers.Readline;

// /**
//  * RICE interface service
//  */
// @Injectable({
// 	providedIn: 'root'
// })
// export class RiceService implements OnDestroy {
// 	/**
// 	 * Emits when the board disconnects
// 	 */
// 	readonly disconnectSignal$: Observable<void>;

// 	private _serialPort: typeof SERIAL_PORT;
// 	private _disconnectSignal: Subject<void> = new Subject();

// 	constructor() {
// 		this.disconnectSignal$ = this._disconnectSignal.asObservable();
// 	}

// 	ngOnDestroy(): void {
// 		this._disconnectSignal.unsubscribe();
// 	}

// 	/**
// 	 * Attempts to find and connect to a lightsaber
// 	 *
// 	 * @returns  a promise of value boolean: true if a connection was made, false if not
// 	 */
// 	async connect(): Promise<boolean> {
// 		if (!this._serialPort) {
// 			const portInfoList: PortInfo[] = await SERIAL_PORT.list();
// 			portInfoList.forEach(async (portInfo: PortInfo) => {
// 				if (!this._serialPort) {
// 					let hasError = false;
// 					const port = new SERIAL_PORT(portInfo.path, (err: Error) => {
// 						if (err) {
// 							console.error(err.message);
// 							hasError = true;
// 						}
// 					});
// 					if (!hasError && this.runCommand('ping', port) === 'echo') {
// 						this._serialPort = port;
// 					} else {
// 						port.close();
// 					}
// 				}
// 			});
// 			if (this._serialPort) {
// 				console.log('Made a connection!', this._serialPort);
// 				this._serialPort.on('error', () => this.disconnect());
// 				return true;
// 			} else {
// 				console.warn('Could not find a connection!');
// 				return false;
// 			}
// 		}
// 	}

// 	/**
// 	 * Disconnects from the saber
// 	 */
// 	disconnect(): void {
// 		if (this._serialPort) {
// 			this._serialPort.close();
// 			this._serialPort = null;
// 		}
// 	}

// 	/**
// 	 * pings the board which should answer "echo"
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	ping(): string {
// 		return this.runCommand('ping');
// 	}

// 	/**
// 	 * forces the board to reset
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	reset(): string {
// 		return this.runCommand('reset');
// 	}

// 	/**
// 	 * resets the board in bootloader mode
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	bootloader(): string {
// 		return this.runCommand('bootloader');
// 	}

// 	/**
// 	 * reports the available free RAM from the board in real time
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	freeram(): string {
// 		return this.runCommand('freeram');
// 	}

// 	/**
// 	 * turns debug on or off
// 	 *
// 	 * @param mode false: off, true: on
// 	 * @returns stdout from command
// 	 */
// 	debug(mode: boolean): string {
// 		return this.runCommand(`debug=${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * display the version
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	version(): string {
// 		return this.runCommand('version');
// 	}

// 	/**
// 	 * forces deep sleep to "kill" the board
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	die(): string {
// 		return this.runCommand('die');
// 	}

// 	/**
// 	 * forces the board to leave sleep (if not in deepsleep)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wtfu(): string {
// 		return this.runCommand('wtfu');
// 	}

// 	/**
// 	 * ask to benchmark the SD over <N sectors>. Usually
// 	 * executed on 10 sectors which is representative of the
// 	 * reading speed of the board with the saber app
// 	 *
// 	 * @param sectors number of sectors
// 	 * @returns stdout from command
// 	 */
// 	testsd(sectors: number): string {
// 		return this.runCommand(`testsd=${sectors}`);
// 	}

// 	/**
// 	 * forces the blade to turn off
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	black(): string {
// 		return this.runCommand('black');
// 	}

// 	/**
// 	 * attempts to read the accelerometer and display the values
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	readsensor(): string {
// 		return this.runCommand('readsensor');
// 	}

// 	/**
// 	 * attempts to init the sensor and report if sensors "found"
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	initsensor(): string {
// 		return this.runCommand('initsensor');
// 	}

// 	/**
// 	 * reports battery voltage as analog input number (12 bits) and
// 	 * scaled voltage (float)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	battery(): string {
// 		return this.runCommand('battery');
// 	}

// 	/**
// 	 * reports a specific battery string with the float battery
// 	 * voltage to Omnisabers wifi dongle
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	getbattery(): string {
// 		return this.runCommand('getbattery');
// 	}

// 	/**
// 	 * reports the current state of the magnetic field and magic score
// 	 * (magic/force lock-unlock-activation with magnet)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	getfield(): string {
// 		return this.runCommand('getfield');
// 	}

// 	/**
// 	 * enables / disables real time logging of the magnetic field and
// 	 * magic score for an easier configuration of the magic activation
// 	 *
// 	 * @param mode false: off, true: on
// 	 * @returns stdout from command
// 	 */
// 	logmagic(mode: boolean): string {
// 		return this.runCommand(`logmagic=${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * enables / disables real time logging of the motion sensor
// 	 * for debug or real-time visualization in RICE
// 	 * magic score for an easier configuration of the magic activation
// 	 *
// 	 * @param mode false: off, true: on
// 	 * @returns stdout from command
// 	 */
// 	logmotion(mode: boolean): string {
// 		return this.runCommand(`logmotion=${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * triggers the magnetic sensor calibration to automatically find
// 	 * (and store) the magnetic reference field
// 	 * (to be accessible via the menu)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	magcalib(): string {
// 		return this.runCommand('magcalib');
// 	}

// 	/**
// 	 * reports the board temperature in °C
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	temperature(): string {
// 		return this.runCommand('temperature');
// 	}

// 	/**
// 	 * starts the mass storage access to the SD card over USB
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	massStorage(): string {
// 		return this.runCommand('mass_storage');
// 	}

// 	/**
// 	 * lists all fonts installed on the SD card
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	dirfonts(): string {
// 		return this.runCommand('dirfonts');
// 	}

// 	/**
// 	 * returns the number of fonts installed on the SD card
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	getfonts(): string {
// 		return this.runCommand('getfonts');
// 	}

// 	/**
// 	 * dumps details about the font <N> (clash, swing, blasters etc)
// 	 *
// 	 * @param fontIndex index of the font
// 	 * @returns stdout from command
// 	 */
// 	scanfont(fontIndex: number): string {
// 		return this.runCommand(`scanfont=${fontIndex}`);
// 	}

// 	/**
// 	 * stores the boot (added) delay
// 	 *
// 	 * @param timeDelayMs delay in milliseconds
// 	 * @returns stdout from command
// 	 */
// 	slowboot(timeDelayMs: number): string {
// 		return this.runCommand(`slowboot=${timeDelayMs}`);
// 	}

// 	/**
// 	 * controls the auxiliary 3.3V
// 	 *
// 	 * @param mode on/off
// 	 * @returns stdout from command
// 	 */
// 	aux33(mode: boolean): string {
// 		return this.runCommand(`aux33=${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * controls the +5V regulator
// 	 *
// 	 * @param mode on/off
// 	 * @returns stdout from command
// 	 */
// 	booster(mode: boolean): string {
// 		return this.runCommand(`booster=${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * disable pullups on both act & aux switches (external pullups required)
// 	 *
// 	 * @param mode on/off
// 	 * @returns stdout from command
// 	 */
// 	pudis(mode: boolean): string {
// 		return this.runCommand(`mode=${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * plays the SW story in ASCII art over the USB and/or TTL port
// 	 * (depends on logport parameter)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playstarwars(): string {
// 		return this.runCommand('playstarwars');
// 	}

// 	/**
// 	 * plays the SW story in ASCII art on the OLED display, if
// 	 * detected by the board at boot time. Due to the very small
// 	 * resolution of the display, rendering is adapted and compressed
// 	 * to fit 128x32 pixels
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	displaystarwars(): string {
// 		return this.runCommand('displaystarwars');
// 	}

// 	/**
// 	 * launches the SNAKE game, returns after game over
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playsnake(): string {
// 		return this.runCommand('playsnake');
// 	}

// 	/**
// 	 * launches the RTFM game, game returns after game over
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playrtfm(): string {
// 		return this.runCommand('playrtfm');
// 	}

// 	/**
// 	 * launches the PONG game, game returns after scoring is > 9 or if
// 	 * aux. is pressed. Paddle controlled by motion sensor over the X axis
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playpong(): string {
// 		return this.runCommand('playpong');
// 	}

// 	/**
// 	 * launches the KILL THE BIT game, a tribute to ALTAIR's first
// 	 * game, uses the accent leds exclusively (no OLED). Be quick
// 	 * enough with AUX. to kill the bit that passes on accent 4.
// 	 * Miss and you add a bit. Reach 6 bits => game over
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	killthebit(): string {
// 		return this.runCommand('killthebit');
// 	}

// 	/**
// 	 * launches the MAAG (Memory ACT+AUX Game), a vocal / audio Simon-
// 	 * like game that uses the act and aux switches for the Ti-Da
// 	 * sequence to memorize
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playmaag(): string {
// 		return this.runCommand('playmaag');
// 	}

// 	/**
// 	 * lauches the tetris game (OLED, aux + act switches)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playtetris(): string {
// 		return this.runCommand('playtetris');
// 	}

// 	/**
// 	 * launches PIXEL INVADERS, a shrunk version of the classic arcade game
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playinvaders(): string {
// 		return this.runCommand('playinvaders');
// 	}

// 	/**
// 	 * manually triggers OLED the screen saver (1 picked randomly out of 6)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	playscreensaver(): string {
// 		return this.runCommand('playscreensaver');
// 	}

// 	/**
// 	 * changes the blade profile on the fly (all configuration
// 	 * that isn't global and that isn't color related)
// 	 * MAX 32 blade profiles hence {0 ; 31 }
// 	 *
// 	 * @param bladeProfileIndex index of the blade profile to load
// 	 * @returns stdout from command
// 	 */
// 	selectblade(bladeProfileIndex: number): string {
// 		return this.runCommand(`selectblade=${bladeProfileIndex}`);
// 	}

// 	/**
// 	 *  change the color profile. {0 ; max_profile - 1}
// 	 * MAX 32 color profiles hence {0 ; 31 }
// 	 *
// 	 * @param colorProfileIndex index of the color profile to load
// 	 * @returns stdout from command
// 	 */
// 	selectcolor(colorProfileIndex: number): string {
// 		return this.runCommand(`selectcolor=${colorProfileIndex}`);
// 	}

// 	/**
// 	 * change the font {0 ; max_font - 1 }
// 	 *
// 	 * @param fontIndex index of the sound font to load (number on folder - 1)
// 	 * @returns stdout from command
// 	 */
// 	selectfont(fontIndex: number): string {
// 		return this.runCommand(`selectfont=${fontIndex}`);
// 	}

// 	/**
// 	 * Sends current font #, general config, current blade
// 	 * profile, current color profile and current font
// 	 * configuration
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	cfgrequest(): string {
// 		return this.runCommand('cfgrequest');
// 	}

// 	/**
// 	 * Saves prefs.txt with the latest used settings (font,
// 	 * color+blade profiles for each font)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	saveprefs(): string {
// 		return this.runCommand('saveprefs');
// 	}

// 	/**
// 	 * Saves config.txt (which has all the general prefs + all
// 	 * the blade profiles)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	saveconfig(): string {
// 		return this.runCommand('saveconfig');
// 	}

// 	/**
// 	 * Save font_config.txt for the currently used font (font
// 	 * specific settings, located in the font dir)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	savefontconfig(): string {
// 		return this.runCommand('savefontconfig');
// 	}

// 	/**
// 	 * Saves colors.txt which has all the color profiles definition
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	savecolors(): string {
// 		return this.runCommand('savecolors');
// 	}

// 	/**
// 	 * power the saber ON (repeat ignition sequence even if blade is on
// 	 *
// 	 * @param ignitionSequence Blade ignition sequence only if blade was off
// 	 * @returns stdout from command
// 	 */
// 	wPon(ignitionSequence?: number): string {
// 		return this.runCommand(`w-pon${ignitionSequence ? '=' + ignitionSequence : ''}`);
// 	}

// 	/**
// 	 * power the saber OFF / retraction sequence
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wPoff(): string {
// 		return this.runCommand('w-poff');
// 	}

// 	/**
// 	 * triggers a clash sound (if saber ON)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wClash(): string {
// 		return this.runCommand('w-clash');
// 	}

// 	/**
// 	 * triggers a swing sound (if saber ON
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wSwing(): string {
// 		return this.runCommand('w-swing');
// 	}

// 	/**
// 	 * triggers a stab sound (if saber ON)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wStab(): string {
// 		return this.runCommand('w-stab');
// 	}

// 	/**
// 	 * triggers a spin sound (if saber ON)
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wSpin(): string {
// 		return this.runCommand('w-spin');
// 	}

// 	/**
// 	 * mutes/unmutes the board (doesn't affect the volume, just disables the amp)
// 	 *
// 	 * @param mode on/off
// 	 * @returns stdout from command
// 	 */
// 	wMute(mode: boolean): string {
// 		return this.runCommand(`w-mute${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * starts/stops lockup
// 	 *
// 	 * @param mode on/off
// 	 * @returns stdout from command
// 	 */
// 	wLock(mode: boolean): string {
// 		return this.runCommand(`w-lock${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * starts/stops drag
// 	 *
// 	 * @param mode on/off
// 	 * @returns stdout from command
// 	 */
// 	wDrag(mode: boolean): string {
// 		return this.runCommand(`w-drag${mode ? '1' : '0'}`);
// 	}

// 	/**
// 	 * triggers a blaster sound
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wBlast(): string {
// 		return this.runCommand('w-blast');
// 	}

// 	/**
// 	 * triggers a Force fx sound
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wForce1(): string {
// 		return this.runCommand('w-force1');
// 	}

// 	/**
// 	 * triggers a Force Clash (force2) sound
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wForce2(): string {
// 		return this.runCommand('w-force2');
// 	}

// 	/**
// 	 * starts playing a music track from local tracks IF any,
// 	 * or from /tracks IF in iSaber mode.
// 	 * Can be called with w-track=<track #> for a specific track or with
// 	 * no arguments w-track for shuffle.
// 	 * Now accepts a name as argument to play a specific track name.
// 	 * w-track=track12.wav or w-track=cantina.wav
// 	 * track fetching will automatically decide on local tracks or global
// 	 * tracks depending on the iSaber mode
// 	 *
// 	 * @param track (optional) track index or name
// 	 * @returns stdout from command
// 	 */
// 	wTrack(track?: number | string): string {
// 		return this.runCommand(`w-track${track ? '=' + track : ''}`);
// 	}

// 	/**
// 	 * same as w-track but forces to use local (font) tracks folder if present
// 	 *
// 	 * @param track (optional) track index or name
// 	 * @returns stdout from command
// 	 */
// 	wLtrack(track?: number | string): string {
// 		return this.runCommand(`w-ltrack${track ? '=' + track : ''}`);
// 	}

// 	/**
// 	 * same as w-track but forces to use the iSaber (/tracks) folder if present
// 	 *
// 	 * @param track (optional) track index or name
// 	 * @returns stdout from command
// 	 */
// 	wItrack(track?: number | string): string {
// 		return this.runCommand(`w-itrack${track ? '=' + track : ''}`);
// 	}

// 	/**
// 	 * stops playing the track
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wStoptrack(): string {
// 		return this.runCommand('w-stoptrack');
// 	}

// 	/**
// 	 * pauses / resumes playing the track
// 	 *
// 	 * @returns stdout from command
// 	 */
// 	wPausetrack(): string {
// 		return this.runCommand('w-pausetrack');
// 	}

// 	/**
// 	 * Wrapper for running a command to the saber
// 	 *
// 	 * @param command the command to write to the port
// 	 * @param port (optional) the port to use instead of the instance stored port
// 	 * @returns the stdout of the command
// 	 */
// 	private runCommand(command: string, port?: any): string {
// 		const usePort = port ? port : this._serialPort;
// 		if (usePort && usePort.write(`${command}\n`)) {
// 			usePort.drain();
// 			return usePort.read().toString();
// 		}
// 		return null;
// 	}
// }
