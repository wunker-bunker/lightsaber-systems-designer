import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EnumForPipe } from 'src/app/pipes/enum-for.pipe';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { EnumerationInputComponent } from './enumeration-input.component';

describe('EnumerationInputComponent', () => {
	let component: EnumerationInputComponent;
	let fixture: ComponentFixture<EnumerationInputComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [EnumerationInputComponent, EnumForPipe, OrderByPipe, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EnumerationInputComponent);
		component = fixture.componentInstance;
		component.input = testData.enumValues[0];
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
