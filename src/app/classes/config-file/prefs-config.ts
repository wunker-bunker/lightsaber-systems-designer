import { computed } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { GroupType } from 'src/app/enums/group-types';
import { ConfigGroup } from '../config-group';
import { IntegerValue } from '../config-value/integer-value';
import { ConfigVar } from '../config-var';
import { BaseFile } from './base-file';
import { ConfigFile } from './config-file';

/**
 * Class for prefs config files.
 */
export class PrefsConfig extends ConfigFile {
	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 * @returns True if the file has been modified since it was last saved, false otherwise.
	 */
	readonly dirty;

	/**
	 * Creates a new instance of the PrefsConfig class.
	 * @param variables The variables in the file.
	 * @param fonts The fonts in the file.
	 * @param filePath The path to the file.
	 */
	constructor(variables: ConfigVar[], fonts: ConfigGroup[], filePath: string) {
		super(ConfigFileType.PREF, variables, fonts, filePath);
		this.dirty = computed(() => this._dirtyVars());
	}

	static isPrefsConfig(config: BaseFile): config is PrefsConfig {
		return config instanceof PrefsConfig;
	}

	/**
	 * Gets the blade preference by font id.
	 * @param fontId The id of the font.
	 * @returns The integer value containing the blade preference.
	 */
	getBladePrefByFontId(fontId: number): IntegerValue | undefined {
		const pref = this.getPrefByFontId(fontId);
		const bladePref = pref?.getVariableByName(GroupType.profile);
		const bladeValue = bladePref?.configValue();

		if (bladeValue && IntegerValue.isIntegerValue(bladeValue)) {
			return bladeValue;
		}
	}

	/**
	 * Gets the color preference by font id.
	 * @param fontId The id of the font.
	 * @returns The integer value containing the color preference.
	 */
	getColorPrefByFontId(fontId: number): IntegerValue | undefined {
		const pref = this.getPrefByFontId(fontId);
		const colorPref = pref?.getVariableByName(GroupType.color);
		const colorValue = colorPref?.configValue();

		if (colorValue && IntegerValue.isIntegerValue(colorValue)) {
			return colorValue;
		}
	}

	/**
	 * Gets the preferences by font id.
	 * @param fontId The id of the font.
	 * @returns The preferences group.
	 * @remarks The preferences group id is one less than the font id.
	 */
	getPrefByFontId(fontId: number): ConfigGroup | undefined {
		return this.groups().find((preference: ConfigGroup) => preference.id() === fontId - 1);
	}
}
