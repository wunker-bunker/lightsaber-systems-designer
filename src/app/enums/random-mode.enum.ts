/**
 * Random mode
 */
export enum RandomMode {
	RANDOM = 0,
	SEQUENCE,
	RANDOM_X
}
