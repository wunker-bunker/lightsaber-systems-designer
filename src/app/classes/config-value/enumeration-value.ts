import { computed } from '@angular/core';
import { ValueType, ValueTypeEnum } from 'src/app/enums/value-type.enum';
import { EnumerationType } from 'src/app/models/enumeration.model';
import { BaseDefinition, EnumDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class EnumerationValue extends ConfigValue<EnumerationType, ValueTypeEnum, EnumDefinition> {
	readonly toString;

	readonly valid;

	constructor(value: string, def: EnumDefinition) {
		super(value, def, def.type);
		this.toString = computed(() => `${this.value()}`);
		this.valid = computed(() => this.definition.enum[this.value()] !== undefined);
	}

	static isEnumerationValue(
		value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>
	): value is EnumerationValue {
		return value instanceof EnumerationValue;
	}

	autoFixValue(): void {
		this.value.set(this.definition.enum[Object.values(this.definition.enum)[0]]);
	}

	protected parseValue(strVal: string): EnumerationType {
		return this.definition.enum[this.definition.enum[strVal]];
	}
}
