/**
 * We thought that it could be interesting to preserve both color profile
 * selection methods, if desired. As a result, we defined several modes
 * that determine which action triggers which color selection
 * method (spectrum_mode configuration  parameter).P+/-refers to next/previous
 * color profile while B+/-refers to blade profile +/-.
 *
 * Aux. + Act., Aux. + Twist
 */
export enum SpectrumMode {
	/** Spectrum Disabled, Spectrum Disabled */
	DISABLED = -1,
	/** Legacy BrowsingP+, Legacy BrowsingP+/- */
	LEGACY_PPL_LEGACY_PPLMN,
	/** Spectrum Dial, Spectrum Dial */
	SPECTRUM_DIAL,
	/** Legacy Browsing, Spectrum Dial */
	LEGACY_SPECTRUM,
	/** Spectrum Dial, Legacy BrowsingP+/- */
	SPECTRUM_LEGACY_PPLMN,
	/** Legacy Browsing P+, Kyber Dial + Profiles */
	LEGACY_PPL_KYBER,
	/** Kyber Dial + Profiles, Legacy Browsing P+ */
	KYBER_LEGACY_PPL,
	/** KyberDial + Profiles, KyberDial + Profiles */
	KYBER_KYBER,
	/** Blade Profile Browsing B+, Legacy Browsing P+/- */
	BLADE_BPL_LEGACY_PPLMN,
	/** Legacy Browsing P+, Blade Profile Browsing B+/- */
	LEGACY_PPL_BLADE_BPL,
	/** Spectrum Dial, Kyber Dial+ Profiles */
	SPECTRUM_KYBER,
	/** Kyber Dial + Profiles, Spectrum Dial */
	KYBER_SPECTRUM,
	/** Spectrum Dial Color, Spectrum Dial Blade */
	SPECTRUM_COLOR_SPECTRUM_BLADE,
	/** Spectrum Dial Blade, Spectrum Dial Color */
	SPECTRUM_BLADE_SPECTRUM_COLOR
}
