import { GroupType } from '../enums/group-types';
import {
	ValueType,
	ValueTypeBitfield,
	ValueTypeBoolean,
	ValueTypeColor,
	ValueTypeEnum,
	ValueTypeInteger,
	ValueTypeIntegerArray,
	ValueTypeProfile,
	ValueTypeString,
	ValueTypeUknown
} from '../enums/value-type.enum';
import { Enumeration } from './enumeration.model';

/**
 * Base definition for all variable definitions.
 * Used in the variable dictionary.
 */
export interface BaseDefinition {
	/**
	 * The description of the variable.
	 * This is the text that is displayed in the UI.
	 */
	readonly description: string;

	/**
	 * The type of the variable.
	 */
	readonly type: ValueType;
}

/**
 * Definition for an integer.
 * Used in the variable dictionary.
 */
export interface IntegerDefinition extends BaseDefinition {
	/**
	 * The maximum value of the integer.
	 */
	readonly maxValue: number;

	/**
	 * The minimum value of the integer
	 */
	readonly minValue: number;

	/**
	 * The type of the integer.
	 * This is always `int`
	 */
	readonly type: ValueTypeInteger;
}

/**
 * Definition for an integer array.
 * Used in the variable dictionary.
 */
export interface IntegerArrayDefinition extends BaseDefinition {
	/**
	 * The length of the array.
	 */
	readonly arrLength: number;

	/**
	 * Flag indicating if the array is expandable.
	 */
	readonly expandable: boolean;

	/**
	 * An array of maximum values for each element in the array.
	 * If expandable is false, The length of this array must be equal to the `arrLength`.
	 * If expandable is true, the first element in this array is used for all elements in the array.
	 */
	readonly maxValueArr: readonly number[];

	/**
	 * An array of minimum values for each element in the array.
	 * If expandable is false, The length of this array must be equal to the `arrLength`.
	 * If expandable is true, the first element in this array is used for all elements in the array.
	 */
	readonly minValueArr: readonly number[];

	/**
	 * The type of the integer array.
	 * This is always `int_arr`.
	 */
	readonly type: ValueTypeIntegerArray;
}

/**
 * Definition for a boolean.
 * Used in the variable dictionary.
 */
export interface BooleanDefinition extends BaseDefinition {
	/**
	 * The type of the boolean.
	 * This is always `bool`.
	 */
	readonly type: ValueTypeBoolean;
}

/**
 * Definition for a bitfield.
 * Used in the variable dictionary.
 */
export interface BitfieldDefinition extends BaseDefinition {
	/**
	 * The length of the bitfield.
	 */
	readonly bitLength: number;

	/**
	 * The type of the bitfield.
	 * This is always `bit`.
	 */
	readonly type: ValueTypeBitfield;
}

/**
 * Definition for an enumeration.
 * Used in the variable dictionary.
 */
export interface EnumDefinition extends BaseDefinition {
	/**
	 * The Enumeration for this definition.
	 */
	readonly enum: Enumeration;

	/**
	 * The type of the enumeration.
	 * This is always `enum`.
	 */
	readonly type: ValueTypeEnum;
}

/**
 * Definition for a color.
 * Used in the variable dictionary.
 */
export interface ColorDefinition extends BaseDefinition {
	/**
	 * The type of the color.
	 * This is always `color`.
	 */
	readonly type: ValueTypeColor;
}

/**
 * Definition for a string.
 * Used in the variable dictionary.
 */
export interface StringDefinition extends BaseDefinition {
	/**
	 * The maximum length of the string.
	 * If undefined or 0, there is no maximum length.
	 * Cannot be less than `minLength`.
	 */
	readonly maxLength?: number;

	/**
	 * The minimum length of the string.
	 * If undefined or 0, there is no minimum length.
	 * Cannot be greater than `maxLength`.
	 */
	readonly minLength?: number;

	/**
	 * The type of the string.
	 * This is always `string`.
	 */
	readonly type: ValueTypeString;
}

/**
 * Definition for a profile.
 * Used in the variable dictionary.
 */
export interface ProfileDefinition extends BaseDefinition {
	/**
	 * The type of the profile.
	 * This is always `profile`.'
	 */
	readonly type: ValueTypeProfile;

	/**
	 * The type of the Group.
	 * Used to determine which profile the variables are associated with.
	 */
	readonly profileType: GroupType;
}

/**
 * Definition for an unknown variable.
 * Used in the variable dictionary.
 *
 * This is used when the variable type is not recognized.
 */
export interface UnknownDefinition extends BaseDefinition {
	/**
	 * The type of the unknown variable.
	 * This is always `unknown`.
	 */
	readonly type: ValueTypeUknown;
}

/**
 * Type for all variable definitions.
 * Used in the variable dictionary.
 */
export type VariableDefinition =
	| IntegerDefinition
	| IntegerArrayDefinition
	| BooleanDefinition
	| BitfieldDefinition
	| EnumDefinition
	| StringDefinition
	| ColorDefinition
	| ProfileDefinition
	| UnknownDefinition;

/**
 * Variable dictionary interface.
 * Used in the variable dictionary.
 * Index is the variable name.
 * Value is the variable definition.
 *
 * Depending on the variable type, the definition will be one of the definitions of the `VariableDefinition` type.
 */
export interface VariableDictionary {
	[name: string]: VariableDefinition;
}
