   __                 _ _           _                                                                
  /__\ _____   _____ | | |_        | |__  _   _                                                      
 / \/// _ \ \ / / _ \| | __|       | '_ \| | | |                                                     
/ _  \  __/\ V / (_) | | |_        | |_) | |_| |                                                     
\/ \_/\___| \_/ \___/|_|\__|       |_.__/ \__, |                                                     
                                          |___/                                                      
   __               _     ___ _       _           __       _                   ___           _       
  / /  ___  _ __ __| |   / __\ | __ _| | _____   / _\ __ _| |__   ___ _ __    / __\__  _ __ | |_ ___ 
 / /  / _ \| '__/ _` |  /__\// |/ _` | |/ / _ \  \ \ / _` | '_ \ / _ \ '__|  / _\/ _ \| '_ \| __/ __|
/ /__| (_) | | | (_| | / \/  \ | (_| |   < (_) | _\ \ (_| | |_) |  __/ |    / / | (_) | | | | |_\__ \
\____/\___/|_|  \__,_| \_____/_|\__,_|_|\_\___/  \__/\__,_|_.__/ \___|_|    \/   \___/|_| |_|\__|___/
                                                                                                     

"Revolt"
by LordBlako - Created January 2014
http://www.youtube.com/LordBlakoSaberFonts

Revolt is a Jedi Font based on the Rebellion. It's characterized by fast, aggressive sounds and powerful Force Effects.

** UPGRADED to smoothswing by David Maltais - JediArmsDealer - 01/2021
** Restoring stock/former "non-smoothswing" behavior of the font is simply achieved by removing the lswing/hswing
   pairs in the font directory. Some other contents like (accent) swings and spins have been altered or removed from
   the font and stored in the /extra folder of the font. Updated fonts also have their combo files renamed as force
   sounds. Feel free to mix-match-remap or duplicate those to also have force-clash sounds.

Force  = Force Push
Force2 = Force Slam
Combo1 = Charged Force Repulse
Combo2 = Charged Lightning Push
Combo3 = Charged Force Push
Combo4 = Charged Force Push

More of my fonts are available at www.saberfont.com