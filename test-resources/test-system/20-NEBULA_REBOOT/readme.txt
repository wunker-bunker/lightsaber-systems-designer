  _   _ ______ ____  _    _ _                                                                       
 | \ | |  ____|  _ \| |  | | |        /\                                                            
 |  \| | |__  | |_) | |  | | |       /  \                                                           
 | . ` |  __| |  _ <| |  | | |      / /\ \                                                          
 | |\  | |____| |_) | |__| | |____ / ____ \                                                         
 |_| \_|______|____/ \____/|______/_/  _ \_\            _     _             _      _____ _   _ _  __
     /\                               | |/ _|          | |   | |           | |    |_   _| \ | | |/ /
    /  \     ___  ___  _   _ _ __   __| | |_ ___  _ __ | |_  | |__  _   _  | |      | | |  \| | ' / 
   / /\ \   / __|/ _ \| | | | '_ \ / _` |  _/ _ \| '_ \| __| | '_ \| | | | | |      | | | . ` |  <  
  / ____ \  \__ \ (_) | |_| | | | | (_| | || (_) | | | | |_  | |_) | |_| | | |____ _| |_| |\  | . \ 
 /_/    \_\ |___/\___/ \__,_|_| |_|\__,_|_| \___/|_| |_|\__| |_.__/ \__, | |______|_____|_| \_|_|\_\
                                                                     __/ |                          
                                                                    |___/                          
=============================

Soundfonts By Link - "NEBULA"
Created by Brandon
aka Link of FX-Sabers Forums
& IRA Forums

=============================

   
This sith font was created specifically for the Crystal Focus v7 Default Sound Package and cannot be found anywhere else ! 
[ADDON] : the font got remastered as polyphonic + smoothswing font for CF-X

It has all new sounds such as Combos, SpinFX, StabFX & Pre Power on Sounds

An all new Hum sound, poweron's, poweroff's, boots, Force, Lockup and much more!!! 

Also included is a custom iSaber Track to get you in the fighting spirit!

I truely hope you enjoy this exclusive font! 

I am glad to be a part of the Saber community and glad to further the hobby ! 

MTFBWY !               Link