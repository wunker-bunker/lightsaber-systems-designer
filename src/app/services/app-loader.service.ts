import { Injectable, NgZone, signal } from '@angular/core';

/**
 * Service to show a loader.
 * The loader is shown in the app-loader component.
 */
@Injectable({
	providedIn: 'root'
})
export class AppLoaderService {
	/**
	 * The message to show.
	 * This is an observable.
	 */
	message = signal('');

	/**
	 * The state of the loader.
	 * This is an observable.
	 */
	state = signal(false);

	/**
	 * The sub message to show under the main message.
	 * This is an observable.
	 */
	subMessage = signal('');

	/**
	 * Create a new app loader service.
	 * @param _ngZone The ng zone.
	 */
	constructor(private _ngZone: NgZone) {}

	/**
	 * Set the state of the loader.
	 * @param state The state to set.
	 * @param message The message to show.
	 */
	setState(state: boolean, message = ''): void {
		this._ngZone.run(() => {
			this.message.set(message);
			this.state.set(state);
			this.subMessage.set('');
		});
	}

	/**
	 * Set the sub message. Displays the sub message under the progress bar.
	 * @param subMessage The sub message to display under the progress bar.
	 */
	setSubMessage(subMessage: string): void {
		this._ngZone.run(() => {
			this.subMessage.set(subMessage);
			console.debug(subMessage);
		});
	}
}
