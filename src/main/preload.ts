import { ipcRenderer, IpcRendererEvent, OpenDialogOptions, OpenDialogReturnValue } from 'electron';
import log from 'electron-log';
import { ProgressInfo } from 'electron-updater';
import { lstatSync } from 'fs';
import {
	access,
	constants,
	copy,
	existsSync,
	mkdirSync,
	move,
	moveSync,
	opendirSync,
	PathLike,
	readdir,
	readdirSync,
	readFileSync,
	rm,
	watch,
	writeFile,
	writeFileSync
} from 'fs-extra';
import { homedir } from 'os';
import { join } from 'path';
import { LsdIpcChannels } from '../common/lsd-ipc-channels.enum';
import { ElectronApi, ElectronLogApi, FsApi, OsApi, PathApi, SettingsApi } from '../types/electron-window';

export const electronApi: ElectronApi = {
	setUpdateChannel: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.SET_UPDATE_CHANNEL),
	openUpdateDebugger: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.DEBUG_OPEN_UPDATE_TESTER),
	openDirecotry: (path: PathLike): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.OPEN_PATH, path),
	openExternal: (url: string): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.OPEN_EXTERNAL, url),
	onAppMaximized: (callback: (maximized: boolean) => void): void => {
		ipcRenderer.on(LsdIpcChannels.APP_IS_MAXIMIZED, (event: IpcRendererEvent, isMaximized) =>
			callback(isMaximized)
		);
	},
	getPlatform: (): NodeJS.Platform => process.platform,
	closeApp: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.APP_CLOSE),
	minimizeApp: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.APP_MINIMIZE),
	maximizeApp: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.APP_MAXIMIZE),
	restoreApp: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.APP_RESTORE),
	isServed: (): Promise<boolean> => ipcRenderer.invoke(LsdIpcChannels.IS_SERVED),
	startUpdateDownload: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.START_DOWNLOAD),
	restartAndInstall: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.RESTART_INSTALL),
	debugTriggerDownloadProgress: (percent: number): Promise<void> =>
		ipcRenderer.invoke(LsdIpcChannels.DEBUG_DOWNLOAD_PROGRESS, { percent }),
	debugTriggerUpdateAvailable: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.DEBUG_UPDATE_AVAIL),
	debugTriggerUpdateDownloaded: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.DEBUG_UPDATE_DOWNL),
	openDialog: (openOptions: OpenDialogOptions): Promise<OpenDialogReturnValue> =>
		ipcRenderer.invoke(LsdIpcChannels.OPEN_DIALOG, openOptions),
	onDownloadProgress: (callback: (progress: ProgressInfo) => void): void => {
		ipcRenderer.on(LsdIpcChannels.DOWNLOAD_PROGRESS, (event: IpcRendererEvent, progressInfo: ProgressInfo) =>
			callback(progressInfo)
		);
	},
	onUpdateAvailable: (callback: () => void): void => {
		ipcRenderer.on(LsdIpcChannels.UPDATE_AVAILABLE, callback);
	},
	onUpdateDownloaded: (callback: () => void): void => {
		ipcRenderer.on(LsdIpcChannels.UPDATE_DOWNLOADED, callback);
	},
	readyToCheckForUpdates: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.READY_TO_CHECK_UPDATES),
	changeTheme: (): Promise<void> => ipcRenderer.invoke(LsdIpcChannels.CHANGE_THEME)
};

export const settingsApi: SettingsApi = {
	hasSync: key => ipcRenderer.sendSync(LsdIpcChannels.SETTINGS_HAS, key),
	getSync: key => ipcRenderer.sendSync(LsdIpcChannels.SETTINGS_GET, key),
	setSync: (key, value) => ipcRenderer.sendSync(LsdIpcChannels.SETTINGS_SET, key, value)
};

export const electronLogApi: ElectronLogApi = {
	functions: {
		debug: (...params: unknown[]): void => log.debug(...params),
		error: (...params: unknown[]): void => log.error(...params),
		info: (...params: unknown[]): void => log.info(...params),
		verbose: (...params: unknown[]): void => log.verbose(...params),
		log: (...params: unknown[]): void => log.log(...params),
		silly: (...params: unknown[]): void => log.silly(...params),
		warn: (...params: unknown[]): void => log.warn(...params)
	}
};

export const fsApi: FsApi = {
	access,
	constants,
	copy,
	existsSync,
	lstatSync,
	mkdirSync,
	move,
	moveSync,
	opendirSync,
	readdir,
	readdirSync,
	readFileSync,
	rm,
	watch,
	writeFile,
	writeFileSync
};

export const osApi: OsApi = {
	homedir
};

export const pathApi: PathApi = {
	join
};

window.settings = settingsApi;
window.electronApi = electronApi;
window.fs = fsApi;
window.os = osApi;
window.path = pathApi;
window.electronLog = electronLogApi;
