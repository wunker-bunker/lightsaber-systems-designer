import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { debounce, DebouncedFunc, DebounceSettings } from 'lodash';
import { ColorEvent, RGBA } from 'ngx-color';
import { ColorValue, COLOR_VAL_MAX } from 'src/app/classes/config-value/color-value';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-color-input',
	templateUrl: './color-input.component.html',
	styleUrls: ['./color-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColorInputComponent implements BaseInputComponent {
	@Input() input!: ColorValue;
	@Input() name = '';
	@Input() readonly = false;

	readonly changeRgbComplete: DebouncedFunc<($event: ColorEvent) => void>;
	readonly changeWhiteComplete: DebouncedFunc<($event: ColorEvent) => void>;

	private readonly DEBOUNCE_OPTS: DebounceSettings = {
		leading: false,
		trailing: true
	};
	private readonly DEBOUNCE_TIME = 50;

	constructor() {
		this.changeRgbComplete = debounce(
			($event: ColorEvent) => this.changeRgb($event.color.rgb),
			this.DEBOUNCE_TIME,
			this.DEBOUNCE_OPTS
		);
		this.changeWhiteComplete = debounce(
			($event: ColorEvent) => this.changeWhite($event.color.rgb),
			this.DEBOUNCE_TIME,
			this.DEBOUNCE_OPTS
		);
	}

	private changeRgb({ r, g, b }: RGBA): void {
		this.input.value.set({
			...this.input.value(),
			red: this.rgbToColorVal(r),
			green: this.rgbToColorVal(g),
			blue: this.rgbToColorVal(b)
		});
	}

	private changeWhite({ a }: RGBA): void {
		this.input.value.set({
			...this.input.value(),
			white: Math.round(a * COLOR_VAL_MAX)
		});
	}

	private rgbToColorVal(val: number): number {
		return Math.round((val * COLOR_VAL_MAX) / 255);
	}
}
