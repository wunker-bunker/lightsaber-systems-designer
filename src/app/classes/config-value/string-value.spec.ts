import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { StringDefinition } from 'src/app/models/variable-dictionary.model';
import { StringValue } from './string-value';

describe('StringValue', () => {
	const def: StringDefinition = {
		description: 'description',
		type: VALUE_TYPE.STRING,
		maxLength: 10,
		minLength: 2
	};
	const defNoLength: StringDefinition = {
		description: 'desctription',
		type: VALUE_TYPE.STRING
	};
	const defMaxLength: StringDefinition = {
		description: 'description',
		type: VALUE_TYPE.STRING,
		maxLength: 10
	};
	const defMinLength: StringDefinition = {
		description: 'description',
		type: VALUE_TYPE.STRING,
		minLength: 2
	};

	it('should create an instance', () => {
		const value = new StringValue('test', def);
		expect(value).toBeTruthy();
		expect(value.definition).toEqual(def);
		expect(value.type).toEqual(def.type);
		expect(value.valid()).toBeTrue();
		expect(value.value()).toEqual('test');
	});

	it('should print toString', () => {
		expect(new StringValue('test', def).toString()).toEqual('test');
	});

	describe('should validate', () => {
		describe('when lengths are set', () => {
			const value = new StringValue('', def);
			it('valid when length between min and max', () => {
				value.value.set('012345');
				expect(value.valid()).toBeTrue();
				value.value.set('01');
				expect(value.valid()).toBeTrue();
				value.value.set('0123456789');
				expect(value.valid()).toBeTrue();
			});

			it('invalid when length outside min/max', () => {
				value.value.set('0123456789a');
				expect(value.valid()).toBeFalse();
				value.value.set('1');
				expect(value.valid()).toBeFalse();
			});
		});

		describe('when lengths are not set', () => {
			const value = new StringValue('', defNoLength);
			it('valid when string is truthy', () => {
				value.value.set('test');
				expect(value.valid()).toBeTrue();
			});

			it('invalid when string is falsy', () => {
				value.value.set('');
				expect(value.valid()).toBeFalse();
			});
		});

		describe('when max lengths are set', () => {
			const value = new StringValue('', defMaxLength);
			it('valid when length <= max', () => {
				value.value.set('012345');
				expect(value.valid()).toBeTrue();
				value.value.set('0123456789');
				expect(value.valid()).toBeTrue();
			});

			it('invalid when length > max', () => {
				value.value.set('0123456789a');
				expect(value.valid()).toBeFalse();
			});
		});

		describe('when min lengths are set', () => {
			const value = new StringValue('', defMinLength);
			it('valid when length >= min', () => {
				value.value.set('01');
				expect(value.valid()).toBeTrue();
				value.value.set('0123456789');
				expect(value.valid()).toBeTrue();
			});

			it('invalid when length < min', () => {
				value.value.set('0');
				expect(value.valid()).toBeFalse();
			});
		});
	});

	describe('should auto fix value', () => {
		it('with min/max length', () => {
			const value = new StringValue('', def);
			let newValue = '';

			if (def.minLength === undefined) {
				throw new Error('minLength is undefined');
			}

			for (let i = 0; i <= def.minLength; i++) {
				newValue += 'x';
			}
			expect(value.valid()).toBeFalse();
			expect(value.value()).toEqual('');
			value.autoFixValue();
			expect(value.valid()).toBeTrue();
			expect(value.value()).toEqual(newValue);
		});

		it('when no min/max length', () => {
			const value = new StringValue('', {
				description: 'No length',
				type: VALUE_TYPE.STRING
			});
			expect(value.valid()).toBeFalse();
			expect(value.value()).toEqual('');
			value.autoFixValue();
			expect(value.valid()).toBeTrue();
			expect(value.value()).toEqual('Default String Value');
		});
	});
});
