import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SoundFile } from 'src/app/classes/sound-file';
import { ModalService } from 'src/app/services/modal.service';

@Component({
	selector: 'lsd-sound-row',
	templateUrl: './sound-row.component.html',
	styleUrls: ['./sound-row.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SoundRowComponent {
	@Output() changeSoundName: EventEmitter<SoundFile> = new EventEmitter();
	@Output() deleteSound: EventEmitter<SoundFile> = new EventEmitter();
	@Input() sound!: SoundFile;

	constructor(private _modalServ: ModalService) {}

	async deleteSoundFile(): Promise<void> {
		try {
			await this._modalServ.deleteConfirm(this.sound?.soundName);
		} catch (err) {
			console.log(`Canceled delete for ${this.sound?.soundName}`);
			return;
		}

		console.log(`Deleting sound ${this.sound?.soundName}`);
		this.deleteSound.emit(this.sound);
	}
}
