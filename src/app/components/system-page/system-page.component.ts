import { ChangeDetectionStrategy, Component, computed, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { FontConfig } from 'src/app/classes/config-file/font-config';
import { ConfigGroup } from 'src/app/classes/config-group';
import { LightsaberSystem } from 'src/app/classes/lightsaber-system';
import { SoundFile } from 'src/app/classes/sound-file';
import { GENERIC_BLADE_PROFILE, GENERIC_COLOR_PROFILE } from 'src/app/constants/objects';
import { GroupType } from 'src/app/enums/group-types';
import { AppAlertsService } from 'src/app/services/app-alerts.service';
import { AppLoaderService } from 'src/app/services/app-loader.service';
import { FileManagementService } from 'src/app/services/file-management.service';
import { ModalService } from 'src/app/services/modal.service';
import { ConfigBodyOpts, ConfigBodyType, ConfigGroupListBody } from '../config-list/config-list.component';

@Component({
	selector: 'lsd-system-page',
	templateUrl: './system-page.component.html',
	styleUrls: ['./system-page.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemPageComponent {
	allGroups = computed(() => [
		...(this.system()?.mainConfig().groups() ?? []),
		...(this.system()?.colorConfig().groups() ?? [])
	]);

	fontBodyOpts = computed<ConfigBodyOpts[]>(() => [
		{
			tabTitle: 'Font',
			type: ConfigBodyType.VAR_LIST,
			configVars: computed(() => this.fontConfig()?.variables() ?? []),
			dirty: computed(
				() =>
					this.fontConfig()
						?.variables()
						.some(confVar => confVar.configValue().dirty()) ?? false
			),
			valid: computed(
				() =>
					this.fontConfig()
						?.variables()
						.every(confVar => confVar.configValue().valid()) ?? false
			)
		},
		{
			tabTitle: 'LED',
			type: ConfigBodyType.TEXT_FILE,
			textFile: this.ledConfig,
			dirty: computed(() => this.ledConfig()?.dirty() ?? false),
			valid: computed(() => this.ledConfig()?.valid() ?? false)
		},
		{
			tabTitle: 'Sounds',
			type: ConfigBodyType.SOUND_LIST,
			soundFiles: computed(() => this.system()?.getSoundsById(this.selectedFontId()) ?? []),
			areTracks: false,
			dirty: computed(() => false),
			valid: computed(() => true)
		},
		{
			tabTitle: 'Tracks',
			type: ConfigBodyType.SOUND_LIST,
			soundFiles: computed(() => this.system()?.getTracksById(this.selectedFontId()) ?? []),
			areTracks: true,
			dirty: computed(() => false),
			valid: computed(() => true)
		}
	]);

	fontConfig = computed(() => this.system()?.getFontConfigById(this.selectedFontId()));

	groupBodyOpts = computed<ConfigGroupListBody[]>(() => [
		{
			tabTitle: 'Blade Profiles',
			type: ConfigBodyType.GROUP_LIST,
			groupType: GroupType.profile,
			groups: computed(() => this.system()?.mainConfig().groups() ?? []),
			dirty: computed(
				() =>
					this.system()
						?.mainConfig()
						.groups()
						.some(group => group.dirty()) ?? false
			),
			valid: computed(
				() =>
					this.system()
						?.mainConfig()
						.groups()
						.every(group => group.valid()) ?? false
			)
		},
		{
			tabTitle: 'Color Profiles',
			type: ConfigBodyType.GROUP_LIST,
			groupType: GroupType.color,
			groups: computed(() => this.system()?.colorConfig().groups() ?? []),
			dirty: computed(
				() =>
					this.system()
						?.colorConfig()
						.groups()
						.some(group => group.dirty()) ?? false
			),
			valid: computed(
				() =>
					this.system()
						?.colorConfig()
						.groups()
						.every(group => group.valid()) ?? false
			)
		}
	]);

	ledConfig = computed(() => this.system()?.getLedConfigById(this.selectedFontId()));

	mainBodyOpts = computed<ConfigBodyOpts[]>(() => [
		{
			tabTitle: 'Main',
			type: ConfigBodyType.VAR_LIST,
			configVars: computed(() => this.system()?.mainConfig().variables() ?? []),
			dirty: computed(
				() =>
					this.system()
						?.mainConfig()
						.variables()
						.some(confVar => confVar.configValue().dirty()) ?? false
			),
			valid: computed(
				() =>
					this.system()
						?.mainConfig()
						.variables()
						.every(confVar => confVar.configValue().valid()) ?? false
			)
		},
		{
			tabTitle: 'Special',
			type: ConfigBodyType.TEXT_FILE,
			textFile: computed(() => this.system()?.specialConfig()),
			dirty: computed(() => this.system()?.specialConfig()?.dirty() ?? false),
			valid: computed(() => this.system()?.specialConfig()?.valid() ?? false)
		}
	]);

	selectedBladeConfigVar;
	selectedColorConfigVar;
	selectedFontId;
	system;

	constructor(
		private _route: ActivatedRoute,
		private _fileServ: FileManagementService,
		private _loaderServ: AppLoaderService,
		private _modalServ: ModalService,
		private _alertServ: AppAlertsService
	) {
		this.system = toSignal<LightsaberSystem>(
			this._route.data.pipe(map(({ system }) => system as LightsaberSystem))
		);
		this.selectedFontId = signal(this.system()?.fontConfigs()[0].id() ?? 1);
		this.selectedBladeConfigVar = signal(this.system()?.prefConfig().getBladePrefByFontId(this.selectedFontId()));
		this.selectedColorConfigVar = signal(this.system()?.prefConfig().getColorPrefByFontId(this.selectedFontId()));
	}

	async addFont(): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		let directoryPath: string;
		try {
			directoryPath = await this._modalServ.openDirectory('Select the directory of your font to import');
		} catch (err) {
			console.log(err);
			return;
		}

		let name: string;
		try {
			name = await this._modalServ.editFontName(directoryPath.split(/[\\/]/).pop());
		} catch (err) {
			console.log(`Stopped trying to import ${directoryPath}`);
			return;
		}

		console.log('Importing font', directoryPath);
		this._loaderServ.setState(true, `Importing font from ${directoryPath}`);

		try {
			const newId = await this._fileServ.importFontFromDir(directoryPath, name, system);
			this.selectedFontId.set(newId);
			this._alertServ.showSuccessAlert(`Finished importing font from ${directoryPath}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to import font from ${directoryPath}: ${err}`, err);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	async addProfile(body: ConfigGroupListBody): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		switch (body.groupType) {
			case GroupType.profile:
				system.addBladeProfile(GENERIC_BLADE_PROFILE);
				break;
			case GroupType.color:
				system.addColorProfile(GENERIC_COLOR_PROFILE);
				break;
			default:
				return;
		}

		try {
			await this.save();
			this._alertServ.showSuccessAlert(`Created new ${body.groupType} profile`);
		} catch (err) {
			return;
		}
	}

	async addSound(isTrack: boolean): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		let directoryPath: string;
		try {
			directoryPath = await this._modalServ.openSoundFile(isTrack);
		} catch (err) {
			console.log(`Canceled adding ${isTrack ? 'track' : 'sound'}`);
			return;
		}

		let newName = '';
		if (!isTrack) {
			try {
				newName = await this._modalServ.chooseSoundName(system.getAvailableSoundNames(this.selectedFontId()));
			} catch (err) {
				console.log('Closed choose sound modal');
				return;
			}
			console.log('New name is', newName);
		}

		try {
			const newSound = await this._fileServ.importSound(
				directoryPath,
				isTrack ? system.getAvailableTrackName(this.selectedFontId()) : newName,
				system,
				this.selectedFontId(),
				isTrack
			);
			this._alertServ.showSuccessAlert(`New ${isTrack ? 'track' : 'sound'} added: ${newSound.filePath}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to import sound: ${err}`, err);
		}
	}

	async changeSoundName(sound: SoundFile): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		let newName: string;
		try {
			newName = await this._modalServ.chooseSoundName(
				system.getAvailableSoundNames(this.selectedFontId(), sound.defSoundName)
			);
		} catch (err) {
			console.log('Closed choose sound modal');
			return;
		}

		console.log('New name is', newName);

		try {
			const newSound = await this._fileServ.renameSound(newName, sound, system);
			this._alertServ.showSuccessAlert(`Sound ${sound.soundName} changed to ${newSound.soundName}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to rename sound: ${err}`, err);
		}
	}

	async deleteFont(font: FontConfig): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		this._loaderServ.setState(true, `Deleting font ${font.name()}`);
		try {
			await this._fileServ.deleteFont(font, system);
			this._alertServ.showSuccessAlert(`Deleted font ${font.name()}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to delete font ${font.name()}: ${err}`, err);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	async deleteGroup([group, body]: [ConfigGroup, ConfigGroupListBody]) {
		const system = this.system();
		if (!system) {
			return;
		}

		switch (body?.groupType) {
			case GroupType.profile:
				system.deleteBladeProfile(group);
				break;
			case GroupType.color:
				system.deleteColorProfile(group);
				break;
			default:
				return;
		}

		try {
			await this.save();

			this._alertServ.showSuccessAlert(`Deleted ${body.groupType} profile ${group.groupName()}`);
			console.log(JSON.stringify(group.toJSON()));
		} catch (err) {
			console.error(err);
		}
	}

	async deleteSound(sound: SoundFile): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		this._loaderServ.setState(true, `Deleting sound ${sound.soundName}`);
		try {
			await this._fileServ.deleteSound(sound, system);
			this._alertServ.showSuccessAlert(`Deleted sound file ${sound.soundName} for font ${sound.name()}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to delete sound ${sound.soundName}: ${err}`, err);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	async export(): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		let directoryPath: string;
		try {
			directoryPath = await this._modalServ.openDirectory('Select your SD Card to export to');
		} catch (err) {
			console.log(err);
			return;
		}

		console.log('Exporting full system', directoryPath);
		this._loaderServ.setState(true, `Exporting system to ${directoryPath}, this will take a while...`);

		try {
			await this._fileServ.exportSystem(directoryPath, system);
			this._alertServ.showSuccessAlert(`Finished exporting system to ${directoryPath}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to export full system to ${directoryPath}: ${err}`, err);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	async exportConfigs(): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		let directoryPath: string;
		try {
			directoryPath = await this._modalServ.openDirectory('Select your SD Card to export to');
		} catch (err) {
			console.log(err);
			return;
		}

		console.log('Exporting configs', directoryPath);
		this._loaderServ.setState(true, `Exporting configs to ${directoryPath}`);

		try {
			await this._fileServ.exportConfigs(directoryPath, system);
			this._alertServ.showSuccessAlert(`Finished exporting configs to ${directoryPath}`);
		} catch (err) {
			this._alertServ.showErrorAlert(
				`Failed to export configs for ${system.name} at ${directoryPath}: ${err}`,
				err
			);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	async importConfigs(): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		let directoryPath: string;
		try {
			directoryPath = await this._modalServ.openDirectory('Select your SD Card to import configs from');
		} catch (err) {
			console.log(err);
			return;
		}

		console.log('Importing configs', directoryPath);
		this._loaderServ.setState(true, `Importing configs from ${directoryPath}`);

		try {
			await this._fileServ.importConfigs(directoryPath, system);
			this._alertServ.showSuccessAlert(`Finished importing configs from ${directoryPath}`);
		} catch (err) {
			this._alertServ.showErrorAlert(
				`Failed to import configs for ${system.name} at ${directoryPath}: ${err}`,
				err
			);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	async openPreferences(): Promise<void> {
		try {
			await this._modalServ.preferences();
		} catch (err) {
			console.log('Closed prefs modal');
		}
	}

	openSystemDir(): void {
		const system = this.system();
		if (!system) {
			return;
		}

		window.electronApi.openDirecotry(system.path);
	}

	orderFonts(newOrder: readonly number[]): void {
		const system = this.system();
		const fontConfig = this.fontConfig();
		if (!system || !fontConfig) {
			return;
		}

		system.orderFonts(newOrder);
		this.selectedFontId.set(fontConfig.id());
	}

	orderGroup([newOrder, body]: [readonly number[], ConfigGroupListBody]): void {
		const system = this.system();
		if (!system) {
			return;
		}

		switch (body?.groupType) {
			case GroupType.profile:
				system.orderBladeProfiles(newOrder);
				break;
			case GroupType.color:
				system.orderColorProfiles(newOrder);
				break;
			default:
				break;
		}
	}

	async save(): Promise<void> {
		const system = this.system();
		if (!system) {
			return;
		}

		try {
			await this._fileServ.saveSystem(system);
			this._alertServ.showSuccessAlert(`Finished saving ${system.name}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to save ${system.name}`, err);
		}
	}
}
