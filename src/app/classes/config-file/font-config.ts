import { computed, signal, WritableSignal } from '@angular/core';
import { FONT_REGEX } from 'src/app/constants/regex';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ConfigVar } from '../config-var';
import { BaseFile } from './base-file';
import { ConfigFile } from './config-file';

/**
 * Class for font config files.
 */
export class FontConfig extends ConfigFile {
	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 * @returns True if the file has been modified since it was last saved, false otherwise.
	 */
	readonly dirty;

	/**
	 * Flag to indicate if the id of the font has changed.
	 * @returns True if the id has changed, false otherwise.
	 */
	readonly hasNewId;

	/**
	 * Flag to indicate if the name of the font has changed.
	 * @returns True if the name has changed, false otherwise.
	 */
	readonly hasNewName;

	/**
	 * The id of the font.
	 * This corresponds to the number of the directory the font is in.
	 */
	readonly id: WritableSignal<number>;

	/**
	 * The name of the font.
	 * This corresponds to the name of the directory the font is in.
	 */
	readonly name: WritableSignal<string>;

	/**
	 * Stores the previous id of the font.
	 * This is used to revert moving the font to a different order.
	 */
	private readonly _previousId: WritableSignal<number>;

	/**
	 * Stores the previous name of the font.
	 * This is used to revert renaming the font.
	 */
	private readonly _previousName: WritableSignal<string>;

	/**
	 * Creates a new instance of the FontConfig class.
	 * @param variables The variables in the file.
	 * @param filePath The path to the file.
	 */
	constructor(variables: ConfigVar[], filePath: string) {
		super(ConfigFileType.FONT, variables, [], filePath);
		const id = +filePath.replace(FONT_REGEX, '$1');
		const name = filePath.replace(FONT_REGEX, '$2');
		this.id = signal(id);
		this.name = signal(name);
		this._previousId = signal(id);
		this._previousName = signal(name);
		this.hasNewId = computed(() => this.id() !== this._previousId());
		this.hasNewName = computed(() => this.name() !== this._previousName());
		this.dirty = computed(() => this._dirtyVars() || this.hasNewId() || this.hasNewName());
	}

	/**
	 * Guard to check if the file is a font config file.
	 * @param file The file to check.
	 * @returns True if the file is a font config file, false otherwise.
	 */
	static isFontConfig(file: BaseFile): file is FontConfig {
		return file instanceof FontConfig;
	}

	/**
	 * Marks the font as saved.
	 * Sets the previous id and name to the current id and name.
	 * Calls the base class's markAsSaved method.
	 */
	markAsSaved(): void {
		this._previousId.set(this.id());
		this._previousName.set(this.name());
		super.markAsSaved();
	}

	/**
	 * Reverts the font to its previous state.
	 * Sets the id and name to the previous id and name.
	 * Calls the base class's revert method.
	 */
	revert(): void {
		this.id.set(this._previousId());
		this.name.set(this._previousName());
		super.revert();
	}
}
