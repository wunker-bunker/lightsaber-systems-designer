import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigVar } from 'src/app/classes/config-var';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-invalid-variables',
	templateUrl: './invalid-variables.component.html',
	styleUrls: ['./invalid-variables.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvalidVariablesComponent {
	variables: ConfigVar[] = [];

	constructor(public activeModal: NgbActiveModal, public settings: SettingsService) {}

	autoFixAll() {
		this.variables.forEach(variable => variable.configValue().autoFixValue());
	}
}
