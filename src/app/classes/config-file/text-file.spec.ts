import { computed } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { specialConfigFile } from 'src/environments/test-data.karma';
import { TextFile } from './text-file';

class TestTextFile extends TextFile {
	dirty = computed(() => this._dirtyText());
}

describe('TextFile', () => {
	let fileObj: TestTextFile;

	beforeEach(() => {
		fileObj = new TestTextFile(specialConfigFile.fileText, ConfigFileType.SPECIAL, specialConfigFile.filePath);
	});

	it('should create an instance', () => {
		expect(fileObj).toBeTruthy();
		expect(fileObj.type).toEqual(ConfigFileType.SPECIAL);
		expect(fileObj.text()).toEqual(specialConfigFile.fileText);
		expect(fileObj.filePath).toEqual(specialConfigFile.filePath);
		expect(fileObj.dirty()).toBeFalse();
	});

	it('should print to string', () => {
		expect(fileObj.toString()).toEqual(specialConfigFile.fileText);
	});

	it('should mark as saved', () => {
		expect(fileObj.dirty()).toBeFalse();
		fileObj.text.update(text => (text += '\ntest line'));
		expect(fileObj.dirty()).toBeTrue();
		fileObj.markAsSaved();
		expect(fileObj.dirty()).toBeFalse();
	});

	it('should revert', () => {
		const line = 'new line';
		expect(fileObj.text()).toEqual(specialConfigFile.fileText);
		expect(fileObj.text()).not.toContain(line);
		expect(fileObj.dirty()).toBeFalse();
		fileObj.text.update(text => (text += line));
		expect(fileObj.text()).not.toEqual(specialConfigFile.fileText);
		expect(fileObj.text()).toContain(line);
		expect(fileObj.dirty()).toBeTrue();
		fileObj.revert();
		expect(fileObj.text()).not.toContain(line);
		expect(fileObj.text()).toEqual(specialConfigFile.fileText);
		expect(fileObj.dirty()).toBeFalse();
	});

	it('should validate', () => {
		expect(fileObj.dirty()).toBeFalse();
		fileObj.text.set('test');
		expect(fileObj.valid()).toBeTrue();
		expect(fileObj.dirty()).toBeTrue();
	});
});
