import { computed, signal, WritableSignal } from '@angular/core';
import { sortBy } from 'lodash';
import { FILE_HEADER } from 'src/app/constants/strings';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ConfigGroup } from '../config-group';
import { ConfigVar } from '../config-var';
import { BaseFile } from './base-file';

/**
 * Base class for all config files.
 */
export abstract class ConfigFile extends BaseFile {
	/**
	 * Flag to indicate if the groups are valid.
	 */
	readonly groupValid;

	/**
	 * The list of groups in the file.
	 * @returns The list of groups in the file.
	 */
	readonly groups: WritableSignal<ConfigGroup[]>;

	/**
	 * Gets the string representation of the file.
	 * @returns The string representation of the file.
	 */
	readonly toString;

	/**
	 * Flag to indicate if the file is valid.
	 */
	readonly valid;

	/**
	 * Flag to indicate if the variables are valid.
	 */
	readonly varValid;

	/**
	 * The list of variables in the file.
	 * @returns The list of variables in the file.
	 */
	readonly variables: WritableSignal<ConfigVar[]>;

	protected readonly _dirtyVars;

	/**
	 * Creates an instance of ConfigFile.
	 * @param {ConfigFileType} type The type of the file.
	 * @param {ConfigVar[]} variables The variables in the file.
	 * @param {ConfigGroup[]} groups The groups in the file.
	 * @param {string} filePath The path to the file.
	 */
	constructor(type: ConfigFileType, variables: ConfigVar[], groups: ConfigGroup[], filePath: string) {
		super(type, filePath);
		this._dirtyVars = computed(
			() =>
				this.variables().some(value => value.configValue().dirty()) ||
				this.groups().some(value => value.dirty())
		);
		this.groups = signal(sortBy(groups, value => value.id()));
		this.variables = signal(variables);
		this.groupValid = computed(() => this.groups().every((group: ConfigGroup) => group.valid()));
		this.toString = computed(() => {
			let str = FILE_HEADER;
			this.variables().forEach((variable: ConfigVar) => (str += `${variable.toString()}\n`));

			str += '\n';

			this.groups().forEach((profile: ConfigGroup) => (str += `${profile.toString()}\n`));
			return str;
		});
		this.varValid = computed(() => this.variables().every((confVar: ConfigVar) => confVar.configValue().valid()));
		this.valid = computed(() => this.groupValid() && this.varValid());
	}

	/**
	 * Gets the variable with the specified name.
	 * @param name The name of the variable to get.
	 * @returns The variable with the specified name.
	 */
	getVariableByName(name: string): ConfigVar | undefined {
		return this.variables().find((variable: ConfigVar) => variable.name === name);
	}

	/**
	 * Iterates over all groups and variables to mark the file as saved.
	 * This will reset the dirty flag.
	 */
	markAsSaved(): void {
		this.variables().forEach((confVar: ConfigVar) => confVar.configValue().markAsSaved());
		this.groups().forEach((group: ConfigGroup) => group.markAsSaved());
	}

	/**
	 * Iterates over all groups and variables to revert the file to the saved state.
	 * This will reset the dirty flag.
	 */
	revert(): void {
		this.variables().forEach((confVar: ConfigVar) => confVar.configValue().revert());
		this.groups().forEach((group: ConfigGroup) => group.revert());
		this.groups.set(sortBy(this.groups(), value => value.id()));
	}
}
