import { computed, Injectable, NgZone, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { DomSanitizer } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { FSWatcher, Stats } from 'fs-extra';
import { sortBy } from 'lodash';
import { filter } from 'rxjs/operators';
import { BaseFile } from '../classes/config-file/base-file';
import { FontConfig } from '../classes/config-file/font-config';
import { LedConfig } from '../classes/config-file/led-config';
import { ConfigGroup } from '../classes/config-group';
import { IntegerValue } from '../classes/config-value/integer-value';
import { ConfigVar } from '../classes/config-var';
import { LightsaberSystem } from '../classes/lightsaber-system';
import { SoundFile } from '../classes/sound-file';
import { FONT_REGEX, SOUND_NAME, WAV_FILE } from '../constants/regex';
import { LED_DEFAULT_TEXT } from '../constants/strings';
import { PREFS_DICTIONARY } from '../dictionaries/variable.dict';
import { ConfigFileType } from '../enums/config-file-type.enum';
import { IntegerDefinition } from '../models/variable-dictionary.model';
import { AppLoaderService } from './app-loader.service';
import { ConfigParserService } from './config-parser.service';
import { LightsaberSystemsService } from './lightsaber-systems.service';
import { SettingsService } from './settings.service';

/**
 * Object containing all the files that need to be processed
 */
interface ProcessObj {
	configArr: BaseFile[];
	soundArr: SoundFile[];
}

/**
 * Service for managing the file system access and file management
 * for the application.
 * This service is responsible for loading and saving files.
 *
 * It also watches the system directory for changes.
 * When a change is detected, it will update the system list.
 *
 * This service is also responsible for loading and saving the
 * application settings.
 */
@Injectable({
	providedIn: 'root'
})
export class FileManagementService {
	/**
	 * The list of systems as a signal.
	 * This is used to update the system list in the app.
	 */
	readonly systemList = signal<readonly string[]>([]);

	/**
	 * The path to the colors directory relative to the library directory.
	 */
	private readonly COLORS = 'colors';

	/**
	 * The path to the fonts directory relative to the library directory.
	 */
	private readonly FONTS = 'fonts';

	/**
	 * The path to the profiles directory relative to the library directory.
	 */
	private readonly PROFILES = 'profiles';

	/**
	 * The path to the systems directory relative to the library directory.
	 */
	private readonly SYSTEMS = 'systems';

	/**
	 * The full path to the colors directory.
	 * This is the path to the directory where the color files are stored.
	 */
	private _colorsPath!: string;

	/**
	 * The full path to the current system directory.
	 * This is the path to the directory where the current system is loaded from.
	 */
	private _currentSystem;

	/**
	 * The full path to the fonts directory.
	 * This is the path to the directory where the font directories are stored.
	 */
	private _fontsPath!: string;

	/**
	 * The full path to the library directory.
	 * This contains all the files and directories for the application.
	 */
	private _libPath!: string;

	/**
	 * The full path to the profiles directory.
	 * This is the path to the directory where the profile files are stored.
	 */
	private _profilesPath!: string;

	/**
	 * The watcher for the systems directory.
	 *
	 * This is used to watch for changes to the systems directory
	 * like when a new system is added, deleted, or renamed.
	 */
	private _systemWatcher!: FSWatcher;

	/**
	 * The full path to the systems directory.
	 * This is the path to the directory where the system directories are stored.
	 */
	private _systemsPath!: string;

	/**
	 * Creates an instance of file management service.
	 * @param _configParser The config parser service.
	 * @param _systemService The system service.
	 * @param _router The router service.
	 * @param _ngZone The ngZone service.
	 * @param _sanitizer The sanitizer service.
	 * @param _loaderServ
	 * @param _settings The settings service.
	 */
	constructor(
		private _configParser: ConfigParserService,
		private _systemService: LightsaberSystemsService,
		private _router: Router,
		private _ngZone: NgZone,
		private _sanitizer: DomSanitizer,
		private _loaderServ: AppLoaderService,
		private _settings: SettingsService
	) {
		this.startup();
		this.systemList.set(this.getSystemList());

		const routerEvents = toSignal(this._router.events.pipe(filter(event => event instanceof NavigationEnd)));

		this._currentSystem = computed(() => {
			const event = routerEvents();

			if (!event || !(event instanceof NavigationEnd)) {
				return '';
			}

			const urlSegments: string[] = event.urlAfterRedirects.split('/');
			if (urlSegments.length < 2) {
				return '';
			}

			// Check if the url is a system route
			const systemName = decodeURI(urlSegments.pop() ?? '');
			const systemRoute = decodeURI(urlSegments.pop() ?? '');

			if (systemRoute !== 'systems') {
				return '';
			}

			return systemName;
		});
	}

	/**
	 * Deletes a font from the system.
	 * @param font The font to delete.
	 * @param system The system to delete the font from.
	 * @returns A promise that resolves when the font is deleted.
	 * @throws An error if the font or system is undefined.
	 */
	async deleteFont(font: FontConfig, system: LightsaberSystem): Promise<void> {
		// Revert the font
		font.revert();

		// Delete the font directory
		await window.fs.rm(window.path.join(system.path, `${font.id}-${font.name}`), {
			recursive: true,
			force: true
		});

		// Remove the font from the system and save the system
		this.readjustSystem(system, {
			fonts: system.fontConfigs().filter(({ id }) => id() !== font.id()),
			leds: system.ledConfigs().filter(({ id }) => id() !== font.id()),
			prefs: system
				.prefConfig()
				.groups()
				.filter(({ id }) => id() + 1 !== font.id()),
			sounds: system.sounds().filter(({ id }) => id() !== font.id())
		});

		return this.saveSystem(system);
	}

	/**
	 * Deletes a sound from the system.
	 * @param sound The sound to delete.
	 * @param sound.filePath
	 * @param system The system to delete the sound from.
	 * @returns A promise that resolves when the sound is deleted.
	 * @throws An error if the sound or system is undefined.
	 */
	async deleteSound({ filePath }: SoundFile, system: LightsaberSystem): Promise<void> {
		// Check if the sound and system exist
		if (!filePath || !window.fs.existsSync(filePath)) {
			throw new Error(`Sound doesn't exist`);
		}

		if (!SOUND_NAME.test(filePath)) {
			throw new Error(`${filePath} is not a sound file`);
		}

		// Delete the sound file
		await window.fs.rm(filePath, {
			force: true
		});

		// Remove the sound from the system and save the system
		this.readjustSystem(system, {
			sounds: system.sounds().filter(s => s.filePath !== filePath)
		});

		return this.saveSystem(system);
	}

	/**
	 * Export the configs for the system to a directory.
	 * @param workingDir The directory to export the configs to.
	 * @param system The system to export the configs from.
	 * @returns A promise that resolves when the configs are exported.
	 * @throws An error if the directory is not a CFX system or the the directory is empty.
	 */
	async exportConfigs(workingDir: string, system: LightsaberSystem): Promise<void> {
		// Check if the directory is a CFX system
		const isCfxDir = this.checkIfCfxDirectory(workingDir);

		// Throw an error if the directory is not a CFX system or the directory is empty
		if (isCfxDir === false || isCfxDir === 'empty') {
			throw new Error('Directory is not a CFX system!');
		}

		// Load the system from the directory
		const copyToSystem: LightsaberSystem = this.loadSystemFromDir(workingDir, system.name);

		// Copy the configs from the system to the new system and save the new system
		copyToSystem.copyConfigsFrom(system);
		this.saveSystem(copyToSystem);
	}

	/**
	 * Export the system to a directory.
	 * @param exportPath The directory to export the system to.
	 * @param system The system to export.
	 * @returns A promise that resolves when the system is exported.
	 * @throws An error if the directory is not a CFX system.
	 */
	async exportSystem(exportPath: string, system: LightsaberSystem): Promise<void> {
		const isCfxDir = this.checkIfCfxDirectory(exportPath);

		// Throw an error if the directory is not a CFX system
		if (isCfxDir === false) {
			throw new Error('Directory is not a CFX system!');
		}

		// Delete the contents of the directory if it is a CFX system
		if (isCfxDir === true) {
			// Iterate through the files in the directory and delete them
			for (const fileName of window.fs.readdirSync(exportPath)) {
				if (!fileName.startsWith('.')) {
					const fullPath = window.path.join(exportPath, fileName);
					try {
						this._loaderServ.setSubMessage(`Deleting ${fullPath}`);
						await window.fs.rm(fullPath, {
							recursive: true,
							force: true
						});
					} catch (e) {
						console.error(e);
					}
				}
			}
		}

		// Get a list of all files in the system and for each file that isn't a hidden file/directory, copy it to the working directory recursively
		for (const file of window.fs.readdirSync(system.path)) {
			if (!file.startsWith('.')) {
				const src = window.path.join(system.path, file);
				const dest = window.path.join(exportPath, file);

				this._loaderServ.setSubMessage(`Copying ${src} to ${dest}...`);
				await window.fs.copy(src, dest, {
					recursive: true
				});
			}
		}
	}

	/**
	 * Get the list of systems.
	 * @returns A promise that resolves with the list of systems.
	 */
	getSystemList(): string[] {
		try {
			// Get the list of systems by reading the directory
			const systemList: string[] = window.fs.readdirSync(this._systemsPath);

			// Filter out the non-CFX systems and sort the list
			return systemList
				?.filter(
					(system: string) => this.checkIfCfxDirectory(window.path.join(this._systemsPath, system)) === true
				)
				.sort();
		} catch (err) {
			console.error(err);
			return [];
		}
	}

	/**
	 * Import the configs for the system from an existing CFX system.
	 * @param workingDir The directory to import the configs from.
	 * @param system The system to import the configs to.
	 * @returns A promise that resolves when the configs are imported.
	 * @throws An error if the directory is not a CFX system or the the directory is empty.
	 */
	async importConfigs(workingDir: string, system: LightsaberSystem) {
		const isCfxDir = this.checkIfCfxDirectory(workingDir);

		// Throw an error if the directory is not a CFX system or the directory is empty
		if (isCfxDir === false || isCfxDir === 'empty') {
			throw new Error('Directory is not a CFX system!');
		}

		// Load the system from the directory, copy the configs from the new system to the system,
		// and save the system
		system.copyConfigsFrom(this.loadSystemFromDir(workingDir, system.name));
		this.saveSystem(system);
	}

	/**
	 * Import a font from a directory.
	 * @param workingDir The directory to import the font from.
	 * @param newName The new name for the font.
	 * @param system The system to import the font to.
	 * @returns A promise that resolves with the ID of the new font.
	 * @throws An error if the directory is not a font directory.
	 */
	async importFontFromDir(workingDir: string, newName: string, system: LightsaberSystem) {
		// Check if the directory is a font directory
		if (!window.fs.existsSync(window.path.join(workingDir, ConfigFileType.FONT))) {
			throw new Error(`${workingDir} is not a font directory`);
		}

		let newId = 0;

		// Find the next available ID
		system.fontConfigs().forEach(({ id }) => {
			if (id() >= newId) {
				newId = id() + 1;
			}
		});

		// Copy the font to the system directory
		const newDir = window.path.join(system.path, `${newId}-${newName}`);

		console.log(`New font dir will be ${newDir}`);

		await window.fs.copy(workingDir, newDir, { recursive: true });

		// Process the directory and add the font to the system
		const { configArr, soundArr } = this.processDir(newDir);
		let fontConfig: FontConfig | undefined;
		let ledConfig: LedConfig | undefined;

		let foundFile = configArr.find(file => file.type === ConfigFileType.FONT);
		if (foundFile && FontConfig.isFontConfig(foundFile)) {
			fontConfig = foundFile;
		}

		if (!fontConfig) {
			throw new Error('Font config not found');
		}

		foundFile = configArr.find(file => file.type === ConfigFileType.LEDS);
		if (foundFile && LedConfig.isLedConfig(foundFile)) {
			ledConfig = foundFile;
		}

		this.readjustSystem(system, {
			fonts: [...system.fontConfigs(), fontConfig],
			leds: ledConfig ? [...system.ledConfigs(), ledConfig] : undefined,
			sounds: [...system.sounds(), ...soundArr]
		});

		// Save the system
		await this.saveSystem(system);
		return newId;
	}

	/**
	 * Import a sound from a file.
	 * @param soundPath The path to the sound file.
	 * @param newName The new name for the sound.
	 * @param system The system to import the sound to.
	 * @param fontId The ID of the font to import the sound to.
	 * @param isTrack Whether the sound is a track.
	 * @returns A promise that resolves with the new sound.
	 * @throws An error if the file is not a wav file or the file does not exist.
	 */
	async importSound(soundPath: string, newName: string, system: LightsaberSystem, fontId: number, isTrack: boolean) {
		// Check if the file exists and is a wav file
		if (!WAV_FILE.test(soundPath)) {
			throw new Error(`${soundPath} is not a wav file`);
		}

		if (!window.fs.existsSync(soundPath)) {
			throw new Error(`${soundPath} does not exist`);
		}

		// Create the new sound file path
		const font = system.getFontConfigById(fontId);
		if (!font) {
			throw new Error(`Font ${fontId} does not exist`);
		}

		const fontDirName = `${font.id()}-${font.name()}`;
		const fullNewName = `${newName}.wav`;
		const newPath = window.path.join(system.path, ...(isTrack ? [fontDirName, 'tracks'] : [fontDirName]));

		const newFullPath = window.path.join(newPath, fullNewName);

		console.log(`New sound will be ${newFullPath}`);

		// Copy the sound to the system directory
		await window.fs.copy(soundPath, newFullPath);

		// Process the file and add the sound to the system
		const {
			soundArr: [newSound]
		} = this.processFile(newPath, fullNewName);

		this.readjustSystem(system, {
			sounds: [...system.sounds(), newSound]
		});

		// Save the system
		await this.saveSystem(system);
		return newSound;
	}

	/**
	 * Import a system from a directory.
	 * @param workingDir The directory to import the system from.
	 * @param systemName The name of the system.
	 * @returns A promise that resolves with the new system.
	 * @throws An error if the directory is not a CFX system, the directory is empty, or the system already exists.
	 */
	async importSystemFromDir(workingDir: string, systemName: string) {
		// Check if the directory is a CFX system
		const isCfxDir = this.checkIfCfxDirectory(workingDir);

		if (isCfxDir === false || isCfxDir === 'empty') {
			throw new Error('Directory is not a CFX system!');
		}

		// Check if the system already exists
		const systemPath: string = window.path.join(this._systemsPath, systemName);

		if (window.fs.existsSync(systemPath)) {
			throw new Error('System already exists!');
		}

		window.fs.mkdirSync(systemPath);

		for (const file of window.fs.readdirSync(workingDir)) {
			if (!file.startsWith('.')) {
				const src = window.path.join(workingDir, file);
				const dest = window.path.join(systemPath, file);

				// Copy the system to the systems directory
				this._loaderServ.setSubMessage(`Copying ${src} to ${dest}...`);
				await window.fs.copy(src, dest, { recursive: true });
			}
		}

		// Process the directory and add the system to the service
		const { configArr, soundArr } = this.processDir(systemPath);

		const newSystem = new LightsaberSystem(systemName, systemPath, configArr, soundArr);

		this.readjustSystem(newSystem);

		// Save any changes made due to operations above
		await this.saveSystem(newSystem);

		this._systemService.add(newSystem);
		this.systemList.set(this.getSystemList());
		return newSystem;
	}

	/**
	 * Load a system from a directory.
	 * @param systemName The name of the system.
	 * @returns A promise that resolves with the system.
	 * @throws An error if the system does not exist.
	 */
	async loadSystem(systemName: string) {
		// Check if the system exists in the service
		const systemPath: string = window.path.join(this._systemsPath, systemName);
		if (this._systemService.has(systemName)) {
			const system = this._systemService.get(systemName);
			if (!system) {
				throw new Error(`System ${systemName} doesn't exist`);
			}
			return system;
		}

		// Check if the system exists in the directory
		if (this.checkIfCfxDirectory(systemPath) === true) {
			const newSystem = this.loadSystemFromDir(systemPath, systemName);
			this._systemService.add(newSystem);
			return newSystem;
		}

		// If the system doesn't exist, throw an error
		throw new Error(`System ${systemName} doesn't exist`);
	}

	/**
	 * Move the library to a new directory.
	 * @param workingDir The new directory to move the library to.
	 * @returns A promise that resolves when the library has been moved.
	 * @throws An error if the directory does not exist, the path is not a directory, or the directory is not writable.
	 */
	async moveLibrary(workingDir: string) {
		// Check if the directory exists and is a directory
		if (!window.fs.existsSync(workingDir)) {
			throw new Error('Directory does not exist');
		}

		// Check if the path is a directory
		const lstat: Stats = window.fs.lstatSync(workingDir);
		if (!lstat.isDirectory()) {
			throw new Error('Path is not a directory');
		}

		// Create the new directory path
		const currentSystem = this._currentSystem();
		const newDir = window.path.join(workingDir, 'Lightsaber Systems Designer');
		console.log('New directory', newDir);

		try {
			// Close the watcher
			this._systemWatcher.close();

			// Check if the directory is writable, then move the library
			await window.fs.access(workingDir, window.fs.constants.W_OK);
			await window.fs.move(this._libPath, newDir);

			// Update the library path and reload the systems
			this._settings.libraryLocation.set(newDir);
			this._systemService.clear();
		} catch (err) {
			throw new Error(`Error moving library: ${err}`);
		} finally {
			// Run the startup function to reinitialize the watcher and reload the systems
			this.startup();
		}
		this._ngZone.run(() =>
			this._router.navigate(['systems', currentSystem], {
				replaceUrl: true
			})
		);
	}

	/**
	 * Rename a sound file.
	 * @param newName The new name of the sound file.
	 * @param sound The sound file to rename.
	 * @param system The system the sound file belongs to.
	 * @returns A promise that resolves with the new sound file.
	 * @throws An error if the sound file does not exist.
	 */
	async renameSound(newName: string, sound: SoundFile, system: LightsaberSystem) {
		// Determine the file paths
		const fontPath = window.path.join(system.path, `${sound.id()}-${sound.name()}`);
		const newFileName = `${newName}.wav`;
		const newPath = window.path.join(fontPath, newFileName);

		// Check if the sound file exists at the new path
		if (window.fs.existsSync(newPath)) {
			throw new Error('Sound file already exists!');
		}

		// Move the sound file to the new path
		await window.fs.move(sound.filePath, newPath);

		// Process the new sound file and update the system
		const {
			soundArr: [newSound]
		} = this.processFile(fontPath, newFileName);

		this.readjustSystem(system, {
			sounds: [newSound, ...system.sounds().filter(s => s.filePath !== sound.filePath)]
		});

		// Save the system
		await this.saveSystem(system);

		return newSound;
	}

	/**
	 * Save a system to the directory.
	 * @param system The system to save.
	 * @returns A promise that resolves when the system has been saved.
	 * @throws An error if the path is not a valid CFX directory.
	 */
	async saveSystem(system: LightsaberSystem): Promise<void> {
		// Check if the system is valid
		if (this.checkIfCfxDirectory(system?.path) !== true) {
			throw new Error('System is invalid!');
		}

		// Create an array of promises to write the files as a batch
		const writePromiseArr = [
			this.writeConfigFile(system.mainConfig()),
			this.writeConfigFile(system.colorConfig()),
			this.writeConfigFile(system.prefConfig()),
			this.writeConfigFile(system.specialConfig())
		];

		// Map the font configs to a list of fonts, leds, and sounds
		const fontFiles = system.fontConfigs().map(font => ({
			font,
			led: system.getLedConfigById(font.id()),
			sounds: [...system.getSoundsById(font.id()), ...system.getTracksById(font.id())]
		}));

		const newFontArr: FontConfig[] = [];
		const newLedArr: LedConfig[] = [];
		const newSoundArr: SoundFile[] = [];

		// Loop through the font configs move the files if necessary
		fontFiles.forEach(({ font, led, sounds }) => {
			// Check if the font has a new id or name
			if (font.hasNewId() || font.hasNewName()) {
				// Move the font folder
				const oldFontFolder = window.path.join(system.path, `${font.filePath.replace(FONT_REGEX, '$1-$2')}`);
				const newFontFolder = window.path.join(system.path, `${font.id()}-${font.name()}`);
				window.fs.moveSync(oldFontFolder, newFontFolder);

				// Process the new font folder
				const { configArr, soundArr } = this.processDir(newFontFolder);

				let foundFile = configArr.find(file => file.type === ConfigFileType.FONT);
				if (foundFile && FontConfig.isFontConfig(foundFile)) {
					font = foundFile;
				}

				foundFile = configArr.find(file => file.type === ConfigFileType.LEDS);
				if (foundFile && LedConfig.isLedConfig(foundFile)) {
					led = foundFile;
				}

				sounds = soundArr;
			}

			// Push the new font, led, and sounds to the arrays
			newFontArr.push(font);
			newSoundArr.push(...sounds);

			// Add the promises to write the files
			writePromiseArr.push(this.writeConfigFile(font));

			if (led) {
				newLedArr.push(led);
				writePromiseArr.push(this.writeConfigFile(led));
			}
		});

		// Write the files
		await Promise.all(writePromiseArr);

		// Sort the arrays and update the system
		system.fontConfigs.set(sortBy(newFontArr, value => value.id()));
		system.ledConfigs.set(sortBy(newLedArr, value => value.id()));
		system.sounds.set(newSoundArr);
		system.markAsSaved();
	}

	/**
	 * Add missing LED config files to a system. This is used to fix systems that were created before LED config files were added.
	 * @param system The system to add the LED config files to.
	 */
	private addMissingLedFiles(system: LightsaberSystem): void {
		// Iterate through the font configs and add a LED config file if it doesn't exist
		system.fontConfigs().forEach(({ id, name }) => {
			if (system.getLedConfigById(id())) {
				return;
			}

			// Create the LED config file
			const newLedPath = window.path.join(system.path, `${id()}-${name()}`, ConfigFileType.LEDS);
			window.fs.writeFileSync(newLedPath, LED_DEFAULT_TEXT, 'utf-8');

			// Add the LED config to the system
			system.ledConfigs.mutate(value => value.push(new LedConfig(LED_DEFAULT_TEXT, newLedPath)));
		});

		// Sort the LED configs by id
		system.ledConfigs.set(sortBy(system.ledConfigs(), value => value.id()));
	}

	/**
	 * Add missing preferences to a system. This is used to fix systems that were created before preferences were added.
	 * @param system The system to add the preferences to.
	 */
	private addMissingPreferences(system: LightsaberSystem): void {
		// Iterate through the font configs and add a preference if it doesn't exist
		system.fontConfigs().forEach(({ id }) => {
			if (system.prefConfig().getPrefByFontId(id())) {
				return;
			}

			// Add the preference to the system
			system
				.prefConfig()
				.groups.mutate(value =>
					value.push(
						new ConfigGroup(id() - 1, 'font', [
							new ConfigVar(
								'profile',
								new IntegerValue('0', PREFS_DICTIONARY.profile as IntegerDefinition)
							),
							new ConfigVar('color', new IntegerValue('0', PREFS_DICTIONARY.color as IntegerDefinition))
						])
					)
				);
		});

		// Sort the preferences by id and validate the system
		system.prefConfig().groups.set(sortBy(system.prefConfig().groups(), value => value.id()));
	}

	/**
	 * Checks if a directory is a CFX directory. This checks if the directory is empty, and if it has the main, color, and prefs files.
	 * @param directoryPath The path to the directory to check.
	 * @returns True if the directory is a CFX directory, false if it is not, or 'empty' if the directory is empty.
	 */
	private checkIfCfxDirectory(directoryPath: string): boolean | 'empty' {
		// Check if the directory exists
		if (!window.fs.existsSync(directoryPath)) {
			return false;
		}

		// Check if the directory is a directory
		const lstat: Stats = window.fs.lstatSync(directoryPath);
		if (!lstat.isDirectory()) {
			return false;
		}

		const fileList = window.fs.readdirSync(directoryPath);

		// Return 'empty' if the directory is empty or contains only hidden files
		if (fileList.length === 0 || fileList.every(file => file.startsWith('.'))) {
			return 'empty';
		}

		let hasMain = false;
		let hasColor = false;
		let hasPrefs = false;
		let hasFont = false;

		// Open the directory and iterate through the files
		const directory = window.fs.opendirSync(directoryPath);
		let entry = directory.readSync();

		while (entry) {
			// Get the full path of the entry
			const fullPath = window.path.join(directoryPath, entry.name);

			// Check if the entry is a directory or file
			if (entry.isDirectory()) {
				// Check if the directory has a font config file
				const fontConfigPath: string = window.path.join(fullPath, ConfigFileType.FONT);
				hasFont ||= window.fs.existsSync(fontConfigPath) && FONT_REGEX.test(fontConfigPath);
			} else if (entry.isFile()) {
				// Check if the file is a main, color, or prefs file
				hasMain ||= entry.name === ConfigFileType.MAIN;
				hasColor ||= entry.name === ConfigFileType.COLORS;
				hasPrefs ||= entry.name === ConfigFileType.PREF;
			}
			// Read the next entry
			entry = directory.readSync();
		}

		// Close the directory
		directory.closeSync();

		// Return if the directory has the main, color, and prefs files
		return hasMain && hasColor && hasPrefs && hasFont;
	}

	/**
	 * Initializes the library directory.
	 */
	private initializeLibrary(): void {
		// Create the library directory if it doesn't exist
		if (!window.fs.existsSync(this._libPath)) {
			window.fs.mkdirSync(this._libPath);
		}

		// Create the systems directory if it doesn't exist
		if (!window.fs.existsSync(this._systemsPath)) {
			window.fs.mkdirSync(this._systemsPath);
		}

		// Create the fonts directory if it doesn't exist
		if (!window.fs.existsSync(this._fontsPath)) {
			window.fs.mkdirSync(this._fontsPath);
		}

		// Create the colors directory if it doesn't exist
		if (!window.fs.existsSync(this._colorsPath)) {
			window.fs.mkdirSync(this._colorsPath);
		}

		// Create the profiles directory if it doesn't exist
		if (!window.fs.existsSync(this._profilesPath)) {
			window.fs.mkdirSync(this._profilesPath);
		}
	}

	/**
	 * Initializes the paths for the library.
	 */
	private initializePaths(): void {
		// Set the paths
		this._libPath = this._settings.libraryLocation();
		this._fontsPath = window.path.join(this._libPath, this.FONTS);
		this._colorsPath = window.path.join(this._libPath, this.COLORS);
		this._systemsPath = window.path.join(this._libPath, this.SYSTEMS);
		this._profilesPath = window.path.join(this._libPath, this.PROFILES);
	}

	/**
	 * Loads the systems from a directory and gives it a name.
	 * @param path The path to the directory to load the systems from.
	 * @param systemName The name of the system to load.
	 * @returns The system that was loaded.
	 */
	private loadSystemFromDir(path: string, systemName: string): LightsaberSystem {
		// Process the directory
		const { configArr, soundArr }: ProcessObj = this.processDir(path);

		// Create and return the system
		return new LightsaberSystem(systemName, path, configArr, soundArr);
	}

	/**
	 * Processes a directory and returns the config and sound files.
	 * This is a recursive function.
	 * @param directoryPath The path to the directory to process.
	 * @param processObj The object to store the config and sound files in.
	 * @returns The object with the config and sound files.
	 */
	private processDir(directoryPath: string, processObj: ProcessObj = { configArr: [], soundArr: [] }): ProcessObj {
		// Open the directory and get the path
		const directory = window.fs.opendirSync(directoryPath);

		if (directory) {
			// Read the first entry
			let entry = directory.readSync();

			// Iterate through the entries
			while (entry) {
				const { name } = entry;

				// Check if the entry is a directory or file
				if (entry.isDirectory()) {
					// Process the directory
					processObj = this.processDir(window.path.join(directoryPath, name), processObj);
				} else if (entry.isFile()) {
					// Process the file
					processObj = this.processFile(directoryPath, name, processObj);
				}

				// Read the next entry
				entry = directory.readSync();
			}
			// Close the directory and return the object
			directory.closeSync();
		}

		return processObj;
	}

	/**
	 * Processes a file and returns the config and sound files.
	 * @param path The path to the file to process.
	 * @param name The name of the file to process.
	 * @param processObj The object to store the config and sound files in.
	 * @param processObj.configArr
	 * @param processObj.soundArr
	 * @returns The object with the config and sound files.
	 */
	private processFile(
		path: string,
		name: string,
		{ configArr, soundArr }: ProcessObj = { configArr: [], soundArr: [] }
	): ProcessObj {
		const fullPath = window.path.join(path, name);

		// Check if the file is a config file or sound file
		if (Object.values<string>(ConfigFileType).includes(name) && !path.includes('tracks')) {
			// Parse the config file
			const parsedConfig = this._configParser.parseFileText(
				name,
				fullPath,
				window.fs.readFileSync(fullPath, 'utf-8')
			);

			// Add the config to the array if it was parsed successfully
			if (parsedConfig) {
				configArr.push(parsedConfig);
			}
		} else if (fullPath.match(SOUND_NAME)) {
			// Add the sound to the array and sanitize the URL for the audio player
			soundArr.push(
				new SoundFile(
					fullPath,
					this._sanitizer.bypassSecurityTrustResourceUrl(`lsd://${encodeURIComponent(fullPath)}`)
				)
			);
		}
		return { configArr, soundArr };
	}

	/**
	 * Adds missing LED and preference files to the system.
	 * Orders the fonts, blade profiles, and color profiles.
	 * @param system The system to add the missing files to.
	 * @param addConfigs The config files to add to the system.
	 * @param addConfigs.fonts
	 * @param addConfigs.leds
	 * @param addConfigs.prefs
	 * @param addConfigs.sounds
	 */
	private readjustSystem(
		system: LightsaberSystem,
		{
			fonts,
			leds,
			prefs,
			sounds
		}: {
			fonts?: FontConfig[];
			leds?: LedConfig[];
			prefs?: ConfigGroup[];
			sounds?: SoundFile[];
		} = {}
	): void {
		// Set the config files if they were passed in
		system.fontConfigs.set(fonts ? fonts : system.fontConfigs());
		system.ledConfigs.set(leds ? leds : system.ledConfigs());
		system.prefConfig().groups.set(prefs ? prefs : system.prefConfig().groups());
		system.sounds.set(sounds ? sounds : system.sounds());

		// Add the missing files
		this.addMissingLedFiles(system);
		this.addMissingPreferences(system);

		// Order the files
		system.orderFonts(system.fontConfigs().map(({ id }) => id()));
		system.orderBladeProfiles();
		system.orderColorProfiles();
	}

	/**
	 * Initializes the library and watches the systems directory.
	 */
	private startup() {
		this.initializePaths();
		this.initializeLibrary();
		this.watchSystems();
	}

	/**
	 * Watches the systems directory for changes.
	 */
	private watchSystems(): void {
		// Watch for changes to the systems directory
		this._systemWatcher = window.fs.watch(this._systemsPath, async (event, filename) => {
			if (filename) {
				// If the file was renamed, delete it from the system service
				if (event === 'rename' && this._systemService.has(filename)) {
					this._systemService.delete(filename);
				}

				// Update the system list
				this.systemList.set(this.getSystemList());

				// If the current system was renamed, navigate to the systems page
				if (event === 'rename' && this._currentSystem() === filename) {
					this._ngZone.run(() =>
						this._router.navigate(['systems'], {
							replaceUrl: true
						})
					);
				}
			}
		});
	}

	/**
	 * Writes the config file to the system.
	 * @param config The config file to write.
	 * @returns A promise that resolves when the file is written.
	 * @throws An error if the file could not be written.
	 */
	private writeConfigFile(config: BaseFile): Promise<void> {
		return window.fs.writeFile(config.filePath, config.toString(), 'utf-8');
	}
}
