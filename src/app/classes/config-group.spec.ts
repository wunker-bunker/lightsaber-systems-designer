import { testData } from 'src/environments/test-defaults.karma';
import { GENERIC_BLADE_PROFILE } from '../constants/objects';
import { MAIN_DICT } from '../dictionaries/variable.dict';
import { VALUE_TYPE } from '../enums/value-type.enum';
import { ConfigGroup } from './config-group';
import { ColorValue } from './config-value/color-value';

describe('GroupConfig', () => {
	let group: ConfigGroup;

	beforeEach(() => {
		testData.generateTestData();
		group = new ConfigGroup(1, 'color', testData.colorConfig.groups()[0].variables());
	});

	it('should create an instance', () => {
		expect(group).toBeTruthy();
		expect(group.dirty()).toBeFalse();
		expect(group.valid()).toBeTrue();
	});

	it('should get variable by name', () => {
		const variable = group.getVariableByName('color');
		if (!variable) {
			throw new Error('variable was undefined');
		}

		expect(variable.name).toEqual('color');
		expect(variable.configValue().type).toEqual(VALUE_TYPE.COLOR);
	});

	describe('should test functions', () => {
		let value: ColorValue;

		beforeEach(() => {
			const v = group.getVariableByName('color');
			const cv = v?.configValue();

			if (!v || !cv || !ColorValue.isColorValue(cv)) {
				throw new Error('variable was undefined');
			}

			value = cv;

			value.value.mutate(val => (val.blue = 100));
		});

		it('should validate', () => {
			value.value.mutate(val => (val.blue = -100));
			expect(group.valid()).toBeFalse();
		});

		it('should mark as saved', () => {
			expect(group.dirty()).toBeTrue();
			group.markAsSaved();
			expect(group.dirty()).toBeFalse();
		});

		it('should revert', () => {
			expect(group.dirty()).toBeTrue();
			expect(value.value().blue).toEqual(100);
			group.revert();
			expect(group.dirty()).toBeFalse();
			expect(value.value().blue).not.toEqual(100);
		});
	});

	it('should print to string', () => {
		const str = group.toString();
		expect(str).toContain('[color=1]');
		expect(str).toContain('fcolor=1023,1023,1023,0');
		expect(str).toContain('mcolor=1111');
	});

	it('should revert to old ID', () => {
		const ogId = group.id();
		const newId = 20;
		group.id.set(newId);
		expect(group.id()).toEqual(newId);
		group.revert();
		expect(group.id()).toEqual(ogId);
	});

	it('should mark as saved', () => {
		const newId = 20;
		group.id.set(newId);
		expect(group.id()).toEqual(newId);
		expect(group.dirty()).toBeTrue();

		group.markAsSaved();
		expect(group.id()).toEqual(newId);
		expect(group.dirty()).toBeFalse();
	});

	it('should be dirty with new ID', () => {
		expect(group.dirty()).toBeFalse();
		const newId = 20;
		group.id.set(newId);
		expect(group.id()).toEqual(newId);
		expect(group.dirty()).toBeTrue();

		group.markAsSaved();
		expect(group.dirty()).toBeFalse();
	});

	it('should create ConfigGroup from JSON object and convert back to JSON', () => {
		const groupJson = ConfigGroup.fromJSON(GENERIC_BLADE_PROFILE, MAIN_DICT);
		expect(groupJson.name).toEqual(GENERIC_BLADE_PROFILE.name);
		expect(groupJson.id()).toEqual(GENERIC_BLADE_PROFILE.id);
		expect(groupJson.getVariableByName(GENERIC_BLADE_PROFILE.variables[0].name)?.configValue().value()).toEqual(
			GENERIC_BLADE_PROFILE.variables[0].configValue
		);
		expect(groupJson.toJSON()).toEqual(GENERIC_BLADE_PROFILE);
	});

	it('should set group name', () => {
		const ogGroupName = group.groupName();

		group.setGroupName('test');
		expect(group.groupName()).toEqual('test');
		expect(group.groupName()).not.toEqual(ogGroupName);
	});
});
