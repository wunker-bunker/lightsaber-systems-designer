/* eslint-disable @typescript-eslint/naming-convention */
/**
 * Profile names used in config files
 */
export enum ProfileNames {
	cname = 'cname',
	pname = 'pname'
}
