import { computed, signal, WritableSignal } from '@angular/core';
import { FONT_REGEX } from 'src/app/constants/regex';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { BaseFile } from './base-file';
import { TextFile } from './text-file';

/**
 * Class for the led config files.
 */
export class LedConfig extends TextFile {
	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 * @returns True if the file has been modified since it was last saved, false otherwise.
	 */
	readonly dirty = computed(() => this._dirtyText() || this.hasNewId() || this.hasNewName());

	/**
	 * Flag to indicate if the id of the font has changed.
	 * @returns True if the id has changed, false otherwise.
	 */
	readonly hasNewId = computed(() => this.id() !== this._previousId());

	/**
	 * Flag to indicate if the name of the font has changed.
	 * @returns True if the name has changed, false otherwise.
	 */
	readonly hasNewName = computed(() => this.name() !== this._previousName());

	/**
	 * The id of the font.
	 * This corresponds to the number of the directory the font is in.
	 * @returns The id of the font.
	 */
	readonly id: WritableSignal<number>;

	/**
	 * The name of the font where the led config is used.
	 * This corresponds to the name of the directory the font is in.
	 * This is readonly because it is derived from the file path.
	 */
	readonly name: WritableSignal<string>;

	/**
	 * Stores the previous id of the font.
	 * This is used to revert moving the font to a different order.
	 */
	private readonly _previousId: WritableSignal<number>;

	/**
	 * Stores the previous name of the font.
	 * This is used to revert renaming the font.
	 */
	private readonly _previousName: WritableSignal<string>;

	/**
	 * Creates a new instance of the LedConfig class.
	 * @param text The text of the file.
	 * @param filePath The path to the file.
	 */
	constructor(text: string, filePath: string) {
		super(text, ConfigFileType.LEDS, filePath);
		const id = +filePath.replace(FONT_REGEX, '$1');
		const name = filePath.replace(FONT_REGEX, '$2');
		this.id = signal(id);
		this.name = signal(name);
		this._previousId = signal(id);
		this._previousName = signal(name);
	}

	/**
	 * Gaurd to check if the file is a led config file.
	 * @param file The file to check.
	 * @returns True if the file is a led config file, false otherwise.
	 */
	static isLedConfig(file: BaseFile): file is LedConfig {
		return file instanceof LedConfig;
	}

	/**
	 * Marks the file as saved.
	 * This also sets the previous id to the current id.
	 * Calls the super markAsSaved method.
	 */
	markAsSaved(): void {
		this._previousId.set(this.id());
		this._previousName.set(this.name());
		super.markAsSaved();
	}

	/**
	 * Reverts the file to its previous state.
	 * This also sets the current id to the previous id.
	 * Calls the super revert method.
	 */
	revert(): void {
		this.id.set(this._previousId());
		this.name.set(this._previousName());
		super.revert();
	}
}
