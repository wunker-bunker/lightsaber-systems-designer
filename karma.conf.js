// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const path = require('path');

module.exports = function (config) {
	config.set({
		basePath: '',
		frameworks: ['jasmine', '@angular-devkit/build-angular'],
		plugins: [
			'karma-jasmine',
			'karma-electron',
			'karma-coverage',
			'karma-spec-reporter',
			'karma-jasmine-html-reporter',
			'karma-junit-reporter',
			'@angular-devkit/build-angular/plugins/karma'
		],
		client: {
			jasmine: {
				// you can add configuration options for Jasmine here
				// the possible options are listed at https://jasmine.github.io/api/edge/Configuration.html
				// for example, you can disable the random execution with `random: false`
				// or set a specific seed with `seed: 4321`
			},
			clearContext: true
		},
		jasmineHtmlReporter: {
			suppressAll: true // removes the duplicated traces
		},
		port: config.singleRun ? 0 : 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		restartOnFileChange: true,
		coverageReporter: {
			dir: path.join(__dirname, './coverage'),
			subdir: '.',
			reporters: [{ type: 'html' }, { type: 'text-summary' }, { type: 'lcov' }]
		},
		browserNoActivityTimeout: 300000,
		junitReporter: {
			outputDir: 'coverage', // results will be saved as $outputDir/$browserName.xml
			outputFile: 'junit.xml', // if included, results will be saved as $outputDir/$browserName/$outputFile
			useBrowserName: false // add browser name to report and classes names
		},
		reporters: ['spec', 'junit'],
		browsers: ['AngularElectron'],
		customLaunchers: {
			AngularElectron: {
				base: 'Electron',
				flags: [config.singleRun ? '' : '--remote-debugging-port=9222', '--no-sandbox'],
				browserWindowOptions: {
					webPreferences: {
						nodeIntegration: true,
						nodeIntegrationInSubFrames: true,
						allowRunningInsecureContent: true,
						contextIsolation: false
					}
				}
			}
		}
	});
};
