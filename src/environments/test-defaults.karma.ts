/* eslint-disable @typescript-eslint/no-empty-function */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, TestModuleMetadata } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { Dir, Dirent, FSWatcher } from 'fs';
import { constants as fsConstants } from 'fs-extra';
import { join } from 'path';
import { AppAlertsComponent } from 'src/app/components/app-alerts/app-alerts.component';
import { TitleBarComponent } from 'src/app/components/title-bar/title-bar.component';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { LSDFontModule } from 'src/app/modules/lsd-font.module';
import { AppChannel } from 'src/common/app-channel.enum';
import { AppSettings, SettingsReturnType } from 'src/common/app-settings.enum';
import { UiThemes } from 'src/common/ui-themes.enum';
import { ElectronApi, FsApi, OsApi, PathApi, SettingsApi } from 'src/types/electron-window';
import {
	testColorConfigText,
	testFontConfigText,
	testFontLedText,
	testMainConfigText,
	testPrefConfigText,
	testSpecialConfigText
} from './test-config-text';
import {
	TestComponent,
	TestData,
	testExportPath,
	testFontDirEntries,
	testFontNoLedDirEntries,
	testFonts,
	testHomeDir,
	testLibraryLocation,
	testNewFontNoLedName,
	testNewFontNoLedPath,
	testNewFontPath,
	testNewLibraryLocation,
	testNewSoundPath,
	testNewSystemLibraryPath,
	testNewSystemPath,
	testSystemDirEntries,
	testSystemList,
	testSystemLocationPaths,
	testTrackFileNames
} from './test-data.karma';

export class MockNgbActiveModal {
	close(): void {}
	dismiss(): void {}
}

export class Message {
	channel!: string;
	params?: unknown[];
}

export const unitTestImports = [
	HttpClientTestingModule,
	FormsModule,
	NgbModule,
	DragDropModule,
	NgSelectModule,
	RouterTestingModule.withRoutes(TestData.routes),
	LSDFontModule
];

export const unitTestDeclarations = [TestComponent, AppAlertsComponent, TitleBarComponent];

export const unitTestProviders = [
	{
		provide: NgbActiveModal,
		useClass: MockNgbActiveModal
	}
];

export const testData: TestData = new TestData();

type MockedSettings = { [K in AppSettings]?: SettingsReturnType[K] };

export let mockedSettings: MockedSettings = {};

// FS Spies
export let accessSpy: jasmine.Spy;
export let copySpy: jasmine.Spy;
export let existsSyncSpy: jasmine.Spy;
export let lstatSyncSpy: jasmine.Spy;
export let mkdirSyncSpy: jasmine.Spy;
export let moveSpy: jasmine.Spy;
export let moveSyncSpy: jasmine.Spy;
export let opendirSyncSpy: jasmine.Spy;
export let readdirSyncSpy: jasmine.Spy<(path: string) => string[]>;
export let readFileSyncSpy: jasmine.Spy;
export let rmSpy: jasmine.Spy;
export let watchSpy: jasmine.Spy;
export let writeFileSpy: jasmine.Spy;
export let writeFileSyncSpy: jasmine.Spy;

// Create a map type of all the functions in the ElectronApi interface
// To a jasmine.spyOn<ElectronApi, "function"> type
type ElectronApiFunctionSpies = {
	[K in keyof ElectronApi]: jasmine.Spy<ElectronApi[K]>;
};

// window.electronapi Spies
export let changeThemeSpy: ElectronApiFunctionSpies['changeTheme'];
export let closeAppSpy: ElectronApiFunctionSpies['closeApp'];
export let debugTriggerDownloadProgressSpy: ElectronApiFunctionSpies['debugTriggerDownloadProgress'];
export let debugTriggerUpdateAvailableSpy: ElectronApiFunctionSpies['debugTriggerUpdateAvailable'];
export let debugTriggerUpdateDownloadedSpy: ElectronApiFunctionSpies['debugTriggerUpdateDownloaded'];
export let getPlatformSpy: ElectronApiFunctionSpies['getPlatform'];
export let isServedSpy: ElectronApiFunctionSpies['isServed'];
export let maximizeAppSpy: ElectronApiFunctionSpies['maximizeApp'];
export let minimizeAppSpy: ElectronApiFunctionSpies['minimizeApp'];
export let onAppMaximizedSpy: ElectronApiFunctionSpies['onAppMaximized'];
export let onDownloadProgressSpy: ElectronApiFunctionSpies['onDownloadProgress'];
export let onUpdateAvailableSpy: ElectronApiFunctionSpies['onUpdateAvailable'];
export let onUpdateDownloadedSpy: ElectronApiFunctionSpies['onUpdateDownloaded'];
export let openDialogSpy: ElectronApiFunctionSpies['openDialog'];
export let openDirecotrySpy: ElectronApiFunctionSpies['openDirecotry'];
export let openExternalSpy: ElectronApiFunctionSpies['openExternal'];
export let openUpdateDebuggerSpy: ElectronApiFunctionSpies['openUpdateDebugger'];
export let readyToCheckForUpdatesSpy: ElectronApiFunctionSpies['readyToCheckForUpdates'];
export let restartAndInstallSpy: ElectronApiFunctionSpies['restartAndInstall'];
export let restoreAppSpy: ElectronApiFunctionSpies['restoreApp'];
export let setUpdateChannelSpy: ElectronApiFunctionSpies['setUpdateChannel'];
export let startUpdateDownloadSpy: ElectronApiFunctionSpies['startUpdateDownload'];

export const configurePreload = (blankSlate?: boolean) => {
	beforeAll(() => {
		window.settings = {
			getSync: () => {},
			hasSync: () => {},
			setSync: () => {}
		} as unknown as SettingsApi;

		window.fs = {
			constants: fsConstants,
			access: () => {},
			copy: () => {},
			existsSync: (path: string) =>
				path === testExportPath ||
				path === testNewFontPath ||
				path === testNewFontNoLedPath ||
				path === testNewSoundPath ||
				path === testNewSystemPath ||
				path === testNewLibraryLocation ||
				path === join(testNewSystemLibraryPath, testFonts[0], ConfigFileType.FONT) ||
				path === join(testNewFontPath, ConfigFileType.FONT) ||
				path === join(testNewFontNoLedPath, ConfigFileType.FONT) ||
				(path === join(testLibraryLocation) && !blankSlate) ||
				(path === join(testLibraryLocation, 'fonts') && !blankSlate) ||
				(path === join(testLibraryLocation, 'systems') && !blankSlate) ||
				(path === join(testLibraryLocation, 'fonts') && !blankSlate) ||
				(path === join(testLibraryLocation, 'colors') && !blankSlate) ||
				(path === join(testLibraryLocation, 'profiles') && !blankSlate) ||
				testSystemLocationPaths.includes(path) ||
				testData.systems
					.find(system => path.includes(system.name))
					?.fontConfigs()
					.some(font => font.filePath === path) ||
				testData.systems
					.find(system => path.includes(system.name))
					?.sounds()
					.some(sound => sound.filePath === path),
			lstatSync: (path: string) => {
				if (
					testSystemLocationPaths.includes(path) ||
					path === testExportPath ||
					path === testNewSystemPath ||
					path === testNewSystemLibraryPath ||
					path === testNewLibraryLocation
				) {
					return {
						isDirectory: () => true
					} as unknown as Dirent;
				}
				return {
					isDirectory: () => false
				} as unknown as Dirent;
			},
			mkdirSync: () => {},
			move: () => {},
			moveSync: () => {},
			opendirSync: (path: string) => {
				const fontDirRegex = /.*\/(\d+)-([a-zA-Z0-9_-]+)(?:\/)?$/;
				const fontTrackDirRegex = /.*\/\d+-[a-zA-Z0-9_-]+\/tracks(?:\/)?$/;
				const createDirEntry = (entries: string[]) =>
					({
						closeSync: () => null,
						readSync: () => {
							if (entries.length === 0) {
								return null;
							}
							const nextEntry = entries.shift();
							const dirNames = [...testFonts, ...testSystemList, 'tracks'];

							if (!nextEntry) {
								return null;
							}

							return {
								name: nextEntry,
								path: join(path, nextEntry),
								isDirectory: () => dirNames.includes(nextEntry),
								isFile: () => !dirNames.includes(nextEntry)
							} as unknown as Dirent;
						}
					} as unknown as Dir);

				const fontPathMatch = path.match(fontDirRegex);

				if (
					fontPathMatch?.length === 3 &&
					fontPathMatch[0] === path &&
					fontPathMatch[2] === testNewFontNoLedName
				) {
					return createDirEntry([...testFontNoLedDirEntries]);
				}

				if (fontDirRegex.test(path)) {
					return createDirEntry([...testFontDirEntries]);
				}

				if (fontTrackDirRegex.test(path)) {
					return createDirEntry([...testTrackFileNames]);
				}

				if (
					testSystemLocationPaths.includes(path) ||
					path === testNewSystemLibraryPath ||
					path === testNewSystemPath
				) {
					return createDirEntry([...testSystemDirEntries]);
				}
				return null;
			},
			readdirSync: (path: string): string[] => {
				if (
					testSystemLocationPaths.includes(path) ||
					path === testNewSystemLibraryPath ||
					path === testNewSystemPath
				) {
					return [
						ConfigFileType.MAIN,
						ConfigFileType.COLORS,
						ConfigFileType.PREF,
						ConfigFileType.SPECIAL,
						...testFonts
					];
				}
				if (path === join(testLibraryLocation, 'systems') && !blankSlate) {
					return [...testSystemList];
				}

				if (path === testExportPath) {
					return [];
				}
				throw new Error('Invalid path');
			},
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			readFileSync: (path: string, options?: unknown) => {
				if (path.includes(ConfigFileType.COLORS)) {
					return testColorConfigText;
				}

				if (path.includes(ConfigFileType.FONT)) {
					return testFontConfigText;
				}

				if (path.includes(ConfigFileType.LEDS)) {
					return testFontLedText;
				}

				if (path.includes(ConfigFileType.MAIN)) {
					return testMainConfigText;
				}

				if (path.includes(ConfigFileType.PREF)) {
					return testPrefConfigText;
				}

				if (path.includes(ConfigFileType.SPECIAL)) {
					return testSpecialConfigText;
				}

				return null;
			},
			rm: () => {},
			watch: () =>
				({
					close: jasmine.createSpy('close')
				} as unknown as FSWatcher),
			writeFile: () => {},
			writeFileSync: () => {}
		} as unknown as FsApi;
		window.os = {
			homedir: () => {}
		} as unknown as OsApi;
		window.path = {
			join: (...paths: string[]): string => join(...paths)
		} as unknown as PathApi;
		window.electronApi = {
			changeTheme: () => {},
			closeApp: () => {},
			debugTriggerDownloadProgress: () => {},
			debugTriggerUpdateAvailable: () => {},
			debugTriggerUpdateDownloaded: () => {},
			getPlatform: () => {},
			isServed: async () => true,
			maximizeApp: () => {},
			minimizeApp: () => {},
			onAppMaximized: () => {},
			onDownloadProgress: () => {},
			onUpdateAvailable: () => {},
			onUpdateDownloaded: () => {},
			openDialog: () => {},
			openDirecotry: () => {},
			openExternal: () => {},
			openUpdateDebugger: () => {},
			readyToCheckForUpdates: () => {},
			restartAndInstall: () => {},
			restoreApp: () => {},
			setUpdateChannel: () => {},
			startUpdateDownload: () => {}
		} as unknown as ElectronApi;
	});

	beforeEach(() => {
		mockedSettings = {};

		if (!blankSlate) {
			mockedSettings = {
				channel: AppChannel.LATEST,
				'location.library': testLibraryLocation,
				theme: UiThemes.LIGHT
			};
		}

		// Settings API
		spyOn(window.settings, 'hasSync').and.callFake(key => !!mockedSettings[key]);
		spyOn(window.settings, 'getSync').and.callFake(key => mockedSettings[key]);
		spyOn(window.settings, 'setSync').and.callFake((key, value) => (mockedSettings[key] = value));

		// FS API
		accessSpy = spyOn(window.fs, 'access');
		copySpy = spyOn(window.fs, 'copy');
		existsSyncSpy = spyOn(window.fs, 'existsSync').and.callThrough();
		lstatSyncSpy = spyOn(window.fs, 'lstatSync').and.callThrough();
		mkdirSyncSpy = spyOn(window.fs, 'mkdirSync');
		moveSpy = spyOn(window.fs, 'move');
		moveSyncSpy = spyOn(window.fs, 'moveSync');
		opendirSyncSpy = spyOn(window.fs, 'opendirSync').and.callThrough();
		readdirSyncSpy = spyOn(window.fs, 'readdirSync').and.callThrough();
		readFileSyncSpy = spyOn(window.fs, 'readFileSync').and.callThrough();
		rmSpy = spyOn(window.fs, 'rm').and.resolveTo();
		watchSpy = spyOn(window.fs, 'watch');
		writeFileSpy = spyOn(window.fs, 'writeFile');
		writeFileSyncSpy = spyOn(window.fs, 'writeFileSync');

		// Electron API
		changeThemeSpy = spyOn(window.electronApi, 'changeTheme');
		closeAppSpy = spyOn(window.electronApi, 'closeApp');
		debugTriggerDownloadProgressSpy = spyOn(window.electronApi, 'debugTriggerDownloadProgress');
		debugTriggerUpdateAvailableSpy = spyOn(window.electronApi, 'debugTriggerUpdateAvailable');
		debugTriggerUpdateDownloadedSpy = spyOn(window.electronApi, 'debugTriggerUpdateDownloaded');
		getPlatformSpy = spyOn(window.electronApi, 'getPlatform');
		isServedSpy = spyOn(window.electronApi, 'isServed').and.callThrough();
		maximizeAppSpy = spyOn(window.electronApi, 'maximizeApp');
		minimizeAppSpy = spyOn(window.electronApi, 'minimizeApp');
		onAppMaximizedSpy = spyOn(window.electronApi, 'onAppMaximized');
		onDownloadProgressSpy = spyOn(window.electronApi, 'onDownloadProgress');
		onUpdateAvailableSpy = spyOn(window.electronApi, 'onUpdateAvailable');
		onUpdateDownloadedSpy = spyOn(window.electronApi, 'onUpdateDownloaded');
		openDialogSpy = spyOn(window.electronApi, 'openDialog');
		openDirecotrySpy = spyOn(window.electronApi, 'openDirecotry');
		openExternalSpy = spyOn(window.electronApi, 'openExternal');
		openUpdateDebuggerSpy = spyOn(window.electronApi, 'openUpdateDebugger');
		readyToCheckForUpdatesSpy = spyOn(window.electronApi, 'readyToCheckForUpdates');
		restartAndInstallSpy = spyOn(window.electronApi, 'restartAndInstall');
		restoreAppSpy = spyOn(window.electronApi, 'restoreApp');
		setUpdateChannelSpy = spyOn(window.electronApi, 'setUpdateChannel');
		startUpdateDownloadSpy = spyOn(window.electronApi, 'startUpdateDownload');

		// OS API
		spyOn(window.os, 'homedir').and.returnValue(testHomeDir);
	});
};

export const configureTestSuite = (config: TestModuleMetadata, blankSettings?: boolean) => {
	configurePreload(blankSettings);

	beforeEach(async () => {
		TestBed.resetTestingModule();
		TestBed.configureTestingModule(config);

		await TestBed.compileComponents();
		testData.generateTestData();
	});
};
