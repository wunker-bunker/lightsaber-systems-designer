import { computed, signal, WritableSignal } from '@angular/core';
import { VALUE_TYPE } from '../enums/value-type.enum';
import { ConfigVarJSON } from '../models/light-saber-system.json';
import { VariableDefinition, VariableDictionary } from '../models/variable-dictionary.model';
import { BitfieldValue } from './config-value/bitfield-value';
import { BooleanValue } from './config-value/boolean-value';
import { ColorValue } from './config-value/color-value';
import { ConfigValueClasses } from './config-value/config-value';
import { EnumerationValue } from './config-value/enumeration-value';
import { IntegerArrayValue } from './config-value/integer-array-value';
import { IntegerValue } from './config-value/integer-value';
import { ProfileSelectorValue } from './config-value/profile-selector-value';
import { StringValue } from './config-value/string-value';
import { UnknownValue } from './config-value/unknown-value';

/**
 * Contains a variable name and its value.
 */
export class ConfigVar {
	readonly configValue: WritableSignal<ConfigValueClasses>;

	readonly toString;

	/**
	 * Creates a new ConfigVar.
	 * @param name The name of the variable.
	 * @param configValue The value of the variable.
	 */
	constructor(public readonly name: string, configValue: ConfigValueClasses) {
		this.configValue = signal(configValue);
		this.toString = computed((): string => `${this.name}=${this.configValue().toString()}`);
	}

	/**
	 * Creates a new ConfigVar from a JSON object.
	 * @param json The JSON object to create the ConfigVar from.
	 * @param json.name
	 * @param json.configValue
	 * @param dict The dictionary of variables.
	 * @returns The ConfigVar created from the JSON object.
	 */
	static fromJSON({ name, configValue }: ConfigVarJSON, dict: VariableDictionary): ConfigVar {
		return new ConfigVar(name, this.valueFrom(configValue, dict[name]));
	}

	/**
	 * Creates a new ConfigVar from a string given the variable definition.
	 * @param value The string to create the ConfigVar from.
	 * @param def The variable definition.
	 * @returns The ConfigValue created from the string.
	 */
	static valueFrom(value: string, def: VariableDefinition): ConfigValueClasses {
		switch (def?.type) {
			case VALUE_TYPE.BITFIELD:
				return new BitfieldValue(value, def);
			case VALUE_TYPE.BOOLEAN:
				return new BooleanValue(value, def);
			case VALUE_TYPE.COLOR:
				return new ColorValue(value, def);
			case VALUE_TYPE.ENUM:
				return new EnumerationValue(value, def);
			case VALUE_TYPE.INTEGER:
				return new IntegerValue(value, def);
			case VALUE_TYPE.INTEGER_ARR:
				return new IntegerArrayValue(value, def);
			case VALUE_TYPE.STRING:
				return new StringValue(value, def);
			case VALUE_TYPE.PROFILE:
				return new ProfileSelectorValue(value, def);
			default:
				return new UnknownValue(value);
		}
	}

	/**
	 * Creates a JSON object from the ConfigVar.
	 * @returns The JSON object created from the ConfigVar.
	 */
	toJSON(): ConfigVarJSON {
		return {
			name: this.name,
			configValue: this.configValue().toString()
		};
	}
}
