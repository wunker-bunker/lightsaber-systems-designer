import { Pipe, PipeTransform } from '@angular/core';
import { isEmpty } from 'lodash';

/**
 * Pipe to check if a value is empty.
 * @example <div *ngIf="value | isEmpty">Value is empty</div>
 */
@Pipe({
	name: 'isEmpty',
	pure: true
})
export class IsEmptyPipe implements PipeTransform {
	/**
	 * Check if a value is empty.
	 * @param value The value to check.
	 * @returns True if the value is empty, false otherwise.
	 * @see https://lodash.com/docs/4.17.15#isEmpty
	 */
	transform(value: unknown): boolean {
		return isEmpty(value);
	}
}
