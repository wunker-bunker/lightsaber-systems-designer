/**
 * Based definition for a sound.
 */
export interface BaseSoundDefinition {
	/**
	 * Description of the sound.
	 */
	readonly description: string;

	/**
	 * Wheter the sound is a loop.
	 * If true, the sound will be played in a loop.
	 * If false, the sound will be played once.
	 */
	readonly loops: boolean;

	/**
	 * Is a numbered sound file.
	 * Example: "sound1.wav", "sound2.wav", "sound3.wav", etc.
	 */
	readonly isMulti: boolean;
}

/**
 * Definition for a single sound.
 * This is a sound that is not a numbered sound file.
 * Example: "sound.wav"
 */
export interface SingleSoundDefinition extends BaseSoundDefinition {
	readonly isMulti: false;
}

/**
 * Definition for a multi sound.
 * This is a sound that is a numbered sound file.
 * Example: "sound1.wav", "sound2.wav", "sound3.wav", etc.
 */
export interface MultiSoundDefinition extends BaseSoundDefinition {
	readonly isMulti: true;

	/**
	 * The minimum number of sound files.
	 */
	readonly minCount: number;
}

/**
 * Definition for a sound.
 * Used in the sound dictionary.
 */
export type SoundDefinition = MultiSoundDefinition | SingleSoundDefinition;

/**
 * Sound dictionary.
 * Used to store the sound definitions.
 * The key is the name of the sound.
 * The value is the sound definition.
 *
 * Example:
 * ```
 * {
 * 	"sound": {
 * 		"description": "Sound",
 * 		"loops": false,
 * 		"isMulti": false
 * 	},
 * 	"sound2": {
 * 		"description": "Sound 2",
 * 		"loops": false,
 * 		"isMulti": true,
 * 		"minCount": 2
 * 	}
 * }
 * ```
 */
export interface SoundDictionary {
	[name: string]: SoundDefinition;
}
