/**
 * Defines the environment model.
 * It is an object with the following properties:
 * - environment: 'DEV' | 'PROD' | 'WEB' | 'TEST'
 * - prefsName: 'preferences.json' | 'preferences-dev.json' | 'preferences-web.json' | 'preferences-test.json'
 * - production: boolean
 */
export interface Environment {
	/**
	 * Environment
	 * - DEV: Development
	 * - PROD: Production
	 * - WEB: Web
	 * - TEST: Test
	 */
	environment: 'DEV' | 'PROD' | 'WEB' | 'TEST';

	/**
	 * Preferences file name
	 * - preferences.json: Production
	 * - preferences-dev.json: Development
	 * - preferences-web.json: Web
	 * - preferences-test.json: Test
	 */
	prefsName: 'preferences.json' | 'preferences-dev.json' | 'preferences-web.json' | 'preferences-test.json';

	/**
	 * Production flag
	 * - true: Production
	 * - false: Development
	 */
	production: boolean;
}
