import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IntegerValue } from 'src/app/classes/config-value/integer-value';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-integer-input',
	templateUrl: './integer-input.component.html',
	styleUrls: ['./integer-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class IntegerInputComponent implements BaseInputComponent {
	@Input() input!: IntegerValue;
	@Input() name = '';
}
