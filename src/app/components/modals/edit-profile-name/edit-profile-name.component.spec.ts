import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { EditProfileNameComponent } from './edit-profile-name.component';

describe('EditProfileNameComponent', () => {
	let component: EditProfileNameComponent;
	let fixture: ComponentFixture<EditProfileNameComponent>;
	const modalMock: jasmine.SpyObj<NgbActiveModal> = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

	configureTestSuite({
		imports: [ReactiveFormsModule, ...unitTestImports],
		declarations: [EditProfileNameComponent, ...unitTestDeclarations],
		providers: [{ provide: NgbActiveModal, useValue: modalMock }, ...unitTestProviders]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditProfileNameComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should get and set name', () => {
		const name = component.name;

		if (!name) {
			throw new Error('name is null');
		}

		component.setName('test system');
		expect(name.value).toEqual('test system');
	});

	it('should print errors to console', () => {
		const name = component.name;

		if (!name) {
			throw new Error('name is null');
		}

		const warnSpy = spyOn(console, 'warn').and.callThrough();
		name.setValue('');
		name.updateValueAndValidity();
		component.printForm();
		expect(console.warn).toHaveBeenCalledWith(name.errors);

		warnSpy.calls.reset();
		name.setValue('test profile 1');
		name.updateValueAndValidity();
		component.printForm();
		expect(console.warn).not.toHaveBeenCalled();
	});
});
