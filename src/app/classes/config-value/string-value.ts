import { computed } from '@angular/core';
import { ValueType, ValueTypeString } from 'src/app/enums/value-type.enum';
import { BaseDefinition, StringDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class StringValue extends ConfigValue<string, ValueTypeString, StringDefinition> {
	readonly toString;

	readonly valid;

	constructor(value: string, def: StringDefinition) {
		super(value, def, def.type);
		this.toString = computed(() => this.value());
		this.valid = computed(
			() =>
				(this.definition.minLength ? this.value().length >= this.definition.minLength : true) &&
				(this.definition.maxLength ? this.value().length <= this.definition.maxLength : true) &&
				!!this.value()
		);
	}

	static isStringValue(value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>): value is StringValue {
		return value instanceof StringValue;
	}

	autoFixValue(): void {
		let newValue = '';
		if (!this.definition.minLength && !this.definition.maxLength) {
			newValue = 'Default String Value';
		} else if (this.definition.minLength) {
			for (let i = 0; i <= this.definition.minLength; i++) {
				newValue += 'x';
			}
		}
		this.value.set(newValue);
	}

	protected parseValue(strVal: string): string {
		return strVal;
	}
}
