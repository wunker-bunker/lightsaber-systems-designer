import { Signal } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';

/**
 * Base class for all files.
 * This class is used to store the common properties and methods for all files.
 */
export abstract class BaseFile {
	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 */
	abstract dirty: Signal<boolean>;

	/**
	 * Creates a string representation of the file.
	 * This is used to save the file to disk.
	 * @returns The string representation of the file.
	 */
	abstract toString: Signal<string>;

	/**
	 * Indicates if the file is valid.
	 * @returns True if the file is valid, false otherwise.
	 */
	abstract valid: Signal<boolean>;

	/**
	 * Creates a new instance of the BaseFile class.
	 * @param type The type of the file.
	 * @param filePath The path to the file.
	 */
	constructor(public readonly type: ConfigFileType, public readonly filePath: string) {}

	/**
	 * Mark the file as saved.
	 * This will reset the dirty flag.
	 */
	abstract markAsSaved(): void;

	/**
	 * Revert the file to the last saved state.
	 * This will reset the dirty flag.
	 */
	abstract revert(): void;
}
