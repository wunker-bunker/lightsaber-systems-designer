import { BoardOrientation } from '../enums/board-orientation.enum';
import { GroupType } from '../enums/group-types';
import { Logport } from '../enums/logport.enum';
import { MappingBehavior, MappingColor } from '../enums/mapping-behavior.enum';
import { MuteBoot } from '../enums/mute-boot.enum';
import { PowerOnInterruptMode } from '../enums/power-on-interrupt-mode.enum';
import { PowerOnMode } from '../enums/power-on-mode.enum';
import { RandomMode } from '../enums/random-mode.enum';
import { SaberLock } from '../enums/saber-lock.enum';
import { ShuffleMode } from '../enums/shuffle-mode.enum';
import { SmoothSwingMode } from '../enums/smooth-swing-mode.enum';
import { SpectrumAxis } from '../enums/spectrum-axis.enum';
import { SpectrumMode } from '../enums/spectrum-mode.enum';
import {
	StyleBlaster,
	StyleClash,
	StyleDrag,
	StyleLockup,
	StylePowerOff,
	StylePowerOn,
	StyleStab
} from '../enums/styles.enum';
import { SwitchType } from '../enums/switch-type.enum';
import { UnstableFx } from '../enums/unstable-fx.enum';
import { VALUE_TYPE as T } from '../enums/value-type.enum';
import { WakeupMode } from '../enums/wakeup-mode.enum';
import { UnknownDefinition, VariableDictionary } from '../models/variable-dictionary.model';

/**
 * Dictionary of all known variables
 */
export const MAIN_DICT: VariableDictionary = {
	gate: {
		description:
			'minimum motion quantity required to start the motion recognition engine. Base value between15 to 70 works fine (default: 50)',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 200
	},
	smooth_gate: {
		description:
			'minimum  required  to  start  the  smoothswing  engine.  Usually  the same as the gate above but we added a specific parameter to it, just in case',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 200
	},
	hswing: {
		description: 'swing threshold',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	hslash: {
		description: 'slashthreshold',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	hhybrid: {
		description: 'hybrid or accent swing threshold when using a smoothswing font',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	hforce: {
		description: 'threshold to trigger force fx',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	hfclash: {
		description: 'threshold to trigger a force clash fx',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	hclash: {
		description: 'clash threshold',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	hstab: {
		description:
			'stab threshold followed by angular range (2 values [-90;90]) in which the stab effect can be executed',
		type: T.INTEGER_ARR,
		arrLength: 3,
		minValueArr: [0, -90, -90],
		maxValueArr: [1023, 90, 90],
		expandable: false
	},
	hspin: {
		description: 'spin threshold',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	htwist: {
		description: 'twist threshold',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 1023
	},
	htemp: {
		description: 'the temperature threshold. (not a gesture/motion parameter but hosted with the other thresholds)',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 150
	},
	hdrag: {
		description:
			'drag threshold followed by the angular range (2 values) in which the drag effect can be triggered. Also defines the Act+Clash activation threshold for lockup when in single  switch  mode  (lockup  is  triggered  instead  of  drag  if  no  drag  sound  is  present  or  if outside of the drag angular range)',
		type: T.INTEGER_ARR,
		arrLength: 3,
		minValueArr: [0, -90, -90],
		maxValueArr: [1023, 90, 90],
		expandable: false
	},
	swing_flow: {
		description: 'swing  rate  flow  limiter.  Delay  during  which  swings  cannot  be furthermore triggered',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 2000
	},
	clash_flow: {
		description: 'clash  rate  flow  limiter.  Delay  during  which  clashes  cannot  be furthermore triggered',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 2000
	},
	spin_flow: {
		description: 'spin rate flow limiter. Delay during which spins cannot be furthermore triggered',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 2000
	},
	smooth_flow: {
		description: 'smoothswing rate flow limiter. Delay during which a smoothswing pair change cannot be triggered',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 2000
	},
	debug: {
		description:
			'enables the board debug which outputs extensive logs of the board activity such as configuration files parsing, sounds selected and played or file lookup.Debug should be enabled only when troubleshooting the board as the very verbose log will slow down the boot time. Also, it is recommended to turn debug off when configuring the board with RICE to avoid a verbose  echo from the board to the computer each time  a parameter is changed',
		type: T.BOOLEAN
	},
	logport: {
		description:
			'defines where the debug / logged information is sent. 0 is the USB serial port, 1 is the TTL (legacy RICE) serial port, 2 sends to both.Set to 1 or 2 if you want to use the legacy RICE or a Bluetooth module',
		type: T.ENUM,
		enum: Logport
	},
	vol: {
		description: 'digital volume setup. 0 mutes the sound output, 100is the maximum volume',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 100
	},
	menuvol: {
		description: 'digital volume of the vocal menu',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 100
	},
	menugest: {
		description:
			'Enables the use of saber motions to browse or select optionswhile in the vocal  menu: twist  the  hilt  left/right  moves  to  the  previous/next  bank  and  a  bang  will validate the currently selected font',
		type: T.BOOLEAN
	},
	trackgest: {
		description:
			'Enablesthe track selection by pressing the activator and twisting the hilt (if the font has local tracks or while in iSaber mode)',
		type: T.BOOLEAN
	},
	fontgest: {
		description:
			'Enablesloading  the  next  font  by  pressing  the  activator  and  clashing  the blade. If set to 0, while in single switch mode (switch=3) and using a ledstrip blade, that same action will toggle the blade PLI on/off',
		type: T.BOOLEAN
	},
	beep: {
		description: 'Sets the volume of the user interface (UI) sounds and the system beeps',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 100
	},
	muteoff: {
		description: 'Defines if the power-off sound will be played when the saber has Mute-on-the-Go engaged',
		type: T.BOOLEAN
	},
	muteb: {
		description: '',
		type: T.ENUM,
		enum: MuteBoot
	},
	switch: {
		description:
			'selects  if  the  saber  is  activated  by  a  normally-open  or  a  normally-closed switch.  Other  switches  might simply have an “inverted” logic (normally closed contact). When switchis  set  to 1,  the  saber  lights  up when  the  electrical  contact  of  the  switch  is closedand conversely when switchis set to 0. We recommend to use a momentary switch for the blade activation, (set switchto 2).Finally, the board also supports single switch with option 3',
		type: T.ENUM,
		enum: SwitchType
	},
	sleep: {
		description:
			'defines the time after which the sound board will move into sleep mode, turning the idle accent sequence off and saving some current. Parameter expressed in multiple of 1ms; 300,000 equates 300 seconds or 5 minutes. 0 to disable',
		type: T.INTEGER,
		minValue: 0,
		maxValue: Number.MAX_SAFE_INTEGER
	},
	deep: {
		description:
			'defines the time after which the sound board will move into deep sleep mode for saving power (<0.25 mA). Same as above.The deep sleep counter starts after the sleep mode has engaged. Sleep and Deep Sleep durations are cumulative',
		type: T.INTEGER,
		minValue: 0,
		maxValue: Number.MAX_SAFE_INTEGER
	},
	qkignite: {
		description:
			'enables igniting the blade right after waking up from deep sleep. The saber will boot normally but will skip the boot sound and will power the blade up',
		type: T.BOOLEAN
	},
	orient: {
		description:
			'defines  the  board  plane  orientation  in  the  hilt.  See  paragraph Board Orientationfor visual reference',
		type: T.ENUM,
		enum: BoardOrientation
	},
	valsnd: {
		description:
			'enables or disables the repeat of the selected soundbank description sound in the vocal menu (font.wav) after selecting it. When disabled, it saves time in the selection process.  Also, when disabled, the aux. switch confirmation process for rebooting the saber is disabled',
		type: T.BOOLEAN
	},
	offp: {
		description:
			'Anti power off protection (A-POP). To avoid accidentally powering off the saber, especially  when  using  a  momentary  button  for  activation,  we  added  a  power  off protection. When this parameter is set to 1, the user must press the activation button and confirm  with  the  auxiliary  button.  It  is  not  necessary  to  press  both  buttons  at  the  same time,  keep  the  activation  switch  pressed  first,  then  press  the  auxiliary  switch:  the  blade goes off',
		type: T.BOOLEAN
	},
	offd: {
		description:
			"Anti power-off delay. An alternative to A-POP™ defining how long the user must press the activation switch before the blade goes off. Values from 100 to 500 msare relevant and very efficient for just ensuring the saber won't be turned off accidentally. Set to zero if you use an anti-poweroff protection with parameter offp",
		type: T.INTEGER,
		minValue: 0,
		maxValue: 10000
	},
	onp: {
		description:
			'same idea as above but for ignition. If the parameter is set to 1, you must press the aux. button after pressing / cycling the activation switch in order to ignite the blade',
		type: T.BOOLEAN
	},
	ond: {
		description:
			'similar to off delay but for power on. This parameter isalso used as a time base for the double click event. Value of 20 to 50ms are relevant',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 10000
	},
	lockup: {
		description:
			'our module features an auxiliary switch to trigger additionalsound/visual effects. A short pressure on the switch generates the blaster effect (the saber blade stops a blaster ray), plays one of the blaster sounds. A longer pressure (maintained) triggers a blade  lockup  effect:  while  the  switch  is  pressed,  the  sound  lockup.wav  is  played  in  loop with some shimmering applied to the high-power LED. The parameter lockupspecifies the duration  of  the  delay  before  triggering  the  lockup  effect.  A  short  value  (100  to 200)  will trigger  the  effect  almost  immediately:  to  trigger  a  blaster  effect,  the  user  will  have  to release the button quickly. Conversely, a highervalue will leave more time to produce a blaster blocking feature',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 2000
	},
	loop: {
		description:
			'iSaber (audio player) parameter defining if the current track plays endlessly or if it the player will fetch another track once it ends',
		type: T.BOOLEAN
	},
	shuffle: {
		description: 'iSaber  (audio  player)  track  random  selection  parameter  (referto Sound Fx Selection Modes)',
		type: T.ENUM,
		enum: ShuffleMode
	},
	plilh: {
		description:
			'Power  level  indicator  range  in  millivolts.  Low  and  high  boundaries  of  the PLI defining a depleted / fullycharged battery. For a single li-ion cell, values likes 3400,4200 are common',
		type: T.INTEGER_ARR,
		arrLength: 2,
		minValueArr: [0, 0],
		maxValueArr: [10000, 10000],
		expandable: false
	},
	omnisense: {
		description:
			'beaconing period of Omnisabers, in ms. Used when Omnisabers is enabled. 0 disables beaconing. Beaconing will prevent the board from entering deep sleep in most cases as it keeps the board active (expected when using Omnisabers)',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 65000
	},
	spectrum_mode: {
		description:
			'Spectrum™ (color profile selection) mode. See the dedicated paragraph related to color profile browsing',
		type: T.ENUM,
		enum: SpectrumMode
	},
	spectrum_axis: {
		description: 'Spectrum™ (color profile selection) axis. 0 is pitch (up/down), 1 is roll (throttle)',
		type: T.ENUM,
		enum: SpectrumAxis
	},
	field: {
		description:
			'Reference magnetic field for the Magic Activation and SaberLock™. List of 3 numbers  defining  the  neutral  field.  See  the  dedicated  section  related  to  those  features further in this manual',
		type: T.INTEGER_ARR,
		arrLength: 3,
		minValueArr: [Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER],
		maxValueArr: [Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER],
		expandable: false
	},
	bladepower: {
		description:
			'Master  control  over  the  blade  power  in  %.  Allows  for  reducing  the power bill without touching the colors themselves',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 100
	},
	ledstrip: {
		description:
			'enables the ledstrip support by defining the number of pixel of the main blade. 0 leaves the board in high-power LED mode. The number of pixels accounts for just one ledstrip: a blade composed of two strips of 120 pixels back to back is defined as 120 pixels',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 180
	},
	auxstrip: {
		description: 'defines  the  number  of  pixels  of  the  secondary blade,  rendered  on  the ledstrip output',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 180
	},
	tridentstrip: {
		description:
			'enables the trident support in ledstrip mode. Defines the quillons length and the offset at which the normal/main blade starts. Positive value will establish the quillon on the main ledstrip output LS1, negative on LS2',
		type: T.INTEGER,
		minValue: -180,
		maxValue: 180
	},
	accentstrip: {
		description:
			' enables  the pixel  accents  (sequencer).  Also  defined  as  an  offset  on either the main blade & strip output LS1 (> 0), or LS2 (< 0)',
		type: T.INTEGER,
		minValue: -25,
		maxValue: 25
	},
	dragstrip: {
		description: 'defines the maximum size (pixels) of the drag tip effect',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 180
	},
	strippower: {
		description:
			'defines which channels {#1;#4} are used to control ledstrip power or leave them mirror the blade color',
		type: T.BITFIELD,
		bitLength: 4
	},
	wakeup: {
		description:
			'defines if the board can be woken  up (leaving sleep mode) by moving the hilt. When parameter is set to 0, and once the board has entered deep sleep mode, board can only be woken up by the blade ignition (activator) switch. When set to 1 or 2, motion of the hilt while the blade is off will reset the sleep timer which maintains the hilt in idle mode,  hence  playing  the  accent  led  sequence  matching  that  mode.  However,  when parameter is set to 1, motion on the hilt allows the board to leave the sleep state, while being  set  to  2,  motion  will  not  allow  the  board  to  leave  sleep  once  that  state  has  been reached',
		type: T.ENUM,
		enum: WakeupMode
	},
	randc: {
		description:
			'randomization (%) of the Flash on Clash color. Brings some color variance to FoC. Applies also to blaster inHB led mode (ledstrip blasters bolts have their own specific color parameters in ledstrip mode)',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 100
	},
	blastp: {
		description:
			'defines  if  the  blaster  priority  is  enabled  or  not.  This  decides  if  a  clash  can interrupt the blaster blocking sounds. Swings are always disabled during blasters. With the blaster  priority  on,  the  sound  of  the  blaster  must  finish  first  then  a  swing  might  be executed. This allows a nice choreography for blocking blaster rays using a long blaster file without   getting   the   sound   stopped   by   a   swing.   If   the   choreography   has  stressed movements that might trigger a clash, set the parameter to 1 so that no gestural events can  interrupt  the  blaster  sequence.  Conversely,  if  you  wish  a  blaster  sequence  to  be possibly interrupted by a clash, disable the parameter',
		type: T.BOOLEAN
	},
	random: {
		description:
			'set the selection mode for picking the clash, swing, spin and stab sounds. With value 0, random play is active: when a clash or swing occurs, the sound is randomly chosen in the available slots. With value 1, the sound is played in sequence (slot 1, then 2 etc.). With the random mode set to 2 we get a random mode that ensures no triggering the same sound twice in a sequence of (n) gestures (in 99% of cases)',
		type: T.ENUM,
		enum: RandomMode
	},
	pof: {
		description:
			'enables the Power on Force saber ignition technique (see paragraph “Motion activated ignition” for more detail). The parameter defines the motion threshold required to ignite the blade with a motion faking the use of the "Force" to activate the saber. 350-500 works usually fine',
		type: T.INTEGER,
		maxValue: 1023,
		minValue: 0
	},
	pom: {
		description:
			'power-on selection mode (4 modes total). When set to 0, board will use current angle of the hilt to choose the power on sound played when saber is ignited by a motion;mode 1 as well but with the poweronf(orce) special sound played when initiating a force triggered  (vertical  shake)  ignition(aka  PoF).  Mode  2  is  random  poweron  +  special PoFsound, mode 3 is random at all times',
		type: T.ENUM,
		enum: PowerOnMode
	},
	poi: {
		description:
			'power-on interrupt. When enabled, a sound fx (clash, swing, smoothswing) can interrupt the power-on sound before the ignition sequence is over.0 disables power-on sound  interruption,  motion  sounds  and  smoothswing  (if  enabled)  will  start  once  the power-on sound is over. With poi=1, motion is enabled once quick-on ignition has timed out  (qon,  per-font  parameter,  in  font_config.txt).  With  poi=2,  motion  is  enabled  when ignition starts',
		type: T.ENUM,
		enum: PowerOnInterruptMode
	},
	magic: {
		description: 'If  enabled  with  a  non-zero  value,  defines  the  magnetic threshold  to activate the saber',
		type: T.INTEGER,
		maxValue: 60000,
		minValue: 0
	},
	saberlock: {
		description:
			'Activation control of the saber using a magnetic field such as a magnetic ring. See further in this document to learn how to use this feature. SaberLock was born during the development of the Magic activation feature. The idea is similar and is also meant for cool demos and dramatic effects. The goal is to prevent the activation (or retraction) of the saber by anyone who doesn\'t have the required "Force" level. The parameter is used jointly with the magic threshold and defines the required Force activation level',
		type: T.ENUM,
		enum: SaberLock
	},
	blastm: {
		description:
			'Enables Blaster Move™. Blaster Move allows the ability to trigger a sequence of blaster sounds with the motion of the hilt. Only the first sound is triggered by a short press  on  the  aux.  switch,  then  further “swing-like” gestures will trigger more blaster sounds,  until  a  blaster  sound  ends,  or  if  a  clash  interrupts  the  sequences  (assuming blastp=0)',
		type: T.BOOLEAN
	},
	randb: {
		description: 'same as random above but for the selection of blaster blocking sounds',
		type: T.ENUM,
		enum: RandomMode
	},
	omnisabers: {
		description:
			'enables the Omnisabers feature which remotely transmits saber actions to a distant system via the serial port',
		type: T.BOOLEAN
	},
	tridenton: {
		description:
			'Defines the delay of the secondary blades, in the case of a cross-guard saber. Should remain < to the duration of the power on sound',
		type: T.INTEGER,
		maxValue: 3000,
		minValue: 0
	},
	tridentoff: {
		description:
			'Same  as  above  but  during  the  power-off.  If  zero,  there  is  no  Trident effect during the blade retraction and all channels fade out normally',
		type: T.INTEGER,
		maxValue: 3000,
		minValue: 0
	},
	tridentfx: {
		description:
			'Defines the mode of the Trident effect. Mode 0 is a simple delay, the normal (non-Trident) channel starts ramping up followed by the delayed channel(s) which start(s) after the duration specified by the tridentonparameter. All channels catch up together at the end of the power-on sound. In mode 1, thenon-delayed channels will ramp up to their max within the duration of tridenton, then the delayed Trident channels will ramp on their own until the end of the power-on sound. The 2 modes show little differences in behavior if the delay duration is close to the power-on sound duration but are interesting if the delay is < to half of the duration of the power-on sound',
		type: T.BOOLEAN
	},
	tridentflk: {
		description:
			'Trident channels have their flicker computed separately from the main channel(s) in real time, producing the effect that they "live on their own" and do not totally mirror the main blade behavior. In addition, they can also have their own flicker specified by this parameter, in order to produce a more dramatic flickering effect on the cross-guard',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	tridentquick: {
		description:
			'Similar to the quick on and quick off parameters, forces the trident channel(s)  ignition  time. In  multiple  of 1ms.  If  zero,  trident  channel(s)  will keep  ramping until the end of the power on sound',
		type: T.INTEGER,
		maxValue: 3000,
		minValue: 0
	},
	shmrd: {
		description:
			'duration of the shimmering effect of the high-power LED during a clash. Make sure this duration is not too much longer than the associated sound to keep a nice result',
		type: T.INTEGER,
		maxValue: 1000,
		minValue: 20
	},
	shmrp: {
		description: 'periodicity of the light bursts during the clash effect. A slow period will produce tight bursts',
		type: T.INTEGER,
		maxValue: 1000,
		minValue: 10
	},
	shmrr: {
		description:
			'random value applied to the periodicity of the light burst during a clash effect.  Allows  having  bursts  that  are  not  regularly  spaced  in  time  which  increases  the realism. For instance, a period shmrpof 20 and a random value shmrrof 10 will produce a period between two bursts varying between 20 and 30ms',
		type: T.INTEGER,
		maxValue: 1000,
		minValue: 10
	},
	'shmr%': {
		description:
			'shimmer  effect  depth.  Defines  how  the  shimmer  will  "dig"  the defined blade brightness  during  a  clash  or  a  lockup  effect.  A  static  flash  is  achieved  by leaving that value to 0. That parameter is actually composed of 2 numbers separated by a comma to define the range to apply to the brightness modification during the shimmer',
		type: T.INTEGER_ARR,
		arrLength: 2,
		maxValueArr: [100, 100],
		minValueArr: [0, 0],
		expandable: false
	},
	focd: {
		description: 'Flash on Clash™ (FoC™) duration',
		type: T.INTEGER,
		maxValue: 1000,
		minValue: 0
	},
	focp: {
		description:
			'periodicity  of  the  light  bursts  duringthe Flash on Clash™ effect. A lowperiod will produce tight bursts. Similar to the shmrpparameter',
		type: T.INTEGER,
		maxValue: 1000,
		minValue: 10
	},
	focr: {
		description:
			'random value applied to the periodicity of the light burst during a Flash on Clash™ effect. Similar to shmrrbut applied to FoC™',
		type: T.INTEGER,
		maxValue: 1000,
		minValue: 0
	},
	'foc%': {
		description:
			'Flash on Clash effect depth. Defines how deep the FoC brightness is dug for “on-top” (or non-mixed)  FoC  di(c)e.  Also  composed  of  2  comma-separated numbers, like the shimmer depth (see above)',
		type: T.INTEGER_ARR,
		arrLength: 2,
		maxValueArr: [100, 100],
		minValueArr: [0, 0],
		expandable: false
	},
	focl: {
		description: 'defines if the Flash on Clash™ is enabled during Blade Lockup effect',
		type: T.BOOLEAN
	},
	focb: {
		description: 'defines if the Flash on Clash™ is enabled during Blaster Blocking effect',
		type: T.BOOLEAN
	},
	focf1: {
		description: 'defines if the Flash on Clash™ is enabled during the Force effect',
		type: T.BOOLEAN
	},
	focf2: {
		description: 'defines if the Flash on Clash™ is enabled during the ForceClash™ effect',
		type: T.BOOLEAN
	},
	focf3: {
		description: 'defines if the Flash on Clash™ is enabled during the StabFx™ effect',
		type: T.BOOLEAN
	},
	qon: {
		description:
			'“quick-on”. Allows having the blade ignited in a specific amount of time rather  than  matching  the  duration  of  the  power  on  sound  andshould  not  exceed  the duration of any power on sounds. Almost mandatory for ledstrip blade to have a visually satisfying scrolling ignition',
		type: T.INTEGER,
		maxValue: 10000,
		minValue: 0
	},
	qoff: {
		description:
			'“quick-off”. Allows having the blade retracted in a specific amount of time rather  than  matching  the  duration  of  the  power  off  sound  and should  not  exceed  the duration of any power off sounds. Almost mandatory for ledstrip blade to have a visually satisfying scrolling retraction',
		type: T.INTEGER,
		maxValue: 10000,
		minValue: 0
	},
	fade: {
		description: 'color profile transition time.Must be lower than the color.wavsound duration',
		type: T.INTEGER,
		maxValue: 10000,
		minValue: 0
	},
	flks: {
		description:
			'speed  of  the  energy  variation  /  flickering  effect  of  the  blade.  A  high  value produces  a  damaged  saber  effect  while  a  small  value  generates  subtile  energy  changes. The value 0 disables the effect (static blade).Keep above 4 when using a ledstrip blade to avoid competing with the pixels (inherent) refresh rate',
		type: T.INTEGER,
		maxValue: 500,
		minValue: 1
	},
	flkd: {
		description:
			'depth (in %) of the energy fluctuation effect, i.e. the the range over which the LED brightness will be affected during the effect. A low value does not modify the energy very  much  while  a  high  value  «digs»  big gapsof  light  intensity.  To  be  used  with  the parameter flks.Set to 0 to obtain a static blade',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	flkon: {
		description: 'enables the blade flicker during power on',
		type: T.BOOLEAN
	},
	flkoff: {
		description: 'enables the blade flicker during power off',
		type: T.BOOLEAN
	},
	pulsel: {
		description: 'Pulse effect length (duration). Defines the ramp up/down time',
		type: T.INTEGER,
		maxValue: 10000,
		minValue: 0
	},
	pulsed: {
		description:
			'Pulse effect depth in %. 100% will dig the full range of the high power LED from max brightness to off',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	on_fade: {
		description:
			'enables the progressive fade in effect (combined with the ledstrip scrolling effect, this isthe ScrollFade™ feature).Applies to both HB leds and ledstrip blades.With off_fade, those are part of the blade profiles',
		type: T.BOOLEAN
	},
	off_fade: {
		description: 'same as above but during blade retraction',
		type: T.BOOLEAN
	},
	over_pon: {
		description:
			'enables or disables the power on fx takeover. Takeover shows exclusively the power on fx (ledstrip) during ramping or scrolling then transitions quickly to the blade fx.  With  takeover  off,  the  blade  fx  is  computed  and  mixed  along  with  the  power  on  fx, resulting on a less abrupt transition. Matter of taste but also power on style dependent',
		type: T.BOOLEAN
	},
	over_poff: {
		description: 'same as above, but for power off fx (ledstrip)',
		type: T.BOOLEAN
	},
	sbolt: {
		description:
			'list of 2 numbers, minimum and maximum size of the bolt impact. When set to 0,0 and in ledstrip mode, the bolt will make the whole blade to flash (legacy Flash on Clash)',
		type: T.INTEGER_ARR,
		arrLength: 2,
		maxValueArr: [179, 179],
		minValueArr: [0, 0],
		expandable: false
	},
	dbolt: {
		description: 'min and max duration of the bolt impacts (also now for HBleds)',
		type: T.INTEGER_ARR,
		arrLength: 2,
		maxValueArr: [3000, 3000],
		minValueArr: [0, 0],
		expandable: false
	},
	lbolt: {
		description:
			'impact location restrictionon the blade. For instance, {15,30} forces the bolt to be at 15 pixelsminimum from the bottomof the blade and 30 pixels away from the topof the blade',
		type: T.INTEGER_ARR,
		arrLength: 2,
		maxValueArr: [179, 179],
		minValueArr: [0, 0],
		expandable: false
	},
	fbolt: {
		description:
			'bolt impact energy distribution and edge fading / blending (in %). 100% will generate a solid flash all over the picked up size. 1% will have the center of the impact at the  maximum  brightness  which  will  fade  progressively  down  to  1%  as  you  move  to  the edges of the impact',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 1
	},
	rbolt: {
		description:
			'Bolt impact color randomization (in %). Provides some varianceon the bolt color from impact to impact. Affect separately each RGB component in order to really alter the hue and not just brightness',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	qbolt: {
		description:
			'Boltimpact speed (in %). Combined with the duration parameter (andbolt size  range)  it  defines  how  fast  it  will  travel  on  the  blade  when  using  certain  blaster  bolt styles (like wave). A ballpark is to see 100% speed as ensuring the bolt energy is sure to travel the whole blade during the bolt duration',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	unstable: {
		description: 'ledstrip special blade fx (0 = no fx)',
		type: T.ENUM,
		enum: UnstableFx
	},
	hcool: {
		description:
			'defines  how  to  temper  the  instability.  Low  values  make  the  blade return quicker to its stable while high values will make the blade overall more unstable / active',
		type: T.INTEGER,
		maxValue: 255,
		minValue: 0
	},
	lcool: {
		description:
			'defines  how  to  temper  the  instability.  Low  values  make  the  blade return quicker to its stable while high values will make the blade overall more unstable / active',
		type: T.INTEGER,
		maxValue: 255,
		minValue: 0
	},
	sparkf: {
		description: 'defines the instability force',
		type: T.INTEGER,
		maxValue: 255,
		minValue: 0
	},
	sparkd: {
		description: 'defines the instability spreading on the blade (0-100% of the blade length)',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	drift: {
		description:
			' defines  if  the  instability  contaminates  over  the  blade.  Some  fx  have  drift automatically enabled',
		type: T.BOOLEAN
	},
	mapb: {
		description:
			'defineswhat % of the instability will affect the blade color (dim). A high value will “dig” more in the blade color and will create darker spots',
		type: T.INTEGER,
		maxValue: 255,
		minValue: 0
	},
	mapc: {
		description:
			'defines what  %  of  the  instability  will  bring  up  the  FoC  color  where  the instability is. High value will produce brighter colored spots',
		type: T.INTEGER,
		maxValue: 255,
		minValue: 0
	},
	refreshfx: {
		description:
			'definesin ms the refresh rate (and computation) of the ledstrip fx. It’s supposed to be equal or greater to the flicker speed. Higher values (as a period) lead to slower refresh rate and can be used for slow effects like fire',
		type: T.INTEGER,
		maxValue: 500,
		minValue: 6
	},
	style_pon: {
		description: 'defines power-on fx style',
		type: T.ENUM,
		enum: StylePowerOn
	},
	style_poff: {
		description: 'defines power-off fx style',
		type: T.ENUM,
		enum: StylePowerOff
	},
	style_blaster: {
		description: 'defines blaster fx style',
		type: T.ENUM,
		enum: StyleBlaster
	},
	style_lockup: {
		description: 'defines lockup fx style',
		type: T.ENUM,
		enum: StyleLockup
	},
	style_flicker: {
		description: 'defines spatial flicker style',
		type: T.ENUM,
		enum: MappingBehavior
	},
	style_stab: {
		description: 'defines stab fx style',
		type: T.ENUM,
		enum: StyleStab
	},
	style_clash: {
		description: 'defines clash fx style',
		type: T.ENUM,
		enum: StyleClash
	},
	style_drag: {
		description: 'defines drag fx style',
		type: T.ENUM,
		enum: StyleDrag
	},
	pos_lockup: {
		description: 'defines localized lockup fx position',
		type: T.INTEGER,
		maxValue: 180,
		minValue: 0
	},
	size_lockup: {
		description: 'definelocalized lockup fx size',
		type: T.INTEGER,
		maxValue: 180,
		minValue: 0
	},
	mapping_color: {
		description: 'defines the fx color map (legacy vs. heat mapping)',
		type: T.ENUM,
		enum: MappingColor
	},
	mapping_unstable: {
		description: 'defines the dynamic behavior of the unstable fx',
		type: T.ENUM,
		enum: MappingBehavior
	},
	mapping_lockup: {
		description: 'defines the dynamic behavior of the lockup fx',
		type: T.ENUM,
		enum: MappingBehavior
	},
	mapping_flicker: {
		description: 'defines the dynamic behavior of the flicker fx',
		type: T.ENUM,
		enum: MappingBehavior
	},
	mapping_drag: {
		description: 'defines the dynamic behavior of the drag fx',
		type: T.ENUM,
		enum: MappingBehavior
	},
	ls1_timings: {
		description:
			'The first 3 numbers are the strip timing, referring tothe zero (state) timing, the one (state) timing and the latch timing in (most) strip  technical  datasheet.  The  NRZ  protocol  used to  drive  the  pixel  defines  the  datastream  with timings rather than logic levels (0 or 1). A zero is represented by a pulse of acertain duration, a one usually by a pulse duration of the double of the zero one. Finally, the strip is updated after the data line stays low for a certain time, called the latch',
		type: T.INTEGER_ARR,
		arrLength: 5,
		maxValueArr: [1000, 1000, 100, 1, 5],
		minValueArr: [1, 1, 1, 0, 1],
		expandable: false
	},
	ls2_timings: {
		description:
			'The first 3 numbers are the strip timing, referring tothe zero (state) timing, the one (state) timing and the latch timing in (most) strip  technical  datasheet.  The  NRZ  protocol  used to  drive  the  pixel  defines  the  datastream  with timings rather than logic levels (0 or 1). A zero is represented by a pulse of acertain duration, a one usually by a pulse duration of the double of the zero one. Finally, the strip is updated after the data line stays low for a certain time, called the latch',
		type: T.INTEGER_ARR,
		arrLength: 5,
		maxValueArr: [1000, 1000, 100, 1, 5],
		minValueArr: [1, 1, 1, 0, 1],
		expandable: false
	},
	'bolt#': {
		description: 'Blasterbolt timing',
		type: T.INTEGER_ARR,
		arrLength: 10,
		maxValueArr: [10000],
		minValueArr: [0],
		expandable: true
	},
	glyph: {
		description:
			'defines the accent LEDs pattern to be displayed when browsing the font from the menu. LED 1 to LED 8 left-to-right ordered, a 1 turns the corresponding LED on',
		type: T.BITFIELD,
		bitLength: 8
	},
	hum_gain: {
		description:
			'specific  mixing  parameter  for  the  hum  sound.  Can  be  used  to dynamically adjust the strength of the hum instead of editing the sound file',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	skip: {
		description:
			'skips  the  font  in  the  vocal  menu.  Allows  for  having  a  large  amount  of  font available on the SD card while reducing the selection in the menu (manual setting)',
		type: T.BOOLEAN
	},
	smooth_sharp: {
		description:
			'brings  softness  (<  1.0)  or  sharpness  (>  1.0)  to  the  mapping between the blade rotation and the smoothswing expressivity. A value of 1.5 adds a bit of exponential   mapping   (small   blademoves   are   amplified).   Values   >   1.0   make   the smoothswing engine more expressive even with small motions',
		type: T.INTEGER,
		maxValue: 4,
		minValue: 0.01
	},
	smooth_sens: {
		description:
			' smoothswing  overall  sensitivity  to  blade  rotation  (in  °/s). A  low valuewill  bring  the  smoothswing  effect  to  its  maximum  volume  with  a  small  motion.  A large value will require faster rotations to bring the volume to the maximum. Works with the sharpness parameter above',
		type: T.INTEGER,
		maxValue: 2000,
		minValue: 0
	},
	smooth_gain: {
		description: 'defines the volume of the smoothswing effect',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	smooth_dampen: {
		description:
			'defines the hum dampening (%) when the smoothswing pair is played. 75% means that the hum volume is reduced of 75%',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	'smooth_width#': {
		description:
			' angular  regions  (°)  in  which  the  high  and  low  pairs  transition and cross fade to produce the pitch effect. The first region is usually set to alow(er) value to start producing a pitch offset early with the beginning of the motion, while the second transition occurs close to a half spin (180°)',
		type: T.INTEGER,
		maxValue: 360,
		minValue: 0
	},
	smooth_physics: {
		description:
			'motion milestone threshold to tell the smoothswing engine to change of pair. Ahigh threshold tends to keep the current pair; a low value will produce more pair picking',
		type: T.INTEGER,
		maxValue: 1023,
		minValue: 0
	},
	smooth_mode: {
		description:
			'smoothswing  mode.  0  disables  the  smoothswing  engine  for  a smoothswing font. 1 is the standard smoothswing mode, 2 is the hybrid font (aka accent swing)  mode  for  which  regular  swing  and  spins  can  be  played  if  the  motion  exceeds  the hswingthreshold3.Mode 3 uses the blade orientation to control the mix between the hum pairs and the main hum to pseudo-replicate the former CF pitch shifting effect',
		type: T.ENUM,
		enum: SmoothSwingMode
	},
	start_blade: {
		description:
			'defines a specific blade profile to start with when this specific font is selected. Overrides the last used profile saved in prefs.txt.-1 disables the feature and let the stored value from prefs.txt be used at boot time',
		type: T.INTEGER,
		maxValue: 31,
		minValue: -1
	},
	start_color: {
		description:
			'defines a specific color profile to start with when this specific font is selected. Overrides the last used profile saved in prefs.txt.-1 disables the feature and let the stored value from prefs.txt be used at boot time',
		type: T.INTEGER,
		maxValue: 31,
		minValue: -1
	},
	hum_delay: {
		description:
			'(beta)  Changes  the  crossfade  mixing  point  of  the  hum  during  the ignition  sequence.  Defined  in  %  of  the  duration  of  the  (selected)  power-on  sound.  0% makes the hum starting to ramp from the beginning of the ignition sound. Getting close to 100%  will  make  the  ramping  coarser  obviously,  even  if  computed  on  a  sample-based timeline. To be used rather with POI = 0 or 1 for smooth swing fonts otherwise, motion to sound smoothswing will take over the hum vs. pairs dynamic mixing which will have very little sense with the point the hum delay tries to achieve',
		type: T.INTEGER,
		maxValue: 100,
		minValue: 0
	},
	oledflip: {
		description:
			'rotates the display 180° to adapt to the wiring or orientation in the hilt. 0 (or commented) is the regular orientation with the text being read normally with the screen wiring pads on the left side. 1 flips the orientation 180°',
		type: T.BOOLEAN
	},
	oledversion: {
		description:
			'defines if the firmware version is displayed at boot time (before the boot logo) and for how long (in ms)',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 4000
	},
	oledscreensaver: {
		description: 'delay after which the screen savers will be triggered on the OLED',
		type: T.INTEGER,
		maxValue: Number.MAX_SAFE_INTEGER,
		minValue: 0
	},
	oledcustom: {
		description:
			'custom  text  to  display  on  the  OLED  along  with  the firmware  version  screen  (boot  time  if  oledversion  is  enabled,  and  idle  time).  About  20 characters maximum',
		type: T.STRING
	},
	oledstop: {
		description: 'disables the oled from further use (for testing purpose)',
		type: T.BOOLEAN
	},
	pname: {
		description:
			'blade profile name for export to / from RICE or any configuration software, to ease browsing and retrieving which is which, while editing',
		type: T.STRING
	},
	used_profiles_color: {
		description: `this bit-field defines some masking to apply to some color
		profiles. This feature is beta and cannot be edited yet via RICE. Putting a zero in the field
		removes the color profile from the browsing (using spectrum or browsing). This provides
		an effective way to keep a large number of color profiles defined on the SD card while
		restricting its selection for a specific font.`,
		type: T.PROFILE,
		profileType: GroupType.color
	},
	used_profiles_blade: {
		description: `this bit-field defines some masking to apply to some blade
		profiles. This feature is beta and cannot be edited yet via RICE. Putting a zero in the field
		removes the blade profile from the browsing (using spectrum or browsing). This provides
		an effective way to keep a large number of blade profiles defined on the SD card while
		restricting its selection for a specific font.`,
		type: T.PROFILE,
		profileType: GroupType.profile
	}
};

/**
 * Dictionary of all the variables for a color config file
 */
export const COLOR_DICTIONARY: VariableDictionary = {
	color: {
		description: 'Blade color',
		type: T.COLOR
	},
	fcolor: {
		description: 'Flash on Clash color',
		type: T.COLOR
	},
	lcolor: {
		description: 'Lockup color',
		type: T.COLOR
	},
	bcolor: {
		description: 'Blaster Bolt color',
		type: T.COLOR
	},
	xcolor: {
		description: 'Crystal Chamber color',
		type: T.COLOR
	},
	ecolor: {
		description: 'FX (ledstrip) color',
		type: T.COLOR
	},
	kcolor: {
		description: 'Flicker color',
		type: T.COLOR
	},
	tcolor: {
		description: 'Stab color',
		type: T.COLOR
	},
	dcolor: {
		description: 'Drag color',
		type: T.COLOR
	},
	mcolor: {
		description: 'Flash on Clash mixing configuration',
		type: T.BITFIELD,
		bitLength: 4
	},
	tridentm: {
		description: 'Trident channles configuration',
		type: T.BITFIELD,
		bitLength: 4
	},
	cname: {
		description:
			'color profile name for export to / from RICE or any configuration software, to ease browsing and retrieving which is which, while editing',
		type: T.STRING
	}
};

/**
 * Dictionary of all the variables for a prefs config file
 */
export const PREFS_DICTIONARY: VariableDictionary = {
	profile: {
		description:
			'Separated  from  the  global  parameters,  the  blade  profiles  are  defined  in  individual  sections identified with a heading section token [profile=n].A profile groups all the blade or saber parameters that aren’t related to the color itself. The configuration file can host up to 32 blade profiles numbered from 0 to 31',
		type: T.INTEGER,
		minValue: 0,
		maxValue: 31
	},
	color: {
		description: 'Corresponding color profile being used for this font',
		type: T.INTEGER,
		maxValue: 31,
		minValue: 0
	}
};

/**
 * Default definition for an unknown variable
 */
export const UNKNOWN_DEFINITION: UnknownDefinition = {
	description: 'Variable is unknown to this app',
	type: T.UNKNOWN
};
