import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, Router, RouterStateSnapshot } from '@angular/router';
import { AppAlertsService } from '../services/app-alerts.service';
import { FileManagementService } from '../services/file-management.service';

/**
 * Get the list of systems.
 * If there are systems, navigate to the first one.
 * If there are no systems, show an error message.
 * @param _fileServ The file management service.
 * @param _route The router.
 * @param _alertServ The alert service.
 * @param route
 * @param state
 * @param fileServ
 * @param router
 * @param alertServ
 * @returns A promise that resolves when the list of systems has been retrieved.
 */
export const systemListResolver: ResolveFn<string[]> = (
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot,
	fileServ: FileManagementService = inject(FileManagementService),
	router: Router = inject(Router),
	alertServ: AppAlertsService = inject(AppAlertsService)
) => {
	let systemList: string[] = [];
	try {
		systemList = fileServ.getSystemList();
		if (systemList.length > 0) {
			console.log(`Found ${systemList.length} systems`);
			router.navigate(['systems', systemList[0]]);
		} else {
			console.log('Found no systems');
		}
	} catch (err) {
		alertServ.showErrorAlert(`Failed to get list of systems: ${err}`, err);
	}
	return systemList;
};
