import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
	selector: 'lsd-update-tester',
	templateUrl: './update-tester.component.html',
	styleUrls: ['./update-tester.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateTesterComponent {
	percentDownloaded = 0;

	async downloadProgress(): Promise<void> {
		return window.electronApi.debugTriggerDownloadProgress(this.percentDownloaded);
	}

	async updateAvailable(): Promise<void> {
		return window.electronApi.debugTriggerUpdateAvailable();
	}

	async updateDownloaded(): Promise<void> {
		return window.electronApi.debugTriggerUpdateDownloaded();
	}
}
