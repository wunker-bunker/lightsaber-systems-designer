/**
 * Alert types
 */
export enum AppAlertType {
	SUCCESS,
	ERROR,
	WARN
}

/**
 * Alert interface for constructing alerts to be displayed
 */
export interface AppAlert {
	/**
	 * Error object to be logged
	 */
	err?: any;

	/**
	 * Delay before hiding alert in milliseconds
	 */
	hideDelay?: number;

	/**
	 * Alert message to be displayed to user
	 */
	message: string;

	/**
	 * Alert type
	 * @see AppAlertType
	 */
	type: AppAlertType;
}
