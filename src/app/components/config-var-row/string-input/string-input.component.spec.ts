import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { StringInputComponent } from './string-input.component';

describe('StringInputComponent', () => {
	let component: StringInputComponent;
	let fixture: ComponentFixture<StringInputComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [StringInputComponent, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(StringInputComponent);
		component = fixture.componentInstance;
		component.input = testData.strValues[0];
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
