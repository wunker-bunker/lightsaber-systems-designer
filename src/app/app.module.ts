import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ColorAlphaModule } from 'ngx-color/alpha';
import { ColorChromeModule } from 'ngx-color/chrome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppAlertsComponent } from './components/app-alerts/app-alerts.component';
import { AppLoaderComponent } from './components/app-loader/app-loader.component';
import { ConfigListComponent } from './components/config-list/config-list.component';
import { BitfieldInputComponent } from './components/config-var-row/bitfield-input/bitfield-input.component';
import { BooleanInputComponent } from './components/config-var-row/boolean-input/boolean-input.component';
import { ColorInputComponent } from './components/config-var-row/color-input/color-input.component';
import { ConfigVarRowComponent } from './components/config-var-row/config-var-row.component';
import { EnumerationInputComponent } from './components/config-var-row/enumeration-input/enumeration-input.component';
import { IntegerArrayInputComponent } from './components/config-var-row/integer-array-input/integer-array-input.component';
import { IntegerInputComponent } from './components/config-var-row/integer-input/integer-input.component';
import { ProfileSelectorInputComponent } from './components/config-var-row/profile-selector-input/profile-selector-input.component';
import { StringInputComponent } from './components/config-var-row/string-input/string-input.component';
import { FontListComponent } from './components/font-list/font-list.component';
import { GroupConfigListComponent } from './components/group-config-list/group-config-list.component';
import { AddSoundComponent } from './components/modals/add-sound/add-sound.component';
import { DeleteConfirmComponent } from './components/modals/delete-confirm/delete-confirm.component';
import { EditFontNameComponent } from './components/modals/edit-font-name/edit-font-name.component';
import { EditProfileNameComponent } from './components/modals/edit-profile-name/edit-profile-name.component';
import { ImportSystemNameComponent } from './components/modals/import-system-name/import-system-name.component';
import { InvalidVariablesComponent } from './components/modals/invalid-variables/invalid-variables.component';
import { PreferencesComponent } from './components/modals/preferences/preferences.component';
import { SoundRowComponent } from './components/sound-row/sound-row.component';
import { SystemListComponent } from './components/system-list/system-list.component';
import { SystemPageComponent } from './components/system-page/system-page.component';
import { TitleBarComponent } from './components/title-bar/title-bar.component';
import { UpdateIndicatorComponent } from './components/update-indicator/update-indicator.component';
import { UpdateTesterComponent } from './components/update-tester/update-tester.component';
import { LSDFontModule } from './modules/lsd-font.module';
import { ColorToRgbStylePipe } from './pipes/color-to-rgb-style.pipe';
import { ColorToWhiteStylePipe } from './pipes/color-to-white-style.pipe';
import { ColorValToAlphaPipe } from './pipes/color-val-to-alpha.pipe';
import { ColorValToRgbPipe } from './pipes/color-val-to-rgb.pipe';
import { ColorValuePipe } from './pipes/color-value.pipe';
import { EnumForPipe } from './pipes/enum-for.pipe';
import { IsEmptyPipe } from './pipes/is-empty.pipe';
import { IsTypeOfPipe } from './pipes/is-type-of.pipe';
import { OrderByPipe } from './pipes/order-by.pipe';
import { TrackIndexByLengthPipe } from './pipes/track-index-by-length.pipe';

@NgModule({
	declarations: [
		AppComponent,
		IsTypeOfPipe,
		IsEmptyPipe,
		OrderByPipe,
		SystemListComponent,
		SystemPageComponent,
		ConfigListComponent,
		FontListComponent,
		ConfigVarRowComponent,
		IntegerInputComponent,
		IntegerArrayInputComponent,
		ColorInputComponent,
		BitfieldInputComponent,
		BooleanInputComponent,
		StringInputComponent,
		EnumerationInputComponent,
		EnumForPipe,
		TrackIndexByLengthPipe,
		GroupConfigListComponent,
		ColorValToRgbPipe,
		ColorValToAlphaPipe,
		AppLoaderComponent,
		ColorToRgbStylePipe,
		ColorToWhiteStylePipe,
		UpdateIndicatorComponent,
		ImportSystemNameComponent,
		PreferencesComponent,
		UpdateTesterComponent,
		ColorValuePipe,
		InvalidVariablesComponent,
		AppAlertsComponent,
		EditFontNameComponent,
		DeleteConfirmComponent,
		EditProfileNameComponent,
		SoundRowComponent,
		TitleBarComponent,
		AddSoundComponent,
		ProfileSelectorInputComponent
	],
	imports: [
		AppRoutingModule,
		BrowserModule,
		CommonModule,
		ColorAlphaModule,
		ColorChromeModule,
		FormsModule,
		NgSelectModule,
		NgbModule,
		LSDFontModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		DragDropModule,
		MatExpansionModule,
		ScrollingModule
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
