/**
 * Value types for the variables in the config files
 * @enum {string}
 */
export const VALUE_TYPE = {
	STRING: 'str',
	INTEGER: 'int',
	INTEGER_ARR: 'int_arr',
	FLOAT: 'float',
	COLOR: 'color',
	BOOLEAN: 'bool',
	BITFIELD: 'bit',
	ENUM: 'emum',
	PROFILE: 'profile',
	UNKNOWN: 'unknown'
} as const;

/**
 * Type for string values
 */
export type ValueTypeString = typeof VALUE_TYPE.STRING;

/**
 * Type for integer values
 */
export type ValueTypeInteger = typeof VALUE_TYPE.INTEGER;

/**
 * Type for integer array values
 */
export type ValueTypeIntegerArray = typeof VALUE_TYPE.INTEGER_ARR;

/**
 * Type for float values
 */
export type ValueTypeFloat = typeof VALUE_TYPE.FLOAT;

/**
 * Type for color values
 */
export type ValueTypeColor = typeof VALUE_TYPE.COLOR;

/**
 * Type for boolean values
 */
export type ValueTypeBoolean = typeof VALUE_TYPE.BOOLEAN;

/**
 * Type for bitfield values
 */
export type ValueTypeBitfield = typeof VALUE_TYPE.BITFIELD;

/**
 * Type for enum values
 */
export type ValueTypeEnum = typeof VALUE_TYPE.ENUM;

/**
 * Type for profile values
 */
export type ValueTypeProfile = typeof VALUE_TYPE.PROFILE;

/**
 * Type for unknown values
 */
export type ValueTypeUknown = typeof VALUE_TYPE.UNKNOWN;

/**
 * Type for all value types
 */
export type ValueType = (typeof VALUE_TYPE)[keyof typeof VALUE_TYPE];
