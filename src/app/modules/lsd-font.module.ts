import { NgModule } from '@angular/core';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

/**
 * Module for the font awesome icons.
 * @see https://fontawesome.com/docs/web/use-with/angular
 */
@NgModule({
	imports: [FontAwesomeModule],
	exports: [FontAwesomeModule]
})
export class LSDFontModule {
	/**
	 * Constructor for the font awesome module.
	 * Injects the font awesome icon library.
	 * @param library
	 */
	constructor(library: FaIconLibrary) {
		// add icons to the library for convenient access in other components
		library.addIconPacks(fas);
		library.addIconPacks(far);
		library.addIcons(faGitlab);
	}
}
