# Lightsaber Systems Designer

## Hello There!

The **Lightsaber Systems Designer** is a cross-platform application (Mac, Windows, and Linux) designed to help you manage all your CFX lightsaber configurations.

![Main App Page](resources/images/screenshot.png)

## Download

You can download the latest release [here](https://gitlab.com/wunker-bunker/lightsaber-systems-designer/-/releases)!

## Features

### General

-   Automatic updates
-   Light and dark UI modes
-   Export/Import
    -   Export all files to SD (slow)
    -   Export Configs to SD (fast)
    -   Import all files from SD (slow)
    -   Import Configs from SD (fast)

![Import and Export](resources/images/expanded.png)

-   Validation and auto fix of varibale values

![Fix Variables](resources/images/fix_variables.PNG)

## Fonts and Sounds

-   Adding and removing fonts
-   Reordering of fonts
    -   Updates file paths automatically
    -   Updates preference file to keep selected blade and color profiles

![Reorder Fonts](resources/images/reorder_fonts.png)

-   Listening to font sounds and tracks

![Listen to sounds](resources/images/sounds.PNG)

-   Renaming sounds

![Rename sounds](resources/images/rename_sounds.png)

### Profiles

-   Color picker for blade colors

![Color Picker](resources/images/color_picker.PNG)

-   Naming blade and color profiles
-   Adding and removing profiles
-   Reordering profiles
    -   Updates preference file to keep selected blade and color profiles
    -   Updates `used_profiles_*` variables to keep your selection for each font

## Future Features

-   R.I.C.E. support (yes on all platforms)
    -   Full remote control over saber
    -   Live value changing
-   Config sync over R.I.C.E.
    -   Choose system to sync to when you connect
    -   Live update config files on computer as you tune your saber
    -   Only need to export to SD Card when adding new fonts
-   Font management
    -   Font library
-   Blade and Color Profile management
    -   Profile library
-   Better LED sequence editor

## Issues

If you run into a potential bug, please [submit an issue](https://gitlab.com/wunker-bunker/lightsaber-systems-designer/-/issues/new) or [email me](mailto:incoming+wunker-bunker-lightsaber-systems-designer-25433126-issue-@incoming.gitlab.com).
