 __ _           _                                                                              
/ _\ |__   ___ | |_ ___                                                                        
\ \| '_ \ / _ \| __/ _ \                                                                       
_\ \ | | | (_) | || (_) |                                                                      
\__/_| |_|\___/ \__\___/                                                                       
                                                                                               
                                    _  __             _      _                ___   ___  __    
  __ _    ___  ___  _   _ _ __   __| |/ _| ___  _ __ | |_   | |__  _   _     / _ \ / __\/ _\   
 / _` |  / __|/ _ \| | | | '_ \ / _` | |_ / _ \| '_ \| __|  | '_ \| | | |   / /_\// /   \ \    
| (_| |  \__ \ (_) | |_| | | | | (_| |  _| (_) | | | | |_   | |_) | |_| |  / /_\\/ /___ _\ \   
 \__,_|  |___/\___/ \__,_|_| |_|\__,_|_|  \___/|_| |_|\__|  |_.__/ \__, |  \____/\____/ \__/   
                                                                   |___/                       

"Shoto"
Created by Rob Petkau (Madcow) / Genesis Custom Sabers, November 2011. Updated for CF7 Dec. 2013.
Sound font specifically for short lightsabers, tonfa etc.

If you have received this font for free, 
please consider a small donation toward the work it
takes to engineer a CF sound font.

You can donate easily at 

http://genesiscustomsabers.com/?page_id=18


More of my fonts are available at www.saberfont.com