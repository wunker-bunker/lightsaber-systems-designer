import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BitfieldValue } from 'src/app/classes/config-value/bitfield-value';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-bitfield-input',
	templateUrl: './bitfield-input.component.html',
	styleUrls: ['./bitfield-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class BitfieldInputComponent implements BaseInputComponent {
	@Input() input!: BitfieldValue;
	@Input() name = '';

	change(index: number, newValue: boolean) {
		this.input.value.mutate(value => {
			value[index] = newValue;
		});
	}
}
