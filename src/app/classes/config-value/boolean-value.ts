import { computed } from '@angular/core';
import { ValueType, ValueTypeBoolean } from 'src/app/enums/value-type.enum';
import { BaseDefinition, BooleanDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class BooleanValue extends ConfigValue<boolean, ValueTypeBoolean, BooleanDefinition> {
	readonly toString = computed(() => (this.value() ? '1' : '0'));
	readonly valid = computed(() => true);

	constructor(value: string, def: BooleanDefinition) {
		super(value, def, def.type);
	}

	static isBooleanValue(value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>): value is BooleanValue {
		return value instanceof BooleanValue;
	}

	autoFixValue(): void {
		return;
	}

	protected parseValue(strVal: string): boolean {
		return strVal === '1';
	}
}
