import { Pipe, PipeTransform } from '@angular/core';
import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { Color } from '../models/color.model';

/**
 * Pipe to convert a color to a white style string.
 * Used in the color picker to display the current color.
 * @example <div [style.background]="color | colorToWhiteStyle"></div>
 */
@Pipe({
	name: 'colorToWhiteStyle',
	pure: true
})
export class ColorToWhiteStylePipe implements PipeTransform {
	/**
	 * Transform a color to a white style string.
	 * Uses the white value of the color.
	 * @param value The color to transform.
	 * @returns The white style string.
	 */
	transform(value: Color): string {
		if (!value) {
			return '';
		}
		return `rgba(255,255,255,${value.white / COLOR_VAL_MAX})`;
	}
}
