import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe to check if a value is of a certain type.
 * @example <div *ngIf="value | isTypeOf:'string'">Value is a string</div>
 */
@Pipe({
	name: 'isTypeOf',
	pure: true
})
export class IsTypeOfPipe implements PipeTransform {
	/**
	 * Check if a value is of a certain type.
	 * @param value The value to check.
	 * @param typestr The type to check for.
	 * @returns True if the value is of the given type, false otherwise.
	 */
	transform(value: unknown, typestr: string): boolean {
		return typeof value === typestr;
	}
}
