import { Injectable } from '@angular/core';
import { LightsaberSystem } from '../classes/lightsaber-system';

/**
 * A service for managing lightsaber systems.
 */
@Injectable({
	providedIn: 'root'
})
export class LightsaberSystemsService {
	private _systems: Map<string, LightsaberSystem> = new Map();

	/**
	 * Adds a lightsaber system to the service.
	 * @param system The lightsaber system to add.
	 */
	add(system: LightsaberSystem): void {
		this._systems.set(system.name, system);
	}

	/**
	 * Clears all lightsaber systems from the service.
	 */
	clear(): void {
		this._systems.clear();
	}

	/**
	 * Deletes a lightsaber system from the service.
	 * @param system The lightsaber system name to delete.
	 */
	delete(system: string): void {
		this._systems.delete(system);
	}

	/**
	 * Gets a lightsaber system from the service.
	 * @param name The name of the lightsaber system to get.
	 * @returns The lightsaber system with the specified name.
	 */
	get(name: string): LightsaberSystem | undefined {
		return this._systems.get(name);
	}

	/**
	 * Checks if the service has a lightsaber system with the specified name.
	 * @param name The name of the lightsaber system to check for.
	 * @returns True if the service has a lightsaber system with the specified name, otherwise false.
	 */
	has(name: string): boolean {
		return this._systems.has(name);
	}

	/**
	 * Gets the number of lightsaber systems in the service.
	 * @returns The number of lightsaber systems in the service.
	 */
	size(): number {
		return this._systems.size;
	}
}
