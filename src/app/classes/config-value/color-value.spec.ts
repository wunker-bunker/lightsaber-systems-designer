import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { Color } from 'src/app/models/color.model';
import { ColorDefinition } from 'src/app/models/variable-dictionary.model';
import { ColorValue } from './color-value';

describe('ColorValue', () => {
	const def: ColorDefinition = {
		description: 'desc',
		type: VALUE_TYPE.COLOR
	};
	const validStr = '123,432,234,0';
	const validObj: Color = {
		red: 123,
		green: 432,
		blue: 234,
		white: 0
	};
	const validMaxObj: Color = {
		red: 1023,
		green: 1023,
		blue: 1023,
		white: 1023
	};
	const validMinStr = '0,0,0,0';
	const validMinObj: Color = {
		red: 0,
		green: 0,
		blue: 0,
		white: 0
	};
	const invalidStr = '0,1,-1,1024';
	const invalidObj: Color = {
		red: 0,
		green: 1,
		blue: 0,
		white: 1023
	};
	const garbage = 'aljkdbnoab';

	it('should create an instance', () => {
		const val = new ColorValue(validStr, def);
		expect(val).toBeTruthy();
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toEqual(validObj);
		expect(val.toString()).toEqual(validStr);

		const inval = new ColorValue(invalidStr, def);
		expect(inval).toBeTruthy();
		expect(inval.type).toEqual(def.type);
		expect(inval.valid()).toBeTrue();
		expect(inval.value()).toEqual(invalidObj);
		expect(inval.toString()).not.toEqual(invalidStr);

		const garbval = new ColorValue(garbage, def);
		expect(garbval).toBeTruthy();
		expect(garbval.type).toEqual(def.type);
		expect(garbval.valid()).toBeTrue();
		expect(garbval.value()).toEqual(validMinObj);
		expect(garbval.toString()).toEqual(validMinStr);
	});

	it('should print toString', () => {
		expect(new ColorValue(validStr, def).toString()).toEqual(validStr);
	});

	describe('should validate', () => {
		const val = new ColorValue(validStr, def);

		it('valid when values in inclusive range 0-1023', () => {
			val.value.set(validObj);
			expect(val.valid()).toBeTrue();
			val.value.set(validMaxObj);
			expect(val.valid()).toBeTrue();
			val.value.set(validMinObj);
			expect(val.valid()).toBeTrue();
		});

		it('invalid when values outside inclusive range 0-1023', () => {
			val.value.set({
				...validObj,
				red: -1
			});
			expect(val.valid()).toBeFalse();
			val.value.set({
				...validObj,
				green: 1024
			});
			expect(val.valid()).toBeFalse();
			val.value.set({
				...validObj,
				white: NaN
			});
			expect(val.valid()).toBeFalse();
		});
	});

	it('should do nothing for auto fix values', () => {
		const val = new ColorValue(validStr, def);
		val.autoFixValue();
	});
});
