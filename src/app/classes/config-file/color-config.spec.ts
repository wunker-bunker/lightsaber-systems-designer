import * as Diff from 'diff';
import { FILE_HEADER } from 'src/app/constants/strings';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { colorConfigFile } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { ConfigGroup } from '../config-group';
import { ColorConfig } from './color-config';

describe('ColorConfig', () => {
	let colorGroups: ConfigGroup[];
	const colorFile = colorConfigFile.filePath;

	beforeAll(() => {
		testData.generateTestData();
		colorGroups = testData.configGroups.filter(group => group.name === 'color');
	});

	it('should create an instance', () => {
		const config = new ColorConfig([], colorGroups, colorFile);
		expect(config).toBeTruthy();
		expect(config.type).toEqual(ConfigFileType.COLORS);
		expect(config.groups()).toEqual(colorGroups);
		expect(config.variables()).toEqual([]);
		expect(config.filePath).toEqual(colorFile);
		expect(config.dirty()).toBeFalse();
		expect(config.valid()).toBeTrue();
		const configString = config.toString();
		const testConfigString = FILE_HEADER + colorConfigFile.fileText + '\n';

		// Use diff package to find line differences between the two strings
		const lines1 = configString.split('\n');
		const lines2 = testConfigString.split('\n');

		const diff = Diff.diffArrays(lines1, lines2);

		const differences = diff.filter(part => part.added || part.removed);

		console.log(differences);

		// expect(differences).toEqual([]);

		expect(configString).toEqual(testConfigString); // This line is no longer needed
	});

	it('should return group by id', () => {
		const config = new ColorConfig([], colorGroups, colorFile);
		expect(config.getColorProfileById(0)?.id()).toEqual(0);
		expect(config.getColorProfileById(1)?.id()).toEqual(1);
	});
});
