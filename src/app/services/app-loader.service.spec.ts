import { TestBed } from '@angular/core/testing';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { AppLoaderService } from './app-loader.service';

describe('AppLoaderService', () => {
	let service: AppLoaderService;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		service = TestBed.inject(AppLoaderService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should set state', async () => {
		service.setState(true, 'test');
		expect(service.state()).toBeTrue();
		expect(service.message()).toEqual('test');
		expect(service.subMessage()).toEqual('');
	});

	it('should set sub message', async () => {
		service.setSubMessage('test');
		expect(service.subMessage()).toEqual('test');
	});
});
