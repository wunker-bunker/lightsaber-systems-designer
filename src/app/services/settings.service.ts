import { effect, Injectable, signal } from '@angular/core';
import { AppChannel } from 'src/common/app-channel.enum';
import { APP_SETTINGS } from 'src/common/app-settings.enum';
import { UiThemes } from 'src/common/ui-themes.enum';
import { LIBRARY_DIR_NAME } from '../constants/strings';

@Injectable({
	providedIn: 'root'
})
export class SettingsService {
	readonly channel;
	readonly libraryLocation;
	readonly theme;

	constructor() {
		const channel = window.settings.getSync('channel') ?? AppChannel.LATEST;
		const libraryLocation =
			window.settings.getSync('location.library') ?? window.path.join(window.os.homedir(), LIBRARY_DIR_NAME);

		const theme = window.settings.getSync('theme') ?? UiThemes.LIGHT;

		this.channel = signal(channel);
		this.libraryLocation = signal(libraryLocation);
		this.theme = signal(theme);

		effect(() => {
			window.settings.setSync(APP_SETTINGS.CHANNEL, this.channel());
			window.electronApi.setUpdateChannel();
		});
		effect(() => window.settings.setSync(APP_SETTINGS.LIBRARY_LOCATION, this.libraryLocation()));
		effect(() => {
			window.settings.setSync(APP_SETTINGS.THEME, this.theme());
			document.body.className = this.theme();
			window.electronApi.changeTheme();
		});
	}
}
