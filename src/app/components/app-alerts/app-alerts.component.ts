import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AppAlertType } from 'src/app/models/app-alert.model';
import { AppAlertsService } from 'src/app/services/app-alerts.service';

@Component({
	selector: 'lsd-app-alerts',
	templateUrl: './app-alerts.component.html',
	styleUrls: ['./app-alerts.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppAlertsComponent {
	readonly T = AppAlertType;

	constructor(public alertServ: AppAlertsService) {}
}
