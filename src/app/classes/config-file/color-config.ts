import { computed } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ConfigGroup } from '../config-group';
import { ConfigVar } from '../config-var';
import { BaseFile } from './base-file';
import { ConfigFile } from './config-file';

/**
 * Class for color config files.
 */
export class ColorConfig extends ConfigFile {
	readonly dirty;

	/**
	 * Creates a new instance of the ColorConfig class.
	 * @param variables The variables in the file.
	 * @param colors The colors in the file.
	 * @param filePath The path to the file.
	 */
	constructor(variables: ConfigVar[], colors: ConfigGroup[], filePath: string) {
		super(ConfigFileType.COLORS, variables, colors, filePath);
		this.dirty = computed(() => this._dirtyVars());
	}

	static isColorConfig(config: BaseFile): config is ColorConfig {
		return config instanceof ColorConfig;
	}

	/**
	 * Gets the color profile by id.
	 * @param id The id of the color profile.
	 * @returns The color profile.
	 */
	getColorProfileById(id: number): ConfigGroup | undefined {
		return this.groups().find((color: ConfigGroup) => color.id() === id);
	}
}
