import { computed } from '@angular/core';
import { ValueType, ValueTypeIntegerArray } from 'src/app/enums/value-type.enum';
import { BaseDefinition, IntegerArrayDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class IntegerArrayValue extends ConfigValue<number[], ValueTypeIntegerArray, IntegerArrayDefinition> {
	readonly toString;

	readonly valid;

	readonly validArr;

	constructor(value: string, def: IntegerArrayDefinition) {
		super(value, def, def.type);
		this.toString = computed(() =>
			this.value().reduce(
				(str: string, val: number, index: number) =>
					`${str}${val}${index + 1 === this.value().length ? '' : ','}`,
				''
			)
		);
		this.validArr = computed(() =>
			this.value().map(
				(val: number, index: number): boolean =>
					val >= this.definition.minValueArr[this.definition.expandable ? 0 : index] &&
					val <= this.definition.maxValueArr[this.definition.expandable ? 0 : index]
			)
		);
		this.valid = computed(
			() =>
				this.validArr().reduce((valid: boolean, val: boolean) => valid && val, true) &&
				(this.value().length === this.definition.arrLength ||
					(this.definition.expandable && this.definition.arrLength >= this.value().length))
		);
	}

	static isIntegerArrayValue(
		value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>
	): value is IntegerArrayValue {
		return value instanceof IntegerArrayValue;
	}

	autoFixValue(): void {
		let newValue: number[] =
			this.definition.expandable && this.value().length <= this.definition.arrLength
				? new Array(this.value().length)
				: new Array(this.definition.arrLength);
		newValue.fill(0);
		newValue = newValue.map((value: number, index: number) => {
			if (!this.validArr()[index]) {
				if (this.value()[index] !== undefined) {
					value =
						this.value()[index] < this.definition.minValueArr[this.definition.expandable ? 0 : index]
							? this.definition.minValueArr[this.definition.expandable ? 0 : index]
							: this.value()[index] > this.definition.maxValueArr[this.definition.expandable ? 0 : index]
							? this.definition.maxValueArr[this.definition.expandable ? 0 : index]
							: value;
				} else {
					value = this.definition.minValueArr[this.definition.expandable ? 0 : index];
				}
			} else {
				value = this.value()[index];
			}
			return value;
		});
		this.value.set(newValue);
	}

	protected parseValue(strVal: string): number[] {
		return strVal.split(',').map(val => +val);
	}
}
