import { COLOR_VAL_MAX, COLOR_VAL_MIN } from '../classes/config-value/color-value';
import { ColorValToAlphaPipe } from './color-val-to-alpha.pipe';

describe('ColorValToAlphaPipe', () => {
	const pipe = new ColorValToAlphaPipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('transform color value to alpha RGBA', () => {
		expect(
			pipe.transform({
				red: COLOR_VAL_MAX,
				green: COLOR_VAL_MIN,
				blue: COLOR_VAL_MAX,
				white: COLOR_VAL_MIN
			})
		).toEqual({
			r: 255,
			g: 255,
			b: 255,
			a: COLOR_VAL_MIN / COLOR_VAL_MAX
		});
	});
});
