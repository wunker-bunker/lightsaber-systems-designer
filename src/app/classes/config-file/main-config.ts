import { computed } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ConfigGroup } from '../config-group';
import { ConfigVar } from '../config-var';
import { BaseFile } from './base-file';
import { ConfigFile } from './config-file';

/**
 * Class for main config files.
 */
export class MainConfig extends ConfigFile {
	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 * @returns True if the file has been modified since it was last saved, false otherwise.
	 */
	readonly dirty;

	/**
	 * Creates a new instance of the MainConfig class.
	 * @param variables The variables in the file.
	 * @param profiles The profiles in the file.
	 * @param filePath The path to the file.
	 */
	constructor(variables: ConfigVar[], profiles: ConfigGroup[], filePath: string) {
		super(ConfigFileType.MAIN, variables, profiles, filePath);
		this.dirty = computed(() => this._dirtyVars());
	}

	static isMainConfig(config: BaseFile): config is MainConfig {
		return config instanceof MainConfig;
	}

	/**
	 * Gets the blade profile by id.
	 * @param id The id of the blade profile.
	 * @returns The blade profile group.
	 */
	getBladeProfileById(id: number): ConfigGroup | undefined {
		return this.groups().find((profile: ConfigGroup) => profile.id() === id);
	}
}
