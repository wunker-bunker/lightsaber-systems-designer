import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IntegerArrayValue } from 'src/app/classes/config-value/integer-array-value';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-integer-array-input',
	templateUrl: './integer-array-input.component.html',
	styleUrls: ['./integer-array-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class IntegerArrayInputComponent implements BaseInputComponent {
	@Input() input!: IntegerArrayValue;
	@Input() name = '';

	addValue(): void {
		if (this.input?.definition.expandable) {
			this.input.value.mutate(value => value.push(this.input.definition.minValueArr[0]));
		}
	}

	removeValue(index: number): void {
		if (this.input?.definition.expandable && index >= 0 && index < this.input.value().length) {
			this.input.value.mutate(value => value.splice(index, 1));
		}
	}

	setValue(index: number, value: number): void {
		if (index >= 0 && index < this.input.value().length) {
			this.input.value.mutate(values => (values[index] = value));
		}
	}
}
