import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, computed, EventEmitter, Input, Output, Signal } from '@angular/core';
import { ConfigGroup } from 'src/app/classes/config-group';
import { showDragCursor } from 'src/app/constants/functions';
import { GroupType } from 'src/app/enums/group-types';
import { ModalService } from 'src/app/services/modal.service';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-group-config-list',
	templateUrl: './group-config-list.component.html',
	styleUrls: ['./group-config-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupConfigListComponent {
	@Output() deleteGroup: EventEmitter<ConfigGroup> = new EventEmitter();
	@Output() groupOrderChange: EventEmitter<readonly number[]> = new EventEmitter();
	@Input() groups!: Signal<ConfigGroup[]>;

	readonly GT = GroupType;

	expandedIndex = -1;
	groupIdOrder = computed(() => this.groups().map(group => group.id()));

	constructor(private _modalServ: ModalService, public settings: SettingsService) {}

	async confirmDelete(group: ConfigGroup): Promise<void> {
		try {
			await this._modalServ.deleteConfirm(group.groupName());
		} catch (err) {
			console.log(`Cancelled delete font ${group.groupName()}`);
			return;
		}

		console.log(`Deleting ${group.groupName()}`);
		this.deleteGroup.emit(group);
	}

	dragStart(): void {
		showDragCursor(true);
		this.expandedIndex = -1;
	}

	drop({ previousIndex, currentIndex }: CdkDragDrop<number[]>): void {
		showDragCursor(false);
		const order = this.groupIdOrder();
		moveItemInArray(order, previousIndex, currentIndex);
		this.groupOrderChange.emit(order);
	}

	async openEditNameModal(group: ConfigGroup): Promise<void> {
		const value = group.groupName();

		try {
			group.setGroupName(await this._modalServ.editProfileName(value));
		} catch (err) {
			console.log(`Closed edit name modal for ${group.id}-${value}`);
		}
	}

	async openInvalidVariablesModal(group: ConfigGroup): Promise<void> {
		try {
			await this._modalServ.invalidVariables(group.variables().filter(confVar => !confVar.configValue().valid()));
		} catch {
			console.log('Closed Invalid Variable Modal');
		}
	}
}
