import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BooleanValue } from 'src/app/classes/config-value/boolean-value';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-boolean-input',
	templateUrl: './boolean-input.component.html',
	styleUrls: ['./boolean-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class BooleanInputComponent implements BaseInputComponent {
	@Input() input!: BooleanValue;
	@Input() name = '';
}
