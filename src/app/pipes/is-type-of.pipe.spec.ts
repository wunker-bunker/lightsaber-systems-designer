import { IsTypeOfPipe } from './is-type-of.pipe';

describe('IsTypeOfPipe', () => {
	const pipe = new IsTypeOfPipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('should transform and check if the type of the value matches', () => {
		expect(pipe.transform('test', 'string')).toBeTrue();
		expect(pipe.transform(123, 'number')).toBeTrue();
		expect(pipe.transform(true, 'boolean')).toBeTrue();

		expect(pipe.transform('test', 'number')).toBeFalse();
		expect(pipe.transform(123, 'boolean')).toBeFalse();
		expect(pipe.transform(true, 'string')).toBeFalse();
	});
});
