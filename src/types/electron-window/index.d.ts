import { OpenDialogOptions, OpenDialogReturnValue } from 'electron';
import { functions } from 'electron-log';
import { ProgressInfo } from 'electron-updater';
import {
	access,
	constants,
	copy,
	existsSync,
	lstatSync,
	mkdirsSync,
	move,
	moveSync,
	opendirSync,
	PathLike,
	readdir,
	readdirSync,
	readFileSync,
	rm,
	watch,
	writeFile,
	writeFileSync
} from 'fs-extra';
import { homedir } from 'os';
import { join } from 'path';
import { AppSettings, SettingsReturnType } from 'src/common/app-settings.enum';

export interface ElectronApi {
	changeTheme: () => Promise<void>;
	closeApp: () => Promise<void>;
	debugTriggerDownloadProgress: (percentDownloaded: number) => Promise<void>;
	debugTriggerUpdateAvailable: () => Promise<void>;
	debugTriggerUpdateDownloaded: () => Promise<void>;
	getPlatform: () => NodeJS.Platform;
	isServed: () => Promise<boolean>;
	maximizeApp: () => Promise<void>;
	minimizeApp: () => Promise<void>;
	onAppMaximized: (callback: (maximized: boolean) => void) => void;
	onDownloadProgress: (callback: (progress: ProgressInfo) => void) => void;
	onUpdateAvailable: (callback: () => void) => void;
	onUpdateDownloaded: (callback: () => void) => void;
	openExternal: (url: string) => Promise<void>;
	openDialog: (openOptions: OpenDialogOptions) => Promise<OpenDialogReturnValue>;
	openDirecotry: (path: PathLike) => Promise<void>;
	openUpdateDebugger: () => Promise<void>;
	readyToCheckForUpdates: () => Promise<void>;
	restartAndInstall: () => Promise<void>;
	restoreApp: () => Promise<void>;
	setUpdateChannel: () => Promise<void>;
	startUpdateDownload: () => Promise<void>;
}

export interface SettingsApi {
	getSync: <T extends AppSettings>(key: T) => SettingsReturnType[T];
	hasSync: (key: AppSettings) => boolean;
	setSync: <T extends AppSettings>(key: T, value: SettingsReturnType[T]) => void;
}

export interface FsApi {
	access: typeof access;
	constants: typeof constants;
	copy: typeof copy;
	existsSync: typeof existsSync;
	lstatSync: typeof lstatSync;
	mkdirSync: typeof mkdirsSync;
	move: typeof move;
	moveSync: typeof moveSync;
	opendirSync: typeof opendirSync;
	readFileSync: typeof readFileSync;
	readdir: typeof readdir;
	readdirSync: typeof readdirSync;
	rm: typeof rm;
	watch: typeof watch;
	writeFile: typeof writeFile;
	writeFileSync: typeof writeFileSync;
}

export interface OsApi {
	homedir: typeof homedir;
}

export interface PathApi {
	join: typeof join;
}

export interface ElectronLogApi {
	functions: typeof functions;
}

declare global {
	interface Window {
		electronApi: ElectronApi;
		electronLog: ElectronLogApi;
		fs: FsApi;
		os: OsApi;
		path: PathApi;
		settings: SettingsApi;
	}
}
