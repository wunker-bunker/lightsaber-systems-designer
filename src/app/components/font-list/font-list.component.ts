import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
	ChangeDetectionStrategy,
	Component,
	computed,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Signal,
	WritableSignal
} from '@angular/core';
import { FontConfig } from 'src/app/classes/config-file/font-config';
import { showDragCursor } from 'src/app/constants/functions';
import { ModalService } from 'src/app/services/modal.service';

@Component({
	selector: 'lsd-font-list',
	templateUrl: './font-list.component.html',
	styleUrls: ['./font-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FontListComponent implements OnInit {
	@Output() addFontEvent: EventEmitter<void> = new EventEmitter();
	@Output() deleteFont: EventEmitter<FontConfig> = new EventEmitter();
	@Input() fontConfigs!: Signal<FontConfig[]>;
	@Output() fontOrderChange: EventEmitter<number[]> = new EventEmitter();
	@Input() selectFont!: WritableSignal<number>;
	@Output() selectFontChange: EventEmitter<number> = new EventEmitter();

	fontIdOrder!: Signal<number[]>;

	constructor(private _modalServ: ModalService) {}

	async confirmDelete(font: FontConfig): Promise<void> {
		try {
			await this._modalServ.deleteConfirm(font.name());
		} catch (err) {
			console.log(`Cancelled delete font ${font.name()}`);
			return;
		}

		console.log(`Deleting ${font.name()}`);
		this.deleteFont.emit(font);
	}

	dragStart(): void {
		showDragCursor(true);
	}

	drop({ previousIndex, currentIndex }: CdkDragDrop<number[]>): void {
		showDragCursor(false);
		const order = this.fontIdOrder();
		moveItemInArray(order, previousIndex, currentIndex);
		this.fontOrderChange.emit(order);
	}

	ngOnInit(): void {
		this.fontIdOrder = computed(() => this.fontConfigs().map(font => font.id()));
	}

	async openEditNameModal(font: FontConfig): Promise<void> {
		try {
			font.name.set(await this._modalServ.editFontName(font.name()));
		} catch (err) {
			console.log(`Closed edit name modal for ${font.id()}-${font.name()}`);
		}
	}

	async openInvalidVariablesModal(font: FontConfig): Promise<void> {
		try {
			await this._modalServ.invalidVariables(font.variables().filter(confVar => !confVar.configValue().valid()));
		} catch {
			console.log('Closed Invalid Variable Modal');
		}
	}
}
