import { GroupType } from 'src/app/enums/group-types';
import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { Bitfield } from 'src/app/models/bitfield.model';
import { ProfileDefinition } from 'src/app/models/variable-dictionary.model';
import { ProfileSelectorValue } from './profile-selector-value';

describe('ProfileSelectorValue', () => {
	const def: ProfileDefinition = {
		description: 'desc',
		type: VALUE_TYPE.PROFILE,
		profileType: GroupType.color
	};
	const validStr = '11111111111111111111111111111111';
	const validObj: Bitfield = new Array(ProfileSelectorValue.MAX_PROFILES).fill(true);
	const longStr = '111111111111111111111111111111111';
	const longObj = new Array(ProfileSelectorValue.MAX_PROFILES + 1).fill(true);

	it('should create an instance', () => {
		const val = new ProfileSelectorValue(validStr, def);
		expect(val).toBeTruthy();
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toEqual(validObj);

		const longVal = new ProfileSelectorValue(longStr, def);
		expect(longVal).toBeTruthy();
		expect(longVal.type).toEqual(def.type);
		expect(longVal.valid()).toBeTrue();
		expect(longVal.value()).toEqual(validObj);
	});

	it('should print toString', () => {
		expect(new ProfileSelectorValue(validStr, def).toString()).toEqual(validStr);
	});

	describe('should validate', () => {
		const val = new ProfileSelectorValue('00000', def);
		it('valid when field is correct length', () => {
			val.value.set([...validObj]);
			expect(val.valid()).toBeTrue();
		});

		it('invalid when field is incorrect length', () => {
			val.value.set(longObj);
			expect(val.valid()).toBeFalse();
		});
	});

	it('should do nothing for auto fix value', () => {
		const val = new ProfileSelectorValue(validStr, def);
		val.autoFixValue();
	});
});
