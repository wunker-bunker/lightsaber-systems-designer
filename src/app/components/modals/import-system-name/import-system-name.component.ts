import { ChangeDetectionStrategy, Component, Signal } from '@angular/core';
import {
	AbstractControl,
	UntypedFormBuilder,
	UntypedFormGroup,
	ValidationErrors,
	ValidatorFn,
	Validators
} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SYSTEM_NAME } from 'src/app/constants/regex';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-import-system-name',
	templateUrl: './import-system-name.component.html',
	styleUrls: ['./import-system-name.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportSystemNameComponent {
	importForm: UntypedFormGroup = this._fb.group({
		name: ['', [Validators.pattern(SYSTEM_NAME), this.existingSystemNameValidator()]]
	});
	systemList?: Signal<readonly string[] | undefined>;

	constructor(
		public activeModal: NgbActiveModal,
		public settings: SettingsService,
		private _fb: UntypedFormBuilder
	) {}

	get name(): AbstractControl | null {
		return this.importForm.get('name');
	}

	existingSystemNameValidator(): ValidatorFn {
		return (control: AbstractControl): ValidationErrors | null => {
			if (!this.systemList) {
				return null;
			}

			return this.systemList()?.includes(control.value) ? { systemExists: true } : null;
		};
	}

	printForm(): void {
		if (this.name?.errors) {
			console.warn(this.name.errors);
		}
	}

	setName(name: string): void {
		this.importForm.setValue({ name });
	}
}
