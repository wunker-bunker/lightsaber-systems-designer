import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UpdateService, UpdateState } from 'src/app/services/update.service';

@Component({
	selector: 'lsd-update-indicator',
	templateUrl: './update-indicator.component.html',
	styleUrls: ['./update-indicator.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateIndicatorComponent {
	@Input() disableRestart = false;
	readonly STATE = UpdateState;

	constructor(public updateServ: UpdateService) {}

	async restartInstall(): Promise<void> {
		return window.electronApi.restartAndInstall();
	}

	async startDownload(): Promise<void> {
		return window.electronApi.startUpdateDownload();
	}
}
