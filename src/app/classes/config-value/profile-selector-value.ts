import { computed } from '@angular/core';
import { BITMASK_VALUE } from 'src/app/constants/regex';
import { ValueType, ValueTypeProfile } from 'src/app/enums/value-type.enum';
import { Bitfield } from 'src/app/models/bitfield.model';
import { BaseDefinition, ProfileDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class ProfileSelectorValue extends ConfigValue<Bitfield, ValueTypeProfile, ProfileDefinition> {
	static readonly MAX_PROFILES = 32;

	readonly toString;

	readonly valid;

	constructor(value: string, def: ProfileDefinition) {
		super(value, def, def.type);
		this.toString = computed(() => {
			let str = '';
			this.value().forEach(bit => (str += bit ? '1' : '0'));
			return str;
		});
		this.valid = computed(() => this.value().length <= ProfileSelectorValue.MAX_PROFILES);
	}

	static isProfileSelectorValue(
		value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>
	): value is ProfileSelectorValue {
		return value instanceof ProfileSelectorValue;
	}

	autoFixValue(): void {
		return;
	}

	protected parseValue(strVal: string): Bitfield {
		const value = BITMASK_VALUE.test(strVal)
			? strVal
					.slice(0, ProfileSelectorValue.MAX_PROFILES)
					.split('')
					.map(bit => bit === '1')
			: (new Array(ProfileSelectorValue.MAX_PROFILES).fill(false) as Bitfield);
		while (value.length < ProfileSelectorValue.MAX_PROFILES) {
			value.push(false);
		}
		return value;
	}
}
