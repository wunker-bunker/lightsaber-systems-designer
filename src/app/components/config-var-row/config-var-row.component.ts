import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ConfigGroup } from 'src/app/classes/config-group';
import { ConfigValueClasses } from 'src/app/classes/config-value/config-value';
import { ConfigVar } from 'src/app/classes/config-var';
import { VALUE_TYPE } from 'src/app/enums/value-type.enum';

export interface BaseInputComponent {
	input?: ConfigValueClasses;
	name: string;
}

@Component({
	selector: 'lsd-config-var-row',
	templateUrl: './config-var-row.component.html',
	styleUrls: ['./config-var-row.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigVarRowComponent {
	@Input() confVar!: ConfigVar;
	@Input() groups: readonly ConfigGroup[] = [];

	readonly T = VALUE_TYPE;
}
