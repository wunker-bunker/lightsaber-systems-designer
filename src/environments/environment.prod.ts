import { Environment } from 'src/app/models/environment.model';

export const environment: Environment = {
	production: true,
	environment: 'PROD',
	prefsName: 'preferences.json'
};
