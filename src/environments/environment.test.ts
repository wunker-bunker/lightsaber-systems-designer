import { Environment } from 'src/app/models/environment.model';

export const environment: Environment = {
	environment: 'TEST',
	production: false,
	prefsName: 'preferences-test.json'
};
