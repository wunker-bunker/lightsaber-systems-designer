import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IntegerArrayValue } from 'src/app/classes/config-value/integer-array-value';
import { ConfigVar } from 'src/app/classes/config-var';
import { TrackIndexByLengthPipe } from 'src/app/pipes/track-index-by-length.pipe';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { IntegerArrayInputComponent } from './integer-array-input.component';

describe('IntegerArrayInputComponent', () => {
	let component: IntegerArrayInputComponent;
	let fixture: ComponentFixture<IntegerArrayInputComponent>;
	let variable: ConfigVar;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [IntegerArrayInputComponent, TrackIndexByLengthPipe, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		const v = testData.configVars.find(confVar => {
			const val = confVar.configValue();

			if (!val || !IntegerArrayValue.isIntegerArrayValue(val)) {
				return false;
			}
			return val.definition.expandable;
		});
		const val = v?.configValue();

		if (!v || !val || !IntegerArrayValue.isIntegerArrayValue(val)) {
			throw new Error('No expandable integer array config var found in test data');
		}

		variable = v;
		fixture = TestBed.createComponent(IntegerArrayInputComponent);
		component = fixture.componentInstance;
		component.input = val;
		component.name = variable.name;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should add value', () => {
		const origLength = component.input.value().length;
		component.addValue();
		expect(component.input.value().length).toEqual(origLength + 1);
		expect(component.input.value()[origLength]).toEqual(component.input.definition.minValueArr[0]);
	});

	it('should remove value', () => {
		const origLength = component.input.value().length;
		const secondVal = component.input.value()[1];
		component.removeValue(0);
		expect(component.input.value().length).toEqual(origLength - 1);
		expect(component.input.value()[0]).toEqual(secondVal);
	});
});
