import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalService } from 'src/app/services/modal.service';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { SoundRowComponent } from './sound-row.component';

describe('SoundRowComponent', () => {
	let component: SoundRowComponent;
	const modalServ: jasmine.SpyObj<ModalService> = jasmine.createSpyObj('ModalService', ['deleteConfirm']);
	let fixture: ComponentFixture<SoundRowComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [SoundRowComponent, ...unitTestDeclarations],
		providers: [...unitTestProviders, { provide: ModalService, useValue: modalServ }]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SoundRowComponent);
		component = fixture.componentInstance;
		component.sound = testData.soundFiles[0];
		spyOn(component.deleteSound, 'emit');
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	describe('should test deleteSoundFile', () => {
		it('when user accepts confirm', async () => {
			modalServ.deleteConfirm.and.resolveTo();

			await component.deleteSoundFile();
			expect(modalServ.deleteConfirm).toHaveBeenCalledWith(component.sound.soundName);
			expect(component.deleteSound.emit).toHaveBeenCalledWith(component.sound);
		});

		it('when user rejects confirm', async () => {
			modalServ.deleteConfirm.and.rejectWith('closed');

			await component.deleteSoundFile();
			expect(modalServ.deleteConfirm).toHaveBeenCalledWith(component.sound.soundName);
			expect(component.deleteSound.emit).not.toHaveBeenCalled();
		});
	});
});
