/**
 * Wakeup mode
 */
export enum WakeupMode {
	SWITCH_ONLY = 0,
	MOTION_WAKE,
	MOTION_NO_WAKE
}
