import { Injectable, Signal } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OpenDialogOptions, OpenDialogReturnValue } from 'electron/main';
import { ConfigVar } from '../classes/config-var';
import { AddSoundComponent } from '../components/modals/add-sound/add-sound.component';
import { DeleteConfirmComponent } from '../components/modals/delete-confirm/delete-confirm.component';
import { EditFontNameComponent } from '../components/modals/edit-font-name/edit-font-name.component';
import { EditProfileNameComponent } from '../components/modals/edit-profile-name/edit-profile-name.component';
import { ImportSystemNameComponent } from '../components/modals/import-system-name/import-system-name.component';
import { InvalidVariablesComponent } from '../components/modals/invalid-variables/invalid-variables.component';
import { PreferencesComponent } from '../components/modals/preferences/preferences.component';

/**
 * A service for managing modals.
 */
@Injectable({
	providedIn: 'root'
})
export class ModalService {
	constructor(private _modal: NgbModal) {}

	/**
	 * Opens a dialog to choose a sound name based on the available names.
	 * @param availableNames The available names.
	 * @returns The chosen name.
	 * @throws If the user cancels the dialog.
	 */
	async chooseSoundName(availableNames: string[]): Promise<string> {
		const modalRef: NgbModalRef = this._modal.open(AddSoundComponent, {
			centered: true,
			animation: true
		});

		const modalComponent: AddSoundComponent = modalRef.componentInstance;
		modalComponent.availableNames = availableNames;

		const { name } = await modalRef.result;
		return name;
	}

	/**
	 * Opens a dialog to confirm a delete action.
	 * @param name The name of the item to delete.
	 * @returns A promise that resolves when the user confirms the delete action.
	 * @throws If the user cancels the dialog.
	 */
	async deleteConfirm(name: string): Promise<void> {
		const modalRef: NgbModalRef = this._modal.open(DeleteConfirmComponent, {
			centered: true,
			animation: true
		});

		const modalComponent: DeleteConfirmComponent = modalRef.componentInstance;
		modalComponent.name = name;

		return modalRef.result;
	}

	/**
	 * Opens a dialog to change the name of a font.
	 * @param oldName The old name of the font.
	 * @returns The new name of the font.
	 * @throws If the user cancels the dialog.
	 */
	async editFontName(oldName = ''): Promise<string> {
		const modalRef: NgbModalRef = this._modal.open(EditFontNameComponent, {
			centered: true,
			animation: true
		});

		const modalComponent: EditFontNameComponent = modalRef.componentInstance;
		modalComponent.setName(oldName);

		const { name } = await modalRef.result;
		return name;
	}

	/**
	 * Opens a dialog to change the name of a profile.
	 * @param oldName The old name of the profile.
	 * @returns The new name of the profile.
	 * @throws If the user cancels the dialog.
	 */
	async editProfileName(oldName: string): Promise<string> {
		const modalRef: NgbModalRef = this._modal.open(EditProfileNameComponent, {
			centered: true,
			animation: true
		});

		const modalComponent: EditProfileNameComponent = modalRef.componentInstance;
		modalComponent.setName(oldName);

		const { name } = await modalRef.result;
		return name;
	}

	/**
	 * Opens a dialog displaying all the variables that are invalid.
	 * The user can use this to fix the variables manually or automatically.
	 * @param variables The invalid variables.
	 * @returns A promise that resolves when the user closes the dialog.
	 * @throws If the user cancels the dialog.
	 */
	async invalidVariables(variables: ConfigVar[]): Promise<void> {
		const modalRef: NgbModalRef = this._modal.open(InvalidVariablesComponent, {
			centered: true,
			size: 'lg',
			animation: true,
			scrollable: true
		});

		const modalComponent: InvalidVariablesComponent = modalRef.componentInstance;
		modalComponent.variables = variables;

		return modalRef.result;
	}

	/**
	 * Opens the OS's native open dialog to choose a directory.
	 * @param title The title of the dialog.
	 * @returns The chosen file.
	 * @throws If the user cancels the dialog.
	 */
	openDirectory(title = 'Open a directory'): Promise<string> {
		return this.openDialog({
			title,
			properties: ['openDirectory']
		});
	}

	/**
	 * Opens the OS's native open dialog to choose a wav file.
	 * @param isTrack Whether the file is a track or a sound.
	 * @returns The chosen file.
	 * @throws If the user cancels the dialog.
	 */
	openSoundFile(isTrack: boolean): Promise<string> {
		return this.openDialog({
			title: `Choose a ${isTrack ? 'track' : 'sound'} to open`,
			buttonLabel: 'Add',
			properties: ['openFile'],
			filters: [
				{
					extensions: ['wav'],
					name: 'Wav Files'
				}
			]
		});
	}

	/**
	 * Opens the preferences dialog.
	 * @returns A promise that resolves when the user closes the dialog.
	 * @throws If the user cancels the dialog.
	 */
	async preferences(): Promise<void> {
		const modalRef = this._modal.open(PreferencesComponent, {
			centered: true,
			size: 'lg',
			animation: true
		});

		const prefs = modalRef.componentInstance as PreferencesComponent;
		prefs.openDirectory = title => this.openDirectory(title);

		return modalRef.result;
	}

	/**
	 * Opens a dialog to edit the name of a system.
	 * @param systemList The current list of system names.
	 * @param oldName The old name of the system.
	 * @returns The new name of the system.
	 * @throws If the user cancels the dialog.
	 */
	async systemName(systemList: Signal<readonly string[]>, oldName = ''): Promise<string> {
		const modalRef: NgbModalRef = this._modal.open(ImportSystemNameComponent, {
			centered: true,
			animation: true
		});

		const modalComponent: ImportSystemNameComponent = modalRef.componentInstance;
		modalComponent.systemList = systemList;
		modalComponent.setName(oldName);

		const { name } = await modalRef.result;
		return name;
	}

	/**
	 * Opens the OS's native open dialog given the options.
	 * @param openOptions The options for the open dialog.
	 * @returns The chosen file.
	 * @throws If the user cancels the dialog.
	 */
	private async openDialog(openOptions: OpenDialogOptions): Promise<string> {
		const {
			canceled,
			filePaths: [directoryPath]
		}: OpenDialogReturnValue = await window.electronApi.openDialog(openOptions);

		if (canceled) {
			throw new Error('canceled open dialog');
		}

		return directoryPath;
	}
}
