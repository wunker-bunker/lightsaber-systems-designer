import { computed, signal, WritableSignal } from '@angular/core';
import { compact, forIn, groupBy, keys, last, mapValues, pickBy, sortBy, without } from 'lodash';
import { SOUND_DICT } from '../dictionaries/sounds.dict';
import { COLOR_DICTIONARY, MAIN_DICT } from '../dictionaries/variable.dict';
import { ConfigFileType } from '../enums/config-file-type.enum';
import { GroupType } from '../enums/group-types';
import { UsedProfiles } from '../enums/used-profiles';
import { ConfigGroupJSON } from '../models/light-saber-system.json';
import { MultiSoundDefinition } from '../models/sound-dictionary.model';
import { BaseFile } from './config-file/base-file';
import { ColorConfig } from './config-file/color-config';
import { ConfigFile } from './config-file/config-file';
import { FontConfig } from './config-file/font-config';
import { LedConfig } from './config-file/led-config';
import { MainConfig } from './config-file/main-config';
import { PrefsConfig } from './config-file/prefs-config';
import { SpecialConfig } from './config-file/special-config';
import { ConfigGroup } from './config-group';
import { IntegerValue } from './config-value/integer-value';
import { ProfileSelectorValue } from './config-value/profile-selector-value';
import { SoundFile } from './sound-file';

type OrderMap = { [index: number]: number };

/**
 * Class representing a lightsaber system
 */
export class LightsaberSystem {
	readonly colorConfig!: WritableSignal<ColorConfig>;

	readonly dirty;

	readonly fontConfigs: WritableSignal<FontConfig[]> = signal([]);
	readonly ledConfigs: WritableSignal<LedConfig[]> = signal([]);
	readonly mainConfig!: WritableSignal<MainConfig>;
	readonly prefConfig!: WritableSignal<PrefsConfig>;
	readonly sounds: WritableSignal<SoundFile[]> = signal([]);
	readonly specialConfig!: WritableSignal<SpecialConfig>;

	readonly valid;

	constructor(public name: string, public path: string, configs: BaseFile[], sounds: SoundFile[]) {
		// Iterate through the configs and assign them to the correct property
		for (const config of configs) {
			switch (config.type) {
				case ConfigFileType.COLORS:
					if (config instanceof ColorConfig) {
						this.colorConfig = signal(config);
					}
					break;
				case ConfigFileType.FONT:
					if (config instanceof FontConfig) {
						this.fontConfigs.mutate(value => value.push(config));
					}
					break;
				case ConfigFileType.LEDS:
					if (config instanceof LedConfig) {
						this.ledConfigs.mutate(value => value.push(config));
					}
					break;
				case ConfigFileType.MAIN:
					if (config instanceof MainConfig) {
						this.mainConfig = signal(config);
					}
					break;
				case ConfigFileType.PREF:
					if (config instanceof PrefsConfig) {
						this.prefConfig = signal(config);
					}
					break;
				case ConfigFileType.SPECIAL:
					if (config instanceof SpecialConfig) {
						this.specialConfig = signal(config);
					}
					break;
				default:
					break;
			}
		}

		if (
			!this.mainConfig ||
			!this.colorConfig ||
			!this.prefConfig ||
			!this.specialConfig ||
			!this.fontConfigs().length
		) {
			throw new Error('Invalid system');
		}

		// Sort the font and led configs by id and validate
		this.fontConfigs.set(sortBy(this.fontConfigs(), value => value.id()));
		this.ledConfigs.set(sortBy(this.ledConfigs(), value => value.id()));
		this.sounds.set(sounds);
		this.dirty = computed(
			() =>
				!!this.mainConfig().dirty() ||
				this.colorConfig().dirty() ||
				this.prefConfig().dirty() ||
				this.specialConfig().dirty() ||
				this.fontConfigs().reduce((dirty: boolean, font: FontConfig) => dirty || font.dirty(), false) ||
				this.ledConfigs().reduce((dirty: boolean, led: LedConfig) => dirty || led.dirty(), false)
		);
		this.valid = computed(
			() =>
				!!this.mainConfig().valid() &&
				this.colorConfig().valid() &&
				this.prefConfig().valid() &&
				this.specialConfig().valid() &&
				this.fontConfigs().reduce((valid: boolean, font: FontConfig) => valid && font.valid(), true) &&
				this.ledConfigs().reduce((valid: boolean, led: LedConfig) => valid && led.valid(), true)
		);
	}

	static isLightsaberSystem(system: unknown): system is LightsaberSystem {
		return system instanceof LightsaberSystem;
	}

	/**
	 * Adds a new blade profile to the system
	 * @param profile a JSON representation of the profile to add
	 */
	addBladeProfile(profile: ConfigGroupJSON): void {
		// Create the new profile
		const id = this.mainConfig().groups().length;
		const newConfigGroup = ConfigGroup.fromJSON(
			{
				...profile,
				id
			},
			MAIN_DICT
		);

		// Add the new profile to the end of the list and re-order
		this.mainConfig().groups.mutate(value => value.push(newConfigGroup));
		this.orderBladeProfiles();
	}

	/**
	 * Adds a new color profile to the system
	 * @param profile a JSON representation of the profile to add
	 */
	addColorProfile(profile: ConfigGroupJSON): void {
		// Create the new profile
		const id = this.colorConfig().groups().length;
		const newConfigGroup = ConfigGroup.fromJSON(
			{
				...profile,
				id
			},
			COLOR_DICTIONARY
		);

		// Add the new profile to the end of the list and re-order
		this.colorConfig().groups.mutate(value => value.push(newConfigGroup));
		this.orderColorProfiles();
	}

	/**
	 * Copies the configs from another system
	 * @param system the system to copy configs from
	 */
	copyConfigsFrom(system: LightsaberSystem): void {
		if (!this.sameFontStructureAs(system)) {
			throw new Error("Can't copy configs due to structure differnece");
		}

		this.mainConfig().groups.set(system.mainConfig().groups());
		this.mainConfig().variables.set(system.mainConfig().variables());
		this.colorConfig().groups.set(system.colorConfig().groups());
		this.colorConfig().variables.set(system.colorConfig().variables());
		this.prefConfig().groups.set(system.prefConfig().groups());
		this.prefConfig().variables.set(system.prefConfig().variables());
		this.specialConfig().text.set(system.specialConfig().text());
		this.fontConfigs().forEach((font: FontConfig) => {
			font.groups.set(system.getFontConfigById(font.id())?.groups() ?? []);
			font.variables.set(system.getFontConfigById(font.id())?.variables() ?? []);
		});
		this.ledConfigs().forEach((led: LedConfig) => led.text.set(system.getLedConfigById(led.id())?.text() ?? ''));
		this.markAsSaved();
	}

	/**
	 * Deletes a blade profile from the system
	 * @param profile the profile to delete
	 */
	deleteBladeProfile(profile: ConfigGroup): void {
		this.deleteProfile(profile, this.mainConfig(), GroupType.profile);
		this.orderBladeProfiles();
	}

	/**
	 * Deletes a color profile from the system
	 * @param profile the profile to delete
	 */
	deleteColorProfile(profile: ConfigGroup): void {
		this.deleteProfile(profile, this.colorConfig(), GroupType.color);
		this.orderColorProfiles();
	}

	/**
	 * Gets the available sound names for a given font id
	 * @param id the font id
	 * @param excludeDefName the name of a sound definition to exclude
	 * @returns an array of sound names
	 */
	getAvailableSoundNames(id: number, excludeDefName?: string): string[] {
		const sounds = this.getSoundsById(id);
		return sortBy([
			...this.findAvailableSingleSoundNames(sounds, excludeDefName),
			...this.findAvailableMultiSoundNames(sounds, false, excludeDefName)
		]);
	}

	/**
	 * Gets the available track names for a given font id
	 * @param id the font id
	 * @returns the first available track name
	 */
	getAvailableTrackName(id: number): string {
		const availableNames = this.findAvailableMultiSoundNames(this.getTracksById(id), true);
		return availableNames.length
			? availableNames[0]
			: `track${(SOUND_DICT['track#'] as MultiSoundDefinition).minCount}`;
	}

	/**
	 * Gets the font config for a given font id
	 * @param id the font id
	 * @returns the font config
	 */
	getFontConfigById(id: number): FontConfig | undefined {
		return this.fontConfigs().find((font: FontConfig) => font.id() === id);
	}

	/**
	 * Gets the LED config for a given LED id
	 * @param id the LED id
	 * @returns the LED config
	 */
	getLedConfigById(id: number): LedConfig | undefined {
		return this.ledConfigs().find((led: LedConfig) => led.id() === id);
	}

	/**
	 * Gets the sound files for a given font id
	 * @param id the font id
	 * @returns an array of sound files
	 */
	getSoundsById(id: number): SoundFile[] {
		return this.sounds().filter((sound: SoundFile) => sound.id() === id && !sound.isTrack);
	}

	/**
	 * Gets the track files for a given font id
	 * @param id the font id
	 * @returns an array of track files
	 */
	getTracksById(id: number): SoundFile[] {
		return this.sounds().filter((sound: SoundFile) => sound.isTrack && sound.id() === id);
	}

	/**
	 * Marks the system and all configs as saved
	 */
	markAsSaved(): void {
		this.mainConfig().markAsSaved();
		this.colorConfig().markAsSaved();
		this.prefConfig().markAsSaved();
		this.specialConfig().markAsSaved();
		this.fontConfigs().forEach(font => font.markAsSaved());
		this.ledConfigs().forEach(led => led.markAsSaved());
	}

	/**
	 * Orders the blade profiles given an array of profile ids
	 * @param newOrder the new order of the profiles. Defaults to the current order
	 */
	orderBladeProfiles(
		newOrder: readonly number[] = this.mainConfig()
			.groups()
			.map(g => g.id())
	): void {
		this.orderGroups(newOrder, this.mainConfig(), GroupType.profile);
	}

	/**
	 * Orders the color profiles given an array of profile ids
	 * @param newOrder the new order of the profiles. Defaults to the current order
	 */
	orderColorProfiles(
		newOrder: readonly number[] = this.colorConfig()
			.groups()
			.map(g => g.id())
	): void {
		this.orderGroups(newOrder, this.colorConfig(), GroupType.color);
	}

	/**
	 * Orders the fonts given an array of font ids
	 * @param newOrder the new order of the fonts
	 */
	orderFonts(newOrder: readonly number[]): void {
		// Create a map of the new order
		const newOrderMap = newOrder.reduce((map: OrderMap, id, index): OrderMap => ({ ...map, [id]: index + 1 }), {});

		// Update the font ids and validate
		this.fontConfigs().forEach(font => {
			font.id.set(newOrderMap[font.id()]);
		});

		// Update the sound ids and validate
		this.prefConfig()
			.groups()
			.forEach(prefGroup => {
				prefGroup.id.set(newOrderMap[prefGroup.id() + 1] - 1);
			});

		// Update the sound ids and validate
		this.ledConfigs().forEach(led => {
			led.id.set(newOrderMap[led.id()]);
		});

		// Update the sound ids
		this.sounds().forEach(sound => {
			sound.id.set(newOrderMap[sound.id()]);
		});

		// Sort the configs
		this.prefConfig().groups.set(sortBy(this.prefConfig().groups(), value => value.id()));
		this.fontConfigs.set(sortBy(this.fontConfigs(), value => value.id()));
		this.ledConfigs.set(sortBy(this.ledConfigs(), value => value.id()));
	}

	/**
	 * Reverts the system and all configs to their last saved state
	 */
	revert(): void {
		this.mainConfig().revert();
		this.colorConfig().revert();
		this.prefConfig().revert();
		this.specialConfig().revert();
		this.fontConfigs().forEach(font => font.revert());
		this.ledConfigs().forEach(led => led.revert());
		this.sounds().forEach(sound => sound.revert());

		this.fontConfigs.set(sortBy(this.fontConfigs(), value => value.id()));
		this.ledConfigs.set(sortBy(this.ledConfigs(), value => value.id()));
	}

	/**
	 * Deletes a profile from the given config file and updates all references to it
	 * @param profile the profile to delete
	 * @param configFile the config file to delete the profile from
	 * @param varName the variable name to update
	 */
	private deleteProfile(
		profile: ConfigGroup,
		configFile: ConfigFile,
		varName: GroupType.profile | GroupType.color
	): void {
		// Delete the profile from the config file
		configFile.groups.set(configFile.groups().filter(g => g.id !== profile.id));

		// Update all references to the profile
		this.prefConfig()
			.groups()
			.forEach(group => {
				const profileVar = group.getVariableByName(varName);
				const profileValue = profileVar?.configValue();

				if (
					profileValue &&
					IntegerValue.isIntegerValue(profileValue) &&
					profileValue.value() === profile.id()
				) {
					profileValue.value.set(configFile.groups()[0].id());
				}
			});
	}

	/**
	 * Finds the next available multi sound name for a given sound
	 * @param sounds list of sounds to search
	 * @param isTrack is the sound a track
	 * @param excludeDefName definition name to exclude from the search
	 * @returns list of available multi sound names
	 */
	private findAvailableMultiSoundNames(sounds: SoundFile[], isTrack: boolean, excludeDefName?: string): string[] {
		// Get all multi sounds that are tracks and not the current sound
		const multiSoundNameAndNumber: {
			defSoundName: string;
			soundNumber: number;
		}[] = sounds
			.filter(sound => sound.isMulti && sound.isTrack === isTrack && sound.defSoundName !== excludeDefName)
			.map(({ defSoundName, soundNumber }: SoundFile) => ({ defSoundName, soundNumber }));

		// Group the multi sounds by definition name and sort the numbers
		const multiSoundNameByNumbers: { [defSoundName: string]: number[] } = mapValues(
			groupBy(multiSoundNameAndNumber, 'defSoundName'),
			value => sortBy(value.map(soundAndNumbers => soundAndNumbers.soundNumber))
		);

		// Get the next available multi sound name for each definition name
		const nextAvailableMultiSoundNames: string[] = keys(multiSoundNameByNumbers).map(defSoundName =>
			defSoundName.replace(
				'#',
				this.findFirstMissingNumber(
					multiSoundNameByNumbers[defSoundName],
					(SOUND_DICT[defSoundName] as MultiSoundDefinition)?.minCount
				)
			)
		);

		if (!isTrack) {
			// Get the list of unused multi sound names excluding the current sound and map them to the minimum count
			const unusedMultiSoundNames: string[] = compact(
				without(
					keys(pickBy(SOUND_DICT, value => value.isMulti)),
					...multiSoundNameAndNumber.map(nameObj => nameObj.defSoundName),
					'track#',
					excludeDefName
				).map(defName => defName?.replace('#', `${(SOUND_DICT[defName] as MultiSoundDefinition).minCount}`))
			);
			return [...nextAvailableMultiSoundNames, ...unusedMultiSoundNames];
		}

		return nextAvailableMultiSoundNames;
	}

	/**
	 * Finds the next available single sound name excluding the given sound
	 * @param sounds list of sounds to search
	 * @param excludeDefName definition name to exclude from the search
	 * @returns list of available single sound names
	 */
	private findAvailableSingleSoundNames(sounds: SoundFile[], excludeDefName?: string): string[] {
		// Get the list of single sound names
		const singleSoundNames: string[] = sounds.filter(sound => !sound.isMulti).map(sound => sound.defSoundName);

		// Return the list of single sound names that are not in the given list of sounds
		return compact(without(keys(pickBy(SOUND_DICT, value => !value.isMulti)), ...singleSoundNames, excludeDefName));
	}

	/**
	 * Finds the first missing number in a given array of numbers
	 * @param numberArr array of numbers to search
	 * @param startingNumber the number to start the search at
	 * @returns the first missing number as a string
	 */
	private findFirstMissingNumber(numberArr: number[], startingNumber: number): string {
		// Create an array of numbers from the starting number to the length of the given array
		const expectedArray = Array.from({ length: numberArr.length }, (value, index) => index + startingNumber);

		// Iterate through the given array and return the first number that doesn't match the expected array
		for (let i = 0; i < numberArr.length; i++) {
			if (numberArr[i] !== expectedArray[i]) {
				return `${expectedArray[i]}`;
			}
		}

		const lastValue = last(expectedArray);

		return `${lastValue ? lastValue + 1 : startingNumber}`;
	}

	/**
	 * Orders the profiles in the given config file and updates all references to them
	 * @param newOrder the new order of the profiles
	 * @param configFile the config file to order the profiles in
	 * @param varName the variable name to update
	 */
	private orderGroups(
		newOrder: readonly number[],
		configFile: ConfigFile,
		varName: GroupType.profile | GroupType.color
	): void {
		// Create a map of the new order
		const newOrderMap = newOrder.reduce(
			(map: OrderMap, id, index): OrderMap => ({ ...map, [id.toString()]: index }),
			{}
		);

		// Iterate through all the profiles and update their order
		this.fontConfigs().forEach(font => {
			// Get the profile variable and update its value, then validate it
			const prefVar = this.prefConfig().getPrefByFontId(font.id())?.getVariableByName(varName);
			const prefValue = prefVar?.configValue();
			if (!prefValue || !IntegerValue.isIntegerValue(prefValue)) {
				return;
			}
			const newId = newOrderMap[prefValue.value()];
			prefValue.value.set(newId);

			// Get the used profiles variable
			const usedProfilesVar = font.getVariableByName(
				varName === GroupType.profile ? UsedProfiles.BLADE : UsedProfiles.COLOR
			);
			const usedProfiles = usedProfilesVar?.configValue();

			if (usedProfiles && ProfileSelectorValue.isProfileSelectorValue(usedProfiles)) {
				// Get the used profiles and create a new array of values
				const newValues: boolean[] = new Array(ProfileSelectorValue.MAX_PROFILES).fill(false);

				// Iterate through the used profiles and update their order
				forIn(newOrderMap, (value: number, key: string) => {
					newValues[value] = usedProfiles.value()[Number(key)];
				});

				// Update the used profiles variable and validate it
				usedProfiles.value.set(newValues);
			}
		});

		// Update the group ids and validate them
		configFile.groups().forEach(group => {
			group.id.set(newOrderMap[group.id()]);
		});

		// Sort the groups by id and validate the config file
		configFile.groups.set(sortBy(configFile.groups(), value => value.id()));
	}

	/**
	 * Checks if the given system has the same fonts and configs as this system
	 * @param system the system to compare to
	 * @returns true if the given system has the same fonts and configs as this system
	 */
	private sameFontStructureAs(system: LightsaberSystem): boolean {
		return (
			system.mainConfig() &&
			system.colorConfig() &&
			system.specialConfig() &&
			system.prefConfig() &&
			system.ledConfigs() &&
			system.fontConfigs() &&
			this.fontConfigs().length === system.fontConfigs().length &&
			this.ledConfigs().length === system.ledConfigs().length &&
			this.fontConfigs().reduce(
				(isSame: boolean, font: FontConfig) =>
					isSame && font.name() === system.getFontConfigById(font.id())?.name(),
				true
			) &&
			this.ledConfigs().reduce(
				(isSame: boolean, led: LedConfig) => isSame && led.name() === system.getLedConfigById(led.id())?.name(),
				true
			)
		);
	}
}
