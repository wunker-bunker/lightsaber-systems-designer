import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { fontConfigFiles } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { FontConfig } from './font-config';

describe('FontConfig', () => {
	let config: FontConfig;
	const testFile = fontConfigFiles[0];

	beforeEach(() => {
		testData.generateTestData();
		config = new FontConfig(testData.fontConfigs[0].variables(), testFile.filePath);
	});

	it('should create an instance', () => {
		expect(config).toBeTruthy();
		expect(config.id()).toEqual(1);
		expect(config.name()).toEqual('NIGHTFALL');
		expect(config.type).toEqual(ConfigFileType.FONT);
		expect(config.filePath).toEqual(testFile.filePath);
		expect(config.variables()).toEqual(testData.fontConfigs[0].variables());
		expect(config.dirty()).toBeFalse();
		expect(config.valid()).toBeTrue();
	});

	it('should check if it has new id', () => {
		expect(config.hasNewId()).toBeFalse();
		config.id.set(20);
		expect(config.hasNewId()).toBeTrue();
	});

	it('should check if it has new name', () => {
		expect(config.hasNewName()).toBeFalse();
		config.name.set('test');
		expect(config.hasNewName()).toBeTrue();
	});

	it('should revert to old ID and name', () => {
		const ogId = config.id();
		const ogName = config.name();
		const newId = 20;
		const newName = 'test';
		config.id.set(newId);
		config.name.set(newName);
		expect(config.hasNewId()).toBeTrue();
		expect(config.hasNewName()).toBeTrue();
		expect(config.id()).toEqual(newId);
		expect(config.name()).toEqual(newName);
		config.revert();
		expect(config.hasNewId()).toBeFalse();
		expect(config.hasNewName()).toBeFalse();
		expect(config.id()).toEqual(ogId);
		expect(config.name()).toEqual(ogName);
	});

	it('should mark as saved', () => {
		const newId = 20;
		const newName = 'test';
		config.id.set(newId);
		config.name.set(newName);
		expect(config.hasNewId()).toBeTrue();
		expect(config.hasNewName()).toBeTrue();
		expect(config.id()).toEqual(newId);
		expect(config.name()).toEqual(newName);
		config.markAsSaved();
		expect(config.hasNewId()).toBeFalse();
		expect(config.hasNewName()).toBeFalse();
		expect(config.id()).toEqual(newId);
		expect(config.name()).toEqual(newName);
	});

	describe('should be dirty with', () => {
		it('with new id', () => {
			expect(config.dirty()).toBeFalse();
			const newId = 20;
			config.id.set(newId);
			expect(config.hasNewId()).toBeTrue();
			expect(config.id()).toEqual(newId);
			expect(config.dirty()).toBeTrue();
			config.markAsSaved();
			expect(config.dirty()).toBeFalse();
		});

		it('with new name', () => {
			expect(config.dirty()).toBeFalse();
			const newName = 'test';
			config.name.set(newName);
			expect(config.hasNewName()).toBeTrue();
			expect(config.name()).toEqual(newName);
			expect(config.dirty()).toBeTrue();
			config.markAsSaved();
			expect(config.dirty()).toBeFalse();
		});
	});
});
