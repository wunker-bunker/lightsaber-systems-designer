import { FILE_HEADER } from 'src/app/constants/strings';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { mainConfigFile } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { MainConfig } from './main-config';

describe('MainConfig', () => {
	let config: MainConfig;

	beforeEach(() => {
		testData.generateTestData();
		config = new MainConfig(testData.mainConfig.variables(), testData.mainConfig.groups(), mainConfigFile.filePath);
	});

	it('should create an instance', () => {
		expect(config).toBeTruthy();
		expect(config.filePath).toEqual(mainConfigFile.filePath);
		expect(config.type).toEqual(ConfigFileType.MAIN);
		expect(config.variables()).toEqual(testData.mainConfig.variables());
		expect(config.groups()).toEqual(testData.mainConfig.groups());
		expect(config.dirty()).toBeFalse();
		expect(config.valid()).toBeTrue();
		expect(config.toString()).toEqual(FILE_HEADER + mainConfigFile.fileText + '\n');
	});

	it('should get profile by id', () => {
		const group0 = config.getBladeProfileById(0);

		if (!group0) {
			throw new Error('group0 is undefined');
		}

		expect(group0.id()).toEqual(0);
		expect(group0.name).toEqual('profile');
		expect(group0.getVariableByName('flks')?.configValue().value()).toEqual(3);

		const group1 = config.getBladeProfileById(1);

		if (!group1) {
			throw new Error('group1 is undefined');
		}

		expect(group1.id()).toEqual(1);
		expect(group1.name).toEqual('profile');
		expect(group1.getVariableByName('size_lockup')?.configValue().value()).toEqual(40);
	});
});
