import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BitfieldValue } from 'src/app/classes/config-value/bitfield-value';
import { ConfigVar } from 'src/app/classes/config-var';
import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { TrackIndexByLengthPipe } from 'src/app/pipes/track-index-by-length.pipe';
import { configureTestSuite, testData, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { BitfieldInputComponent } from './bitfield-input.component';

describe('BitfieldInputComponent', () => {
	let component: BitfieldInputComponent;
	let fixture: ComponentFixture<BitfieldInputComponent>;
	let variable: ConfigVar;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [BitfieldInputComponent, TrackIndexByLengthPipe],
		providers: unitTestProviders
	});

	beforeEach(() => {
		const v = testData.configVars.find(confVar => confVar.configValue().type === VALUE_TYPE.BITFIELD);
		const val = v?.configValue();

		if (!v || !val || !BitfieldValue.isBitfieldValue(val)) {
			throw new Error('No bitfield config var found in test data');
		}

		variable = v;
		fixture = TestBed.createComponent(BitfieldInputComponent);
		component = fixture.componentInstance;
		component.input = val;
		component.name = variable.name;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should change value', () => {
		const origValue = component.input.value();
		const index = 2;
		const newValue = !origValue[index];

		expect(origValue[index]).not.toEqual(newValue);
		component.change(index, newValue);
		expect(component.input.value()[index]).toEqual(newValue);
	});
});
