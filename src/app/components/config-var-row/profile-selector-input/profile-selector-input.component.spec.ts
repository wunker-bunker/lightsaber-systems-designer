import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileSelectorValue } from 'src/app/classes/config-value/profile-selector-value';
import { ColorValuePipe } from 'src/app/pipes/color-value.pipe';
import { configureTestSuite, testData, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { ProfileSelectorInputComponent } from './profile-selector-input.component';

describe('ProfileSelectorInputComponent', () => {
	let component: ProfileSelectorInputComponent;
	let fixture: ComponentFixture<ProfileSelectorInputComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [ProfileSelectorInputComponent, ColorValuePipe],
		providers: unitTestProviders
	});

	beforeEach(() => {
		const value = testData.profileSelectValues.find(value => value.definition.profileType === 'color');

		if (!value) {
			throw new Error('No profile selector value found');
		}

		fixture = TestBed.createComponent(ProfileSelectorInputComponent);
		component = fixture.componentInstance;
		component.input = value;
		component.groups = testData.configGroups;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should map the input value to the selectedGroups property', () => {
		expect(component.selectedGroups().length).toBe(0);

		// Update component's input value to be an array containing only one true and fill the rest with false
		component.input.value.set(
			new Array(ProfileSelectorValue.MAX_PROFILES).fill(false).map((value, index) => index === 0)
		);

		expect(component.selectedGroups().length).toBe(1);

		// Update component's input value to be an array containing only one true and fill the rest with false
		component.input.value.set(
			new Array(ProfileSelectorValue.MAX_PROFILES).fill(false).map((value, index) => index === 0 || index === 1)
		);

		expect(component.selectedGroups().length).toBe(2);
	});

	it('should update value on add', () => {
		expect(component.selectedGroups().length).toBe(0);
		component.onAdd(testData.configGroups[0]);
		expect(component.selectedGroups().length).toBe(1);
		expect(component.input.value()).toEqual(
			new Array(ProfileSelectorValue.MAX_PROFILES)
				.fill(false)
				.map((value, index) => index === testData.configGroups[0].id())
		);
	});
});
