   ___                                    _                             
  / _ \_ __ ___ _   _    /\/\   ___  __ _| |_                           
 / /_\/ '__/ _ \ | | |  /    \ / _ \/ _` | __|                          
/ /_\\| | |  __/ |_| | / /\/\ \  __/ (_| | |_                           
\____/|_|  \___|\__, | \/    \/\___|\__,_|\__|                          
                |___/                                                   
                                    _  __             _     _           
  __ _    ___  ___  _   _ _ __   __| |/ _| ___  _ __ | |_  | |__  _   _ 
 / _` |  / __|/ _ \| | | | '_ \ / _` | |_ / _ \| '_ \| __| | '_ \| | | |
| (_| |  \__ \ (_) | |_| | | | | (_| |  _| (_) | | | | |_  | |_) | |_| |
 \__,_|  |___/\___/ \__,_|_| |_|\__,_|_|  \___/|_| |_|\__| |_.__/ \__, |
                                                                  |___/ 
 __      _                                                              
/ _\ ___| | ___ __ ___   __ _ _   _  ___                                
\ \ / _ \ |/ / '__/ _ \ / _` | | | |/ _ \                               
_\ \  __/   <| | | (_) | (_| | |_| |  __/                               
\__/\___|_|\_\_|  \___/ \__, |\__,_|\___|                               
                        |___/                                           

"Greymeat"
by Sekrogue1985, a TCSS Member, occasional FXsabers browser, and LDM's first customer.

** UPGRADED to smoothswing by David Maltais - JediArmsDealer - 01/2021
** Restoring stock/former "non-smoothswing" behavior of the font is simply achieved by removing the lswing/hswing
   pairs in the font directory. Some other contents like (accent) swings and spins have been altered or removed from
   the font and stored in the /extra folder of the font. Updated fonts also have their combo files renamed as force
   sounds. Feel free to mix-match-remap or duplicate those to also have force-clash sounds.

A soundfont inspired by the duels from Star Wars Episodes.
Font initially crafted for the ISG/Plecter Labs "Petit Crouton" Sound Board.

Enjoy !

-Scott

More of my fonts on www.saberfont.com !