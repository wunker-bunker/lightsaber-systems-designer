import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { specialConfigFile } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { SpecialConfig } from './special-config';

describe('SpecialConfig', () => {
	let config: SpecialConfig;

	beforeEach(() => {
		testData.generateTestData();
		config = new SpecialConfig(specialConfigFile.fileText, specialConfigFile.filePath);
	});

	it('should create an instance', () => {
		expect(config).toBeTruthy();
		expect(config.type).toEqual(ConfigFileType.SPECIAL);
		expect(config.filePath).toEqual(specialConfigFile.filePath);
		expect(config.text()).toEqual(specialConfigFile.fileText);
		expect(config.dirty()).toBeFalse();
		expect(config.valid()).toBeTrue();
	});
});
