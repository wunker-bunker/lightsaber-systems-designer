import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { EnumDefinition } from 'src/app/models/variable-dictionary.model';
import { EnumerationValue } from './enumeration-value';

enum TestEnum {
	TEST1 = -1,
	TEST2,
	TEST3
}

describe('EnumerationValue', () => {
	const def: EnumDefinition = {
		description: 'desc',
		enum: TestEnum,
		type: VALUE_TYPE.ENUM
	};

	it('should create an instance', () => {
		const val = new EnumerationValue('-1', def);
		expect(val).toBeTruthy();
		expect(val.definition).toEqual(def);
		expect(val.type).toEqual(def.type);
		expect(val.valid()).toBeTrue();
		expect(val.value()).toEqual(TestEnum.TEST1);
	});

	it('should print toString', () => {
		expect(new EnumerationValue('1', def).toString()).toEqual('1');
	});

	describe('should validate', () => {
		const val = new EnumerationValue('0', def);

		it('valid when value is in enum', () => {
			val.value.set(-1);
			expect(val.valid()).toBeTrue();
			val.value.set(0);
			expect(val.valid()).toBeTrue();
			val.value.set(1);
			expect(val.valid()).toBeTrue();
		});

		it('invalid when value is not in enum', () => {
			val.value.set(-2);
			expect(val.valid()).toBeFalse();
			val.value.set(2);
			expect(val.valid()).toBeFalse();
		});
	});

	it('should auto fix value', () => {
		const value = new EnumerationValue('-2', def);
		expect(value.valid()).toBeFalse();
		expect(value.value()).toBeUndefined();
		value.autoFixValue();
		expect(value.valid()).toBeTrue();
		expect(value.value()).toEqual(0);
	});
});
