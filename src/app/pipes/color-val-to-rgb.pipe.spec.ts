import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { ColorValToRgbPipe } from './color-val-to-rgb.pipe';

describe('ColorValToRgbPipe', () => {
	const pipe = new ColorValToRgbPipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('should transform Color to RGBA', () => {
		const rgbw = {
			red: 0,
			green: COLOR_VAL_MAX,
			blue: COLOR_VAL_MAX,
			white: 0
		};
		expect(pipe.transform(rgbw)).toEqual({
			r: Math.round((rgbw.red * 255) / COLOR_VAL_MAX),
			g: Math.round((rgbw.green * 255) / COLOR_VAL_MAX),
			b: Math.round((rgbw.blue * 255) / COLOR_VAL_MAX),
			a: 1
		});
	});
});
