import { soundAndTrackFilePaths } from 'src/environments/test-data.karma';
import { SOUND_NAME } from '../constants/regex';
import { SOUND_DICT } from '../dictionaries/sounds.dict';
import { SoundFile } from './sound-file';

describe('SoundFile', () => {
	it('should create an instance', () => {
		const sound = new SoundFile(soundAndTrackFilePaths[1], soundAndTrackFilePaths[1]);
		const def = SOUND_DICT[sound.soundName.replace(/\d+/, '#')];
		expect(sound).toBeTruthy();
		expect(sound.id()).toEqual(+soundAndTrackFilePaths[1].replace(SOUND_NAME, '$1'));
		expect(sound.isTrack).toBeFalse();
		expect(sound.filePath).toEqual(soundAndTrackFilePaths[1]);
		expect(sound.name()).toEqual(soundAndTrackFilePaths[1].replace(SOUND_NAME, '$2'));
		expect(sound.soundName).toEqual(soundAndTrackFilePaths[1].replace(SOUND_NAME, '$3'));
		expect(sound.uriPath).toEqual(soundAndTrackFilePaths[1]);
		expect(sound.description).toEqual(def.description);
		expect(sound.loops).toEqual(def.loops);
	});
});
