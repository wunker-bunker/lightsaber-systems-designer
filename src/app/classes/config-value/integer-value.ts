import { computed } from '@angular/core';
import { ValueType, ValueTypeInteger } from 'src/app/enums/value-type.enum';
import { BaseDefinition, IntegerDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class IntegerValue extends ConfigValue<number, ValueTypeInteger, IntegerDefinition> {
	readonly toString;

	readonly valid;

	constructor(value: string, def: IntegerDefinition) {
		super(value, def, def.type);
		this.toString = computed(() => `${this.value()}`);
		this.valid = computed(
			() => this.value() >= this.definition.minValue && this.value() <= this.definition.maxValue
		);
	}

	static isIntegerValue(value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>): value is IntegerValue {
		return value instanceof IntegerValue;
	}

	autoFixValue(): void {
		this.value.set(
			this.value() < this.definition.minValue
				? this.definition.minValue
				: this.value() > this.definition.maxValue
				? this.definition.maxValue
				: this.value()
		);
	}

	protected parseValue(strVal: string): number {
		return +strVal;
	}
}
