import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-add-sound',
	templateUrl: './add-sound.component.html',
	styleUrls: ['./add-sound.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddSoundComponent {
	availableNames: string[] = [];
	soundNameForm: FormGroup = this._fb.group({
		name: [null, [Validators.required]]
	});

	constructor(public activeModal: NgbActiveModal, public settings: SettingsService, private _fb: FormBuilder) {}

	get name(): AbstractControl | null {
		return this.soundNameForm.get('name');
	}

	printForm(): void {
		if (this.name?.errors) {
			console.warn(this.name.errors);
		}
	}
}
