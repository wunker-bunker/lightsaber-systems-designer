import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { EnumerationValue } from 'src/app/classes/config-value/enumeration-value';
import { BaseInputComponent } from '../config-var-row.component';

@Component({
	selector: 'lsd-enumeration-input',
	templateUrl: './enumeration-input.component.html',
	styleUrls: ['./enumeration-input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnumerationInputComponent implements BaseInputComponent {
	@Input() input!: EnumerationValue;
	@Input() name = '';
}
