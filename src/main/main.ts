import { GenericServerOptions } from 'builder-util-runtime';
import {
	app,
	BrowserWindow,
	dialog,
	ipcMain,
	IpcMainEvent,
	OpenDialogOptions,
	protocol,
	screen,
	session,
	shell
} from 'electron';
import log, { functions as logFunctions, transports as logTransports } from 'electron-log';
import { getSync, hasSync, setSync } from 'electron-settings';
import { AppImageUpdater, AppUpdater, MacUpdater, NsisUpdater, ProgressInfo } from 'electron-updater';
import windowState from 'electron-window-state';
import { IpcMainInvokeEvent } from 'electron/main';
import { join, normalize } from 'path';
import { AppChannel } from '../common/app-channel.enum';
import { APP_SETTINGS } from '../common/app-settings.enum';
import { LsdIpcChannels } from '../common/lsd-ipc-channels.enum';
import { UiThemes } from '../common/ui-themes.enum';

const scheme = 'lsd';
const partition = 'persist:lsd';
const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
const isWin = process.platform === 'win32';
const isMac = process.platform === 'darwin';
const isLinux = process.platform === 'linux';
let splash: BrowserWindow | null = null;
let mainWindow: BrowserWindow | null = null;
let updateDebugWin: BrowserWindow | null = null;
let mainWindowState: windowState.State | null = null;
let autoUpdater: AppUpdater | null = null;

const initialSetup = (): void => {
	// Setup loggers
	logTransports.file.level = serve ? 'debug' : 'info';
	Object.assign(console, logFunctions);

	if (isLinux) {
		app.commandLine.appendSwitch('in-process-gpu');
	}

	protocol.registerSchemesAsPrivileged([{ scheme, privileges: { bypassCSP: true, stream: true } }]);

	if (!hasSync(APP_SETTINGS.CHANNEL)) {
		setSync(APP_SETTINGS.CHANNEL, AppChannel.LATEST);
	}

	if (!hasSync(APP_SETTINGS.THEME)) {
		setSync(APP_SETTINGS.THEME, UiThemes.LIGHT);
	}

	const publishOptions: GenericServerOptions = {
		provider: 'generic',
		url: `https://gitlab.com/api/v4/projects/25433126/packages/generic/lightsaber-systems-designer/release/`,
		channel: getSync(APP_SETTINGS.CHANNEL) as string
	};

	if (!serve) {
		if (isWin) {
			autoUpdater = new NsisUpdater(publishOptions);
		} else if (isMac) {
			autoUpdater = new MacUpdater(publishOptions);
		} else if (isLinux && process.env.IS_FLATPAK !== 'true') {
			autoUpdater = new AppImageUpdater(publishOptions);
		}

		if (autoUpdater) {
			autoUpdater.autoDownload = false;
			autoUpdater.logger = log;
		}
	}
};

const determineBackgroundColor = (theme: UiThemes): string => {
	switch (theme) {
		case UiThemes.DARK:
			return '#343a40';
		case UiThemes.LIGHT:
			return '#f8f9fa';
	}
};

const startApp = (): void => {
	const screenSize = screen.getPrimaryDisplay().workAreaSize;
	mainWindowState = windowState({
		maximize: true,
		defaultWidth: screenSize.width,
		defaultHeight: screenSize.height
	});
	const { x, y, width, height } = mainWindowState;

	// Create the browser window.
	mainWindow = new BrowserWindow({
		x,
		y,
		width,
		height,
		autoHideMenuBar: true,
		show: false,
		paintWhenInitiallyHidden: true,
		frame: false,
		fullscreenable: true,
		titleBarStyle: 'hidden',
		acceptFirstMouse: true,
		backgroundColor: determineBackgroundColor(getSync(APP_SETTINGS.THEME) as UiThemes),
		webPreferences: {
			preload: join(__dirname, 'preload.js'),
			nodeIntegration: true,
			contextIsolation: false,
			devTools: serve,
			partition
		}
	});

	if (serve) {
		mainWindow.loadURL('http://localhost:4200');
		mainWindowState.manage(mainWindow);
		mainWindow.show();
		const devTools = new BrowserWindow();
		mainWindow.webContents.setDevToolsWebContents(devTools.webContents);
		mainWindow.webContents.openDevTools({
			mode: 'detach'
		});
	} else {
		splash = new BrowserWindow({
			width: 600,
			height: 600,
			autoHideMenuBar: true,
			frame: false,
			transparent: true,
			alwaysOnTop: true,
			skipTaskbar: true,
			center: true,
			movable: false
		});
		mainWindow?.loadURL(`file://${__dirname}/../lightsaber-systems-designer/index.html`);
		splash.loadURL(`file://${__dirname}/../lightsaber-systems-designer/splash.html`);
	}

	mainWindow.on('maximize', () => mainWindow?.webContents.send(LsdIpcChannels.APP_IS_MAXIMIZED, true));
	mainWindow.on('unmaximize', () => mainWindow?.webContents.send(LsdIpcChannels.APP_IS_MAXIMIZED, false));

	// Emitted when the window is closed.
	mainWindow.on('closed', () => {
		// Dereference the window object, usually you would store window
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null;
	});

	// Emitted when the window is closed.
	splash?.on('closed', () => {
		// Dereference the window object, usually you would store window
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		splash = null;
	});
};

const openUpdateDebugger = (): void => {
	if (serve) {
		updateDebugWin = new BrowserWindow({
			x: 200,
			y: 200,
			width: 800,
			height: 600,
			autoHideMenuBar: true,
			modal: true,
			resizable: false,
			webPreferences: {
				preload: join(__dirname, 'preload.js'),
				nodeIntegration: true,
				contextIsolation: false
			}
		});

		updateDebugWin.loadURL('http://localhost:4200/update-test');

		updateDebugWin.on('close', () => {
			updateDebugWin = null;
		});
	}
};

const initializeServIpcHandles = (): void => {
	ipcMain.handle(LsdIpcChannels.READY_TO_CHECK_UPDATES, () => console.log('Ready to check for updates'));
	// Listen for restart or download
	ipcMain.handle(LsdIpcChannels.RESTART_INSTALL, () => console.log('Restart and install updates'));
	ipcMain.handle(LsdIpcChannels.START_DOWNLOAD, () => console.log('Download update'));
	ipcMain.handle(LsdIpcChannels.SET_UPDATE_CHANNEL, () =>
		console.log(`Setting channel to ${getSync(APP_SETTINGS.CHANNEL)}`)
	);

	ipcMain.handle(LsdIpcChannels.DEBUG_UPDATE_AVAIL, () => {
		console.log('Sending Update Available');
		mainWindow?.webContents.send(LsdIpcChannels.UPDATE_AVAILABLE);
	});
	ipcMain.handle(LsdIpcChannels.DEBUG_UPDATE_DOWNL, () => {
		console.log('Sending Update Downloaded');
		mainWindow?.webContents.send(LsdIpcChannels.UPDATE_DOWNLOADED);
	});
	ipcMain.handle(LsdIpcChannels.DEBUG_DOWNLOAD_PROGRESS, (event: IpcMainInvokeEvent, progress: ProgressInfo) => {
		console.log('Received progress update', progress);
		mainWindow?.webContents.send(LsdIpcChannels.DOWNLOAD_PROGRESS, progress);
	});
};

const initializeProdIpcHandles = (): void => {
	// Handle Updating
	ipcMain.handle(LsdIpcChannels.READY_TO_CHECK_UPDATES, () => autoUpdater?.checkForUpdates());

	autoUpdater?.on('update-available', () => mainWindow?.webContents.send(LsdIpcChannels.UPDATE_AVAILABLE));
	autoUpdater?.on('update-downloaded', () => mainWindow?.webContents.send(LsdIpcChannels.UPDATE_DOWNLOADED));
	autoUpdater?.on('download-progress', (progress: ProgressInfo) =>
		mainWindow?.webContents.send(LsdIpcChannels.DOWNLOAD_PROGRESS, progress)
	);

	// Listen for restart or download
	ipcMain.handle(LsdIpcChannels.RESTART_INSTALL, () => autoUpdater?.quitAndInstall());
	ipcMain.handle(LsdIpcChannels.START_DOWNLOAD, () => autoUpdater?.downloadUpdate());
	ipcMain.handle(LsdIpcChannels.SET_UPDATE_CHANNEL, () => {
		if (autoUpdater) {
			autoUpdater.channel = getSync(APP_SETTINGS.CHANNEL) as string;
			autoUpdater.checkForUpdates();
		}
	});
};

const initializeIpcHandles = (): void => {
	ipcMain.handle(LsdIpcChannels.OPEN_DIALOG, (event: IpcMainInvokeEvent, options: OpenDialogOptions) =>
		dialog.showOpenDialog(mainWindow as BrowserWindow, options)
	);

	ipcMain.handle(LsdIpcChannels.IS_SERVED, () => serve);

	ipcMain.handle(LsdIpcChannels.OPEN_PATH, (event: IpcMainInvokeEvent, filePath: string) => {
		if (process.platform === 'linux') {
			shell.openExternal(`file://${encodeURI(filePath)}`);
		} else {
			shell.openPath(filePath);
		}
	});

	ipcMain.handle(LsdIpcChannels.OPEN_EXTERNAL, (event: IpcMainInvokeEvent, url: string) => shell.openExternal(url));

	ipcMain.handle(LsdIpcChannels.DEBUG_OPEN_UPDATE_TESTER, () => openUpdateDebugger());

	ipcMain.handle(LsdIpcChannels.APP_CLOSE, () => mainWindow?.close());
	ipcMain.handle(LsdIpcChannels.APP_MAXIMIZE, () => mainWindow?.maximize());
	ipcMain.handle(LsdIpcChannels.APP_MINIMIZE, () => mainWindow?.minimize());
	ipcMain.handle(LsdIpcChannels.APP_RESTORE, () => mainWindow?.restore());

	ipcMain.handle(LsdIpcChannels.CHANGE_THEME, () =>
		mainWindow?.setBackgroundColor(determineBackgroundColor(getSync(APP_SETTINGS.THEME) as UiThemes))
	);

	ipcMain
		.on(LsdIpcChannels.SETTINGS_GET, (event: IpcMainEvent, key: string) => (event.returnValue = getSync(key)))
		.on(LsdIpcChannels.SETTINGS_HAS, (event: IpcMainEvent, key: string) => (event.returnValue = hasSync(key)))
		.on(
			LsdIpcChannels.SETTINGS_SET,
			(event: IpcMainEvent, key: string, value: string | number | boolean) =>
				(event.returnValue = setSync(key, value))
		);

	// eslint-disable-next-line @typescript-eslint/no-unused-expressions
	serve ? initializeServIpcHandles() : initializeProdIpcHandles();
};

const initializeAppEvents = () => {
	// This method will be called when Electron has finished
	// initialization and is ready to create browser windows.
	// Some APIs can only be used after this event occurs.
	// Added 400 ms to fix the black background issue while using transparent window. More detais at https://github.com/electron/electron/issues/15947
	app.on('ready', () => {
		const ses = session.fromPartition(partition);

		ses.protocol.registerFileProtocol(scheme, ({ url }, callback) => {
			try {
				callback({ path: normalize(decodeURIComponent(url.replace(`${scheme}://`, ''))) });
			} catch (error) {
				// Handle the error as needed
				console.error(error);
			}
		});

		startApp();

		if (!serve) {
			setTimeout(() => {
				splash?.close();

				mainWindowState?.manage(mainWindow as BrowserWindow);
				mainWindow?.show();
				mainWindow?.webContents.send(LsdIpcChannels.APP_IS_MAXIMIZED, mainWindow.isMaximized());
			}, 5000);
		}
	});

	// Quit when all windows are closed.
	app.on('window-all-closed', () => {
		// On OS X it is common for applications and their menu bar
		// to stay active until the user quits explicitly with Cmd + Q
		if (process.platform !== 'darwin') {
			app.quit();
		}
	});

	app.on('activate', () => {
		// On OS X it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (mainWindow === null) {
			startApp();
		}
	});
};

initialSetup();
initializeAppEvents();
initializeIpcHandles();
