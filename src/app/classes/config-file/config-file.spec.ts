import { computed } from '@angular/core';
import { FILE_HEADER } from 'src/app/constants/strings';
import { MAIN_DICT } from 'src/app/dictionaries/variable.dict';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ShuffleMode } from 'src/app/enums/shuffle-mode.enum';
import { VALUE_TYPE } from 'src/app/enums/value-type.enum';
import { IntegerDefinition } from 'src/app/models/variable-dictionary.model';
import { testData } from 'src/environments/test-defaults.karma';
import { ConfigGroup } from '../config-group';
import { IntegerValue } from '../config-value/integer-value';
import { ConfigVar } from '../config-var';
import { ConfigFile } from './config-file';

class TestFile extends ConfigFile {
	dirty = computed(() => this._dirtyVars());
	constructor(variables: ConfigVar[], groups: ConfigGroup[], filePath: string) {
		super(ConfigFileType.MAIN, variables, groups, filePath);
	}
}

describe('ConfigFile', () => {
	let config: ConfigFile;

	beforeEach(() => {
		testData.generateTestData();
		config = new TestFile(testData.configVars, testData.configGroups, 'test.txt');
	});

	it('should create an instance', () => {
		expect(config).toBeTruthy();
		expect(config.type).toEqual(ConfigFileType.MAIN);
		expect(config.filePath).toEqual('test.txt');
		expect(config.variables().length).toEqual(testData.configVars.length);
		expect(config.groups().length).toEqual(testData.configGroups.length);
		expect(config.groupValid()).toBeTrue();
		expect(config.varValid()).toBeTrue();
		expect(config.valid()).toBeTrue();
	});

	it('should get variable by name', () => {
		expect(config.getVariableByName('sleep')?.configValue().value()).toEqual(25000);
		expect(config.getVariableByName('plilh')?.configValue().value()).toEqual([3400, 4200]);
		expect(config.getVariableByName('oledflip')?.configValue().value()).toBeTrue();
		expect(config.getVariableByName('strippower')?.configValue().value()).toEqual([true, true, true, true]);
		expect(config.getVariableByName('shuffle')?.configValue().value()).toEqual(ShuffleMode.RANDOMX);
	});

	it('should validate', () => {
		const testValue = config.getVariableByName('swing_flow')?.configValue();
		const def = MAIN_DICT.swing_flow;

		if (!testValue || def.type !== VALUE_TYPE.INTEGER || !IntegerValue.isIntegerValue(testValue)) {
			throw new Error('testValue is undefined');
		}

		testValue.value.set(def.maxValue);
		expect(testValue.valid()).toBeTrue();
		expect(testValue.dirty()).toBeTrue();
		expect(config.valid()).toBeTrue();
		expect(config.varValid()).toBeTrue();
		expect(config.groupValid()).toBeTrue();
		testValue.value.set(def.minValue - 1);
		expect(testValue.valid()).toBeFalse();
		expect(testValue.dirty()).toBeTrue();
		expect(config.valid()).toBeFalse();
		expect(config.varValid()).toBeFalse();
		expect(config.groupValid()).toBeTrue();
	});

	it('should revert', () => {
		const testValue = config.getVariableByName('hforce')?.configValue();
		const def = MAIN_DICT.hforce;

		if (!testValue || def.type !== VALUE_TYPE.INTEGER || !IntegerValue.isIntegerValue(testValue)) {
			throw new Error('testValue is undefined');
		}

		const oldValue = testValue.value();
		testValue.value.set(def.minValue + 10);
		expect(testValue.value()).not.toEqual(oldValue);
		config.revert();
		expect(testValue.value()).toEqual(oldValue);
	});

	it('should mark as saved', () => {
		const testValue = config.getVariableByName('hclash')?.configValue();
		const def = MAIN_DICT.hclash as IntegerDefinition;

		if (!testValue || def.type !== VALUE_TYPE.INTEGER || !IntegerValue.isIntegerValue(testValue)) {
			throw new Error('testValue is undefined');
		}

		testValue.value.set(def.minValue + 5);
		expect(config.valid()).toBeTrue();
		expect(config.dirty()).toBeTrue();
		expect(testValue.dirty()).toBeTrue();
		config.markAsSaved();
		expect(config.dirty()).toBeFalse();
		expect(testValue.dirty()).toBeFalse();
	});

	it('should print file to string', () => {
		const fileText = config.toString();
		expect(fileText).toContain(FILE_HEADER);
		expect(fileText).toContain('[profile=2]');
		expect(fileText).toContain('unstable=0');
		expect(fileText).toContain('deep=50000');
		expect(fileText).toContain('smooth_width1=45');
		expect(fileText).toContain('bolt1=275,529');
		expect(fileText).toContain('glyph=10000000');
		expect(fileText).toContain('[color=0]');
		expect(fileText).toContain('fcolor=1023,1023,1023,0');
		expect(fileText).toContain('xcolor=400,0,1023,0');
		expect(fileText).toContain('[color=31]');
	});
});
