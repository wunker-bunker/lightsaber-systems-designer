/**
 * Mute boot options
 */
export enum MuteBoot {
	DISABLED = 0,
	BOOT_ONLY,
	BOOT_SABER_PAIR,
	BOOT_SABER_SEPARATE
}
