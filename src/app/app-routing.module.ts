import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SystemListComponent } from './components/system-list/system-list.component';
import { SystemPageComponent } from './components/system-page/system-page.component';
import { UpdateTesterComponent } from './components/update-tester/update-tester.component';
import { systemListResolver } from './resolvers/system-list.resolver';
import { systemResolver } from './resolvers/system.resolver';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'systems',
		pathMatch: 'full'
	},
	{
		path: 'systems',
		resolve: {
			systems: systemListResolver
		},
		component: SystemListComponent
	},
	{
		path: 'systems/:systemName',
		component: SystemPageComponent,
		resolve: {
			system: systemResolver
		},
		runGuardsAndResolvers: 'always'
	},
	{
		path: 'update-test',
		component: UpdateTesterComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
	exports: [RouterModule]
})
export class AppRoutingModule {}
