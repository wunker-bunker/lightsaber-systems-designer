/**
 * Power on interrupt mode
 */
export enum PowerOnInterruptMode {
	DISABLED = 0,
	QUICK_IGNITE,
	ENABLED
}
