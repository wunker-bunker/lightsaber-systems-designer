______                      _          _   _           ___  ___      _                     _   _   _ 
| ___ \                    | |        | | | |          |  \/  |     | |                   | | | | | |
| |_/ / __ ___  _ __  ___  | |_ ___   | |_| |__   ___  | .  . | __ _| | _____ _ __ ___    | | | | | |
|  __/ '__/ _ \| '_ \/ __| | __/ _ \  | __| '_ \ / _ \ | |\/| |/ _` | |/ / _ \ '__/ __|   | | | | | |
| |  | | | (_) | |_) \__ \ | || (_) | | |_| | | |  __/ | |  | | (_| |   <  __/ |  \__ \   |_| |_| |_|
\_|  |_|  \___/| .__/|___/  \__\___/   \__|_| |_|\___| \_|  |_/\__,_|_|\_\___|_|  |___/   (_) (_) (_)
               | |                                                                                   
               |_|                                                                                   

This brand new Crystal Focus default sound package would not have been possible without the precious
help and work of the following people, in random order (because you all rock !!!).
More fonts (and more of *their* fonts) can be obtained at saberfont.com

Font List :
BANK1  : NIGHTFALL (LBS) - Smoothswing upgraded (JAD)
BANK2  : CRYSTAL FOCUS (Link) - Legacy (mono-smoothswing upgraded by JAD) + Smoothswing (poly) versions
BANK3  : NEBULA (Link) - Legacy (mono-smoothswing upgraded by JAD) + Smoothswing (poly) versions
BANK4  : GREYMEAT (SekRogue) - Smoothswing upgraded (JAD)
BANK5  : HOTH (GCS) - Smoothswing upgraded
BANK6  : REVOLT (LBS) - Smoothswing upgraded (JAD)
BANK7  : SHATTERPOINT (GCS) - Smoothswing upgraded
BANK8  : KAMINO (NIGHTSTORM) - Smoothswing upgraded (JAD)
BANK9  : CRUCIBLE (NIGHTSTORM) - Smoothswing upgraded (JAD)
BANK10 : MALIGN (NIGHTSTORM) - Smoothswing upgraded (JAD)
BANK11 : SHOTO (GCS) - Smoothswing upgraded
BANK12 : YOUNGLING (LBS) - Smoothswing upgraded (JAD)
BANK13 : THE NEGOTIATOR (FOURZZE) - Smoothswing native
BANK14 : SHRODINGER's CAT - Smoothswing native

You'll find a detailed readme.txt file in each bank regarding the font theme, style and author

***********************************
** David Maltais (JediArmDealer) **
***********************************
Constructing one-of-a-kind Space Age Weaponry since 2005 and beyond......
Refactored the default CFX package by updating and upgrading many of the former Crystal Focus sound fonts
provided as in the default package of the soundboard
If you would like to see many of Master Maltais� creations check out his YouTube video channel at:

https://youtube.com/channel/UCo87wxxsSzvzd0pvPFWvOWQ

Enjoy, have fun and pop some corn or toast some buttered toast using the Force<>
And a Special Thanks to Erv Plecter for making realistic Lightsaber construction a Wonderful possibility from the very beginning

***********************************
** Tatyana Ostrovski (LadyBug)   **
***********************************


*****************************
** Tristan Kam aka "Trip"  **
*****************************
I've been a Star Wars and Sci-fi fan since childhood. Stumbling onto the saber community through the CFX and instantly loving the options this board brings 
I started using my IT-skills to provide additional resources and bring awareness of what is possible to achieve with this board, so people can enjoy this hobby even more
Feeling very passionate about people helping other people in this community!
On my spare time I run www.crystalfocus.net with a CFX configurator and a fx library


**********************
** Brandon aka Link **
**********************
Served in the US Navy. Love Star Wars, Video Games and taking on new challenges. 
Soundfonts provide a healthy outlet for my creative side and 
I enjoy very much being part of the community :)


*******************************************
** Rob Petkau from Genesis Custom Sabers **
*******************************************
I have been an avid sabersmith since 1997. As a Sabersmith I like to think of myself as a closet designer. 
Even more than the construction of a lightsaber � it�s the design that is my passion. 
I love it when I can design and innovate new ways to meld form and function, 
to creat a unique piece that is as much art as it is hobby.

Designing illuminated sabers is now my full time job. 
I get a tremendous sense of satisfaction when I design something that is appreciated 
by Star Wars fans and sabersmiths alike. My goal is build artful sabers that inspire, and awaken 
the child in all of us.

http://genesiscustomsabers.com
https://www.facebook.com/GenesisCustomSabers
http://www.youtube.com/user/MadcowGenesis


***************************
**   Lord Blako (LBS)    **
***************************
LordBlako joined the Saber Community back in 2012. Based on his background in music production, 
he quickly discovered his passion for soundfont design and was soon to release his first font. 
Over the years his specific skills improved and he created probably one of the biggest lightsaber
sound archives in the galaxy. 

Beside the font making part in this hobby, LordBlako also creates his own lightsabers from the scratch.
If you like LordBlako�s Saber Fonts, you might want to check out his Portfolio at his Youtube Channel 
http://www.youtube.com/LordBlakoSaberFonts or at saberfont.com


**************************************
** David A. McMasters aka Nighstorm **
**************************************
I'm an IT professional in my early forties. I've been an avid Star Wars and general Sci-Fi fan 
since my parents made the mistake of taking to me to see Star Wars in 1977!  
I first got into the custom Lightsaber scene in 2009 and started making fonts for sound cards in 2011.
More of my work can be found under the screen name Nightstorm on www.saberfont.com.


****************************************
**           Protongamer              **
****************************************
I'm a (newbie) maker who loves to build movies / games props, sound fonts and fun projects. 
Overall I like to bring the sound experience to silent objects !
I started this as a hobby back in 2013 when creating my first prop... a glue gun blaster with 
a cheap audio sound board and a font selector menu.
I'm an avid Ghostbusters & Star Wars fan, and Sci-fi fan in general.
I post videos about my Props work on my Youtube Channel https://www.youtube.com/channel/UCMbmKHOVKirfIyfgGIubMAw


**************************************
*     Aaron aka "Fourzze"            *
**************************************
Avid font creator and lightsaber fan, I've been crafting sound fonts since 2014. 
I'm a video game / starwars nerd and enjoy them as a fan and a father.
Find me on "The Fourzze Effect" on Youtube. 
Be sure and check out my fonts on www.saberfont.com under Fonts by Fourzze


