import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { configureTestSuite, testData, unitTestImports } from 'src/environments/test-defaults.karma';
import { LightsaberSystem } from '../classes/lightsaber-system';
import { AppAlertsService } from '../services/app-alerts.service';
import { AppLoaderService } from '../services/app-loader.service';
import { FileManagementService } from '../services/file-management.service';
import { systemResolver } from './system.resolver';

describe('SystemResolver', () => {
	const fileServ: jasmine.SpyObj<FileManagementService> = jasmine.createSpyObj('FileManagementService', [
		'loadSystem'
	]);
	const router: jasmine.SpyObj<Router> = jasmine.createSpyObj('Router', ['navigate']);
	const loaderServ: jasmine.SpyObj<AppLoaderService> = jasmine.createSpyObj('AppLoaderService', ['setState']);
	const alertServ: jasmine.SpyObj<AppAlertsService> = jasmine.createSpyObj('AppAlertsService', [
		'showSuccessAlert',
		'showErrorAlert'
	]);
	const state: jasmine.SpyObj<RouterStateSnapshot> = jasmine.createSpyObj('RouterStateSnapshot', [], {
		url: ''
	});
	const systemName = 'test';
	const route: jasmine.SpyObj<ActivatedRouteSnapshot> = jasmine.createSpyObj('ActivatedRouteSnapshot', [], {
		params: {
			systemName
		}
	});

	configureTestSuite({
		imports: unitTestImports,
		providers: [
			{ provide: FileManagementService, useValue: fileServ },
			{ provide: Router, useValue: router },
			{ provide: AppLoaderService, useValue: loaderServ },
			{ provide: AppAlertsService, useValue: alertServ },
			{ provide: ActivatedRouteSnapshot, useValue: { params: { systemName } } }
		]
	});

	describe('should resolve', () => {
		afterEach(() => {
			fileServ.loadSystem.calls.reset();
			router.navigate.calls.reset();
			loaderServ.setState.calls.reset();
			alertServ.showSuccessAlert.calls.reset();
			alertServ.showErrorAlert.calls.reset();
		});

		it('success', fakeAsync(() => {
			fileServ.loadSystem.and.resolveTo(testData.system);

			TestBed.runInInjectionContext(
				() => systemResolver(route, state) as unknown as Observable<LightsaberSystem>
			).subscribe(
				(system: LightsaberSystem) => {
					expect(fileServ.loadSystem).toHaveBeenCalledWith(systemName);
					expect(router.navigate).not.toHaveBeenCalled();
					expect(system).toEqual(testData.system);
				},
				() => fail()
			);
			tick();
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
		}));

		it('fail', fakeAsync(() => {
			fileServ.loadSystem.and.rejectWith('Reason');
			TestBed.runInInjectionContext(
				() => systemResolver(route, state) as unknown as Observable<LightsaberSystem>
			).subscribe(
				() => fail(),
				() => {
					expect(fileServ.loadSystem).toHaveBeenCalledWith(systemName);
					expect(router.navigate).toHaveBeenCalledWith(['/systems']);
					expect(alertServ.showErrorAlert).toHaveBeenCalled();
				}
			);
			tick();
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
		}));
	});
});
