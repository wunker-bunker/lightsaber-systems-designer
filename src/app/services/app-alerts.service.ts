import { Injectable, signal, WritableSignal } from '@angular/core';
import { isEqual } from 'lodash';
import { AppAlert, AppAlertType } from '../models/app-alert.model';

/**
 * Service to show alerts.
 * Alerts are shown in the app-alerts component.
 * Alerts are also logged to the console.
 * Alerts are removed after a delay.
 * Alerts can be removed manually.
 * Alerts can be of different types.
 * Alerts can have an error object.
 */
@Injectable({
	providedIn: 'root'
})
export class AppAlertsService {
	/**
	 * The list of alerts.
	 */
	alerts: WritableSignal<AppAlert[]> = signal([]);

	/**
	 * Remove an alert.
	 * @param alert The alert to remove.
	 */
	remove(alert: AppAlert): void {
		this.alerts.update(alerts => alerts.filter(a => !isEqual(a, alert)));
	}

	/**
	 * Show an alert.
	 * @param alert The alert to show.
	 * @returns The alert.
	 */
	show(alert: AppAlert): AppAlert {
		this.alerts.mutate(alerts => alerts.push(alert));

		if (alert.err) {
			console.error(alert.message, alert.err);
		} else {
			console.log(alert.message);
		}

		return alert;
	}

	/**
	 * Show an error alert.
	 * @param message The message to show.
	 * @param err The error object.
	 * @param hideDelay The delay before the alert is removed.
	 * @returns The alert.
	 */
	showErrorAlert(message: string, err?: unknown, hideDelay?: number): AppAlert {
		return this.show({
			message,
			hideDelay,
			err,
			type: AppAlertType.ERROR
		});
	}

	/**
	 * Show an success alert.
	 * @param message The message to show.
	 * @param hideDelay The delay before the alert is removed.
	 * @returns The alert.
	 */
	showSuccessAlert(message: string, hideDelay?: number): AppAlert {
		return this.show({
			message,
			hideDelay,
			type: AppAlertType.SUCCESS
		});
	}

	/**
	 * Show an warn alert.
	 * @param message The message to show.
	 * @param hideDelay The delay before the alert is removed.
	 * @returns The alert.
	 */
	showWarnAlert(message: string, hideDelay?: number): AppAlert {
		return this.show({
			message,
			hideDelay,
			type: AppAlertType.WARN
		});
	}
}
