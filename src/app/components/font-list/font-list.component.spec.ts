import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FontConfig } from 'src/app/classes/config-file/font-config';
import * as ConstFunctions from 'src/app/constants/functions';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import { ModalService } from 'src/app/services/modal.service';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { FontListComponent } from './font-list.component';

describe('FontListComponent', () => {
	const modalServ: jasmine.SpyObj<ModalService> = jasmine.createSpyObj('ModalService', [
		'invalidVariables',
		'editFontName',
		'deleteConfirm'
	]);
	let component: FontListComponent;
	let fixture: ComponentFixture<FontListComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [FontListComponent, OrderByPipe, ...unitTestDeclarations],
		providers: [...unitTestProviders, { provide: ModalService, useValue: modalServ }]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(FontListComponent);
		component = fixture.componentInstance;
		component.fontConfigs = signal(testData.fontConfigs);
		component.selectFont = signal(component.fontConfigs()[0].id());
		fixture.detectChanges();
		spyOn(component.selectFontChange, 'emit');
		spyOn(component.fontOrderChange, 'emit');
		spyOn(component.deleteFont, 'emit');
		spyOn(ConstFunctions, 'showDragCursor').and.callThrough();
		modalServ.invalidVariables.and.rejectWith('closed');
	});

	afterEach(() => {
		modalServ.invalidVariables.calls.reset();
		modalServ.editFontName.calls.reset();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	describe('should open edit font name modal', () => {
		const newFontName = 'test_system';
		let font: FontConfig;
		let ogFontName: string;

		beforeEach(() => {
			font = component.fontConfigs()[0];
			ogFontName = font.name();
			modalServ.editFontName.and.resolveTo(newFontName);
		});

		it('when result of name', async () => {
			await component.openEditNameModal(font);
			expect(modalServ.editFontName).toHaveBeenCalledWith(ogFontName);
			expect(font.name()).toEqual(newFontName);
		});

		it('when result of name canceled', async () => {
			modalServ.editFontName.and.rejectWith('canceled');

			await component.openEditNameModal(font);
			expect(modalServ.editFontName).toHaveBeenCalledWith(ogFontName);
			expect(font.name()).toEqual(ogFontName);
		});
	});

	it('should open invalid variables modal', async () => {
		const font = component.fontConfigs()[0];
		await component.openInvalidVariablesModal(font);
		expect(modalServ.invalidVariables).toHaveBeenCalledWith(
			font.variables().filter(confVar => !confVar.configValue().valid())
		);
	});

	describe('should open delete confirm modal', () => {
		it('when deletConfirm resolves', async () => {
			const font = component.fontConfigs()[0];
			modalServ.deleteConfirm.and.resolveTo();
			await component.confirmDelete(font);
			expect(modalServ.deleteConfirm).toHaveBeenCalledWith(font.name());

			// Expect deleteFont emit to be called
			expect(component.deleteFont.emit).toHaveBeenCalledWith(font);
		});

		// when deleteConfirm rejects
		it('when deleteConfirm rejects', async () => {
			const font = component.fontConfigs()[0];
			modalServ.deleteConfirm.and.rejectWith('canceled');
			await component.confirmDelete(font);
			expect(modalServ.deleteConfirm).toHaveBeenCalledWith(font.name());

			// do not expect deleteFont emit to be called
			expect(component.deleteFont.emit).not.toHaveBeenCalled();
		});
	});

	it('should start dragging', () => {
		component.dragStart();
		expect(ConstFunctions.showDragCursor).toHaveBeenCalledWith(true);
	});

	it('should drop', () => {
		const fontorder = [...component.fontIdOrder()];
		const dragDrop = {
			previousIndex: 2,
			currentIndex: 4
		} as unknown as CdkDragDrop<number[]>;

		component.drop(dragDrop);
		expect(component.fontIdOrder()).not.toEqual(fontorder);
		moveItemInArray(fontorder, dragDrop.previousIndex, dragDrop.currentIndex);
		expect(component.fontIdOrder()).toEqual(fontorder);
		expect(component.fontOrderChange.emit).toHaveBeenCalledWith(component.fontIdOrder());
		expect(ConstFunctions.showDragCursor).toHaveBeenCalledWith(false);
	});
});
