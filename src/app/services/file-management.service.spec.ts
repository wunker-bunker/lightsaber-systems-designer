import { moveItemInArray } from '@angular/cdk/drag-drop';
import { fakeAsync, flushMicrotasks, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { FSWatcher } from 'fs';
import { CopyOptions, Stats, WatchListener } from 'fs-extra';
import { join } from 'path';
import { Subscription } from 'rxjs';
import {
	testExportPath,
	testFonts,
	testLibraryLocation,
	testNewFontName,
	testNewFontNoLedName,
	testNewFontNoLedPath,
	testNewFontPath,
	testNewLibraryLocation,
	testNewSoundPath,
	testNewSystemLibraryPath,
	testNewSystemName,
	testNewSystemPath,
	testSoundFileNames,
	testSystemDirEntries,
	testSystemList,
	testSystemLocation1,
	testSystemLocations
} from 'src/environments/test-data.karma';
import {
	accessSpy,
	configureTestSuite,
	copySpy,
	existsSyncSpy,
	lstatSyncSpy,
	mkdirSyncSpy,
	moveSpy,
	moveSyncSpy,
	opendirSyncSpy,
	readdirSyncSpy,
	readFileSyncSpy,
	rmSpy,
	testData,
	unitTestImports,
	unitTestProviders,
	watchSpy,
	writeFileSpy,
	writeFileSyncSpy
} from 'src/environments/test-defaults.karma';
import { IntegerValue } from '../classes/config-value/integer-value';
import { LightsaberSystem } from '../classes/lightsaber-system';
import { SoundFile } from '../classes/sound-file';
import { FONT_REGEX } from '../constants/regex';
import { LED_DEFAULT_TEXT, LIBRARY_DIR_NAME } from '../constants/strings';
import { ConfigFileType } from '../enums/config-file-type.enum';
import { AppLoaderService } from './app-loader.service';
import { FileManagementService } from './file-management.service';
import { LightsaberSystemsService } from './lightsaber-systems.service';

/*
 * These tests are dependent on the spies set in test-defaults.karma.ts, configurePreload.
 * If you add or remove functionality from the FileManagementService,
 * you may need to update the spies.
 */
describe('FileManagementService', () => {
	let service: FileManagementService;
	let router: Router;
	let systemlistSubscriber: Subscription;
	let watchCallback: WatchListener<Buffer> | WatchListener<string> | WatchListener<string | Buffer> | undefined;
	let watchCloseSpy: jasmine.Spy;
	let saveSpy: jasmine.Spy;

	const loaderServ: jasmine.SpyObj<AppLoaderService> = jasmine.createSpyObj('AppLoaderService', [
		'setState',
		'setSubMessage'
	]);

	const expectCheckIfCfxDirectory = (systemPath: string) => {
		expect(existsSyncSpy).toHaveBeenCalledWith(systemPath);
		expect(lstatSyncSpy).toHaveBeenCalledWith(systemPath);
		expect(opendirSyncSpy).toHaveBeenCalledWith(systemPath);
		expect(existsSyncSpy).toHaveBeenCalledWith(join(systemPath, testFonts[0], ConfigFileType.FONT));
	};

	const expectProcessFontDir = (path: string, ledExists = true) => {
		expect(readFileSyncSpy).toHaveBeenCalledWith(join(path, ConfigFileType.FONT), 'utf-8');
		expect(opendirSyncSpy).toHaveBeenCalledWith(join(path, 'tracks'));

		if (ledExists) {
			expect(readFileSyncSpy).toHaveBeenCalledWith(join(path, ConfigFileType.LEDS), 'utf-8');
		}
	};

	const expectProcessSystemDir = (systemPath: string) => {
		expect(opendirSyncSpy).toHaveBeenCalledWith(systemPath);
		expect(readFileSyncSpy).toHaveBeenCalledWith(join(systemPath, ConfigFileType.MAIN), 'utf-8');
		expect(readFileSyncSpy).toHaveBeenCalledWith(join(systemPath, ConfigFileType.PREF), 'utf-8');
		expect(readFileSyncSpy).toHaveBeenCalledWith(join(systemPath, ConfigFileType.COLORS), 'utf-8');
		expect(readFileSyncSpy).toHaveBeenCalledWith(join(systemPath, ConfigFileType.SPECIAL), 'utf-8');

		testFonts.forEach(font => expectProcessFontDir(join(systemPath, font)));
	};

	const expectSystemCopy = (systemPath: string, exportPath: string, copyOpts: CopyOptions) => {
		for (const entry of testSystemDirEntries) {
			expect(copySpy).toHaveBeenCalledWith(join(systemPath, entry), join(exportPath, entry), copyOpts);
		}
	};

	const setupWatchSpy = () => {
		(watchSpy as jasmine.Spy).and.callFake((path, callback) => {
			watchCallback = callback;
			watchCloseSpy = jasmine.createSpy('close').and.callFake(() => console.log('watch closed'));
			return { close: watchCloseSpy } as unknown as FSWatcher;
		});
	};

	configureTestSuite({
		imports: unitTestImports,
		providers: [...unitTestProviders, { provide: AppLoaderService, useValue: loaderServ }]
	});

	beforeEach(async () => {
		testData.generateTestData();
		setupWatchSpy();
		service = TestBed.inject(FileManagementService);
		router = TestBed.inject(Router);

		saveSpy = spyOn(service, 'saveSystem').and.callThrough();
	});

	afterEach(() => {
		if (systemlistSubscriber) {
			systemlistSubscriber.unsubscribe();
		}
		loaderServ.setState.calls.reset();
		loaderServ.setSubMessage.calls.reset();
	});

	it('should be created', fakeAsync(() => {
		expect(service).toBeTruthy();
		expect(existsSyncSpy).toHaveBeenCalledWith(testLibraryLocation);
		expect(mkdirSyncSpy).not.toHaveBeenCalledWith(testLibraryLocation);
		expect(existsSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems'));
		expect(mkdirSyncSpy).not.toHaveBeenCalledWith(join(testLibraryLocation, 'systems'));
		expect(existsSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'fonts'));
		expect(mkdirSyncSpy).not.toHaveBeenCalledWith(join(testLibraryLocation, 'fonts'));
		expect(existsSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'colors'));
		expect(mkdirSyncSpy).not.toHaveBeenCalledWith(join(testLibraryLocation, 'colors'));
		expect(existsSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'profiles'));
		expect(mkdirSyncSpy).not.toHaveBeenCalledWith(join(testLibraryLocation, 'profiles'));
		expect(readdirSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems'));
		// tick();
		flushMicrotasks();
		expect(watchSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems'), watchCallback);
		expect(service.systemList()).toEqual(testSystemList);

		for (const system of testSystemList) {
			expect(existsSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems', system));
			expect(lstatSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems', system));
			expect(readdirSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems', system));
			expect(opendirSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems', system));
		}
	}));

	describe('should delete font', () => {
		it('deletes font', async () => {
			const currentFirstFont = testData.system.getFontConfigById(1);
			const currentSecondFont = testData.system.getFontConfigById(2);
			const currentThirdFont = testData.system.getFontConfigById(3);
			const currentFontCount = testData.system.fontConfigs().length;

			if (!currentFirstFont || !currentSecondFont || !currentThirdFont) {
				throw new Error('Fonts not found');
			}

			expect(currentFirstFont.id()).toBe(1);
			expect(currentFirstFont.name()).toBe('NIGHTFALL');
			expect(currentSecondFont.id()).toBe(2);
			expect(currentSecondFont.name()).toBe('SHOTO');
			expect(currentThirdFont.id()).toBe(3);
			expect(currentThirdFont.name()).toBe('CRYSTAL_FOCUS_REBOOT');

			await service.deleteFont(currentFirstFont, testData.system);

			const newFirstFont = testData.system.getFontConfigById(1);
			const newSecondFont = testData.system.getFontConfigById(2);

			if (!newFirstFont || !newSecondFont) {
				throw new Error('Fonts not found');
			}

			expect(newFirstFont.id()).toBe(1);
			expect(newFirstFont.name()).toBe('SHOTO');
			expect(newSecondFont.id()).toBe(2);
			expect(newSecondFont.name()).toBe('CRYSTAL_FOCUS_REBOOT');
			expect(testData.system.fontConfigs().length).toBe(currentFontCount - 1);
			expect(rmSpy).toHaveBeenCalledWith(
				join(testData.system.path, `${currentFirstFont.id}-${currentFirstFont.name}`),
				{
					recursive: true,
					force: true
				}
			);
			expect(saveSpy).toHaveBeenCalledWith(testData.system);
		});
	});

	describe('should delete sound', () => {
		it('deletes sound', async () => {
			const soundToDelete = testData.system.getSoundsById(1)[0];
			expect(soundToDelete.soundName).toEqual(testSoundFileNames[0].split('.')[0]);

			await service.deleteSound(soundToDelete, testData.system);

			const newFirstSound = testData.system.getSoundsById(1)[0];
			expect(soundToDelete).not.toEqual(newFirstSound);
			expect(newFirstSound.soundName).toEqual(testSoundFileNames[1].split('.')[0]);
			expect(testData.system.getSoundsById(1).some(sound => sound.soundName === 'blaster')).toBeFalse();
			expect(rmSpy).toHaveBeenCalledWith(soundToDelete.filePath, {
				force: true
			});
			expect(saveSpy).toHaveBeenCalledWith(testData.system);
		});

		it('will not delete sound if file is not a wav', async () => {
			existsSyncSpy.and.returnValue(true);
			const newSoundPath = join(testSystemLocation1, testFonts[0], 'test.mp3');
			const newSound = new SoundFile(newSoundPath, newSoundPath);

			try {
				await service.deleteSound(newSound, testData.system);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(newSoundPath);
				expect(rmSpy).not.toHaveBeenCalled();
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});

		it('will not delete sound if file does not exist', async () => {
			const newSoundPath = join('file', 'does', 'not', 'exist', 'test.mp3');
			const newSound = new SoundFile(newSoundPath, newSoundPath);

			try {
				await service.deleteSound(newSound, testData.system);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(newSoundPath);
				expect(rmSpy).not.toHaveBeenCalled();
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should export configs', () => {
		it('exports configs', async () => {
			const newGateVarValue = 20;
			const gateVar = testData.system.mainConfig().getVariableByName('gate');
			const gateConfigValue = gateVar?.configValue();

			if (!gateConfigValue || !IntegerValue.isIntegerValue(gateConfigValue)) {
				throw new Error('Gate variable not found');
			}

			expect(gateConfigValue.value()).not.toEqual(newGateVarValue);

			gateConfigValue.value.set(newGateVarValue);

			await service.exportConfigs(testSystemLocations[testSystemList[1]], testData.system);

			const copiedSystem = saveSpy.calls.mostRecent().args[0];

			expect(saveSpy).toHaveBeenCalled();
			expect(copiedSystem.mainConfig().getVariableByName('gate').configValue().value()).toEqual(newGateVarValue);
			expectCheckIfCfxDirectory(testData.systems.find(system => system.name === testSystemList[1])?.path ?? '');
			expectProcessSystemDir(testData.systems.find(system => system.name === testSystemList[1])?.path ?? '');
		});

		it('when it is not a CFX directory', async () => {
			try {
				await service.exportConfigs(
					join(testSystemLocations[testSystemList[0]], testFonts[0]),
					testData.system
				);
				fail();
			} catch (e) {
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});

		it('when the directory is empty', async () => {
			readdirSyncSpy.and.rejectWith();
			try {
				await service.exportConfigs(join('path', 'to', 'empty', 'dir'), testData.system);
				fail();
			} catch (e) {
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should export system', () => {
		const copyOpts = { recursive: true };

		it('exports system to empty directory', async () => {
			await service.exportSystem(testExportPath, testData.system);

			expect(existsSyncSpy).toHaveBeenCalledWith(testExportPath);
			expect(lstatSyncSpy).toHaveBeenCalledWith(testExportPath);
			expect(readdirSyncSpy).toHaveBeenCalledWith(testExportPath);
			expectSystemCopy(testData.system.path, testExportPath, copyOpts);
			expect(rmSpy).not.toHaveBeenCalled();
		});

		it('when the export path is a CFX directory', async () => {
			const rmOpts = { recursive: true, force: true };
			const systemToExportTo = testData.systems.find(system => system.name === testSystemList[1]);

			if (!systemToExportTo) {
				throw new Error('System not found');
			}

			await service.exportSystem(systemToExportTo.path, testData.system);

			expectCheckIfCfxDirectory(systemToExportTo.path);
			expect(existsSyncSpy).toHaveBeenCalledWith(systemToExportTo.path);
			expect(readdirSyncSpy).toHaveBeenCalledWith(systemToExportTo.path);
			expect(rmSpy).toHaveBeenCalledWith(systemToExportTo.mainConfig().filePath, rmOpts);
			expect(rmSpy).toHaveBeenCalledWith(systemToExportTo.colorConfig().filePath, rmOpts);
			expect(rmSpy).toHaveBeenCalledWith(systemToExportTo.prefConfig().filePath, rmOpts);
			expect(rmSpy).toHaveBeenCalledWith(systemToExportTo.specialConfig().filePath, rmOpts);

			systemToExportTo
				.fontConfigs()
				.forEach(font =>
					expect(rmSpy).toHaveBeenCalledWith(
						join(systemToExportTo.path, `${font.id()}-${font.name()}`),
						rmOpts
					)
				);
			expectSystemCopy(testData.system.path, systemToExportTo.path, copyOpts);
		});

		it('when it is not a CFX directory', async () => {
			try {
				await service.exportSystem(join(testExportPath, testFonts[0]), testData.system);
				fail();
			} catch (e) {
				expect(rmSpy).not.toHaveBeenCalled();
				expect(copySpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should get system list', () => {
		it('gets system list', async () => {
			const systemList = await service.getSystemList();
			expect(systemList).toEqual(testSystemList);
			testData.systems.forEach(system => expectCheckIfCfxDirectory(system.path));
		});

		it('when the system list is empty', async () => {
			readdirSyncSpy.and.returnValue([]);
			const systemList = await service.getSystemList();
			expect(systemList).toEqual([]);
		});

		it('when readdirSync throws an error', async () => {
			readdirSyncSpy.and.throwError('test error');
			const systemList = await service.getSystemList();
			expect(systemList).toEqual([]);
		});
	});

	describe('should import configs', () => {
		it('imports configs', async () => {
			const newGateVarValue = 20;
			const systemToImportFrom = testData.systems.find(system => system.name === testSystemList[1]);
			let gateConfigValue = testData.system?.mainConfig().getVariableByName('gate')?.configValue();

			if (!systemToImportFrom || !gateConfigValue || !IntegerValue.isIntegerValue(gateConfigValue)) {
				throw new Error('Gate variable not found');
			}

			gateConfigValue.value.set(newGateVarValue);
			expect(gateConfigValue.value()).toEqual(newGateVarValue);

			await service.importConfigs(systemToImportFrom.path, testData.system);

			gateConfigValue = testData.system.mainConfig().getVariableByName('gate')?.configValue();

			if (!gateConfigValue || !IntegerValue.isIntegerValue(gateConfigValue)) {
				throw new Error('Gate variable not found');
			}

			expect(gateConfigValue.value()).not.toEqual(newGateVarValue);
			expectProcessSystemDir(systemToImportFrom.path);
			expect(saveSpy).toHaveBeenCalled();
		});

		it('when it is not a CFX directory', async () => {
			try {
				await service.importConfigs(
					join(testSystemLocations[testSystemList[0]], testFonts[0]),
					testData.system
				);
				fail();
			} catch (e) {
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});

		it('when the directory is empty', async () => {
			readdirSyncSpy.and.returnValue([]);
			try {
				await service.importConfigs(join('path', 'to', 'empty', 'dir'), testData.system);
				fail();
			} catch (e) {
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should import font from directory', () => {
		it('imports font from directory', async () => {
			const oldFontCount = testData.system.fontConfigs().length;

			await service.importFontFromDir(testNewFontPath, testNewFontName, testData.system);

			const newFont = testData.system.fontConfigs().find(font => font.name() === testNewFontName);

			if (!newFont) {
				throw new Error('New font not found');
			}

			const newLedConfig = testData.system.getLedConfigById(newFont.id());

			if (!newLedConfig) {
				throw new Error('New led config not found');
			}

			const newFontPath = join(testData.system.path, `${newFont.id()}-${newFont.name()}`);
			expect(newFont).toBeDefined();
			expect(newFont.name()).toEqual(testNewFontName);
			expect(newFont.id()).toEqual(oldFontCount + 1);
			expect(testData.system.fontConfigs().length).toEqual(oldFontCount + 1);
			expect(newLedConfig).toBeDefined();
			expect(newLedConfig.name()).toEqual(testNewFontName);
			expect(newLedConfig.id()).toEqual(oldFontCount + 1);
			expect(copySpy).toHaveBeenCalledWith(testNewFontPath, newFontPath, {
				recursive: true
			});
			expectProcessFontDir(newFontPath, false);
			expect(saveSpy).toHaveBeenCalled();
		});

		it('imports font without led config', async () => {
			const oldFontCount = testData.system.fontConfigs().length;

			await service.importFontFromDir(testNewFontNoLedPath, testNewFontNoLedName, testData.system);

			const newFont = testData.system.fontConfigs().find(font => font.name() === testNewFontNoLedName);

			if (!newFont) {
				throw new Error('New font not found');
			}

			const newLedConfig = testData.system.getLedConfigById(newFont.id());

			if (!newLedConfig) {
				throw new Error('New led config not found');
			}

			const newFontPath = join(testData.system.path, `${newFont.id()}-${newFont.name()}`);
			expect(newFont).toBeDefined();
			expect(newFont.name()).toEqual(testNewFontNoLedName);
			expect(newFont.id()).toEqual(oldFontCount + 1);
			expect(testData.system.fontConfigs().length).toEqual(oldFontCount + 1);
			expect(newLedConfig).toBeDefined();
			expect(newLedConfig.name()).toEqual(testNewFontNoLedName);
			expect(newLedConfig.id()).toEqual(oldFontCount + 1);
			expect(copySpy).toHaveBeenCalledWith(testNewFontNoLedPath, newFontPath, {
				recursive: true
			});

			expect(writeFileSyncSpy).toHaveBeenCalledWith(
				join(newFontPath, ConfigFileType.LEDS),
				LED_DEFAULT_TEXT,
				'utf-8'
			);
			expectProcessFontDir(newFontPath, false);
			expect(saveSpy).toHaveBeenCalled();
		});

		it('when the font directory is empty', async () => {
			existsSyncSpy.and.returnValue(false);

			try {
				await service.importFontFromDir(testNewFontPath, testNewFontName, testData.system);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(join(testNewFontPath, ConfigFileType.FONT));
			}
		});
	});

	describe('should import sound', () => {
		it('imports sound', async () => {
			const fontId = 1;
			const oldSoundCount = testData.system.sounds().length;
			const newSoundName = testData.system.getAvailableSoundNames(fontId)[0];
			const font = testData.system.getFontConfigById(fontId);

			if (!font) {
				throw new Error('Font not found');
			}

			await service.importSound(testNewSoundPath, newSoundName, testData.system, fontId, false);

			expect(testData.system.sounds().length).toEqual(oldSoundCount + 1);
			expect(testData.system.getSoundsById(fontId).find(sound => sound.soundName === newSoundName)).toBeDefined();
			expect(copySpy).toHaveBeenCalledWith(
				testNewSoundPath,
				join(testData.system.path, `${font.id()}-${font.name()}`, `${newSoundName}.wav`)
			);
		});

		it('imports track', async () => {
			const fontId = 1;
			const oldSoundCount = testData.system.sounds().length;
			const font = testData.system.getFontConfigById(fontId);
			const newSoundName = testData.system.getAvailableTrackName(fontId)[0];

			if (!font) {
				throw new Error('Font not found');
			}

			await service.importSound(testNewSoundPath, newSoundName, testData.system, fontId, true);

			expect(testData.system.sounds().length).toEqual(oldSoundCount + 1);
			expect(testData.system.getTracksById(fontId).find(sound => sound.soundName === newSoundName)).toBeDefined();
			expect(copySpy).toHaveBeenCalledWith(
				testNewSoundPath,
				join(testData.system.path, `${font.id()}-${font.name()}`, 'tracks', `${newSoundName}.wav`)
			);
		});

		it('when the sound file does not exist', async () => {
			const fontId = 1;
			const newSoundName = testData.system.getAvailableSoundNames(fontId)[0];
			existsSyncSpy.and.returnValue(false);

			try {
				await service.importSound(testNewSoundPath, newSoundName, testData.system, fontId, false);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(testNewSoundPath);
			}
		});

		it('when the sound file is not a wav file', async () => {
			const fontId = 1;
			const newSoundName = testData.system.getAvailableSoundNames(fontId)[0];
			existsSyncSpy.calls.reset();

			try {
				await service.importSound('test.mp3', newSoundName, testData.system, fontId, false);
				fail();
			} catch (e) {
				expect(existsSyncSpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should import system from directory', () => {
		it('imports system', async () => {
			const systemServ = TestBed.inject(LightsaberSystemsService);
			const oldSystemCount = systemServ.size();
			saveSpy.and.resolveTo();

			const newSystem = await service.importSystemFromDir(testNewSystemPath, testNewSystemName);

			expectCheckIfCfxDirectory(testNewSystemPath);
			expect(newSystem).toBeDefined();
			expect(newSystem.name).toEqual(testNewSystemName);
			expect(systemServ.size()).toEqual(oldSystemCount + 1);
			expect(saveSpy).toHaveBeenCalledWith(newSystem);
			expect(existsSyncSpy).toHaveBeenCalledWith(testNewSystemLibraryPath);
			expect(mkdirSyncSpy).toHaveBeenCalledWith(testNewSystemLibraryPath);
			expect(readdirSyncSpy).toHaveBeenCalledWith(testNewSystemPath);
			expectSystemCopy(testNewSystemPath, testNewSystemLibraryPath, { recursive: true });
			expectProcessSystemDir(testNewSystemLibraryPath);
		});

		it('when the system directory is empty', async () => {
			opendirSyncSpy.calls.reset();
			readdirSyncSpy.and.returnValue([]);

			try {
				await service.importSystemFromDir(testNewSystemPath, testNewSystemName);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(testNewSystemPath);
				expect(lstatSyncSpy).toHaveBeenCalledWith(testNewSystemPath);
				expect(opendirSyncSpy).not.toHaveBeenCalled();
				expect(mkdirSyncSpy).not.toHaveBeenCalled();
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});

		it('when the directory does not exist', async () => {
			lstatSyncSpy.calls.reset();
			opendirSyncSpy.calls.reset();
			existsSyncSpy.and.returnValue(false);

			try {
				await service.importSystemFromDir(testNewSystemPath, testNewSystemName);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(testNewSystemPath);
				expect(lstatSyncSpy).not.toHaveBeenCalled();
				expect(opendirSyncSpy).not.toHaveBeenCalled();
				expect(mkdirSyncSpy).not.toHaveBeenCalled();
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});

		it('when the system already exists', async () => {
			try {
				await service.importSystemFromDir(testData.system.path, testData.system.name);
				fail();
			} catch (e) {
				expectCheckIfCfxDirectory(testData.system.path);
				expect(existsSyncSpy).toHaveBeenCalledWith(testData.system.path);
				expect(mkdirSyncSpy).not.toHaveBeenCalled();
				expect(copySpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should load system', () => {
		let systemServ: LightsaberSystemsService;

		beforeEach(() => {
			systemServ = TestBed.inject(LightsaberSystemsService);
			systemServ.clear();
			existsSyncSpy.calls.reset();
		});

		it('loads system', async () => {
			const oldSystemCount = systemServ.size();
			const system = await service.loadSystem(testData.system.name);

			expect(system).toBeDefined();
			expect(systemServ.size()).toEqual(oldSystemCount + 1);
			expect(systemServ.get(system.name)).toEqual(system);
			expectCheckIfCfxDirectory(testData.system.path);
			expectProcessSystemDir(testData.system.path);
		});

		it('when system already exists', async () => {
			systemServ.add(testData.system);
			const oldSystemCount = systemServ.size();
			const system = await service.loadSystem(testData.system.name);

			expect(system).toBeDefined();
			expect(systemServ.size()).toEqual(oldSystemCount);
			expect(systemServ.get(system.name)).toEqual(system);
			expect(existsSyncSpy).not.toHaveBeenCalled();
		});

		it('when system does not exist', async () => {
			try {
				await service.loadSystem('not found');
				fail();
			} catch (e) {
				expect(systemServ.size()).toEqual(0);
				expect(existsSyncSpy).toHaveBeenCalledWith(join(testLibraryLocation, 'systems', 'not found'));
			}
		});
	});

	describe('should move library', () => {
		beforeEach(fakeAsync(() => {
			spyOn(router, 'navigate');
			existsSyncSpy.calls.reset();
			lstatSyncSpy.calls.reset();
			accessSpy.calls.reset();
			moveSpy.calls.reset();
			(window.settings.setSync as jasmine.Spy).calls.reset();
		}));

		it('moves library', async () => {
			accessSpy.and.resolveTo();
			const currentWatchCloseSpy = watchCloseSpy;
			const newLibraryLocation = join(testNewLibraryLocation, LIBRARY_DIR_NAME);
			await service.moveLibrary(testNewLibraryLocation);

			expect(currentWatchCloseSpy).toHaveBeenCalled();
			expect(existsSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
			expect(lstatSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
			expect(accessSpy).toHaveBeenCalledWith(testNewLibraryLocation, window.fs.constants.W_OK);
			expect(moveSpy).toHaveBeenCalledWith(testLibraryLocation, newLibraryLocation);
			expect(router.navigate).toHaveBeenCalledWith(['systems', ''], {
				// We namvigate to undefined because there is no current system
				replaceUrl: true
			});
		});

		it('when the directory does not exist', async () => {
			existsSyncSpy.and.returnValue(false);
			try {
				await service.moveLibrary(testNewLibraryLocation);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
				expect(lstatSyncSpy).not.toHaveBeenCalled();
				expect(accessSpy).not.toHaveBeenCalled();
				expect(moveSpy).not.toHaveBeenCalled();
				expect(router.navigate).not.toHaveBeenCalled();
				expect(window.settings.setSync).not.toHaveBeenCalled();
			}
		});

		it('when the directory is not a directory', async () => {
			lstatSyncSpy.and.returnValue({ isDirectory: () => false } as unknown as Stats);
			try {
				await service.moveLibrary(testNewLibraryLocation);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
				expect(lstatSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
				expect(accessSpy).not.toHaveBeenCalled();
				expect(moveSpy).not.toHaveBeenCalled();
				expect(router.navigate).not.toHaveBeenCalled();
				expect(window.settings.setSync).not.toHaveBeenCalled();
			}
		});

		it('when the directory is not writable', async () => {
			accessSpy.and.rejectWith();
			try {
				await service.moveLibrary(testNewLibraryLocation);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
				expect(lstatSyncSpy).toHaveBeenCalledWith(testNewLibraryLocation);
				expect(accessSpy).toHaveBeenCalledWith(testNewLibraryLocation, window.fs.constants.W_OK);
				expect(moveSpy).not.toHaveBeenCalled();
				expect(router.navigate).not.toHaveBeenCalled();
				expect(window.settings.setSync).not.toHaveBeenCalled();
			}
		});
	});

	describe('should rename sound', () => {
		const fontId = 1;
		let oldSound: SoundFile;
		let newSoundName: string;
		let newSoundPath: string;

		beforeEach(() => {
			existsSyncSpy.calls.reset();
			oldSound = testData.system.getSoundsById(fontId)[0];
			newSoundName = testData.system.getAvailableSoundNames(fontId)[0];
			newSoundPath = join(testData.system.path, `${oldSound.id()}-${oldSound.name()}`, `${newSoundName}.wav`);
		});

		it('renames sound', async () => {
			const oldSoundCount = testData.system.getSoundsById(fontId).length;
			const newSound = await service.renameSound(newSoundName, oldSound, testData.system);
			const newSoundCount = testData.system.getSoundsById(fontId).length;

			expect(existsSyncSpy).toHaveBeenCalledWith(newSoundPath);
			expect(oldSound.soundName).not.toEqual(newSound.soundName);
			expect(
				testData.system.getSoundsById(fontId).find(sound => sound.soundName === oldSound.soundName)
			).toBeUndefined();
			expect(newSoundCount).toEqual(oldSoundCount);
			expect(moveSpy).toHaveBeenCalledWith(oldSound.filePath, newSoundPath);
			expect(saveSpy).toHaveBeenCalledWith(testData.system);
		});

		it('when the sound already exists', async () => {
			existsSyncSpy.and.returnValue(true);
			try {
				await service.renameSound(newSoundName, oldSound, testData.system);
				fail();
			} catch (e) {
				expect(existsSyncSpy).toHaveBeenCalledWith(newSoundPath);
				expect(moveSpy).not.toHaveBeenCalled();
				expect(saveSpy).not.toHaveBeenCalled();
			}
		});
	});

	describe('should save system', () => {
		const expectWriteConfigFile = (system: LightsaberSystem) => {
			expect(writeFileSpy).toHaveBeenCalledWith(
				system.mainConfig().filePath,
				system.mainConfig().toString(),
				'utf-8'
			);
			expect(writeFileSpy).toHaveBeenCalledWith(
				system.colorConfig().filePath,
				system.colorConfig().toString(),
				'utf-8'
			);
			expect(writeFileSpy).toHaveBeenCalledWith(
				system.prefConfig().filePath,
				system.prefConfig().toString(),
				'utf-8'
			);
			expect(writeFileSpy).toHaveBeenCalledWith(
				system.specialConfig().filePath,
				system.specialConfig().toString(),
				'utf-8'
			);

			for (const font of system.fontConfigs()) {
				expect(writeFileSpy).toHaveBeenCalledWith(font.filePath, font.toString(), 'utf-8');
			}

			for (const led of system.ledConfigs()) {
				expect(writeFileSpy).toHaveBeenCalledWith(led.filePath, led.toString(), 'utf-8');
			}
		};
		let ogFontConfigCount: number;
		let ogSoundCount: number;

		beforeEach(() => {
			existsSyncSpy.calls.reset();
			lstatSyncSpy.calls.reset();
			opendirSyncSpy.calls.reset();
			writeFileSpy.calls.reset();

			const volConfigValue = testData.system.mainConfig().getVariableByName('vol')?.configValue();

			if (!volConfigValue || !IntegerValue.isIntegerValue(volConfigValue)) {
				throw new Error('vol config value is not a percentage value');
			}

			volConfigValue.value.set(20);
			ogFontConfigCount = testData.system.fontConfigs().length;
			ogSoundCount = testData.system.sounds().length;
		});

		it('saves system', async () => {
			expect(testData.system.dirty()).toBeTruthy();

			await service.saveSystem(testData.system);

			expectCheckIfCfxDirectory(testData.system.path);
			expect(moveSyncSpy).not.toHaveBeenCalled();
			expectWriteConfigFile(testData.system);
			expect(testData.system.dirty()).toBeFalsy();
			expect(testData.system.fontConfigs().length).toEqual(ogFontConfigCount);
			expect(testData.system.sounds().length).toEqual(ogSoundCount);
		});

		it('when the font order has changed', async () => {
			const ogId = 4;
			const newId = 1;
			const newOrder = testData.system.fontConfigs().map(font => font.id());
			moveItemInArray(newOrder, ogId - 1, newId - 1); // Font IDs are 1 indexed

			testData.system.orderFonts(newOrder);
			const ogFontArray = testData.system.fontConfigs();

			await service.saveSystem(testData.system);

			expectCheckIfCfxDirectory(testData.system.path);
			expectWriteConfigFile(testData.system);
			expect(testData.system.fontConfigs().length).toEqual(ogFontConfigCount);
			expect(testData.system.sounds().length).toEqual(ogSoundCount);

			for (const font of ogFontArray) {
				if (font.hasNewId()) {
					const oldFontFolder = window.path.join(
						testData.system.path,
						`${font.filePath.replace(FONT_REGEX, '$1-$2')}`
					);
					const newFontFolder = window.path.join(testData.system.path, `${font.id()}-${font.name()}`);
					expect(moveSyncSpy).toHaveBeenCalledWith(oldFontFolder, newFontFolder);
				}
			}
		});

		it('when a font name has changed', async () => {
			const ogFont = testData.system.getFontConfigById(1);

			if (!ogFont) {
				throw new Error('OG Font is undefined');
			}

			const newFontName = 'new-font-name';
			const newFontPath = join(testData.system.path, `${ogFont.id()}-${newFontName}`);
			const ogFontPath = join(testData.system.path, `${ogFont.id()}-${ogFont.name()}`);
			ogFont.name.set(newFontName);

			await service.saveSystem(testData.system);

			expectCheckIfCfxDirectory(testData.system.path);
			expectWriteConfigFile(testData.system);
			expect(moveSyncSpy).toHaveBeenCalledWith(ogFontPath, newFontPath);
			expect(testData.system.fontConfigs().length).toEqual(ogFontConfigCount);
			expect(testData.system.sounds().length).toEqual(ogSoundCount);
		});
	});
});
