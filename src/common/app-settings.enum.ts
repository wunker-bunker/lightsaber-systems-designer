import { AppChannel } from './app-channel.enum';
import { UiThemes } from './ui-themes.enum';

export const APP_SETTINGS = {
	CHANNEL: 'channel',
	LIBRARY_LOCATION: 'location.library',
	THEME: 'theme'
} as const;

export type AppSettings = (typeof APP_SETTINGS)[keyof typeof APP_SETTINGS];

export type SettingsReturnType = {
	[APP_SETTINGS.CHANNEL]?: AppChannel;
	[APP_SETTINGS.LIBRARY_LOCATION]?: string;
	[APP_SETTINGS.THEME]?: UiThemes;
};
