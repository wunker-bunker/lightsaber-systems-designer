import { ChangeDetectionStrategy, Component, computed, Signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute, Router } from '@angular/router';
import { AppAlertsService } from 'src/app/services/app-alerts.service';
import { AppLoaderService } from 'src/app/services/app-loader.service';
import { FileManagementService } from 'src/app/services/file-management.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
	selector: 'lsd-systems-list',
	templateUrl: './system-list.component.html',
	styleUrls: ['./system-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemListComponent {
	selectedSystem: Signal<string | null | undefined>;
	systemList: Signal<readonly string[]>;

	constructor(
		private _fileServ: FileManagementService,
		private _router: Router,
		private _route: ActivatedRoute,
		public loaderServ: AppLoaderService,
		private _modalServ: ModalService,
		private _alertServ: AppAlertsService
	) {
		const params = toSignal(this._route.paramMap);
		this.selectedSystem = computed(() => params()?.get('systemName') ?? '');

		this.systemList = computed(() => this._fileServ.systemList());
	}

	async addSystem(): Promise<void> {
		let directoryPath: string;
		try {
			directoryPath = await this._modalServ.openDirectory('Import a CFX Lightsaber System');
		} catch (err) {
			console.log(err);
			return;
		}

		let name: string;
		try {
			name = await this._modalServ.systemName(this.systemList, directoryPath.split(/[\\/]/).pop());
		} catch (err) {
			console.log(err);
			console.log(`Stopped importing from ${directoryPath}`);
			return;
		}

		this.loaderServ.setState(true, 'Copying system to library...');
		try {
			const system = await this._fileServ.importSystemFromDir(directoryPath, name);
			this._router.navigate(['systems', system.name]);
			this._alertServ.showSuccessAlert(`Imported ${system.name}!`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to import system from ${directoryPath}: ${err}`, err);
		} finally {
			this.loaderServ.setState(false);
		}
	}
}
