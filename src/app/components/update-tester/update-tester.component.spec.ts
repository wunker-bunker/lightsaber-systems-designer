import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	debugTriggerDownloadProgressSpy,
	debugTriggerUpdateAvailableSpy,
	debugTriggerUpdateDownloadedSpy,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { UpdateTesterComponent } from './update-tester.component';

describe('UpdateTesterComponent', () => {
	let component: UpdateTesterComponent;
	let fixture: ComponentFixture<UpdateTesterComponent>;

	configureTestSuite({
		imports: [NgbModule, ...unitTestImports],
		declarations: [UpdateTesterComponent, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(UpdateTesterComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should update available', async () => {
		await component.updateAvailable();
		expect(debugTriggerUpdateAvailableSpy).toHaveBeenCalled();
	});

	it('should update downloaded', async () => {
		await component.updateDownloaded();
		expect(debugTriggerUpdateDownloadedSpy).toHaveBeenCalled();
	});

	it('should download progress', async () => {
		component.percentDownloaded = 25.5;
		await component.downloadProgress();
		expect(debugTriggerDownloadProgressSpy).toHaveBeenCalledWith(component.percentDownloaded);
	});
});
