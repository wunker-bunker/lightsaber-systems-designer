############################################
Memo color profiles :
0  - RED	vader OT, father
1  - Orange
2  - Amber
3  - Yellow
4  - Lime
5  - Lime2
6  - GREEN   	hero font
7  - Cyan
8  - Light
9  - Ice blue   graflex, rey
10 - BLUE       luke battle
11 - Mauve
12 - Purple
13 - Pink
14 - Crimson
15 - White      imperial knights

############################################
Memo blade profiles:
0  - Standard blade with a bit of flicker, standard bolts and lockup + full flare + dissolve-off takeover - VADER
1  - Standard blade with a bit of flicker, wave2 bolts random color + localized lockup + base flare + quicksilver-Off takeover
2  - Standard blade with a bit of flicker, expandable bolts random color + localized lockup 2 + quick silver + super nova
3  - Standard blade with less flicker + low pulse + movie flicker / graflex + classic bolts + force power ON
4  - Fire
5  - Fire heatmap
6  - Unstable
7  - Wave~
8  - Starkiller
9  - Unstable TFU
10 - Jedi fallen order
11 - Sauron heatmap
12 - Rainbow
13 - Rain sizzle with proper swing bright
14 - Theater demo (slow)
15 - Gradient
16 - Rain sizzle + SwingBright (Robert Sotomayor)