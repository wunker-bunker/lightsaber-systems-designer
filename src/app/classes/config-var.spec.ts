import { VALUE_TYPE } from '../enums/value-type.enum';
import { StringValue } from './config-value/string-value';
import { ConfigVar } from './config-var';

describe('ConfigVar', () => {
	const name = 'test';
	const value = 'value';
	const description = 'test description';
	let confVar: ConfigVar;

	beforeEach(() => {
		confVar = new ConfigVar(
			name,
			new StringValue(value, {
				description,
				type: VALUE_TYPE.STRING
			})
		);
	});

	it('should create an instance', () => {
		expect(confVar).toBeTruthy();
		expect(confVar.name).toEqual(name);
		expect(confVar.configValue().value()).toEqual(value);
		expect(confVar.configValue().definition.description).toEqual(description);
		expect(confVar.configValue().type).toEqual(VALUE_TYPE.STRING);
	});

	it('should print to string', () => {
		expect(confVar.toString()).toEqual(`${name}=${value}`);
	});
});
