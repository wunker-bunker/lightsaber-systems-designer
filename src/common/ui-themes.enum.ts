export enum UiThemes {
	LIGHT = 'theme-light',
	DARK = 'theme-dark'
}
