import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { join } from 'path';
import { AppLoaderService } from 'src/app/services/app-loader.service';
import { FileManagementService } from 'src/app/services/file-management.service';
import { APP_SETTINGS } from 'src/common/app-settings.enum';
import {
	configureTestSuite,
	openUpdateDebuggerSpy,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { PreferencesComponent } from './preferences.component';

describe('PreferencesComponent', () => {
	const fileServ: jasmine.SpyObj<FileManagementService> = jasmine.createSpyObj('FileManagementService', [
		'moveLibrary'
	]);
	const loaderServ: jasmine.SpyObj<AppLoaderService> = jasmine.createSpyObj('AppLoaderService', ['setState']);
	const newPath = join('.', 'test-resources', 'newPath');
	let component: PreferencesComponent;
	let fixture: ComponentFixture<PreferencesComponent>;
	let logSpy: jasmine.Spy;
	let errorSpy: jasmine.Spy;

	configureTestSuite({
		imports: [NgbModule, ...unitTestImports],
		declarations: [PreferencesComponent, ...unitTestDeclarations],
		providers: [
			{ provide: FileManagementService, useValue: fileServ },
			{ provide: AppLoaderService, useValue: loaderServ },
			...unitTestProviders
		]
	});

	beforeEach(() => {
		testData.generateTestData();
		fixture = TestBed.createComponent(PreferencesComponent);
		component = fixture.componentInstance;
		component.openDirectory = () => Promise.resolve(newPath);
		fixture.detectChanges();
		logSpy = spyOn(console, 'log');
		errorSpy = spyOn(console, 'error');
		fileServ.moveLibrary.and.resolveTo();
	});

	afterEach(() => {
		fileServ.moveLibrary.calls.reset();
		loaderServ.setState.calls.reset();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	describe('should choose library path', () => {
		it('when directory found and successful move', fakeAsync(async () => {
			(window.settings.getSync as jasmine.Spy).and.returnValue(newPath);
			await component.chooseLibPath();
			expect(logSpy).toHaveBeenCalledWith('Moving Library', newPath);
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Moving Library to ${newPath}`);
			tick();
			expect(window.settings.getSync).toHaveBeenCalledWith(APP_SETTINGS.LIBRARY_LOCATION);
			expect(fileServ.moveLibrary).toHaveBeenCalledWith(newPath);
		}));

		it('when directory found and failed move', fakeAsync(async () => {
			fileServ.moveLibrary.and.rejectWith('fail');
			await component.chooseLibPath();
			expect(logSpy).toHaveBeenCalledWith('Moving Library', newPath);
			expect(loaderServ.setState).toHaveBeenCalledWith(true, `Moving Library to ${newPath}`);
			tick();
			expect(fileServ.moveLibrary).toHaveBeenCalledWith(newPath);
		}));

		it('when directory canceled', async () => {
			component.openDirectory = () => Promise.reject('canceled');
			await component.chooseLibPath();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.moveLibrary).not.toHaveBeenCalled();
			expect(errorSpy).not.toHaveBeenCalled();
			expect(logSpy).toHaveBeenCalled();
		});
	});

	it('should open update debugger', async () => {
		await component.openUpdateDebugger();
		expect(openUpdateDebuggerSpy).toHaveBeenCalled();
	});
});
