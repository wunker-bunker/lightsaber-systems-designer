/**
 * Used profiles
 */
export enum UsedProfiles {
	BLADE = 'used_profiles_blade',
	COLOR = 'used_profiles_color'
}
