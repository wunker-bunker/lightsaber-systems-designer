import { ChangeDetectionStrategy, Component, NgZone, signal } from '@angular/core';

@Component({
	selector: 'lsd-title-bar',
	templateUrl: './title-bar.component.html',
	styleUrls: ['./title-bar.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TitleBarComponent {
	maximized = signal(false);
	platform: NodeJS.Platform;

	constructor(private _ngZone: NgZone) {
		this.platform = window.electronApi.getPlatform();
		window.electronApi.onAppMaximized((isMaximized: boolean) =>
			this._ngZone.run(() => this.maximized.set(isMaximized))
		);
	}

	async close(): Promise<void> {
		return window.electronApi.closeApp();
	}

	async maxOrRestore(): Promise<void> {
		if (this.maximized()) {
			return window.electronApi.restoreApp();
		}
		return window.electronApi.maximizeApp();
	}

	async minimize(): Promise<void> {
		return window.electronApi.minimizeApp();
	}
}
