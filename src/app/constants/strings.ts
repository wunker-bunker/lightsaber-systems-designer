/**
 * Header for the config files
 */
export const FILE_HEADER = '// Created by Lightsaber Systems Designer\n';

/**
 * The name of the directory where the library is stored.
 */
export const LIBRARY_DIR_NAME = 'Lightsaber Systems Designer';

/**
 * Default text for the led config file.
 * Used when a font does not have an led config file.
 */
export const LED_DEFAULT_TEXT = `
// Blinking LED configuration file
## NOTES : only accents #2-5-6-7-8-9 can be PWM / pulsing / mirror / audio flicker

polarity=NNNNNNNN

[POWERON]
accents=aaaaaaao
##accents=aaaa1234
<repeat=1>
state=10000000
state=11000000
state=11100000
state=11110000
state=11111000
state=11111100
state=11111110
state=11111111
stop
</repeat>


[POWEROFF]
accents=aaaaa321
##accents=aaaa1234
<repeat=6>
state=01010101
state=10101010
stop
</repeat>


[LEDS]
accents=aaaaaaao
accpulsed=90
accpulsel=1000

<forever>
delay=100
state=10000000
state=01000000
state=00100000
state=00010000
state=00001000
state=00000100
state=00000010
state=00000001
state=00000010
state=00000100
state=00010000
state=00100000
state=01000000
</forever>


[IDLE]
accents=aaaaCRYS
accpulsed=99
accpulsel=2000

<forever>
delay=1000
state=00000000
state=10000000
state=01000000
state=11000000
state=00100000
state=10100000
state=01100000
state=11100000
state=00010000
state=10010000
state=01010000
state=11010000
state=00110000
state=10110000
state=01110000
state=11110000
state=00001000
state=10001000
state=01001000
state=11001000
state=00101000
state=10101000
state=01101000
state=11101000
state=00011000
state=10011000
state=01011000
state=11011000
state=00111000
state=10111000
state=01111000
state=11111000
</forever>

[LOCKUP]
accents=rrrrraao
accpulsed=90
accpulsel=200

<forever>
delay=100
state=10000000
state=10000000
</forever>`;
