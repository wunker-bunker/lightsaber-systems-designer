import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { IntegerInputComponent } from './integer-input.component';

describe('IntegerInputComponent', () => {
	let component: IntegerInputComponent;
	let fixture: ComponentFixture<IntegerInputComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [IntegerInputComponent, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(IntegerInputComponent);
		component = fixture.componentInstance;
		component.input = testData.intValues[0];
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
