import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite, testData, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { BooleanInputComponent } from './boolean-input/boolean-input.component';
import { ConfigVarRowComponent } from './config-var-row.component';
import { EnumerationInputComponent } from './enumeration-input/enumeration-input.component';

describe('ConfigVarRowComponent', () => {
	let component: ConfigVarRowComponent;
	let fixture: ComponentFixture<ConfigVarRowComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [ConfigVarRowComponent, EnumerationInputComponent, BooleanInputComponent],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ConfigVarRowComponent);
		component = fixture.componentInstance;
		component.confVar = testData.configVars[0];
		component.groups = testData.configGroups;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
