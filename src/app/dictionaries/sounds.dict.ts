import { SoundDictionary } from '../models/sound-dictionary.model';

/**
 * Dictionary of valid sound files.
 */
export const SOUND_DICT: SoundDictionary = {
	font: {
		description: 'a font/bank description sound (played when selectingthe font via the vocal menu)',
		loops: false,
		isMulti: false
	},
	boot: {
		description: 'boot sound',
		loops: false,
		isMulti: false
	},
	'boot#': {
		description: 'boot sounds randomly selected at boot time',
		loops: false,
		minCount: 2,
		isMulti: true
	},
	poweron: {
		description: 'power on sounds (up to 9 works fine with the angular selection)',
		loops: false,
		isMulti: false
	},
	'poweron#': {
		description: 'power on sounds (up to 9 works fine with the angular selection)',
		loops: false,
		minCount: 2,
		isMulti: true
	},
	poweronf: {
		description: 'power on Force',
		loops: false,
		isMulti: false
	},
	poweroff: {
		description: 'power off sounds+ alternate power off (when power off is triggered while moving)',
		loops: false,
		isMulti: false
	},
	'poweroff#': {
		description: 'power off sounds+ alternate power off (when power off is triggered while moving)',
		loops: false,
		minCount: 2,
		isMulti: true
	},
	pwroff2: {
		description: 'power off sounds+ alternate power off (when power off is triggered while moving)',
		loops: false,
		isMulti: false
	},
	hum: {
		description: 'monophonic (pre-mixed) font continuous humming sound',
		loops: true,
		isMulti: false
	},
	'humM#': {
		description: 'polyphonic font continuous humming sounds',
		loops: true,
		minCount: 1,
		isMulti: true
	},
	'clash#': {
		description: 'clash sounds',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'swing#': {
		description: 'swing sounds',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'slash#': {
		description: 'slash sounds',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	blaster: {
		description: 'blaster blocking sounds',
		loops: false,
		isMulti: false
	},
	'blaster#': {
		description: 'blaster blocking sounds',
		loops: false,
		minCount: 2,
		isMulti: true
	},
	lockup: {
		description: 'blade lockup sound',
		loops: true,
		isMulti: false
	},
	'lockup#': {
		description: 'additional lockup sounds',
		loops: true,
		minCount: 2,
		isMulti: true
	},
	drag: {
		description: 'blade drag sound',
		loops: true,
		isMulti: false
	},
	'drag#': {
		description: 'more drag sounds',
		loops: true,
		minCount: 2,
		isMulti: true
	},
	'endlock#': {
		description: 'lockup end sounds',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'enddrag#': {
		description: 'drag end sounds',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	force: {
		description: 'force effect (aux+swing)',
		loops: false,
		isMulti: false
	},
	'force#': {
		description:
			'force effect sound (aux+clash). If  more  than  2  force  sounds (force,  force2,  force3, force<N>) are found in the font, they all belong to the Force class and Force-Clash sounds must be named fclash<N>.wav',
		loops: false,
		minCount: 2,
		isMulti: true
	},
	'spin#': {
		description: 'spin sounds(if not there, swings are played instead)',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'stab#': {
		description: 'stab sounds(if not there, clashesare played instead)',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	color: {
		description: 'acolor profile transition sound, used also when accessing Spectrum™',
		loops: false,
		isMulti: false
	},
	'lswing#': {
		description: 'smoothswing hum pairs to be used in smoothswing font',
		loops: true,
		minCount: 1,
		isMulti: true
	},
	'hswing#': {
		description: 'smoothswing hum pairs to be used in smoothswing font',
		loops: true,
		minCount: 1,
		isMulti: true
	},
	'preon#': {
		description:
			'pre-power on sounds. Those sounds will be chosen randomly prior to the ignition power-on sounds. It allows the ability to add any type of sound (unmixed) prior to powering on the blade. It can be an iconic / character sound if that matches the saber “theme” or it can be a mechanical sound highlighting the activation switch, if the sounds are present in the font. To disable the feature, simply remove or rename the sounds.',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'pstoff#': {
		description:
			'post-power off sounds. Same as above, but played at the end of the power off sound. Essentially these are there to add a quote after having the saber turned off. While it can be simply tailed to the power off sound, having it separated provides more flexibility.',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'startlock#': {
		description: 'played just before starting the lockupsound',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'startdrag#': {
		description: 'played just before starting the drag sound',
		loops: false,
		minCount: 1,
		isMulti: true
	},
	'track#': {
		description: 'Audio tracks',
		loops: false,
		minCount: 1,
		isMulti: true
	}
};

export const SOUND_UNKNOWN_DESCR = 'Unknown sound file';
