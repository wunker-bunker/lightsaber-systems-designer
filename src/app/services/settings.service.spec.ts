import { TestBed } from '@angular/core/testing';

import { UiThemes } from 'src/common/ui-themes.enum';
import { testLibraryLocation } from 'src/environments/test-data.karma';
import { configureTestSuite, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { AppChannel } from '../../common/app-channel.enum';
import { SettingsService } from './settings.service';

describe('SettingsService', () => {
	let service: SettingsService;

	configureTestSuite({
		imports: unitTestImports,
		providers: unitTestProviders
	});

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(SettingsService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
		expect(service.channel()).toEqual(AppChannel.LATEST);
		expect(service.libraryLocation()).toEqual(testLibraryLocation);
		expect(service.theme()).toEqual(UiThemes.LIGHT);
	});
});
