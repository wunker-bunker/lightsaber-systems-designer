/**
 * Enum for the spectrum axis.
 */
export enum SpectrumAxis {
	PITCH = 0,
	ROLL
}
