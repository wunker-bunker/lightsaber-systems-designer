import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { ColorToRgbStylePipe } from './color-to-rgb-style.pipe';

describe('ColorToRgbStylePipe', () => {
	const pipe = new ColorToRgbStylePipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('transforms color value', () => {
		const rgbw = {
			red: 0,
			green: COLOR_VAL_MAX,
			blue: COLOR_VAL_MAX,
			white: 0
		};
		expect(pipe.transform(rgbw)).toEqual(
			`rgba(${Math.round((rgbw.red * 255) / COLOR_VAL_MAX)},${Math.round(
				(rgbw.green * 255) / COLOR_VAL_MAX
			)},${Math.round((rgbw.blue * 255) / COLOR_VAL_MAX)},1)`
		);
	});
});
