/**
 * Board orientation
 */
export enum BoardOrientation {
	TOP_FRONT = 0,
	TOP_REAR,
	BOTTOM_FRONT,
	BOTTOM_REAR
}
