import { NgZone } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ProgressInfo } from 'builder-util-runtime';
import {
	configureTestSuite,
	onDownloadProgressSpy,
	onUpdateAvailableSpy,
	onUpdateDownloadedSpy,
	readyToCheckForUpdatesSpy,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { UpdateService, UpdateState } from './update.service';

describe('UpdateService', () => {
	let service: UpdateService;
	let onUpdateAvailableCallback: () => void;
	let onUpdateDownloadedCallback: () => void;
	let onDownloadProgressCallback: (progressInfo: ProgressInfo) => void;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		onUpdateAvailableSpy.and.callFake(callback => {
			onUpdateAvailableCallback = callback;
		});
		onUpdateDownloadedSpy.and.callFake(callback => {
			onUpdateDownloadedCallback = callback;
		});
		onDownloadProgressSpy.and.callFake(callback => {
			onDownloadProgressCallback = callback;
		});

		service = new UpdateService(TestBed.inject(NgZone));
	});

	afterEach(() => {
		onUpdateAvailableSpy.calls.reset();
		onUpdateDownloadedSpy.calls.reset();
		onDownloadProgressSpy.calls.reset();
		readyToCheckForUpdatesSpy.calls.reset();
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
		expect(readyToCheckForUpdatesSpy).toHaveBeenCalled();
	});

	it('should have update available', () => {
		onUpdateAvailableCallback();
		expect(service.updateState().state).toEqual(UpdateState.AVAILABLE);
		// service.onChange.subscribe((updateAvailable: UpdateObj) => {
		// 	expect(updateAvailable.state).toEqual(UpdateState.AVAILABLE);
		// });
	});

	it('should have update downloaded', () => {
		onUpdateDownloadedCallback();
		expect(service.updateState().state).toEqual(UpdateState.DOWNLOADED);
		// service.onChange.subscribe((updateAvailable: UpdateObj) => {
		// 	expect(updateAvailable.state).toEqual(UpdateState.DOWNLOADED);
		// });
	});

	it('should have update progress', () => {
		const progressInfo: ProgressInfo = {
			percent: 50,
			bytesPerSecond: 1000,
			total: 1000,
			transferred: 500,
			delta: 500
		};
		onDownloadProgressCallback(progressInfo);
		expect(service.updateState().state).toEqual(UpdateState.DOWNLOADING);
		// service.onChange.subscribe((updateAvailable: UpdateObj) => {
		// 	expect(updateAvailable.state).toEqual(UpdateState.DOWNLOADING);
		// 	expect(updateAvailable.percentDone).toEqual(50);
		// });
	});
});
