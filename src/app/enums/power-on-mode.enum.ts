/**
 * Power on mode
 */
export enum PowerOnMode {
	CURR_ANGLE = 0,
	CURR_ANGLE_POF,
	RAND_POF,
	RAND
}
