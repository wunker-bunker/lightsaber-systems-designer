import { ChangeDetectionStrategy, Component, computed } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';
import { AppAlertsService } from 'src/app/services/app-alerts.service';
import { AppLoaderService } from 'src/app/services/app-loader.service';
import { FileManagementService } from 'src/app/services/file-management.service';
import { SettingsService } from 'src/app/services/settings.service';
import { AppChannel } from 'src/common/app-channel.enum';
import { UiThemes } from 'src/common/ui-themes.enum';

@Component({
	selector: 'lsd-preferences',
	templateUrl: './preferences.component.html',
	styleUrls: ['./preferences.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreferencesComponent {
	readonly BUILDS = AppChannel;
	readonly THEMES = UiThemes;
	served;

	constructor(
		public activeModal: NgbActiveModal,
		private _loaderServ: AppLoaderService,
		private _fileServ: FileManagementService,
		private _alertServ: AppAlertsService,
		public settings: SettingsService
	) {
		const isServed = toSignal(from(window.electronApi.isServed()));
		this.served = computed(() => isServed() ?? false);
	}

	async chooseLibPath(): Promise<void> {
		let directoryPath: string;
		try {
			directoryPath = await this.openDirectory('Select a new location for your library');
		} catch (err) {
			console.log(err);
			return;
		}

		console.log('Moving Library', directoryPath);
		this._loaderServ.setState(true, `Moving Library to ${directoryPath}`);

		try {
			await this._fileServ.moveLibrary(directoryPath);
			this._alertServ.showSuccessAlert(`Moved library to ${directoryPath}`);
		} catch (err) {
			this._alertServ.showErrorAlert(`Failed to move library: ${err}`, err);
		} finally {
			this._loaderServ.setState(false);
		}
	}

	openDirectory: (title: string) => Promise<string> = () => Promise.reject();

	openSourceLink(): void {
		window.electronApi.openExternal('https://gitlab.com/wunker-bunker/lightsaber-systems-designer');
	}

	async openUpdateDebugger(): Promise<void> {
		return window.electronApi.openUpdateDebugger();
	}
}
