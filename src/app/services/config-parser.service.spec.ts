import { TestBed } from '@angular/core/testing';
import {
	colorConfigFile,
	fontConfigFiles,
	ledConfigFiles,
	mainConfigFile,
	prefConfigFile,
	specialConfigFile
} from 'src/environments/test-data.karma';
import { configureTestSuite, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { ColorConfig } from '../classes/config-file/color-config';
import { FontConfig } from '../classes/config-file/font-config';
import { LedConfig } from '../classes/config-file/led-config';
import { MainConfig } from '../classes/config-file/main-config';
import { PrefsConfig } from '../classes/config-file/prefs-config';
import { SpecialConfig } from '../classes/config-file/special-config';
import { SmoothSwingMode } from '../enums/smooth-swing-mode.enum';
import { ConfigParserService } from './config-parser.service';

describe('ConfigParserService', () => {
	let service: ConfigParserService;

	configureTestSuite({
		imports: unitTestImports,
		providers: [...unitTestProviders]
	});

	beforeEach(() => {
		service = TestBed.inject(ConfigParserService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	describe('should parse file text', () => {
		it('font config file', () => {
			const file = fontConfigFiles[0];
			const baseFile = service.parseFileText(file.fileName, file.filePath, file.fileText);

			if (!baseFile || !FontConfig.isFontConfig(baseFile)) {
				throw new Error('Base file is not a FontConfig');
			}

			expect(baseFile.getVariableByName('hum_gain')?.configValue().value()).toEqual(100);
			expect(baseFile.getVariableByName('smooth_mode')?.configValue().value()).toEqual(SmoothSwingMode.HYBRID);
			expect(baseFile.getVariableByName('bolt0')?.configValue().value()).toEqual([1030, 1349]);
		});

		it('main config file', () => {
			const baseFile = service.parseFileText(
				mainConfigFile.fileName,
				mainConfigFile.filePath,
				mainConfigFile.fileText
			);

			if (!baseFile || !MainConfig.isMainConfig(baseFile)) {
				throw new Error('Base file is not a MainConfig');
			}

			expect(baseFile.groups().length).toBe(27);
			expect(baseFile.getVariableByName('strippower')?.configValue().value()).toEqual([true, true, true, true]);
			expect(baseFile.getVariableByName('omnisabers')?.configValue().value()).toBeTrue();
			expect(baseFile.getVariableByName('lockup')?.configValue().value()).toEqual(300);
		});

		it('font config file', () => {
			const baseFile = service.parseFileText(
				colorConfigFile.fileName,
				colorConfigFile.filePath,
				colorConfigFile.fileText
			);

			if (!baseFile || !ColorConfig.isColorConfig(baseFile)) {
				throw new Error('Base file is not a ColorConfig');
			}

			expect(baseFile.getColorProfileById(0)?.getVariableByName('color')?.configValue().value()).toEqual({
				red: 1023,
				green: 0,
				blue: 0,
				white: 0
			});
			expect(baseFile.getColorProfileById(1)?.getVariableByName('xcolor')?.configValue().value()).toEqual({
				red: 0,
				green: 1023,
				blue: 0,
				white: 0
			});
			expect(baseFile.getColorProfileById(1)?.getVariableByName('tridentm')?.configValue().value()).toEqual([
				false,
				false,
				false,
				false
			]);
		});

		it('pref config file', () => {
			const baseFile = service.parseFileText(
				prefConfigFile.fileName,
				prefConfigFile.filePath,
				prefConfigFile.fileText
			);

			if (!baseFile || !PrefsConfig.isPrefsConfig(baseFile)) {
				throw new Error('Base file is not a PrefsConfig');
			}

			expect(baseFile.groups().length).toBe(6);
			expect(baseFile.getVariableByName('bank')?.configValue().value()).toEqual('00');
			expect(baseFile.getPrefByFontId(1)?.getVariableByName('color')?.configValue().value()).toEqual(20);
			expect(baseFile.getPrefByFontId(6)?.getVariableByName('profile')?.configValue().value()).toEqual(19);
		});

		it('special config file', () => {
			const baseFile = service.parseFileText(
				specialConfigFile.fileName,
				specialConfigFile.filePath,
				specialConfigFile.fileText
			);

			if (!baseFile || !SpecialConfig.isSpecialConfig(baseFile)) {
				throw new Error('Base file is not a SpecialConfig');
			}

			expect(baseFile.text()).toEqual(specialConfigFile.fileText);
		});

		it('led config file', () => {
			const baseFile = service.parseFileText(
				ledConfigFiles[0].fileName,
				ledConfigFiles[0].filePath,
				ledConfigFiles[0].fileText
			);

			if (!baseFile || !LedConfig.isLedConfig(baseFile)) {
				throw new Error('Base file is not a LedConfig');
			}

			expect(baseFile.text()).toEqual(ledConfigFiles[0].fileText);
		});
	});
});
