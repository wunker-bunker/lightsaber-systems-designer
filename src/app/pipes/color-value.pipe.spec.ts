import { testData } from 'src/environments/test-defaults.karma';
import { ColorValue } from '../classes/config-value/color-value';
import { ColorValuePipe } from './color-value.pipe';

describe('ColorValuePipe', () => {
	const pipe = new ColorValuePipe();

	beforeEach(() => {
		testData.generateTestData();
	});

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('can transform', () => {
		const colorGroup = testData.configGroups.find(group => group.name === 'color');
		const colorValue = colorGroup?.getVariableByName('color')?.configValue();

		if (!colorGroup || !colorValue || !ColorValue.isColorValue(colorValue)) {
			throw new Error('colorValue was undefined');
		}

		expect(pipe.transform(colorGroup)).toEqual(colorValue);
	});
});
