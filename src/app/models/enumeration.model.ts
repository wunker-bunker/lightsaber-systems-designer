/**
 * Enumeration interface
 */
export interface Enumeration {
	/**
	 * Enumeration index that can be a number or a string
	 */
	[index: string]: number | string;
}

/**
 * Enumeration type mainly used for type checking and looping through the enumeration
 */
export type EnumerationType = keyof Enumeration;
