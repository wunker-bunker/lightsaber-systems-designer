import { signal } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	openDialogSpy,
	testData,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { AddSoundComponent } from '../components/modals/add-sound/add-sound.component';
import { DeleteConfirmComponent } from '../components/modals/delete-confirm/delete-confirm.component';
import { EditFontNameComponent } from '../components/modals/edit-font-name/edit-font-name.component';
import { EditProfileNameComponent } from '../components/modals/edit-profile-name/edit-profile-name.component';
import { ImportSystemNameComponent } from '../components/modals/import-system-name/import-system-name.component';
import { InvalidVariablesComponent } from '../components/modals/invalid-variables/invalid-variables.component';
import { PreferencesComponent } from '../components/modals/preferences/preferences.component';
import { ModalService } from './modal.service';

describe('ModalService', () => {
	let service: ModalService;
	const modalServ: jasmine.SpyObj<NgbModal> = jasmine.createSpyObj('NgbModal', ['open']);
	const modalOpts: NgbModalOptions = { centered: true, animation: true };

	configureTestSuite({
		imports: unitTestImports,
		providers: [...unitTestProviders, { provide: NgbModal, useValue: modalServ }]
	});

	beforeEach(() => {
		service = TestBed.inject(ModalService);
	});

	afterEach(() => {
		modalServ.open.calls.reset();
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should choose sound name', async () => {
		const componentInstance = { availableNames: undefined } as unknown as AddSoundComponent;
		const availableNames = ['test1', 'test2'];
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve({ name: 'test1' })
		} as unknown as NgbModalRef);
		const soundName = await service.chooseSoundName(availableNames);
		expect(soundName).toEqual('test1');
		expect(modalServ.open).toHaveBeenCalledWith(AddSoundComponent, modalOpts);
		expect(componentInstance.availableNames).toEqual(availableNames);
	});

	it('should confirm delete', async () => {
		const componentInstance = { name: undefined } as unknown as DeleteConfirmComponent;
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve(true)
		} as unknown as NgbModalRef);
		await service.deleteConfirm('test');
		expect(modalServ.open).toHaveBeenCalledWith(DeleteConfirmComponent, modalOpts);
		expect(componentInstance.name).toEqual('test');
	});

	it('should edit font name', async () => {
		const componentInstance = { setName: jasmine.createSpy() } as unknown as EditFontNameComponent;
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve({ name: 'test1' })
		} as unknown as NgbModalRef);
		const fontName = await service.editFontName('test');
		expect(fontName).toEqual('test1');
		expect(modalServ.open).toHaveBeenCalledWith(EditFontNameComponent, modalOpts);
		expect(componentInstance.setName).toHaveBeenCalledWith('test');
	});

	it('should edit profile name', async () => {
		const componentInstance = { setName: jasmine.createSpy() } as unknown as EditProfileNameComponent;
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve({ name: 'test1' })
		} as unknown as NgbModalRef);
		const profileName = await service.editProfileName('test');
		expect(profileName).toEqual('test1');
		expect(modalServ.open).toHaveBeenCalledWith(EditProfileNameComponent, modalOpts);
		expect(componentInstance.setName).toHaveBeenCalledWith('test');
	});

	it('should show invalid variables modal', async () => {
		const componentInstance = { variables: undefined } as unknown as InvalidVariablesComponent;
		const variables = testData.configVars;
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve(true)
		} as unknown as NgbModalRef);
		await service.invalidVariables(variables);
		expect(modalServ.open).toHaveBeenCalledWith(InvalidVariablesComponent, {
			...modalOpts,
			size: 'lg',
			scrollable: true
		});
		expect(componentInstance.variables).toEqual(variables);
	});

	describe('should open directory', async () => {
		it('should open directory', async () => {
			openDialogSpy.and.resolveTo({
				canceled: false,
				filePaths: ['test']
			});
			const directory = await service.openDirectory();
			expect(openDialogSpy).toHaveBeenCalledWith({
				title: 'Open a directory',
				properties: ['openDirectory']
			});
			expect(directory).toEqual('test');
		});

		it('should not open directory when canceled', async () => {
			openDialogSpy.and.resolveTo({
				canceled: true,
				filePaths: []
			});

			try {
				await service.openDirectory();
				fail('should not resolve');
			} catch (e) {
				expect(openDialogSpy).toHaveBeenCalledWith({
					title: 'Open a directory',
					properties: ['openDirectory']
				});
			}
		});
	});

	describe('should open sound file', async () => {
		it('should open sound file that is a track', async () => {
			openDialogSpy.and.resolveTo({
				canceled: false,
				filePaths: ['test']
			});
			const soundFile = await service.openSoundFile(true);
			expect(openDialogSpy).toHaveBeenCalledWith({
				title: 'Choose a track to open',
				properties: ['openFile'],
				buttonLabel: 'Add',
				filters: [{ name: 'Wav Files', extensions: ['wav'] }]
			});
			expect(soundFile).toEqual('test');
		});

		it('should open sound file that is not a track', async () => {
			openDialogSpy.and.resolveTo({
				canceled: false,
				filePaths: ['test']
			});
			const soundFile = await service.openSoundFile(false);
			expect(openDialogSpy).toHaveBeenCalledWith({
				title: 'Choose a sound to open',
				properties: ['openFile'],
				buttonLabel: 'Add',
				filters: [{ name: 'Wav Files', extensions: ['wav'] }]
			});
			expect(soundFile).toEqual('test');
		});

		it('should not open sound file when canceled', async () => {
			openDialogSpy.and.resolveTo({
				canceled: true,
				filePaths: []
			});

			try {
				await service.openSoundFile(false);
				fail('should not resolve');
			} catch (e) {
				expect(openDialogSpy).toHaveBeenCalledWith({
					title: 'Choose a sound to open',
					properties: ['openFile'],
					buttonLabel: 'Add',
					filters: [{ name: 'Wav Files', extensions: ['wav'] }]
				});
			}
		});
	});

	it('should open preferences', async () => {
		const componentInstance = { openDirectory: undefined } as unknown as PreferencesComponent;
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve(true)
		} as unknown as NgbModalRef);
		await service.preferences();
		expect(modalServ.open).toHaveBeenCalledWith(PreferencesComponent, { ...modalOpts, size: 'lg' });
		expect(componentInstance.openDirectory).toBeDefined();
	});

	it('should open system name modal', async () => {
		const componentInstance = {
			systemList: undefined,
			setName: jasmine.createSpy()
		} as unknown as ImportSystemNameComponent;
		const systemNames = signal(['test', 'test1']);
		modalServ.open.and.returnValue({
			componentInstance,
			result: Promise.resolve({ name: 'test2' })
		} as unknown as NgbModalRef);
		const newName = await service.systemName(systemNames, 'test');
		expect(modalServ.open).toHaveBeenCalledWith(ImportSystemNameComponent, modalOpts);
		expect(componentInstance.systemList).toEqual(systemNames);
		expect(componentInstance.setName).toHaveBeenCalledWith('test');
		expect(newName).toEqual('test2');
	});
});
