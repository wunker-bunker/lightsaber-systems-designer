import { Pipe, PipeTransform } from '@angular/core';
import { RGBA } from 'ngx-color';
import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { Color } from '../models/color.model';

/**
 * Pipe to convert a color to a RGBA object.
 * Used in the color picker to contain the current color.
 * The alpha value is set to 1.
 * @example <color-chrome [color]="input.value | colorValToRgb"></color-chrome>
 */
@Pipe({
	name: 'colorValToRgb',
	pure: true
})
export class ColorValToRgbPipe implements PipeTransform {
	/**
	 * Transform a color to a RGBA object.
	 * The alpha value is set to 1.
	 * The red, green and blue values are converted to rgb values.
	 * @param value The color to transform.
	 * @returns The RGBA object.
	 */
	transform(value: Color): RGBA {
		return {
			r: this.colorValToRgb(value.red),
			g: this.colorValToRgb(value.green),
			b: this.colorValToRgb(value.blue),
			a: 1
		};
	}

	/**
	 * Convert a color value to a rgb value.
	 * @param val The color value to convert.
	 * @returns The rgb value.
	 */
	private colorValToRgb(val: number): number {
		return Math.round((val * 255) / COLOR_VAL_MAX);
	}
}
