import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { ledConfigFiles } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { LedConfig } from './led-config';

describe('LedConfig', () => {
	let config: LedConfig;
	const testFile = ledConfigFiles[0];

	beforeEach(() => {
		testData.generateTestData();
		config = new LedConfig(testFile.fileText, testFile.filePath);
	});

	it('should create an instance', () => {
		expect(config).toBeTruthy();
		expect(config.filePath).toEqual(testFile.filePath);
		expect(config.id()).toEqual(1);
		expect(config.name()).toEqual('NIGHTFALL');
		expect(config.type).toEqual(ConfigFileType.LEDS);
		expect(config.text()).toEqual(testFile.fileText);
		expect(config.dirty()).toBeFalse();
		expect(config.valid()).toBeTrue();
		expect(config.toString()).toEqual(testFile.fileText);
	});

	it('should check if it has new id', () => {
		expect(config.hasNewId()).toBeFalse();
		config.id.set(20);
		expect(config.hasNewId()).toBeTrue();
	});

	it('should revert to old ID', () => {
		const ogId = config.id();
		const newId = 20;
		config.id.set(newId);
		expect(config.hasNewId()).toBeTrue();
		expect(config.id()).toEqual(newId);
		config.revert();
		expect(config.hasNewId()).toBeFalse();
		expect(config.id()).toEqual(ogId);
	});

	it('should mark as saved', () => {
		const newId = 20;
		config.id.set(newId);
		expect(config.hasNewId()).toBeTrue();
		expect(config.id()).toEqual(newId);
		config.markAsSaved();
		expect(config.hasNewId()).toBeFalse();
		expect(config.id()).toEqual(newId);
	});

	it('should be dirty with new ID', () => {
		expect(config.dirty()).toBeFalse();
		const newId = 20;
		config.id.set(newId);
		expect(config.hasNewId()).toBeTrue();
		expect(config.id()).toEqual(newId);
		expect(config.dirty()).toBeTrue();
		config.markAsSaved();
		expect(config.dirty()).toBeFalse();
	});
});
