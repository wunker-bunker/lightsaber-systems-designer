import { inject, NgZone } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, delay, finalize, switchMap, tap } from 'rxjs/operators';
import { LightsaberSystem } from '../classes/lightsaber-system';
import { AppAlertsService } from '../services/app-alerts.service';
import { AppLoaderService } from '../services/app-loader.service';
import { FileManagementService } from '../services/file-management.service';

/**
 * Get a system.
 * If the system is not found, navigate to the system list.
 * If the system is found, return the system.
 * If there is an error, show an error message.
 * @param route The route snapshot. The system name is in the `systemName` parameter.
 * @param _fileService The file management service.
 * @param _router The router.
 * @param _loaderServ The loader service.
 * @param _alertServ The alert service.
 * @param route.params
 * @param route.params.systemName
 * @param state
 * @param fileService
 * @param router
 * @param alertServ
 * @param loaderServ
 * @param ngZone
 * @returns A promise that resolves when the system has been retrieved.
 */
export const systemResolver: ResolveFn<LightsaberSystem> = (
	{ params: { systemName } }: ActivatedRouteSnapshot,
	state: RouterStateSnapshot,
	fileService: FileManagementService = inject(FileManagementService),
	router: Router = inject(Router),
	alertServ: AppAlertsService = inject(AppAlertsService),
	loaderServ = inject(AppLoaderService),
	ngZone = inject(NgZone)
): Observable<LightsaberSystem> =>
	of(systemName).pipe(
		delay(0), // Simulate delay like setTimeout
		tap(() => console.log(`Loading system ${systemName}`)),
		switchMap(name => ngZone.runOutsideAngular(() => fileService.loadSystem(name))),
		catchError(err => {
			alertServ.showErrorAlert(`Failed to load system ${systemName}: ${err}`, err);
			router.navigate(['/systems']);
			return EMPTY; // Emit no items but also do not error
		}),
		finalize(() => loaderServ.setState(false))
	);
