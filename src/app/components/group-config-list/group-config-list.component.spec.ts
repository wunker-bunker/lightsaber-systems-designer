import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfigGroup } from 'src/app/classes/config-group';
import * as ConstFunctions from 'src/app/constants/functions';
import { ColorValuePipe } from 'src/app/pipes/color-value.pipe';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import { ModalService } from 'src/app/services/modal.service';
import { MockConfigVarRowComponent } from 'src/environments/test-data.karma';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { StringInputComponent } from '../config-var-row/string-input/string-input.component';
import { GroupConfigListComponent } from './group-config-list.component';

describe('GroupConfigListComponent', () => {
	const modalServ: jasmine.SpyObj<ModalService> = jasmine.createSpyObj('ModalService', [
		'editProfileName',
		'invalidVariables',
		'deleteConfirm'
	]);
	let component: GroupConfigListComponent;
	let fixture: ComponentFixture<GroupConfigListComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [
			GroupConfigListComponent,
			OrderByPipe,
			MockConfigVarRowComponent,
			ColorValuePipe,
			StringInputComponent,
			...unitTestDeclarations
		],
		providers: [...unitTestProviders, { provide: ModalService, useValue: modalServ }]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(GroupConfigListComponent);
		component = fixture.componentInstance;
		component.groups = testData.mainConfig.groups;
		fixture.detectChanges();
		spyOn(component.groupOrderChange, 'emit');
		spyOn(component.deleteGroup, 'emit');
		spyOn(ConstFunctions, 'showDragCursor').and.callThrough();
	});

	afterEach(() => {
		modalServ.invalidVariables.calls.reset();
		modalServ.deleteConfirm.calls.reset();
		testData.generateTestData();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	describe('should open invalid variables modal', () => {
		let group: ConfigGroup;

		beforeEach(() => {
			group = component.groups()[0];
			modalServ.invalidVariables.and.rejectWith(null);
		});

		afterEach(() => {
			modalServ.invalidVariables.calls.reset();
		});

		it('with one group', async () => {
			await component.openInvalidVariablesModal(group);
			expect(modalServ.invalidVariables).toHaveBeenCalledWith(
				group.variables().filter(confVar => !confVar.configValue().valid())
			);
		});
	});

	it('should start dragging', () => {
		component.expandedIndex = 4;
		expect(component.expandedIndex).toBe(4);
		component.dragStart();
		expect(component.expandedIndex).toBe(-1);
		expect(ConstFunctions.showDragCursor).toHaveBeenCalledWith(true);
	});

	it('should drop', () => {
		const groupOrder = [...component.groupIdOrder()];
		const dragDrop = {
			previousIndex: 2,
			currentIndex: 4
		} as unknown as CdkDragDrop<number[]>;

		component.drop(dragDrop);
		expect(component.groupIdOrder).not.toEqual(groupOrder);
		moveItemInArray(groupOrder, dragDrop.previousIndex, dragDrop.currentIndex);
		expect(component.groupIdOrder()).toEqual(groupOrder);
		expect(component.groupOrderChange.emit).toHaveBeenCalledWith(component.groupIdOrder());
		expect(ConstFunctions.showDragCursor).toHaveBeenCalledWith(false);
	});

	// describe should test confirmDelete
	describe('should test confirmDelete', () => {
		let group: ConfigGroup;

		beforeEach(() => {
			group = component.groups()[0];
		});

		it('when deleteConfirm resolves', async () => {
			modalServ.deleteConfirm.and.resolveTo();
			await component.confirmDelete(group);

			// expect deleteGroup to have been called with the group
			expect(component.deleteGroup.emit).toHaveBeenCalledWith(group);

			// expect modalServ deleteConfirm to have been called with the group name
			expect(modalServ.deleteConfirm).toHaveBeenCalledWith(group.groupName());
		});

		it('when deleteConfirm rejects', async () => {
			modalServ.deleteConfirm.and.rejectWith(null);
			await component.confirmDelete(group);

			// expect deleteGroup to not have been called
			expect(component.deleteGroup.emit).not.toHaveBeenCalled();

			// expect modalServ deleteConfirm to not have been called with the group name
			expect(modalServ.deleteConfirm).toHaveBeenCalledWith(group.groupName());
		});
	});

	describe('should test openEditNameModal', () => {
		let group: ConfigGroup;

		beforeEach(() => {
			group = component.groups()[0];
		});

		it('when modalService resolves', async () => {
			const currentGroupName = group.groupName();
			modalServ.editProfileName.and.resolveTo('newName');
			await component.openEditNameModal(group);
			expect(group.groupName()).toBe('newName');
			expect(modalServ.editProfileName).toHaveBeenCalledWith(currentGroupName);
		});

		it('when modalService rejects', async () => {
			modalServ.editProfileName.and.rejectWith(null);
			await component.openEditNameModal(group);
			expect(group.groupName()).not.toBe('newName');
			expect(modalServ.editProfileName).toHaveBeenCalledWith(group.groupName());
		});
	});
});
