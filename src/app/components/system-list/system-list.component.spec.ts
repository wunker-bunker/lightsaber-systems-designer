import { signal } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, ParamMap, Router } from '@angular/router';
import { join } from 'path';
import { BehaviorSubject } from 'rxjs';
import { LightsaberSystem } from 'src/app/classes/lightsaber-system';
import { AppAlertsService } from 'src/app/services/app-alerts.service';
import { AppLoaderService } from 'src/app/services/app-loader.service';
import { FileManagementService } from 'src/app/services/file-management.service';
import { ModalService } from 'src/app/services/modal.service';
import { testSystemList } from 'src/environments/test-data.karma';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { SystemListComponent } from './system-list.component';

describe('SystemListComponent', () => {
	const fileServ: jasmine.SpyObj<FileManagementService> = jasmine.createSpyObj(
		'FileManagementService',
		['getSystemList', 'importSystemFromDir'],
		{
			systemList: signal(testSystemList)
		}
	);
	const loaderServ: jasmine.SpyObj<AppLoaderService> = jasmine.createSpyObj('AppLoaderService', ['setState']);
	const route: jasmine.SpyObj<ActivatedRoute> = jasmine.createSpyObj('ActivatedRoute', [], {
		paramMap: new BehaviorSubject<ParamMap>(
			convertToParamMap({
				systemName: testSystemList[0]
			})
		)
	});
	const alertServ: jasmine.SpyObj<AppAlertsService> = jasmine.createSpyObj('AppAlertsService', [
		'showSuccessAlert',
		'showErrorAlert'
	]);
	const modalServ: jasmine.SpyObj<ModalService> = jasmine.createSpyObj('ModalService', [
		'openDirectory',
		'systemName'
	]);
	let component: SystemListComponent;
	let fixture: ComponentFixture<SystemListComponent>;
	let router: Router;
	let navigateSpy: jasmine.Spy;
	let logSpy: jasmine.Spy;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [SystemListComponent, ...unitTestDeclarations],
		providers: [
			{ provide: FileManagementService, useValue: fileServ },
			{ provide: AppLoaderService, useValue: loaderServ },
			{ provide: ActivatedRoute, useValue: route },
			{ provide: AppAlertsService, useValue: alertServ },
			{ provide: ModalService, useValue: modalServ },
			...unitTestProviders
		]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SystemListComponent);
		component = fixture.componentInstance;
		router = TestBed.inject(Router);
		fixture.detectChanges();
		navigateSpy = spyOn(router, 'navigate');
		logSpy = spyOn(console, 'log');
	});

	afterEach(() => {
		loaderServ.setState.calls.reset();
		fileServ.getSystemList.calls.reset();
		fileServ.importSystemFromDir.calls.reset();
		alertServ.showSuccessAlert.calls.reset();
		alertServ.showErrorAlert.calls.reset();
		modalServ.openDirectory.calls.reset();
		modalServ.systemName.calls.reset();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
		expect(component.selectedSystem()).toEqual(testSystemList[0]);
		expect(component.systemList()).toEqual(testSystemList);
	});

	describe('should add system', () => {
		const newSystemDir = 'test4';
		const newSystemName = 'Test System';
		const newSystemPath = join('test-resources', newSystemDir);

		beforeEach(() => {
			modalServ.openDirectory.and.resolveTo(newSystemPath);
			modalServ.systemName.and.resolveTo(newSystemName);
			fileServ.importSystemFromDir.and.resolveTo(
				new LightsaberSystem(newSystemName, newSystemPath, testData.configs, testData.soundFiles)
			);
		});

		it('when directory found and system is imported', fakeAsync(async () => {
			await component.addSystem();
			expect(modalServ.openDirectory).toHaveBeenCalled();
			expect(modalServ.systemName).toHaveBeenCalledWith(component.systemList, newSystemDir);
			expect(loaderServ.setState).toHaveBeenCalledWith(true, 'Copying system to library...');
			tick();
			expect(fileServ.importSystemFromDir).toHaveBeenCalledWith(newSystemPath, newSystemName);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(navigateSpy).toHaveBeenCalledWith(['systems', newSystemName]);
		}));

		it('when directory found and system is not imported', fakeAsync(async () => {
			fileServ.importSystemFromDir.and.rejectWith('fail');
			await component.addSystem();
			expect(loaderServ.setState).toHaveBeenCalledWith(true, 'Copying system to library...');
			tick();
			expect(fileServ.importSystemFromDir).toHaveBeenCalledWith(newSystemPath, newSystemName);
			expect(loaderServ.setState).toHaveBeenCalledWith(false);
			expect(navigateSpy).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		}));

		it('when directory canceled', async () => {
			modalServ.openDirectory.and.rejectWith('canceled');
			await component.addSystem();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.importSystemFromDir).not.toHaveBeenCalled();
			expect(navigateSpy).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
		});

		it('when system name add canceled', async () => {
			modalServ.systemName.and.rejectWith('canceled');
			await component.addSystem();
			expect(loaderServ.setState).not.toHaveBeenCalled();
			expect(fileServ.importSystemFromDir).not.toHaveBeenCalled();
			expect(navigateSpy).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).not.toHaveBeenCalled();
			expect(logSpy).toHaveBeenCalledWith(`Stopped importing from ${newSystemPath}`);
		});
	});
});
