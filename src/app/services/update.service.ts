import { Injectable, NgZone, signal } from '@angular/core';
import { ProgressInfo } from 'electron-updater';

/**
 * The state of the update.
 */
export enum UpdateState {
	NONE,
	AVAILABLE,
	DOWNLOADING,
	DOWNLOADED
}

/**
 * An object containing information about the update.
 */
export interface UpdateObj {
	/**
	 * The percentage of the download that has been completed.
	 * Only present if the state is `DOWNLOADING`.
	 */
	percentDone?: number;

	/**
	 * The state of the update.
	 * Defaults to `NONE`.
	 * @see UpdateState
	 */
	state: UpdateState;
}

/**
 * A service for managing updates.
 */
@Injectable({
	providedIn: 'root'
})
export class UpdateService {
	/**
	 * An observable for the current update state.
	 */
	updateState = signal<UpdateObj>({
		state: UpdateState.NONE
	});

	constructor(private _ngZone: NgZone) {
		window.electronApi.onUpdateAvailable(() =>
			this._ngZone.run(() => this.updateState.set({ state: UpdateState.AVAILABLE }))
		);

		window.electronApi.onUpdateDownloaded(() =>
			this._ngZone.run(() =>
				this.updateState.set({
					state: UpdateState.DOWNLOADED
				})
			)
		);

		window.electronApi.onDownloadProgress((progress: ProgressInfo) =>
			this._ngZone.run(() =>
				this.updateState.set({
					state: UpdateState.DOWNLOADING,
					percentDone: progress.percent
				})
			)
		);
		window.electronApi.readyToCheckForUpdates();
	}
}
