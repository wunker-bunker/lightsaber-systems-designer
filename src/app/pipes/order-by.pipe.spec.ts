import { OrderByPipe } from './order-by.pipe';

describe('OrderByPipe', () => {
	let pipe: OrderByPipe;

	beforeEach(() => {
		pipe = new OrderByPipe();
	});

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('will order a list by the given property and direction', () => {
		const unorderedList = [
			{
				test: 1
			},
			{
				test: 5
			},
			{
				test: 3
			}
		];
		const orderedListAsc = [
			{
				test: 1
			},
			{
				test: 3
			},
			{
				test: 5
			}
		];
		const orderedListDesc = [
			{
				test: 5
			},
			{
				test: 3
			},
			{
				test: 1
			}
		];

		expect(pipe.transform(unorderedList, 'test', 'asc')).toEqual(orderedListAsc);
		expect(pipe.transform(unorderedList, 'test', 'desc')).toEqual(orderedListDesc);
	});
});
