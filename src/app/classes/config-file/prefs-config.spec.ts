import { FILE_HEADER } from 'src/app/constants/strings';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { prefConfigFile } from 'src/environments/test-data.karma';
import { testData } from 'src/environments/test-defaults.karma';
import { PrefsConfig } from './prefs-config';

describe('PrefsConfig', () => {
	let config: PrefsConfig;

	beforeEach(() => {
		testData.generateTestData();
		config = new PrefsConfig(
			testData.prefConfig.variables(),
			testData.prefConfig.groups(),
			prefConfigFile.filePath
		);
	});

	it('should create an instance', () => {
		expect(config).toBeTruthy();
		expect(config.type).toEqual(ConfigFileType.PREF);
		expect(config.variables()).toEqual(testData.prefConfig.variables());
		expect(config.groups()).toEqual(testData.prefConfig.groups());
		expect(config.filePath).toEqual(prefConfigFile.filePath);
		expect(config.dirty()).toBeFalse();
		expect(config.valid()).toBeTrue();
		expect(config.toString()).toEqual(FILE_HEADER + prefConfigFile.fileText + '\n');
	});

	it('should get font by id', () => {
		const font0 = config.getPrefByFontId(0);
		expect(font0).toBeUndefined();

		const font1 = config.getPrefByFontId(1); // Group ID is indexed one less than the font's actual id

		if (!font1) {
			throw new Error('font1 is undefined');
		}

		expect(font1.id()).toEqual(0);
		expect(font1.name).toEqual('font');
		expect(font1.getVariableByName('color')?.configValue().value()).toEqual(20);
		expect(font1.getVariableByName('profile')?.configValue().value()).toEqual(8);

		const font4 = config.getPrefByFontId(4);

		if (!font4) {
			throw new Error('font4 is undefined');
		}

		expect(font4.id()).toEqual(3);
		expect(font4.name).toEqual('font');
		expect(font4.getVariableByName('color')?.configValue().value()).toEqual(23);
		expect(font4.getVariableByName('profile')?.configValue().value()).toEqual(11);
	});
});
