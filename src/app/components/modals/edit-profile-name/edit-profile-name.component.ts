import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-edit-profile-name',
	templateUrl: './edit-profile-name.component.html',
	styleUrls: ['./edit-profile-name.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditProfileNameComponent {
	importForm: UntypedFormGroup = this._fb.group({
		name: ['', [Validators.required]]
	});
	constructor(
		public activeModal: NgbActiveModal,
		public settings: SettingsService,
		private _fb: UntypedFormBuilder
	) {}

	get name(): AbstractControl | null {
		return this.importForm.get('name');
	}

	printForm(): void {
		if (this.name?.errors) {
			console.warn(this.name.errors);
		}
	}

	setName(name: string): void {
		this.importForm.setValue({ name });
	}
}
