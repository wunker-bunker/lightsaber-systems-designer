import {
	ChangeDetectionStrategy,
	Component,
	computed,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Signal,
	signal,
	TemplateRef
} from '@angular/core';
import { TextFile } from 'src/app/classes/config-file/text-file';
import { ConfigGroup } from 'src/app/classes/config-group';
import { ConfigVar } from 'src/app/classes/config-var';
import { SoundFile } from 'src/app/classes/sound-file';
import { GroupType } from 'src/app/enums/group-types';
import { ModalService } from 'src/app/services/modal.service';

export enum ConfigBodyType {
	VAR_LIST,
	TEXT_FILE,
	SOUND_LIST,
	GROUP_LIST
}

export interface ConfigBody {
	readonly tabTitle: string;
	readonly type: ConfigBodyType;
	readonly dirty: Signal<boolean>;
	readonly valid: Signal<boolean>;
}

export interface ConfigListBody extends ConfigBody {
	readonly configVars: Signal<ConfigVar[]>;
	readonly type: typeof ConfigBodyType.VAR_LIST;
}

export interface ConfigTextFileBody extends ConfigBody {
	readonly textFile: Signal<TextFile | undefined>;
	readonly type: typeof ConfigBodyType.TEXT_FILE;
}

export interface ConfigSoundFilesBody extends ConfigBody {
	readonly soundFiles: Signal<SoundFile[]>;
	readonly type: typeof ConfigBodyType.SOUND_LIST;
	readonly areTracks: boolean;
}

export interface ConfigGroupListBody extends ConfigBody {
	readonly groupType: GroupType;
	readonly groups: Signal<ConfigGroup[]>;
	readonly type: typeof ConfigBodyType.GROUP_LIST;
}

export type ConfigBodyOpts = ConfigListBody | ConfigTextFileBody | ConfigSoundFilesBody | ConfigGroupListBody;

@Component({
	selector: 'lsd-config-list',
	templateUrl: './config-list.component.html',
	styleUrls: ['./config-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigListComponent implements OnInit {
	@Output() addGroup: EventEmitter<ConfigGroupListBody> = new EventEmitter();
	@Output() addSound: EventEmitter<boolean> = new EventEmitter();
	@Input() bodyOpts: ConfigBodyOpts[] = [];
	@Input() cardBody?: TemplateRef<unknown>;
	@Output() changeSoundName: EventEmitter<SoundFile> = new EventEmitter();
	@Output() deleteGroup: EventEmitter<[ConfigGroup, ConfigGroupListBody]> = new EventEmitter();
	@Output() deleteSound: EventEmitter<SoundFile> = new EventEmitter();
	@Output() groupOrderChange: EventEmitter<[readonly number[], ConfigGroupListBody]> = new EventEmitter();
	@Input() groups: readonly ConfigGroup[] = [];
	@Input() name = '';

	readonly BT = ConfigBodyType;

	activeTab = computed(() => this.bodyOpts.find(opt => opt.tabTitle === this.activeTabTitle()));
	activeTabTitle = signal('');

	dirty = computed(() => this.bodyOpts.some(opt => opt.dirty()));

	valid = computed(() => this.bodyOpts.every(opt => opt.valid()));

	constructor(private _modalServ: ModalService) {}

	ngOnInit(): void {
		this.activeTabTitle.set(this.bodyOpts[0].tabTitle);
	}

	async openInvalidVariablesModal(): Promise<void> {
		let variables: ConfigVar[] = [];

		const configBodies: ConfigBodyOpts[] = this.bodyOpts.filter(
			body => body.type === ConfigBodyType.VAR_LIST || body.type === ConfigBodyType.GROUP_LIST
		);

		configBodies.forEach(body => {
			if (body.type === ConfigBodyType.VAR_LIST) {
				variables = variables.concat(body.configVars().filter(confVar => !confVar.configValue().valid()));
			} else if (body.type === ConfigBodyType.GROUP_LIST) {
				variables = variables.concat(
					body
						.groups()
						.reduce(
							(vars: ConfigVar[], confGroup: ConfigGroup) => [
								...vars,
								...confGroup.variables().filter(confVar => !confVar.configValue().valid())
							],
							[]
						)
				);
			}
		});

		try {
			await this._modalServ.invalidVariables(variables);
		} catch {
			console.log('Closed Invalid Variable Modal');
		}
	}
}
