import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { AddSoundComponent } from './add-sound.component';

describe('AddSoundComponent', () => {
	let component: AddSoundComponent;
	let fixture: ComponentFixture<AddSoundComponent>;
	const modalMock: jasmine.SpyObj<NgbActiveModal> = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

	configureTestSuite({
		imports: [ReactiveFormsModule, ...unitTestImports],
		declarations: [AddSoundComponent, ...unitTestDeclarations],
		providers: [{ provide: NgbActiveModal, useValue: modalMock }, ...unitTestProviders]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddSoundComponent);
		component = fixture.componentInstance;
		component.availableNames = testData.system.getAvailableSoundNames(1);
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should print errors to console', () => {
		const name = component.name;

		if (!name) {
			throw new Error('name is null');
		}

		const warnSpy = spyOn(console, 'warn').and.callThrough();
		name.setValue(null);
		name.updateValueAndValidity();
		component.printForm();
		expect(console.warn).toHaveBeenCalledWith(name.errors);

		warnSpy.calls.reset();
		name.setValue(component.availableNames[0]);
		name.updateValueAndValidity();
		component.printForm();
		expect(console.warn).not.toHaveBeenCalled();
	});
});
