/**
 * Sets the cursor to grabbing or unset based on the show parameter.
 * Used while dragging a CdkDrag element.
 * @param show True to show the grabbing cursor, false to show the unset cursor.
 */
export const showDragCursor = (show: boolean): void => {
	if (show) {
		document.body.classList.add('inherit-cursors');
		document.body.style.cursor = 'grabbing';
	} else {
		document.body.classList.remove('inherit-cursors');
		document.body.style.cursor = 'unset';
	}
};
