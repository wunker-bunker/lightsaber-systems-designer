import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { testSystemList } from 'src/environments/test-data.karma';
import { configureTestSuite, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { AppAlertsService } from '../services/app-alerts.service';
import { FileManagementService } from '../services/file-management.service';
import { systemListResolver } from './system-list.resolver';

describe('SystemListResolver', () => {
	const fileServ: jasmine.SpyObj<FileManagementService> = jasmine.createSpyObj('FileManagementService', [
		'getSystemList'
	]);
	const alertServ: jasmine.SpyObj<AppAlertsService> = jasmine.createSpyObj('AppAlertsService', [
		'showSuccessAlert',
		'showErrorAlert'
	]);
	const router: jasmine.SpyObj<Router> = jasmine.createSpyObj('Router', ['navigate']);
	const route: jasmine.SpyObj<ActivatedRouteSnapshot> = jasmine.createSpyObj('ActivatedRouteSnapshot', [], {
		params: {}
	});
	const state: jasmine.SpyObj<RouterStateSnapshot> = jasmine.createSpyObj('RouterStateSnapshot', [], {
		url: ''
	});

	configureTestSuite({
		imports: unitTestImports,
		providers: [
			{ provide: FileManagementService, useValue: fileServ },
			{ provide: Router, useValue: router },
			{ provide: AppAlertsService, useValue: alertServ },
			{ provide: ActivatedRouteSnapshot, useValue: route },
			{ provide: RouterStateSnapshot, useValue: state },
			...unitTestProviders
		]
	});

	describe('should resolve', () => {
		afterEach(() => {
			fileServ.getSystemList.calls.reset();
			router.navigate.calls.reset();
			alertServ.showSuccessAlert.calls.reset();
			alertServ.showErrorAlert.calls.reset();
		});

		it('has systems', async () => {
			fileServ.getSystemList.and.returnValue([...testSystemList]);

			const systemList = TestBed.runInInjectionContext(
				() => systemListResolver(route, state) as unknown as string[]
			);
			expect(systemList).toEqual(testSystemList);
			expect(fileServ.getSystemList).toHaveBeenCalledTimes(1);
			expect(router.navigate).toHaveBeenCalledWith(['systems', testSystemList[0]]);
		});

		it('does not have systems', async () => {
			fileServ.getSystemList.and.returnValue([]);
			const systemList = TestBed.runInInjectionContext(
				() => systemListResolver(route, state) as unknown as string[]
			);
			expect(systemList).toEqual([]);
			expect(fileServ.getSystemList).toHaveBeenCalledTimes(1);
			expect(router.navigate).not.toHaveBeenCalled();
		});

		it('fails to get systems', async () => {
			fileServ.getSystemList.and.throwError('fail');
			TestBed.runInInjectionContext(() => systemListResolver(route, state));
			expect(fileServ.getSystemList).toHaveBeenCalledTimes(1);
			expect(router.navigate).not.toHaveBeenCalled();
			expect(alertServ.showErrorAlert).toHaveBeenCalled();
		});
	});
});
