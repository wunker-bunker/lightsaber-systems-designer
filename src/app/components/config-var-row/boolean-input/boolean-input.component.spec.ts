import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite, testData, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { BooleanInputComponent } from './boolean-input.component';

describe('BooleanInputComponent', () => {
	let component: BooleanInputComponent;
	let fixture: ComponentFixture<BooleanInputComponent>;

	configureTestSuite({
		imports: unitTestImports,
		declarations: [BooleanInputComponent],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(BooleanInputComponent);
		component = fixture.componentInstance;
		component.input = testData.booleanValues[0];
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
