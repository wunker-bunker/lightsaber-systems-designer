import { computed } from '@angular/core';
import { ConfigFileType } from 'src/app/enums/config-file-type.enum';
import { BaseFile } from './base-file';
import { TextFile } from './text-file';

/**
 * Class for the special config files.
 */
export class SpecialConfig extends TextFile {
	/**
	 * Flag to indicate if the file has been modified since it was last saved.
	 * @returns True if the file has been modified since it was last saved, false otherwise.
	 */
	readonly dirty;

	/**
	 * Creates a new instance of the SpecialConfig class.
	 * @param text The text of the file.
	 * @param filePath The path to the file.
	 */
	constructor(text: string, filePath: string) {
		super(text, ConfigFileType.SPECIAL, filePath);
		this.dirty = computed(() => this._dirtyText());
	}

	static isSpecialConfig(obj: BaseFile): obj is SpecialConfig {
		return obj instanceof SpecialConfig;
	}
}
