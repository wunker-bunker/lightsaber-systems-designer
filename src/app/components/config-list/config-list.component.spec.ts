import { computed, signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfigGroup } from 'src/app/classes/config-group';
import { IntegerArrayValue } from 'src/app/classes/config-value/integer-array-value';
import { IntegerValue } from 'src/app/classes/config-value/integer-value';
import { StringValue } from 'src/app/classes/config-value/string-value';
import { ConfigVar } from 'src/app/classes/config-var';
import { GroupType } from 'src/app/enums/group-types';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import { ModalService } from 'src/app/services/modal.service';
import {
	configureTestSuite,
	testData,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { GroupConfigListComponent } from '../group-config-list/group-config-list.component';
import { SoundRowComponent } from '../sound-row/sound-row.component';
import { ConfigBodyOpts, ConfigBodyType, ConfigListComponent } from './config-list.component';

describe('ConfigListComponent', () => {
	const modalServ: jasmine.SpyObj<ModalService> = jasmine.createSpyObj('ModalService', ['invalidVariables']);
	let component: ConfigListComponent;
	let fixture: ComponentFixture<ConfigListComponent>;

	configureTestSuite({
		imports: [NgbNavModule, ...unitTestImports],
		declarations: [
			ConfigListComponent,
			OrderByPipe,
			...unitTestDeclarations,
			SoundRowComponent,
			GroupConfigListComponent
		],
		providers: [...unitTestProviders, { provide: ModalService, useValue: modalServ }]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ConfigListComponent);
		component = fixture.componentInstance;
		component.name = 'test list';
		component.bodyOpts = [
			{
				type: ConfigBodyType.TEXT_FILE,
				tabTitle: 'Test text',
				textFile: signal(testData.specialConfig),
				dirty: testData.specialConfig.dirty,
				valid: testData.specialConfig.valid
			},
			{
				type: ConfigBodyType.VAR_LIST,
				configVars: testData.mainConfig.variables,
				tabTitle: 'Test variables',
				dirty: computed(() => testData.mainConfig.variables().some(v => v.configValue().dirty())),
				valid: testData.mainConfig.varValid
			},
			{
				type: ConfigBodyType.GROUP_LIST,
				groupType: GroupType.profile,
				groups: testData.mainConfig.groups,
				tabTitle: 'Test Profiles',
				dirty: computed(() => testData.mainConfig.groups().some(g => g.dirty())),
				valid: testData.mainConfig.groupValid
			}
		];
		fixture.detectChanges();
		modalServ.invalidVariables.and.rejectWith('closed');
	});

	afterEach(() => {
		modalServ.invalidVariables.calls.reset();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should open invalid variables modal', async () => {
		const invalidateFn = (v: ConfigVar) => {
			const val = v.configValue();

			if (IntegerValue.isIntegerValue(val) && val.definition.minValue !== undefined) {
				val.value.set(val.definition.minValue - 1);
			} else if (StringValue.isStringValue(val)) {
				val.value.set('');
			} else if (IntegerArrayValue.isIntegerArrayValue(val) && val.definition.minValueArr[0] !== undefined) {
				val.value.mutate(value => (value[0] = val.definition.minValueArr[0] - 1));
			}
		};
		testData.mainConfig.variables().forEach(invalidateFn);
		testData.mainConfig.groups().forEach(g => g.variables().forEach(invalidateFn));
		let variables: ConfigVar[] = [];

		const configBodies: ConfigBodyOpts[] = component.bodyOpts.filter(
			body => body.type === ConfigBodyType.VAR_LIST || body.type === ConfigBodyType.GROUP_LIST
		);

		configBodies.forEach(body => {
			if (body.type === ConfigBodyType.VAR_LIST) {
				variables = variables.concat(body.configVars().filter(confVar => !confVar.configValue().valid()));
			} else if (body.type === ConfigBodyType.GROUP_LIST) {
				variables = variables.concat(
					body
						.groups()
						.reduce(
							(vars: ConfigVar[], confGroup: ConfigGroup) => [
								...vars,
								...confGroup.variables().filter(confVar => !confVar.configValue().valid())
							],
							[]
						)
				);
			}
		});

		await component.openInvalidVariablesModal();
		expect(modalServ.invalidVariables).toHaveBeenCalledWith(variables);
	});
});
