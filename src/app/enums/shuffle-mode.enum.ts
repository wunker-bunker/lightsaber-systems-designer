/**
 * Enum for shuffle modes
 */
export enum ShuffleMode {
	SIMPLE = 0,
	LINEAR,
	RANDOMX
}
