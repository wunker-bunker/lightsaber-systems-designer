import { computed, signal, WritableSignal } from '@angular/core';
import { GroupType } from '../enums/group-types';
import { ProfileNames } from '../enums/profile-names';
import { ConfigGroupJSON } from '../models/light-saber-system.json';
import { VariableDictionary } from '../models/variable-dictionary.model';
import { StringValue } from './config-value/string-value';
import { ConfigVar } from './config-var';

/**
 * A group of variables that are related to each other.
 */
export class ConfigGroup {
	/**
	 * Flag indicating if the group is dirty.
	 */
	readonly dirty;

	/**
	 * The name of the group.
	 */
	readonly groupName;

	readonly id: WritableSignal<number>;

	readonly toString;

	/**
	 * Flag indicating if the group is valid.
	 * A group is valid if all of its variables are valid.
	 */
	readonly valid;

	readonly variables: WritableSignal<ConfigVar[]>;

	/**
	 * The id of the group before it was changed.
	 * Used to revert the id if revert is called.
	 */
	private readonly _previousId: WritableSignal<number>;

	/**
	 * Creates a new ConfigGroup.
	 * @param id The id of the group.
	 * @param name The name of the group.
	 * @param variables The variables in the group.
	 */
	constructor(id: number, public readonly name: string, variables: ConfigVar[] = []) {
		this.variables = signal(variables);
		this._previousId = signal(id);
		this.id = signal(id);
		this.dirty = computed(
			() => this.variables().some(confVar => confVar.configValue().dirty()) || this.id() !== this._previousId()
		);
		this.groupName = computed(() => {
			const nameValue = this.getGroupNameValue();

			if (!nameValue) {
				return '';
			}

			return nameValue.value();
		});
		this.toString = computed(() => {
			// Create the string with the group name and id in square brackets
			let str = `[${this.name}=${this.id()}]\n`;

			// Add all variables to the string
			this.variables().forEach((variable: ConfigVar) => (str += `${variable.toString()}\n`));
			return str;
		});
		this.valid = computed(() => this.variables().every(confVar => confVar.configValue().valid()));
	}

	/**
	 * Creates a new ConfigGroup from a JSON object.
	 * @param json The JSON object to create the ConfigGroup from.
	 * @param json.id
	 * @param json.name
	 * @param dict The dictionary of variables.
	 * @param json.variables
	 * @returns The ConfigGroup created from the JSON object.
	 */
	static fromJSON({ id, name, variables }: ConfigGroupJSON, dict: VariableDictionary): ConfigGroup {
		return new ConfigGroup(
			id,
			name,
			variables.map(v => ConfigVar.fromJSON(v, dict))
		);
	}

	/**
	 * Gets a variable by its name.
	 * @param name The name of the variable to get.
	 * @returns The variable with the given name.
	 */
	getVariableByName(name: string): ConfigVar | undefined {
		return this.variables().find((variable: ConfigVar) => variable.name === name);
	}

	/**
	 * Marks the group as saved.
	 * This means that the group is no longer dirty and that the previous id is set to the current id.
	 * This also marks all variables in the group as saved.
	 * The group is also validated.
	 */
	markAsSaved(): void {
		this.variables().forEach((confVar: ConfigVar) => confVar.configValue().markAsSaved());
		this._previousId.set(this.id());
	}

	/**
	 * Reverts the group and all of its variables to their previous state.
	 * This means that the group is no longer dirty and that the id is reverted to the previous id.
	 * The group is also validated.
	 */
	revert(): void {
		this.variables().forEach((confVar: ConfigVar) => confVar.configValue().revert());
		this.id.set(this._previousId());
	}

	/**
	 * Sets the name of the group.
	 * @param newName The new name of the group.
	 */
	setGroupName(newName: string): void {
		// Get the group name variable
		const nameValue = this.getGroupNameValue();

		if (!nameValue) {
			return;
		}

		// Set the value of the group name variable and validate the group
		nameValue.value.set(newName);
	}

	/**
	 * Converts the ConfigGroup to a JSON object.
	 * @returns The JSON object representing the ConfigGroup.
	 */
	toJSON(): ConfigGroupJSON {
		return {
			id: this.id(),
			name: this.name,
			variables: this.variables().map(v => v.toJSON()) // Convert all variables to JSON
		};
	}

	/**
	 * Gets the value of the group name variable.
	 * @returns The value of the group name variable.
	 */
	private getGroupNameValue(): StringValue | undefined {
		let configVar: ConfigVar | undefined;
		switch (this.name) {
			case GroupType.profile:
				configVar = this.getVariableByName(ProfileNames.pname);
				break;
			case GroupType.color:
				configVar = this.getVariableByName(ProfileNames.cname);
				break;
			default:
				return undefined;
		}
		const configValue = configVar?.configValue();
		if (!configValue || !StringValue.isStringValue(configValue)) {
			return undefined;
		}
		return configValue;
	}
}
