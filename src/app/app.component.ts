import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';
import { SettingsService } from './services/settings.service';

@Component({
	selector: 'lsd-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
	constructor(public settings: SettingsService, private _ngbTooltipConf: NgbTooltipConfig) {
		this._ngbTooltipConf.openDelay = 500;
		this._ngbTooltipConf.animation = true;
		this._ngbTooltipConf.container = 'body';
	}
}
