import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { ColorToWhiteStylePipe } from './color-to-white-style.pipe';

describe('ColorToWhiteStylePipe', () => {
	const pipe = new ColorToWhiteStylePipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('should transform color value', () => {
		expect(
			pipe.transform({
				red: 134,
				green: 255,
				blue: 0,
				white: COLOR_VAL_MAX
			})
		).toEqual(`rgba(255,255,255,${COLOR_VAL_MAX / COLOR_VAL_MAX})`);
	});
});
