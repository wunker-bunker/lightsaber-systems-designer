/**
 * Tells the logger where to log to.
 */
export enum Logport {
	USB = 0,
	TTL,
	BOTH
}
