/**
 * Switch type for the ignition switch
 */
export enum SwitchType {
	NORM_COLSED = 0,
	NORM_OPEN,
	MOMENTARY,
	SINGLE
}
