import { Pipe, PipeTransform } from '@angular/core';
import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { Color } from '../models/color.model';

/**
 * Pipe to convert a color to a rgb style string
 * Used in the color picker to display the current color
 * @example <div [style.background]="color | colorToRgbStyle"></div>
 */
@Pipe({
	name: 'colorToRgbStyle',
	pure: true
})
export class ColorToRgbStylePipe implements PipeTransform {
	/**
	 * Transform a color to a rgb style string
	 * @param value The color to transform
	 * @returns The rgb style string
	 */
	transform(value: Color): string {
		if (!value) {
			return '';
		}
		return `rgba(${this.colorValToRgb(value.red)},${this.colorValToRgb(value.green)},${this.colorValToRgb(
			value.blue
		)},1)`;
	}

	/**
	 * Convert a color value to a rgb value
	 * @param val The color value to convert
	 * @returns The rgb value
	 */
	private colorValToRgb(val: number): number {
		return Math.round((val * 255) / COLOR_VAL_MAX);
	}
}
