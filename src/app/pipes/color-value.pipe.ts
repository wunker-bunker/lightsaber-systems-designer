import { Pipe, PipeTransform } from '@angular/core';
import { ConfigGroup } from '../classes/config-group';
import { ColorValue } from '../classes/config-value/color-value';

/**
 * Pipe to get the color value of a config group.
 * @example <lsd-color-input [input]="group | colorValue" [readonly]="true"></lsd-color-input>
 */
@Pipe({
	name: 'colorValue',
	pure: true
})
export class ColorValuePipe implements PipeTransform {
	/**
	 * Get the color value of a config group.
	 * @param value The config group to get the color value from.
	 * @returns The color value.
	 */
	transform(value: ConfigGroup): ColorValue | null {
		const color = value.getVariableByName('color');
		const colorValue = color?.configValue();
		if (colorValue && ColorValue.isColorValue(colorValue)) {
			return colorValue;
		}
		return null;
	}
}
