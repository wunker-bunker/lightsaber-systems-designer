/**
 * Power on style
 */
export enum StylePowerOn {
	NORMAL = 0,
	LIGHTSTICK,
	SIMPLE_FLARE,
	BASE_FLARE,
	TIP_FLARE,
	FULL_FLARE,
	CRUMBLE,
	QUICKSILVER,
	MERCURY
}

/**
 * Power off style
 */
export enum StylePowerOff {
	NORMAL = 0,
	LIGHTSTICK,
	CRUMBLE,
	STAR_DUST,
	SUPER_NOVA,
	LEAK,
	DISSOLVE,
	STAR_FLAKES,
	QUICKSILVER,
	MERCURY
}

/**
 * Blaster fx style
 */
export enum StyleBlaster {
	DISABLED = 0,
	LEGACY,
	FADING,
	SPLIT,
	SPLIT_TAIL,
	SPLIT_FLARE,
	SPLIT_TAIL_FLARE,
	BOLT_GRADIENT,
	BOLT_GRADIENT_FLARE
}

/**
 * Lockup fx style
 */
export enum StyleLockup {
	DISABLED = 0,
	LEGACY,
	LOCALIZED,
	NOISE_LOCALIZED,
	NOISE
}

/**
 * Flicker fx style
 */
export enum StyleFlicker {
	DISABLED = 0
}

/**
 * Stab fx style
 */
export enum StyleStab {
	DISABLED = 0,
	LEGACY,
	FLASH,
	SCROLL,
	SCROLL_FLASH,
	BURST,
	BURST_FADE
}

/**
 * Clash fx style
 */
export enum StyleClash {
	DISABLED = 0,
	LEGACY,
	FADE_FOC,
	FADE_LOCAL,
	LOCALIZED,
	BASE_FLARE,
	UNICORN
}

/**
 * Drag fx style
 */
export enum StyleDrag {
	DISABLED = 0,
	SIMPLE,
	SPARKING,
	GRADIENT,
	GRADIENT_HEATMAP
}
