/* eslint-disable @typescript-eslint/naming-convention */
/**
 * Group types used in config files
 */
export enum GroupType {
	profile = 'profile',
	font = 'font',
	color = 'color'
}
