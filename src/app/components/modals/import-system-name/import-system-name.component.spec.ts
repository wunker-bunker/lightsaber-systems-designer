import { signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
	configureTestSuite,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { ImportSystemNameComponent } from './import-system-name.component';

describe('ImportSystemNameComponent', () => {
	let component: ImportSystemNameComponent;
	let fixture: ComponentFixture<ImportSystemNameComponent>;
	const modalMock: jasmine.SpyObj<NgbActiveModal> = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

	configureTestSuite({
		imports: [ReactiveFormsModule, ...unitTestImports],
		declarations: [ImportSystemNameComponent, ...unitTestDeclarations],
		providers: [{ provide: NgbActiveModal, useValue: modalMock }, ...unitTestProviders]
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ImportSystemNameComponent);
		component = fixture.componentInstance;
		component.systemList = signal(['test1', 'test2', 'system']);
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should get and set name', () => {
		component.setName('test system');
		expect(component.name?.value).toEqual('test system');
	});

	it('should check for existing system names', () => {
		const control = component.name;

		if (!control) {
			throw new Error('control is null');
		}

		control.setValue('test1');
		expect(component.existingSystemNameValidator()(control)?.systemExists).toBeTrue();

		control.setValue('test system');
		expect(component.existingSystemNameValidator()(control)).toBeNull();
	});

	it('should print errors to console', () => {
		const control = component.name;

		if (!control) {
			throw new Error('control is null');
		}

		const warnSpy = spyOn(console, 'warn').and.callThrough();
		component.setName('test1');
		component.importForm.updateValueAndValidity();
		component.printForm();
		expect(console.warn).toHaveBeenCalledWith(control.errors);

		warnSpy.calls.reset();
		component.setName('test;');
		component.importForm.updateValueAndValidity();
		component.printForm();
		expect(console.warn).toHaveBeenCalledWith(control.errors);

		warnSpy.calls.reset();
		component.setName('test system');
		component.importForm.updateValueAndValidity();
		component.printForm();
		expect(console.warn).not.toHaveBeenCalled();
	});
});
