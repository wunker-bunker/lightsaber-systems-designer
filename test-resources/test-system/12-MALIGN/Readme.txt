              _ _                                                   
  /\/\   __ _| (_) __ _ _ __                                        
 /    \ / _` | | |/ _` | '_ \                                       
/ /\/\ \ (_| | | | (_| | | | |                                      
\/    \/\__,_|_|_|\__, |_| |_|                                      
                  |___/                                             
 _                 __ _       _     _       _                       
| |__  _   _    /\ \ (_) __ _| |__ | |_ ___| |_ ___  _ __ _ __ ___  
| '_ \| | | |  /  \/ / |/ _` | '_ \| __/ __| __/ _ \| '__| '_ ` _ \ 
| |_) | |_| | / /\  /| | (_| | | | | |_\__ \ || (_) | |  | | | | | |
|_.__/ \__, | \_\ \/ |_|\__, |_| |_|\__|___/\__\___/|_|  |_| |_| |_|
       |___/            |___/                                       


The Old Republic Jedi and Sith Editions 
By David McMasters
Nightstorm aka Mace Windu on FX Forums
More of my fonts are available at www.saberfont.com

** UPGRADED to smoothswing by David Maltais - JediArmsDealer - 01/2021
** Restoring stock/former "non-smoothswing" behavior of the font is simply achieved by removing the lswing/hswing
   pairs in the font directory. Some other contents like (accent) swings and spins have been altered or removed from
   the font and stored in the /extra folder of the font. Updated fonts also have their combo files renamed as force
   sounds. Feel free to mix-match-remap or duplicate those to also have force-clash sounds.


Welcome to The Old Republic:
This is the largest font project I've taken on to date, and I hope the best I've done.
All sounds contained in these fonts originate from the upcoming MMO The Old Republic.  
The sound extraction process was complicated and I wont bore you with the details.  
Suffice it to say, here they are, ready for use in your Crystal Focus Saber Core v5 or Petite Crouton sound boards.  
These fonts are not tested on Crystal Focus v4 but may work fine.

This font is compatible with Crystal Focus v7 sound boards and below by Plecter Labs.
It consists of:

4 Boots
4 Blaster effects
4 Combo Effects
4 Spin effects
4 Stab effects
16 Clash effects
16 Swing effects
2 Force effects
2 Power off effects
5 Power on effects
1 lockup effect
1 hum loop



**Optional Download, not included**

The Old Republic Jedi and Sith Edition Extras Pack by Nightstorm:

Welcome to The Old Republic.

***BASE FONT REQUIRED***
This pack includes files used in the base font.

Each Extras pack contains the following:

10 of 30 Boots for either Jedi or Sith edition. (Font A 1-10, Font B 11-20, Font C 21-30)
2 Menu selection announcements, 1 mixed, 1 unmixed
4 of 12 Special iSaber tracks (Font A 1-4, Font B 5-8, Font C 9-12. See below)
32 Blaster effects in 4 sets
32 Swing effects in 4 sets
32 Clash effects in 4 sets
28 Force abilities
40 Activation effects in 20 matched sets (Poweron/Poweroff)


These are each mixed with the corresponding font hum file and matched so that the swing and blaster sets have the same swing effects contained in them.
What this means is, in effect you can have 16 different versions of each font by mixing up the sets, not counting the 28 different force effects and other combinations.

For example:

(You want to keep the swing and blaster sets together but each could be combined with a different clash set.)

01. 111
02. 222
03. 333
04. 444
05. 112
06. 113
07. 114
08. 221
09. 223
10. 224
11. 331
12. 332
13. 334
14. 441
15. 442
16. 443





********************************************************************************************************************************************************iSaber Readme:

Originally created for Swtor Holonet Timeline

http://www.swtor.com/info/holonet/timeline

Remastered for Crystal Focus (v5) iSaber by Dgdve

Timeline1 - The Treaty of Coruscant
Timeline2 - Mandalorian Blockade
Timeline3 - The Return of the Mandalorians
Timeline4 - The Empire Changes Strategy
Timeline5 - The Battle of Bothawui
Timeline6 - Onslaught of the Sith Empire
Timeline7 - Peace for the Republic
Timeline8 - The Jedi Civil War
Timeline9 - The Mandalorian Wars
Timeline10 - The Exar Kun War
Timeline11 - Rebirth of the Sith Empire
Timeline12 - The Great Hyperspace War


Adjusted by Nightstorm and used with permission.