   ___               _        _      ___                                                      
  / __\ __ _   _ ___| |_ __ _| |    / __\__   ___ _   _ ___                                   
 / / | '__| | | / __| __/ _` | |   / _\/ _ \ / __| | | / __|                                  
/ /__| |  | |_| \__ \ || (_| | |  / / | (_) | (__| |_| \__ \                                  
\____/_|   \__, |___/\__\__,_|_|  \/   \___/ \___|\__,_|___/                                  
           |___/                                                                              
                                    _  __             _      _                __ _       _    
  __ _    ___  ___  _   _ _ __   __| |/ _| ___  _ __ | |_   | |__  _   _     / /(_)_ __ | | __
 / _` |  / __|/ _ \| | | | '_ \ / _` | |_ / _ \| '_ \| __|  | '_ \| | | |   / / | | '_ \| |/ /
| (_| |  \__ \ (_) | |_| | | | | (_| |  _| (_) | | | | |_   | |_) | |_| |  / /__| | | | |   < 
 \__,_|  |___/\___/ \__,_|_| |_|\__,_|_|  \___/|_| |_|\__|  |_.__/ \__, |  \____/_|_| |_|_|\_\
                                                                   |___/                      

===============================================================================================

Soundfonts By Link - "Crystal Focus"
Created by Brandon aka Link of FX-Sabers Forums & IRA Forums

===============================================================================================
   
This font was created specifically for the Crystal Focus v7 Default Sound Package
and cannot be found anywhere else ! 
[ADDON] : the font got remastered as polyphonic + smoothswing font for CF-X

It has all new sounds such as Combos, SpinFX, StabFX & Pre Power on Sounds

An all new Hum sound, poweron's, poweroff's, boots, Force & Lockup! 

Also included are an EXTRAS folder that has menu and menu background sounds to further 
customize your saber experience.

I truely hope you enjoy this exclusive font! 

I am glad to be a part of the Saber community and glad to further the hobby ! 

MTFBWY !               Link

More of my fonts are available at www.saberfont.com