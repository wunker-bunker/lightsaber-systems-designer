import { TestBed } from '@angular/core/testing';
import { configureTestSuite, testData, unitTestImports, unitTestProviders } from 'src/environments/test-defaults.karma';
import { LightsaberSystemsService } from './lightsaber-systems.service';

describe('LightsaberSystemsService', () => {
	let service: LightsaberSystemsService;

	configureTestSuite({
		imports: unitTestImports,
		providers: [...unitTestProviders]
	});

	beforeEach(() => {
		testData.generateTestData();
		service = TestBed.inject(LightsaberSystemsService);
		service.clear();
		service.add(testData.system);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should get', () => {
		expect(service.get(testData.system.name)).toEqual(testData.system);
	});

	it('should add', () => {
		const ogName = testData.system.name;
		testData.generateTestData();
		const newSystem = testData.system;
		newSystem.name = 'New Test System';

		expect(service.has(newSystem.name)).toBeFalse();
		service.add(newSystem);
		expect(service.has(ogName)).toBeTrue();
		expect(service.has(newSystem.name)).toBeTrue();
	});

	it('should has', () => {
		expect(service.has(testData.system.name)).toBeTrue();
		expect(service.has('not here')).toBeFalse();
	});

	it('should delete', () => {
		expect(service.has(testData.system.name)).toBeTrue();
		service.delete(testData.system.name);
		expect(service.has(testData.system.name)).toBeFalse();
	});

	it('should clear', () => {
		const ogName = testData.system.name;
		testData.generateTestData();
		const newSystem = testData.system;
		newSystem.name = 'New Test System';
		service.add(newSystem);

		expect(service.has(ogName)).toBeTrue();
		expect(service.has(newSystem.name)).toBeTrue();
		service.clear();
		expect(service.has(ogName)).toBeFalse();
		expect(service.has(newSystem.name)).toBeFalse();
	});

	it('should size', () => {
		expect(service.size()).toEqual(1);
		service.add(testData.systems[1]);
		expect(service.size()).toEqual(2);
	});
});
