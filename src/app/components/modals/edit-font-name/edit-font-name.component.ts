import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FONT_NAME } from 'src/app/constants/regex';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
	selector: 'lsd-edit-font-name',
	templateUrl: './edit-font-name.component.html',
	styleUrls: ['./edit-font-name.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditFontNameComponent {
	importForm: UntypedFormGroup = this._fb.group({
		name: ['', [Validators.pattern(FONT_NAME)]]
	});
	constructor(
		public activeModal: NgbActiveModal,
		public settings: SettingsService,
		private _fb: UntypedFormBuilder
	) {}

	get name(): AbstractControl | null {
		return this.importForm.get('name');
	}

	printForm(): void {
		if (this.name?.errors) {
			console.warn(this.name.errors);
		}
	}

	setName(name: string): void {
		this.importForm.setValue({ name });
	}
}
