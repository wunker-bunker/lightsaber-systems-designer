import { NgZone } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateState } from 'src/app/services/update.service';
import {
	configureTestSuite,
	restartAndInstallSpy,
	startUpdateDownloadSpy,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { UpdateIndicatorComponent } from './update-indicator.component';

describe('UpdateIndicatorComponent', () => {
	let component: UpdateIndicatorComponent;
	let fixture: ComponentFixture<UpdateIndicatorComponent>;
	let ngZone: NgZone;

	configureTestSuite({
		imports: [NgbModule, ...unitTestImports],
		declarations: [UpdateIndicatorComponent, ...unitTestDeclarations],
		providers: unitTestProviders
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(UpdateIndicatorComponent);
		ngZone = TestBed.inject(NgZone);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
		ngZone.onStable.subscribe(() => {
			expect(component.updateServ.updateState()).toEqual({
				state: UpdateState.NONE
			});
		});
	});

	it('should start download', async () => {
		await component.startDownload();
		expect(startUpdateDownloadSpy).toHaveBeenCalled();
	});

	it('should restart and install', async () => {
		await component.restartInstall();
		expect(restartAndInstallSpy).toHaveBeenCalled();
	});
});
