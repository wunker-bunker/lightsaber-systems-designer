import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
	closeAppSpy,
	configureTestSuite,
	maximizeAppSpy,
	minimizeAppSpy,
	onAppMaximizedSpy,
	restoreAppSpy,
	unitTestDeclarations,
	unitTestImports,
	unitTestProviders
} from 'src/environments/test-defaults.karma';
import { TitleBarComponent } from './title-bar.component';

describe('TitleBarComponent', () => {
	let component: TitleBarComponent;
	let fixture: ComponentFixture<TitleBarComponent>;
	let onAppMaximizedCallback: (maximized: boolean) => void;

	configureTestSuite({
		imports: [...unitTestImports],
		declarations: [TitleBarComponent, ...unitTestDeclarations],
		providers: [...unitTestProviders]
	});

	beforeEach(() => {
		onAppMaximizedSpy.and.callFake((callback: typeof onAppMaximizedCallback) => {
			onAppMaximizedCallback = callback;
		});
		fixture = TestBed.createComponent(TitleBarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should close', async () => {
		await component.close();
		expect(closeAppSpy).toHaveBeenCalled();
	});

	it('should minimize', async () => {
		await component.minimize();
		expect(minimizeAppSpy).toHaveBeenCalled();
	});

	describe('should maximize or restore', () => {
		it('should maximize', async () => {
			component.maximized.set(false);
			await component.maxOrRestore();
			expect(maximizeAppSpy).toHaveBeenCalled();
		});

		it('should restore', async () => {
			component.maximized.set(true);
			await component.maxOrRestore();
			expect(restoreAppSpy).toHaveBeenCalled();
		});
	});

	it('should set maximized', () => {
		onAppMaximizedCallback(true);
		expect(component.maximized()).toBeTrue();
		onAppMaximizedCallback(false);
		expect(component.maximized()).toBeFalse();
	});
});
