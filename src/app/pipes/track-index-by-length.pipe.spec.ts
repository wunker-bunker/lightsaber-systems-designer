import { TrackIndexByLengthPipe } from './track-index-by-length.pipe';

describe('TrackIndexByLengthPipe', () => {
	const pipe = new TrackIndexByLengthPipe();

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('transform length into array to iterate over', () => {
		expect(pipe.transform(3)).toEqual([0, 1, 2]);
		expect(pipe.transform(6)).toEqual([0, 1, 2, 3, 4, 5]);
		expect(pipe.transform(1)).toEqual([0]);
		expect(pipe.transform(-1)).toEqual([]);
	});
});
