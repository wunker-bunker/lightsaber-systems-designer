import { computed } from '@angular/core';
import { UNKNOWN_DEFINITION } from 'src/app/dictionaries/variable.dict';
import { ValueType, ValueTypeUknown } from 'src/app/enums/value-type.enum';
import { BaseDefinition, UnknownDefinition } from 'src/app/models/variable-dictionary.model';
import { ConfigValue, ConfigValueTypes } from './config-value';

export class UnknownValue extends ConfigValue<string, ValueTypeUknown, UnknownDefinition> {
	readonly toString;

	readonly valid;

	constructor(value = '') {
		super(value, UNKNOWN_DEFINITION, UNKNOWN_DEFINITION.type);
		this.toString = computed(() => this.value());
		this.valid = computed(() => !!this.value());
	}

	static isUnknownValue(value: ConfigValue<ConfigValueTypes, ValueType, BaseDefinition>): value is UnknownValue {
		return value instanceof UnknownValue;
	}

	autoFixValue(): void {
		this.value.set('Unknown Value');
	}

	protected parseValue(strVal: string): string {
		return strVal;
	}
}
