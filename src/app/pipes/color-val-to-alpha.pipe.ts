import { Pipe, PipeTransform } from '@angular/core';
import { RGBA } from 'ngx-color';
import { COLOR_VAL_MAX } from '../classes/config-value/color-value';
import { Color } from '../models/color.model';

/**
 * Pipe to convert a color to a RGBA object.
 * Used in the color picker to contain the current color.
 * The alpha value is set to the white value of the color.
 * @example <color-alpha-picker [color]="input.value | colorValToAlpha"></color-alpha-picker>
 */
@Pipe({
	name: 'colorValToAlpha',
	pure: true
})
export class ColorValToAlphaPipe implements PipeTransform {
	transform(value: Color): RGBA {
		return {
			r: 255,
			g: 255,
			b: 255,
			a: value.white / COLOR_VAL_MAX
		};
	}
}
